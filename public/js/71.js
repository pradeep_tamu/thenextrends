(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[71],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/frontend/Filter.vue?vue&type=script&lang=js&":
/*!**************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/frontend/Filter.vue?vue&type=script&lang=js& ***!
  \**************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var lodash_debounce__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! lodash/debounce */ "./node_modules/lodash/debounce.js");
/* harmony import */ var lodash_debounce__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(lodash_debounce__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _helpers_AddToWishList__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./helpers/AddToWishList */ "./resources/js/components/frontend/helpers/AddToWishList.vue");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  name: "FilterSearch",
  components: {
    WishList: _helpers_AddToWishList__WEBPACK_IMPORTED_MODULE_1__["default"]
  },
  data: function data() {
    return {
      isOpen: false,
      allCategory: {},
      loader: false,
      isCategory: false,
      sort: 'nosort',
      brand: undefined,
      found: 0,
      products: {},
      category: '',
      subCategory: '',
      high: 'high',
      low: 'low',
      latest: 'latest',
      oldest: 'oldest',
      checked: {
        sizes: [],
        colors: [],
        fabrics: [],
        occasions: [],
        price: {
          min: 0,
          max: 0
        },
        brands: [],
        subcategory: []
      },
      filter: {
        size: [],
        colors: [],
        fabrics: [],
        occasions: [],
        brands: [],
        subcategory: []
      }
    };
  },
  beforeRouteEnter: function beforeRouteEnter(to, from, next) {
    if (to.params.gender == 'All' || to.params.gender == 'Women' || to.params.gender == 'Men') {
      next();
    } else {
      next('/' + to.params.gender + '!!');
    }
  },
  watch: {
    $route: function $route() {
      this.getRelatedCategory(this.$route.params.category);

      if (this.$route.query.brand !== undefined) {
        this.brand = this.$route.query.brand;
        this.subCategory = this.brand;
        this.category = this.$route.params.category;
        this.loadBrandFilter(this.$route.params.gender);
        this.submitBrandFilter();
      } else {
        this.subCategory = this.$route.params.subCategory;
        this.category = this.$route.params.category;
        this.submitFilter();
      }
    }
  },
  created: function created() {
    this.initialState();
    this.getRelatedCategory(this.$route.params.category);
  },
  methods: {
    viewSimilar: function viewSimilar(similar, index) {
      var _this = this;

      var subCategory;

      if (this.$route.params.subCategory) {
        subCategory = this.$route.params.subCategory;
      } else {
        subCategory = this.$route.params.category;
      }

      var brand = this.brand;
      var category = this.$route.params.category;
      var gender = this.$route.params.gender;
      var filter = JSON.stringify(this.checked);
      var sales = this.$route.query.sales;
      var search = this.$route.query.search;
      axios.get('/api/afterFilterProducts/' + gender + '/' + category + '/' + subCategory + '/' + filter + '/' + sales + '/' + search + '/' + brand + '/' + similar).then(function (data) {
        // slice items before similar is clicked
        _this.products.data = _this.products.data.slice(0, index);

        var _loop = function _loop(item) {
          // check if items already exists, if not then push
          if (!_this.products.data.some(function (e) {
            return e.id === data.data[item].id;
          })) // push items
            _this.products.data.push(data.data[item]);
        };

        for (var item in data.data) {
          _loop(item);
        }

        _this.found = _this.products.data.length;
      })["catch"](function (err) {});
      this.subCategory = subCategory;
      this.category = category;
    },
    //filter section
    getResults: function getResults() {
      var _this2 = this;

      var page = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 1;
      this.loader = true;
      var subCategory;

      if (this.$route.params.subCategory) {
        subCategory = this.$route.params.subCategory;
      } else {
        subCategory = this.$route.params.category;
      }

      var category = this.$route.params.category;
      var gender = this.$route.params.gender;
      var sales = this.$route.query.sales;
      var search = this.$route.query.search;

      if (this.checkBrand()) {
        var filter = JSON.stringify(this.checked);
        axios.get('/api/afterFilterProducts/' + gender + '/' + category + '/' + subCategory + '/' + filter + '/' + sales + '/' + search + '/' + this.brand + '?page=' + page).then(function (data) {
          _this2.products = data.data;
          _this2.found = _this2.products.data.length;
          _this2.subCategory = _this2.brand;
          _this2.category = category;
          _this2.loader = false;
        })["catch"](function (err) {});
      } else {
        var _filter = JSON.stringify(this.checked);

        axios.get('/api/afterFilterProducts/' + gender + '/' + category + '/' + subCategory + '/' + _filter + '/' + sales + '/' + search + '?page=' + page).then(function (data) {
          _this2.products = data.data;
          _this2.found = _this2.products.data.length;
          _this2.subCategory = subCategory;
          _this2.category = category;
          _this2.loader = false;
        })["catch"](function (err) {});
      }
    },
    checkBrand: function checkBrand() {
      if (this.$route.query.brand !== undefined) {
        return true;
      }

      return false;
    },
    changeValue: function changeValue(val) {
      this.checked.sort = this.sort;

      if (this.$route.query.brand !== undefined) {
        this.loader = true;
        this.submitBrandFilter(this.$route.query.brand);
      } else {
        this.submitFilter();
      }
    },
    initialState: function initialState() {
      var _this3 = this;

      this.loader = true;
      var subCategory;

      if (this.$route.params.subCategory) {
        subCategory = this.$route.params.subCategory;
      } else {
        subCategory = this.$route.params.category;
      }

      var category = this.$route.params.category;
      var gender = this.$route.params.gender;
      var sales = this.$route.query.sales;
      var search = this.$route.query.search;

      if (this.$route.query.brand !== undefined) {
        var brand = this.$route.query.brand;
        this.subCategory = brand;
        this.category = category;
        axios.get('/api/getFilterProduct/' + gender + '/' + category + '/' + subCategory + '/' + brand).then(function (data) {
          _this3.filter.size = data.data[0];
          _this3.filter.colors = data.data[1];
          _this3.filter.occasions = data.data[2];
          _this3.filter.fabrics = data.data[3];
          _this3.filter.brands = data.data[5];
          _this3.filter.subcategory = data.data[7];
        });
        axios.get('/api/getProductFront/' + gender + '/' + category + '/' + subCategory + '/' + sales + '/' + search + '/' + brand).then(function (data) {
          _this3.products = data.data;
          _this3.found = _this3.products.data.length;
          _this3.subCategory = brand;
          _this3.category = category;
          _this3.loader = false;
        });
      } else if (subCategory === undefined) {
        this.loadFilter();
        axios.get('/api/getProductFront/' + gender + '/' + category + '/' + subCategory + '/' + sales + '/' + search).then(function (data) {
          _this3.products = data.data;
          _this3.found = _this3.products.data.length;
          _this3.subCategory = subCategory;
          _this3.category = category;
          _this3.loader = false;
        });
      } else {
        this.subCategory = subCategory;
        this.category = category;
        this.loadFilter();
        axios.get('/api/getProductFront/' + gender + '/' + category + '/' + subCategory + '/' + sales + '/' + search).then(function (data) {
          _this3.products = data.data;
          _this3.found = _this3.products.data.length;
          _this3.subCategory = subCategory;
          _this3.category = category;
          _this3.loader = false;
        });
      }
    },
    clearAll: function clearAll() {
      Object.assign(this.$data, this.$options.data.call(this));
      this.initialState();
    },
    isEmpty: function isEmpty(obj) {
      for (var prop in obj) {
        if (obj.hasOwnProperty(prop)) return false;
      }

      return true;
    },
    submitPrice: function submitPrice() {
      this.loader = true;
      this.submitFilter();
    },
    arraySearch: function arraySearch(arr, val) {
      for (var i = 0; i < arr.length; i++) {
        if (arr[i] === val) return i;
      }

      return false;
    },
    getFilterProducts: function getFilterProducts($key) {
      var index = this.arraySearch(this.checked[$key], 'all');

      if (index === 0) {
        this.checked[$key].splice(0, 1);
      }

      this.loader = true;

      if (this.$route.query.brand !== undefined) {
        this.submitBrandFilter(this.$route.query.brand);
      } else {
        this.submitFilter();
      }
    },
    //this function is for removing all filter tags if clicked all
    getFilterProductsPoly: function getFilterProductsPoly($key) {
      this.checked[$key].splice(0, 3);
      this.checked[$key].push('all');
      this.loader = true;

      if (this.$route.query.brand !== undefined) {
        this.submitBrandFilter(this.$route.query.brand);
      } else {
        this.submitFilter();
      }
    },
    submitFilter: lodash_debounce__WEBPACK_IMPORTED_MODULE_0___default()(function () {
      var _this4 = this;

      var subCategory;

      if (this.$route.params.subCategory) {
        subCategory = this.$route.params.subCategory;
      } else {
        subCategory = this.$route.params.category;
      }

      var category = this.$route.params.category;
      var gender = this.$route.params.gender;
      var sales = this.$route.query.sales;
      var search = this.$route.query.search;
      var filter = JSON.stringify(this.checked);

      if (subCategory === undefined) {
        this.loadFilter();
        axios.get('/api/afterFilterProducts/' + gender + '/' + category + '/' + subCategory + '/' + filter + '/' + sales + '/' + search).then(function (data) {
          _this4.loader = false;
          _this4.products = data.data;
          _this4.found = _this4.products.data.length;
        })["catch"](function (err) {});
      } else {
        this.loadFilter();
        axios.get('/api/afterFilterProducts/' + gender + '/' + category + '/' + subCategory + '/' + filter + '/' + sales + '/' + search).then(function (data) {
          _this4.loader = false;
          _this4.products = data.data;
          _this4.found = _this4.products.data.length;
        })["catch"](function (err) {});
      }

      this.subCategory = subCategory;
      this.category = category;
    }, 1000),
    submitBrandFilter: lodash_debounce__WEBPACK_IMPORTED_MODULE_0___default()(function () {
      var _this5 = this;

      var subCategory;

      if (this.$route.params.subCategory) {
        subCategory = this.$route.params.subCategory;
      } else {
        subCategory = this.$route.params.category;
      }

      var brand = this.brand;
      var category = this.$route.params.category;
      var gender = this.$route.params.gender;
      var filter = JSON.stringify(this.checked);
      var sales = this.$route.query.sales;
      var search = this.$route.query.search;
      axios.get('/api/afterFilterProducts/' + gender + '/' + category + '/' + subCategory + '/' + filter + '/' + sales + '/' + search + '/' + brand).then(function (data) {
        _this5.loader = false;
        _this5.products = data.data;
        _this5.found = _this5.products.data.length;
      })["catch"](function (err) {});
      this.subCategory = brand;
      this.category = category;
    }, 1000),
    loadFilter: function loadFilter() {
      var _this6 = this;

      this.loader = true;
      var subCategory;

      if (this.$route.params.subCategory) {
        subCategory = this.$route.params.subCategory;
      } else {
        subCategory = this.$route.params.category;
      }

      var gender = this.$route.params.gender;
      var category = this.$route.params.category;
      axios.get('/api/getFilterProduct/' + gender + '/' + category + '/' + subCategory).then(function (data) {
        _this6.filter.size = data.data[0];
        _this6.filter.colors = data.data[1];
        _this6.filter.occasions = data.data[2];
        _this6.filter.fabrics = data.data[3];
        _this6.filter.brands = data.data[5];
      });
    },
    loadBrandFilter: function loadBrandFilter(gender) {
      var _this7 = this;

      this.loader = true;
      var brand = this.brand;
      var subCategory;

      if (this.$route.params.subCategory) {
        subCategory = this.$route.params.subCategory;
      } else {
        subCategory = this.$route.params.category;
      }

      var category = this.$route.params.category;
      this.category = category;
      axios.get('/api/getFilterProduct/' + gender + '/' + category + '/' + subCategory + '/' + brand).then(function (data) {
        _this7.filter.size = data.data[0];
        _this7.filter.colors = data.data[1];
        _this7.filter.occasions = data.data[2];
        _this7.filter.fabrics = data.data[3];
        _this7.filter.brands = data.data[5];
        _this7.filter.subcategory = data.data[7];
      });
    },
    //end of filterSection
    toggle: function toggle(event) {
      var parent = event.target.parentElement;

      if (parent.tagName === 'SPAN') {
        parent = parent.parentElement;
      }

      var val = parent.childNodes[4].style.display;

      if (val === "block") {
        parent.childNodes[4].style.display = "none";
      } else {
        parent.childNodes[4].style.display = "block";
      }
    },
    openFilter: function openFilter() {
      this.isOpen = !this.isOpen;
    },
    getImage: function getImage(src) {
      return "/image/thumbnail/" + src;
    },
    getRelatedCategory: function getRelatedCategory(category) {
      var _this8 = this;

      if (category) {
        axios.get('/api/getCategoryData').then(function (data) {
          _this8.allCategory = data.data[category];
        });
        this.isCategory = true;
      }
    },
    goFilter: function goFilter(gender, subCategory, category) {
      this.$router.push({
        name: 'filter',
        params: {
          gender: gender,
          subCategory: subCategory,
          category: category
        }
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/frontend/helpers/AddToWishList.vue?vue&type=script&lang=js&":
/*!*****************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/frontend/helpers/AddToWishList.vue?vue&type=script&lang=js& ***!
  \*****************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'add-to-wishList',
  props: {
    product: Object
  },
  data: function data() {
    return {
      item: {}
    };
  },
  methods: {
    addToWish: function addToWish(e) {
      var _this = this;

      this.item['productId'] = this.product.id;
      this.item['slug'] = this.product.slug;
      this.item['icon'] = 'fas';
      var found = this.$store.getters.getWish.find(function (product) {
        return product.productId == _this.product.id;
      });

      if (found) {
        this.$store.commit("removeWish", this.item);
        e.target.classList.remove('fas');
        e.target.classList.add('far');
        Toast.fire({
          type: 'error',
          title: 'You have removed the product from your wish list !!!'
        });
      } else {
        this.$store.commit("addToWish", this.item);
        e.target.classList.remove('far');
        e.target.classList.add('fas');
        Toast.fire({
          type: 'success',
          title: 'You have successfully added your product to your wish list !!!'
        });
      }
    },
    getHeart: function getHeart() {
      var _this2 = this;

      var found = this.$store.getters.getWish.find(function (product) {
        return product.productId === _this2.product.id;
      });

      if (found) {
        return 'fas';
      } else {
        return 'far';
      }
    }
  }
});

/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/frontend/Filter.vue?vue&type=style&index=0&id=f06cd78c&lang=scss&scoped=true&":
/*!*************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/sass-loader/dist/cjs.js??ref--7-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/frontend/Filter.vue?vue&type=style&index=0&id=f06cd78c&lang=scss&scoped=true& ***!
  \*************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "@media only screen and (max-width: 768px) {\n.filter-product-wrapper[data-v-f06cd78c] {\n    padding: 0;\n}\n}\n.breadcrumb-item a[data-v-f06cd78c] {\n  color: black;\n  text-decoration: none;\n}\n.cat-list[data-v-f06cd78c] {\n  cursor: pointer;\n}\ninput[type=number][data-v-f06cd78c] {\n  width: 80px;\n}\n.row[data-v-f06cd78c] {\n  margin: 0;\n}\n.container-fluid[data-v-f06cd78c] {\n  background: white;\n  color: #707070;\n}\n.title-2[data-v-f06cd78c] {\n  display: flex;\n  align-items: center;\n  flex-direction: column;\n  color: black;\n  padding: 0.5rem;\n}\n.title-2 span[data-v-f06cd78c] {\n  display: flex;\n  align-items: center;\n  background: #e5aeae;\n  padding: 12px;\n  color: white;\n  justify-content: center;\n  cursor: pointer;\n}\n.title-2 span p[data-v-f06cd78c] {\n  margin: 0 8px 0 0;\n}\n.title-2 span i[data-v-f06cd78c] {\n  color: white;\n}\n.title-2 b[data-v-f06cd78c] {\n  font-size: 17px;\n}\n@media only screen and (min-width: 770px) {\n.title-2[data-v-f06cd78c] {\n    display: none;\n}\n}\n.title-2-desktop b[data-v-f06cd78c] {\n  font-size: 17px;\n}\n.title-2-desktop span[data-v-f06cd78c] {\n  float: right;\n}\n@media only screen and (max-width: 768px) {\n.title-2-desktop[data-v-f06cd78c] {\n    display: none;\n}\n}\n.fa-chevron-down[data-v-f06cd78c] {\n  float: right;\n  color: black;\n}\n.item-found[data-v-f06cd78c] {\n  color: black;\n  margin-right: 5em;\n}\n@media only screen and (max-width: 768px) {\n.item-found[data-v-f06cd78c] {\n    display: none;\n}\n}\n.title-filter[data-v-f06cd78c] {\n  color: black;\n  font-weight: bolder;\n  -webkit-font-kerning: auto;\n          font-kerning: auto;\n  font-size: 21px;\n  padding-left: 12px;\n}\n.filterMain[data-v-f06cd78c] {\n  display: flex;\n  align-items: center;\n  justify-content: space-between;\n  flex-direction: row;\n  background: white;\n  padding-top: 2em;\n  color: #707070;\n  padding-right: 20px;\n  padding-left: 20px;\n}\n.filterMain select[data-v-f06cd78c] {\n  width: 158px;\n}\n.bread[data-v-f06cd78c] {\n  font-size: 18px;\n}\n.bread span[data-v-f06cd78c] {\n  font-weight: bolder;\n}\nhr[data-v-f06cd78c] {\n  color: #A8CFB4;\n  background-color: #A8CFB4;\n}\n.ruler[data-v-f06cd78c] {\n  margin-top: 2px;\n  border: none;\n  height: 1px;\n  width: 98%;\n}\n.divider[data-v-f06cd78c] {\n  margin-top: 1px;\n  margin-left: 12px;\n  margin-bottom: 6px;\n}\n.divider1[data-v-f06cd78c] {\n  margin-top: 1px;\n  margin-bottom: 6px;\n}\n@media only screen and (max-width: 768px) {\n.filterMain[data-v-f06cd78c] {\n    display: flex;\n    align-items: center;\n    justify-content: center;\n    flex-direction: column;\n}\n.filterMain select[data-v-f06cd78c] {\n    margin-top: 5px;\n}\n.item-found[data-v-f06cd78c] {\n    margin-right: 0;\n}\n.ruler[data-v-f06cd78c] {\n    margin-top: 10px;\n}\n}\n/*for filter assets*/\n/*this section is for filter */\nli > p[data-v-f06cd78c] {\n  display: none;\n}\nli[data-v-f06cd78c] {\n  font-size: 17px;\n}\n.slide-down-hover[data-v-f06cd78c] {\n  color: black;\n  font-size: 18px;\n  cursor: pointer;\n}\n.list-filter[data-v-f06cd78c] {\n  list-style-type: none;\n  padding: 17px;\n}\n\n/*---------------this section is for card display in right side*/\n.search-title[data-v-f06cd78c], .dropdown-sort[data-v-f06cd78c] {\n  display: inline;\n}\n\n/*media query for right side...*/\n@media only screen and (max-width: 525px) {\n.dropdown-sort[data-v-f06cd78c] {\n    position: static;\n    display: block;\n    margin: auto;\n    margin-top: 7px;\n}\n.search-title[data-v-f06cd78c] {\n    display: block;\n    width: 288px;\n    margin: auto;\n}\n.title-filter[data-v-f06cd78c] {\n    display: block;\n}\n}\n/* The container */\n.container[data-v-f06cd78c] {\n  display: block;\n  position: relative;\n  padding-left: 20px;\n  cursor: pointer;\n  -webkit-user-select: none;\n  -moz-user-select: none;\n  -ms-user-select: none;\n  user-select: none;\n}\n\n/* Hide the browser's default checkbox */\n.container input[data-v-f06cd78c] {\n  position: absolute;\n  opacity: 0;\n  cursor: pointer;\n  height: 0;\n  width: 0;\n}\n\n/* Create a custom checkbox */\n.checkmark[data-v-f06cd78c] {\n  position: absolute;\n  top: 4px;\n  left: 0;\n  height: 15px;\n  width: 15px;\n  background-color: #fff;\n  border: solid 1px black;\n}\n\n/* On mouse-over, add a grey background color */\n.container:hover input ~ .checkmark[data-v-f06cd78c] {\n  background-color: #ccc;\n}\n\n/* When the checkbox is checked, add a blue background */\n.container input:checked ~ .checkmark[data-v-f06cd78c] {\n  background-color: #2196F3;\n}\n\n/* Create the checkmark/indicator (hidden when not checked) */\n.checkmark[data-v-f06cd78c]:after {\n  content: \"\";\n  position: absolute;\n  display: none;\n}\n\n/* Show the checkmark when checked */\n.container input:checked ~ .checkmark[data-v-f06cd78c]:after {\n  display: block;\n}\n\n/* Style the checkmark/indicator */\n.container .checkmark[data-v-f06cd78c]:after {\n  left: 5px;\n  top: 1px;\n  width: 5px;\n  height: 10px;\n  border: solid white;\n  border-width: 0 3px 3px 0;\n  transform: rotate(45deg);\n}\n.card[data-v-f06cd78c] {\n  border: none;\n  box-shadow: none;\n}\n.card a[data-v-f06cd78c] {\n  text-decoration: none;\n}\n.text-box[data-v-f06cd78c] {\n  position: absolute;\n  visibility: hidden;\n  width: 0;\n  font-family: Titillium Web;\n}\n.similar[data-v-f06cd78c] {\n  position: absolute;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  font-size: 1rem;\n  background: #435061;\n  right: 1rem;\n  top: 1rem;\n  height: 2rem;\n  width: 2rem;\n  border-radius: 50%;\n  cursor: pointer;\n}\n.similar i[data-v-f06cd78c] {\n  color: whitesmoke;\n}\n.similar:hover span.text-box[data-v-f06cd78c] {\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  right: 2rem;\n  visibility: visible;\n  transition: all 0.2s linear;\n  width: 6rem;\n  height: 2rem;\n  background-color: #435061;\n  color: whitesmoke;\n  border-radius: 25px;\n  border: none;\n  font-size: 0.7rem;\n  top: 0;\n  cursor: pointer;\n}\n.container[data-v-f06cd78c] {\n  max-width: 80%;\n}\n.brand[data-v-f06cd78c], .name[data-v-f06cd78c] {\n  font-style: normal;\n  font-weight: 400;\n  color: #4d4d4d;\n  padding: 0;\n  font-family: Titillium Web;\n}\n.brand[data-v-f06cd78c] {\n  font-weight: bold;\n}\n.card-body[data-v-f06cd78c] {\n  padding: 0;\n  padding-top: 0.5rem;\n}\n.heart[data-v-f06cd78c] {\n  display: flex;\n  justify-content: flex-end;\n}\n.desc-padd[data-v-f06cd78c] {\n  padding: 0;\n}\n.dropdown-sort[data-v-f06cd78c] {\n  width: 14em;\n  position: absolute;\n  right: 40px;\n  top: 10px;\n  background: #ebecf0;\n  border: none;\n}\n.items-box[data-v-f06cd78c] {\n  text-decoration: none;\n  padding: 0.5rem !important;\n}\n.price-cut[data-v-f06cd78c] {\n  background-color: transparent;\n  background-image: gradient(linear, 19.1% -7.9%, 81% 107.9%, color-stop(0, transparent), color-stop(0.48, transparent), color-stop(0.5, #000), color-stop(0.52, transparent), color-stop(1, transparent));\n  background-image: repeating-linear-gradient(172deg, transparent 0%, transparent 46%, black 50%, transparent 54%, transparent 100%);\n}\n.price-off[data-v-f06cd78c] {\n  color: orangered;\n}\n@media only screen and (max-width: 768px) {\n.desktop-sort[data-v-f06cd78c] {\n    display: none;\n}\n}\n@media only screen and (min-width: 770px) {\n.mobile-sort[data-v-f06cd78c] {\n    display: none;\n    cursor: pointer;\n}\n}\n.list-filter i[data-v-f06cd78c] {\n  cursor: pointer;\n}\n@media only screen and (min-width: 770px) {\n.ul-mobile[data-v-f06cd78c] {\n    display: none;\n}\n}\n@media only screen and (max-width: 768px) {\n.ul-desktop[data-v-f06cd78c] {\n    display: none;\n}\n}", ""]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/frontend/helpers/AddToWishList.vue?vue&type=style&index=0&id=045a22a5&scoped=true&lang=css&":
/*!************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/frontend/helpers/AddToWishList.vue?vue&type=style&index=0&id=045a22a5&scoped=true&lang=css& ***!
  \************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\ni[data-v-045a22a5]:hover{\n    cursor: pointer;\n}\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/frontend/Filter.vue?vue&type=style&index=0&id=f06cd78c&lang=scss&scoped=true&":
/*!*****************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/sass-loader/dist/cjs.js??ref--7-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/frontend/Filter.vue?vue&type=style&index=0&id=f06cd78c&lang=scss&scoped=true& ***!
  \*****************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../node_modules/css-loader!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--7-2!../../../../node_modules/sass-loader/dist/cjs.js??ref--7-3!../../../../node_modules/vue-loader/lib??vue-loader-options!./Filter.vue?vue&type=style&index=0&id=f06cd78c&lang=scss&scoped=true& */ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/frontend/Filter.vue?vue&type=style&index=0&id=f06cd78c&lang=scss&scoped=true&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/frontend/helpers/AddToWishList.vue?vue&type=style&index=0&id=045a22a5&scoped=true&lang=css&":
/*!****************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/frontend/helpers/AddToWishList.vue?vue&type=style&index=0&id=045a22a5&scoped=true&lang=css& ***!
  \****************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../../node_modules/css-loader??ref--6-1!../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../node_modules/postcss-loader/src??ref--6-2!../../../../../node_modules/vue-loader/lib??vue-loader-options!./AddToWishList.vue?vue&type=style&index=0&id=045a22a5&scoped=true&lang=css& */ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/frontend/helpers/AddToWishList.vue?vue&type=style&index=0&id=045a22a5&scoped=true&lang=css&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/frontend/Filter.vue?vue&type=template&id=f06cd78c&scoped=true&":
/*!******************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/frontend/Filter.vue?vue&type=template&id=f06cd78c&scoped=true& ***!
  \******************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "container-fluid" }, [
    _c("div", { staticClass: "filterMain" }, [
      _c("nav", { staticClass: "p-0", attrs: { "aria-label": "breadcrumb" } }, [
        _c(
          "ol",
          {
            staticClass: "breadcrumb p-0",
            staticStyle: { background: "none" }
          },
          [
            _c(
              "li",
              { staticClass: "breadcrumb-item" },
              [
                _c(
                  "router-link",
                  {
                    attrs: {
                      to: {
                        name: "filter",
                        params: { gender: _vm.$route.params.gender }
                      }
                    }
                  },
                  [
                    _vm._v(
                      _vm._s(_vm._f("upText")(_vm.$route.params.gender)) +
                        "\n                    "
                    )
                  ]
                )
              ],
              1
            ),
            _vm._v(" "),
            _vm.category
              ? _c(
                  "li",
                  { staticClass: "breadcrumb-item" },
                  [
                    _c(
                      "router-link",
                      {
                        attrs: {
                          to: {
                            name: "filter",
                            params: {
                              gender: _vm.$route.params.gender,
                              category: _vm.category
                            }
                          }
                        }
                      },
                      [
                        _vm._v(
                          _vm._s(_vm._f("upText")(_vm.category)) +
                            "\n                    "
                        )
                      ]
                    )
                  ],
                  1
                )
              : _vm._e(),
            _vm._v(" "),
            _vm.category !== _vm.subCategory && _vm.subCategory !== undefined
              ? _c(
                  "li",
                  {
                    staticClass: "breadcrumb-item active",
                    attrs: { "aria-current": "page" }
                  },
                  [
                    _c("span", [
                      _vm._v(_vm._s(_vm._f("upText")(_vm.subCategory)))
                    ])
                  ]
                )
              : _vm._e()
          ]
        )
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "item-found" }, [
        _vm._v(_vm._s(_vm.found) + " items found")
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "desktop-sort" }, [
        _c(
          "select",
          {
            directives: [
              {
                name: "model",
                rawName: "v-model",
                value: _vm.sort,
                expression: "sort"
              }
            ],
            staticClass: "form-control",
            staticStyle: { "font-family": "'FontAwesome', 'Arial'" },
            attrs: { name: "sortBy" },
            on: {
              change: [
                function($event) {
                  var $$selectedVal = Array.prototype.filter
                    .call($event.target.options, function(o) {
                      return o.selected
                    })
                    .map(function(o) {
                      var val = "_value" in o ? o._value : o.value
                      return val
                    })
                  _vm.sort = $event.target.multiple
                    ? $$selectedVal
                    : $$selectedVal[0]
                },
                function($event) {
                  return _vm.changeValue("sort")
                }
              ]
            }
          },
          [
            _c(
              "option",
              { attrs: { value: "nosort", disabled: "", selected: "" } },
              [_vm._v("SORT BY")]
            ),
            _vm._v(" "),
            _c("option", { attrs: { value: "high" } }, [
              _vm._v("HIGHEST PRICE")
            ]),
            _vm._v(" "),
            _c("option", { attrs: { value: "low" } }, [_vm._v("LOWEST PRICE")]),
            _vm._v(" "),
            _c("option", { attrs: { value: "latest" } }, [_vm._v("LATEST")]),
            _vm._v(" "),
            _c("option", { attrs: { value: "oldest" } }, [_vm._v("OLDEST")])
          ]
        )
      ])
    ]),
    _vm._v(" "),
    _c("hr", { staticClass: "ruler", attrs: { size: "20" } }),
    _vm._v(" "),
    _c("div", { staticClass: "row justify-content-center" }, [
      _c(
        "div",
        {
          staticClass: "col-xs-12 col-lg-3  p-0",
          staticStyle: { "padding-right": "31px" }
        },
        [
          _c("span", { staticClass: "title-filter" }, [
            _vm._v(_vm._s(_vm.subCategory))
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "title-2" }, [
            _c(
              "span",
              { staticClass: "col-12", on: { click: _vm.openFilter } },
              [
                _c("p", [_vm._v("REFINE (" + _vm._s(_vm.found) + " ITEMS)")]),
                _vm._v(" "),
                _c("i", { staticClass: "fas fa-chevron-down slide-down-hover" })
              ]
            )
          ]),
          _vm._v(" "),
          _c(
            "ul",
            {
              directives: [
                {
                  name: "show",
                  rawName: "v-show",
                  value: _vm.isOpen,
                  expression: "isOpen"
                }
              ],
              staticClass: "list-filter ul-mobile"
            },
            [
              _c("div", { staticClass: "title-2-desktop" }, [
                _c("b", [_vm._v("Refine")]),
                _vm._v(" "),
                _c(
                  "span",
                  {
                    staticStyle: { cursor: "grab" },
                    on: {
                      click: function($event) {
                        return _vm.clearAll()
                      }
                    }
                  },
                  [_c("u", [_vm._v("Clear All")])]
                )
              ]),
              _vm._v(" "),
              _c("li", { staticClass: "mobile-sort" }, [
                _c("span", { on: { click: _vm.toggle } }, [
                  _vm._v("Sort\n                    "),
                  _c("i", {
                    staticClass: "fas fa-chevron-down slide-down-hover"
                  })
                ]),
                _vm._v(" "),
                _c("hr", { staticClass: "divider1" }),
                _vm._v(" "),
                _c("p", { staticClass: "mt-3" }, [
                  _c("label", { staticClass: "container" }, [
                    _vm._v(
                      "\n                            Highest Price\n                            "
                    ),
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.sort,
                          expression: "sort"
                        }
                      ],
                      attrs: { type: "radio", id: "highest" },
                      domProps: {
                        value: _vm.high,
                        checked: _vm._q(_vm.sort, _vm.high)
                      },
                      on: {
                        change: [
                          function($event) {
                            _vm.sort = _vm.high
                          },
                          function($event) {
                            return _vm.changeValue("sort")
                          }
                        ]
                      }
                    })
                  ]),
                  _vm._v(" "),
                  _c("label", { staticClass: "container" }, [
                    _vm._v(
                      "\n                            Lowest Price\n                            "
                    ),
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.sort,
                          expression: "sort"
                        }
                      ],
                      attrs: { type: "radio", id: "lowest" },
                      domProps: {
                        value: _vm.low,
                        checked: _vm._q(_vm.sort, _vm.low)
                      },
                      on: {
                        change: [
                          function($event) {
                            _vm.sort = _vm.low
                          },
                          function($event) {
                            return _vm.changeValue("sort")
                          }
                        ]
                      }
                    })
                  ]),
                  _vm._v(" "),
                  _c("label", { staticClass: "container" }, [
                    _vm._v(
                      "\n                            Latest\n                            "
                    ),
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.sort,
                          expression: "sort"
                        }
                      ],
                      attrs: { type: "radio", id: "latest" },
                      domProps: {
                        value: _vm.latest,
                        checked: _vm._q(_vm.sort, _vm.latest)
                      },
                      on: {
                        change: [
                          function($event) {
                            _vm.sort = _vm.latest
                          },
                          function($event) {
                            return _vm.changeValue("sort")
                          }
                        ]
                      }
                    })
                  ]),
                  _vm._v(" "),
                  _c("label", { staticClass: "container" }, [
                    _vm._v(
                      "\n                            Oldest\n                            "
                    ),
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.sort,
                          expression: "sort"
                        }
                      ],
                      attrs: { type: "radio", id: "oldest" },
                      domProps: {
                        value: _vm.oldest,
                        checked: _vm._q(_vm.sort, _vm.oldest)
                      },
                      on: {
                        change: [
                          function($event) {
                            _vm.sort = _vm.oldest
                          },
                          function($event) {
                            return _vm.changeValue("sort")
                          }
                        ]
                      }
                    })
                  ])
                ])
              ]),
              _vm._v(" "),
              _c(
                "li",
                {
                  directives: [
                    {
                      name: "show",
                      rawName: "v-show",
                      value:
                        _vm.subCategory == "All" ||
                        (_vm.category != "" &&
                          _vm.category !== undefined &&
                          _vm.category == _vm.subCategory),
                      expression:
                        "(subCategory == 'All' || (category != '' && category !== undefined && category == subCategory ))"
                    }
                  ]
                },
                [
                  _c("span", { on: { click: _vm.toggle } }, [
                    _vm._v("Sub-Category\n                    "),
                    _c("i", {
                      staticClass: "fas fa-chevron-down slide-down-hover"
                    })
                  ]),
                  _vm._v(" "),
                  _c("hr", { staticClass: "divider1" }),
                  _vm._v(" "),
                  _c(
                    "p",
                    { staticClass: "mt-3" },
                    [
                      _vm.$route.params.gender == "Women" ||
                      _vm.$route.params.gender == "women"
                        ? [
                            _vm._l(_vm.allCategory.women, function(cat) {
                              return [
                                _c(
                                  "span",
                                  {
                                    staticClass: "cat-list ml-2 container",
                                    on: {
                                      click: function($event) {
                                        return _vm.goFilter(
                                          "Women",
                                          cat.label,
                                          _vm.category
                                        )
                                      }
                                    }
                                  },
                                  [_vm._v(_vm._s(cat.label))]
                                )
                              ]
                            })
                          ]
                        : [
                            _vm._l(_vm.allCategory.men, function(cat) {
                              return [
                                _c(
                                  "span",
                                  {
                                    staticClass: "cat-list ml-2 container",
                                    on: {
                                      click: function($event) {
                                        return _vm.goFilter(
                                          "Men",
                                          cat.label,
                                          _vm.category
                                        )
                                      }
                                    }
                                  },
                                  [_vm._v(_vm._s(cat.label))]
                                )
                              ]
                            })
                          ]
                    ],
                    2
                  )
                ]
              ),
              _vm._v(" "),
              _c("li", [
                _c("span", { on: { click: _vm.toggle } }, [
                  _vm._v("Price\n                    "),
                  _c("i", {
                    staticClass: "fas fa-chevron-down slide-down-hover"
                  })
                ]),
                _vm._v(" "),
                _c("hr", { staticClass: "divider1" }),
                _vm._v(" "),
                _c("p", { staticClass: "mt-3" }, [
                  _vm._v(
                    "\n                        Min\n                        "
                  ),
                  _c("input", {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.checked.price.min,
                        expression: "checked.price.min"
                      }
                    ],
                    attrs: { type: "number", min: "0" },
                    domProps: { value: _vm.checked.price.min },
                    on: {
                      change: function($event) {
                        return _vm.submitPrice()
                      },
                      input: function($event) {
                        if ($event.target.composing) {
                          return
                        }
                        _vm.$set(_vm.checked.price, "min", $event.target.value)
                      }
                    }
                  }),
                  _vm._v(
                    "\n                        Max\n                        "
                  ),
                  _c("input", {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.checked.price.max,
                        expression: "checked.price.max"
                      }
                    ],
                    attrs: { type: "number", min: "0" },
                    domProps: { value: _vm.checked.price.max },
                    on: {
                      change: function($event) {
                        return _vm.submitPrice()
                      },
                      input: function($event) {
                        if ($event.target.composing) {
                          return
                        }
                        _vm.$set(_vm.checked.price, "max", $event.target.value)
                      }
                    }
                  })
                ])
              ]),
              _vm._v(" "),
              _c("li", [
                _c("span", { on: { click: _vm.toggle } }, [
                  _vm._v("Sizes\n                    "),
                  _c("i", {
                    staticClass: "fas fa-chevron-down slide-down-hover"
                  })
                ]),
                _vm._v(" "),
                _c("hr", { staticClass: "divider1" }),
                _vm._v(" "),
                _c(
                  "p",
                  { staticClass: "mt-3" },
                  [
                    _c("label", { staticClass: "container" }, [
                      _c("span", [_vm._v("All Sizes")]),
                      _vm._v(" "),
                      _c("input", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model",
                            value: _vm.checked.sizes,
                            expression: "checked.sizes"
                          }
                        ],
                        attrs: {
                          type: "checkbox",
                          value: "all",
                          checked: "checked"
                        },
                        domProps: {
                          checked: Array.isArray(_vm.checked.sizes)
                            ? _vm._i(_vm.checked.sizes, "all") > -1
                            : _vm.checked.sizes
                        },
                        on: {
                          click: function($event) {
                            return _vm.getFilterProductsPoly("sizes")
                          },
                          change: function($event) {
                            var $$a = _vm.checked.sizes,
                              $$el = $event.target,
                              $$c = $$el.checked ? true : false
                            if (Array.isArray($$a)) {
                              var $$v = "all",
                                $$i = _vm._i($$a, $$v)
                              if ($$el.checked) {
                                $$i < 0 &&
                                  _vm.$set(
                                    _vm.checked,
                                    "sizes",
                                    $$a.concat([$$v])
                                  )
                              } else {
                                $$i > -1 &&
                                  _vm.$set(
                                    _vm.checked,
                                    "sizes",
                                    $$a.slice(0, $$i).concat($$a.slice($$i + 1))
                                  )
                              }
                            } else {
                              _vm.$set(_vm.checked, "sizes", $$c)
                            }
                          }
                        }
                      }),
                      _vm._v(" "),
                      _c("span", { staticClass: "checkmark" })
                    ]),
                    _vm._v(" "),
                    _vm._l(_vm.filter.size, function(item) {
                      return _c("label", { staticClass: "container" }, [
                        _vm._v(
                          "\n                            " +
                            _vm._s(item.sizes) +
                            "\n                            "
                        ),
                        _c("input", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.checked.sizes,
                              expression: "checked.sizes"
                            }
                          ],
                          attrs: { type: "checkbox" },
                          domProps: {
                            value: item.id,
                            checked: Array.isArray(_vm.checked.sizes)
                              ? _vm._i(_vm.checked.sizes, item.id) > -1
                              : _vm.checked.sizes
                          },
                          on: {
                            click: function($event) {
                              return _vm.getFilterProducts("sizes")
                            },
                            change: function($event) {
                              var $$a = _vm.checked.sizes,
                                $$el = $event.target,
                                $$c = $$el.checked ? true : false
                              if (Array.isArray($$a)) {
                                var $$v = item.id,
                                  $$i = _vm._i($$a, $$v)
                                if ($$el.checked) {
                                  $$i < 0 &&
                                    _vm.$set(
                                      _vm.checked,
                                      "sizes",
                                      $$a.concat([$$v])
                                    )
                                } else {
                                  $$i > -1 &&
                                    _vm.$set(
                                      _vm.checked,
                                      "sizes",
                                      $$a
                                        .slice(0, $$i)
                                        .concat($$a.slice($$i + 1))
                                    )
                                }
                              } else {
                                _vm.$set(_vm.checked, "sizes", $$c)
                              }
                            }
                          }
                        }),
                        _vm._v(" "),
                        _c("span", { staticClass: "checkmark" })
                      ])
                    })
                  ],
                  2
                )
              ]),
              _vm._v(" "),
              !_vm.checkBrand()
                ? _c("li", [
                    _c("span", { on: { click: _vm.toggle } }, [
                      _vm._v("Brands\n                    "),
                      _c("i", {
                        staticClass: "fas fa-chevron-down slide-down-hover"
                      })
                    ]),
                    _vm._v(" "),
                    _c("hr", { staticClass: "divider1" }),
                    _vm._v(" "),
                    _c(
                      "p",
                      { staticClass: "mt-3" },
                      _vm._l(_vm.filter.brands, function(item) {
                        return _c("label", { staticClass: "container" }, [
                          _vm._v(
                            "\n                            " +
                              _vm._s(item.brands) +
                              "\n                            "
                          ),
                          _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.checked.brands,
                                expression: "checked.brands"
                              }
                            ],
                            attrs: { type: "checkbox" },
                            domProps: {
                              value: item.id,
                              checked: Array.isArray(_vm.checked.brands)
                                ? _vm._i(_vm.checked.brands, item.id) > -1
                                : _vm.checked.brands
                            },
                            on: {
                              click: function($event) {
                                return _vm.getFilterProducts("brands")
                              },
                              change: function($event) {
                                var $$a = _vm.checked.brands,
                                  $$el = $event.target,
                                  $$c = $$el.checked ? true : false
                                if (Array.isArray($$a)) {
                                  var $$v = item.id,
                                    $$i = _vm._i($$a, $$v)
                                  if ($$el.checked) {
                                    $$i < 0 &&
                                      _vm.$set(
                                        _vm.checked,
                                        "brands",
                                        $$a.concat([$$v])
                                      )
                                  } else {
                                    $$i > -1 &&
                                      _vm.$set(
                                        _vm.checked,
                                        "brands",
                                        $$a
                                          .slice(0, $$i)
                                          .concat($$a.slice($$i + 1))
                                      )
                                  }
                                } else {
                                  _vm.$set(_vm.checked, "brands", $$c)
                                }
                              }
                            }
                          }),
                          _vm._v(" "),
                          _c("span", { staticClass: "checkmark" })
                        ])
                      }),
                      0
                    )
                  ])
                : _vm._e(),
              _vm._v(" "),
              _c("li", [
                _c("span", { on: { click: _vm.toggle } }, [
                  _vm._v("Colour\n                    "),
                  _c("i", {
                    staticClass: "fas fa-chevron-down slide-down-hover"
                  })
                ]),
                _vm._v(" "),
                _c("hr", { staticClass: "divider1" }),
                _vm._v(" "),
                _c(
                  "p",
                  { staticClass: "mt-3" },
                  [
                    _c("label", { staticClass: "container" }, [
                      _c("span", [_vm._v("All Colors")]),
                      _vm._v(" "),
                      _c("input", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model",
                            value: _vm.checked.colors,
                            expression: "checked.colors"
                          }
                        ],
                        attrs: {
                          type: "checkbox",
                          checked: "checked",
                          value: "all"
                        },
                        domProps: {
                          checked: Array.isArray(_vm.checked.colors)
                            ? _vm._i(_vm.checked.colors, "all") > -1
                            : _vm.checked.colors
                        },
                        on: {
                          click: function($event) {
                            return _vm.getFilterProductsPoly("colors")
                          },
                          change: function($event) {
                            var $$a = _vm.checked.colors,
                              $$el = $event.target,
                              $$c = $$el.checked ? true : false
                            if (Array.isArray($$a)) {
                              var $$v = "all",
                                $$i = _vm._i($$a, $$v)
                              if ($$el.checked) {
                                $$i < 0 &&
                                  _vm.$set(
                                    _vm.checked,
                                    "colors",
                                    $$a.concat([$$v])
                                  )
                              } else {
                                $$i > -1 &&
                                  _vm.$set(
                                    _vm.checked,
                                    "colors",
                                    $$a.slice(0, $$i).concat($$a.slice($$i + 1))
                                  )
                              }
                            } else {
                              _vm.$set(_vm.checked, "colors", $$c)
                            }
                          }
                        }
                      }),
                      _vm._v(" "),
                      _c("span", { staticClass: "checkmark" })
                    ]),
                    _vm._v(" "),
                    _vm._l(_vm.filter.colors, function(item) {
                      return _c("label", { staticClass: "container" }, [
                        _vm._v(
                          "\n                            " +
                            _vm._s(item.colors) +
                            "\n                            "
                        ),
                        _c("input", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.checked.colors,
                              expression: "checked.colors"
                            }
                          ],
                          attrs: { type: "checkbox" },
                          domProps: {
                            value: item.id,
                            checked: Array.isArray(_vm.checked.colors)
                              ? _vm._i(_vm.checked.colors, item.id) > -1
                              : _vm.checked.colors
                          },
                          on: {
                            click: function($event) {
                              return _vm.getFilterProducts("colors")
                            },
                            change: function($event) {
                              var $$a = _vm.checked.colors,
                                $$el = $event.target,
                                $$c = $$el.checked ? true : false
                              if (Array.isArray($$a)) {
                                var $$v = item.id,
                                  $$i = _vm._i($$a, $$v)
                                if ($$el.checked) {
                                  $$i < 0 &&
                                    _vm.$set(
                                      _vm.checked,
                                      "colors",
                                      $$a.concat([$$v])
                                    )
                                } else {
                                  $$i > -1 &&
                                    _vm.$set(
                                      _vm.checked,
                                      "colors",
                                      $$a
                                        .slice(0, $$i)
                                        .concat($$a.slice($$i + 1))
                                    )
                                }
                              } else {
                                _vm.$set(_vm.checked, "colors", $$c)
                              }
                            }
                          }
                        }),
                        _vm._v(" "),
                        _c("span", { staticClass: "checkmark" })
                      ])
                    })
                  ],
                  2
                )
              ]),
              _vm._v(" "),
              _c("li", [
                _c("span", { on: { click: _vm.toggle } }, [
                  _vm._v("Fabric/Material\n                    "),
                  _c("i", {
                    staticClass: "fas fa-chevron-down slide-down-hover"
                  })
                ]),
                _vm._v(" "),
                _c("hr", { staticClass: "divider1" }),
                _vm._v(" "),
                _c(
                  "p",
                  { staticClass: "mt-3" },
                  [
                    _c("label", { staticClass: "container" }, [
                      _c("span", [_vm._v("All Fabrics")]),
                      _vm._v(" "),
                      _c("input", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model",
                            value: _vm.checked.fabrics,
                            expression: "checked.fabrics"
                          }
                        ],
                        attrs: {
                          type: "checkbox",
                          value: "all",
                          checked: "checked"
                        },
                        domProps: {
                          checked: Array.isArray(_vm.checked.fabrics)
                            ? _vm._i(_vm.checked.fabrics, "all") > -1
                            : _vm.checked.fabrics
                        },
                        on: {
                          click: function($event) {
                            return _vm.getFilterProductsPoly("fabrics")
                          },
                          change: function($event) {
                            var $$a = _vm.checked.fabrics,
                              $$el = $event.target,
                              $$c = $$el.checked ? true : false
                            if (Array.isArray($$a)) {
                              var $$v = "all",
                                $$i = _vm._i($$a, $$v)
                              if ($$el.checked) {
                                $$i < 0 &&
                                  _vm.$set(
                                    _vm.checked,
                                    "fabrics",
                                    $$a.concat([$$v])
                                  )
                              } else {
                                $$i > -1 &&
                                  _vm.$set(
                                    _vm.checked,
                                    "fabrics",
                                    $$a.slice(0, $$i).concat($$a.slice($$i + 1))
                                  )
                              }
                            } else {
                              _vm.$set(_vm.checked, "fabrics", $$c)
                            }
                          }
                        }
                      }),
                      _vm._v(" "),
                      _c("span", { staticClass: "checkmark" })
                    ]),
                    _vm._v(" "),
                    _vm._l(_vm.filter.fabrics, function(item) {
                      return _c("label", { staticClass: "container" }, [
                        _vm._v(
                          "\n                            " +
                            _vm._s(item.fabrics) +
                            "\n                            "
                        ),
                        _c("input", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.checked.fabrics,
                              expression: "checked.fabrics"
                            }
                          ],
                          attrs: { type: "checkbox" },
                          domProps: {
                            value: item.id,
                            checked: Array.isArray(_vm.checked.fabrics)
                              ? _vm._i(_vm.checked.fabrics, item.id) > -1
                              : _vm.checked.fabrics
                          },
                          on: {
                            click: function($event) {
                              return _vm.getFilterProducts("fabrics")
                            },
                            change: function($event) {
                              var $$a = _vm.checked.fabrics,
                                $$el = $event.target,
                                $$c = $$el.checked ? true : false
                              if (Array.isArray($$a)) {
                                var $$v = item.id,
                                  $$i = _vm._i($$a, $$v)
                                if ($$el.checked) {
                                  $$i < 0 &&
                                    _vm.$set(
                                      _vm.checked,
                                      "fabrics",
                                      $$a.concat([$$v])
                                    )
                                } else {
                                  $$i > -1 &&
                                    _vm.$set(
                                      _vm.checked,
                                      "fabrics",
                                      $$a
                                        .slice(0, $$i)
                                        .concat($$a.slice($$i + 1))
                                    )
                                }
                              } else {
                                _vm.$set(_vm.checked, "fabrics", $$c)
                              }
                            }
                          }
                        }),
                        _vm._v(" "),
                        _c("span", { staticClass: "checkmark" })
                      ])
                    })
                  ],
                  2
                )
              ]),
              _vm._v(" "),
              _c("li", [
                _c("span", { on: { click: _vm.toggle } }, [
                  _vm._v("Occasion\n                    "),
                  _c("i", {
                    staticClass: "fas fa-chevron-down slide-down-hover"
                  })
                ]),
                _vm._v(" "),
                _c("hr", { staticClass: "divider1" }),
                _vm._v(" "),
                _c(
                  "p",
                  { staticClass: "mt-3" },
                  [
                    _c("label", { staticClass: "container" }, [
                      _c("span", [_vm._v("All Occasions")]),
                      _vm._v(" "),
                      _c("input", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model",
                            value: _vm.checked.occasions,
                            expression: "checked.occasions"
                          }
                        ],
                        attrs: {
                          type: "checkbox",
                          checked: "checked",
                          value: "all"
                        },
                        domProps: {
                          checked: Array.isArray(_vm.checked.occasions)
                            ? _vm._i(_vm.checked.occasions, "all") > -1
                            : _vm.checked.occasions
                        },
                        on: {
                          click: function($event) {
                            return _vm.getFilterProductsPoly("occasions")
                          },
                          change: function($event) {
                            var $$a = _vm.checked.occasions,
                              $$el = $event.target,
                              $$c = $$el.checked ? true : false
                            if (Array.isArray($$a)) {
                              var $$v = "all",
                                $$i = _vm._i($$a, $$v)
                              if ($$el.checked) {
                                $$i < 0 &&
                                  _vm.$set(
                                    _vm.checked,
                                    "occasions",
                                    $$a.concat([$$v])
                                  )
                              } else {
                                $$i > -1 &&
                                  _vm.$set(
                                    _vm.checked,
                                    "occasions",
                                    $$a.slice(0, $$i).concat($$a.slice($$i + 1))
                                  )
                              }
                            } else {
                              _vm.$set(_vm.checked, "occasions", $$c)
                            }
                          }
                        }
                      }),
                      _vm._v(" "),
                      _c("span", { staticClass: "checkmark" })
                    ]),
                    _vm._v(" "),
                    _vm._l(_vm.filter.occasions, function(item) {
                      return _c("label", { staticClass: "container" }, [
                        _vm._v(
                          "\n                            " +
                            _vm._s(item.occasions) +
                            "\n                            "
                        ),
                        _c("input", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.checked.occasions,
                              expression: "checked.occasions"
                            }
                          ],
                          attrs: { type: "checkbox" },
                          domProps: {
                            value: item.id,
                            checked: Array.isArray(_vm.checked.occasions)
                              ? _vm._i(_vm.checked.occasions, item.id) > -1
                              : _vm.checked.occasions
                          },
                          on: {
                            click: function($event) {
                              return _vm.getFilterProducts("occasions")
                            },
                            change: function($event) {
                              var $$a = _vm.checked.occasions,
                                $$el = $event.target,
                                $$c = $$el.checked ? true : false
                              if (Array.isArray($$a)) {
                                var $$v = item.id,
                                  $$i = _vm._i($$a, $$v)
                                if ($$el.checked) {
                                  $$i < 0 &&
                                    _vm.$set(
                                      _vm.checked,
                                      "occasions",
                                      $$a.concat([$$v])
                                    )
                                } else {
                                  $$i > -1 &&
                                    _vm.$set(
                                      _vm.checked,
                                      "occasions",
                                      $$a
                                        .slice(0, $$i)
                                        .concat($$a.slice($$i + 1))
                                    )
                                }
                              } else {
                                _vm.$set(_vm.checked, "occasions", $$c)
                              }
                            }
                          }
                        }),
                        _vm._v(" "),
                        _c("span", { staticClass: "checkmark" })
                      ])
                    })
                  ],
                  2
                )
              ])
            ]
          ),
          _vm._v(" "),
          _c("ul", { staticClass: "list-filter ul-desktop" }, [
            _c("div", { staticClass: "title-2-desktop" }, [
              _c("b", [_vm._v("Refine")]),
              _vm._v(" "),
              _c(
                "span",
                {
                  staticStyle: { cursor: "grab" },
                  on: {
                    click: function($event) {
                      return _vm.clearAll()
                    }
                  }
                },
                [_c("u", [_vm._v("Clear All")])]
              )
            ]),
            _vm._v(" "),
            _c(
              "li",
              {
                directives: [
                  {
                    name: "show",
                    rawName: "v-show",
                    value:
                      _vm.subCategory == "All" ||
                      (_vm.category != "" &&
                        _vm.category !== undefined &&
                        _vm.category == _vm.subCategory),
                    expression:
                      "(subCategory == 'All' || (category != '' && category !== undefined && category == subCategory ))"
                  }
                ]
              },
              [
                _c("span", { on: { click: _vm.toggle } }, [
                  _vm._v("Sub-Category\n                    "),
                  _c("i", {
                    staticClass: "fas fa-chevron-down slide-down-hover"
                  })
                ]),
                _vm._v(" "),
                _c("hr", { staticClass: "divider1" }),
                _vm._v(" "),
                _c(
                  "p",
                  { staticClass: "mt-3" },
                  [
                    _vm.$route.params.gender == "Women" ||
                    _vm.$route.params.gender == "women"
                      ? [
                          _vm._l(_vm.allCategory.women, function(cat) {
                            return [
                              _c(
                                "span",
                                {
                                  staticClass: "cat-list ml-2 container",
                                  on: {
                                    click: function($event) {
                                      return _vm.goFilter(
                                        "Women",
                                        cat.label,
                                        _vm.category
                                      )
                                    }
                                  }
                                },
                                [_vm._v(_vm._s(cat.label))]
                              )
                            ]
                          })
                        ]
                      : [
                          _vm._l(_vm.allCategory.men, function(cat) {
                            return [
                              _c(
                                "span",
                                {
                                  staticClass: "cat-list ml-2 container",
                                  on: {
                                    click: function($event) {
                                      return _vm.goFilter(
                                        "Men",
                                        cat.label,
                                        _vm.category
                                      )
                                    }
                                  }
                                },
                                [_vm._v(_vm._s(cat.label))]
                              )
                            ]
                          })
                        ]
                  ],
                  2
                )
              ]
            ),
            _vm._v(" "),
            _c("li", [
              _c("span", { on: { click: _vm.toggle } }, [
                _vm._v("Price\n                    "),
                _c("i", { staticClass: "fas fa-chevron-down slide-down-hover" })
              ]),
              _vm._v(" "),
              _c("hr", { staticClass: "divider1" }),
              _vm._v(" "),
              _c("p", { staticClass: "mt-3" }, [
                _vm._v(
                  "\n                        Min\n                        "
                ),
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.checked.price.min,
                      expression: "checked.price.min"
                    }
                  ],
                  attrs: { type: "number", min: "0" },
                  domProps: { value: _vm.checked.price.min },
                  on: {
                    change: function($event) {
                      return _vm.submitPrice()
                    },
                    input: function($event) {
                      if ($event.target.composing) {
                        return
                      }
                      _vm.$set(_vm.checked.price, "min", $event.target.value)
                    }
                  }
                }),
                _vm._v(
                  "\n                        Max\n                        "
                ),
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.checked.price.max,
                      expression: "checked.price.max"
                    }
                  ],
                  attrs: { type: "number", min: "0" },
                  domProps: { value: _vm.checked.price.max },
                  on: {
                    change: function($event) {
                      return _vm.submitPrice()
                    },
                    input: function($event) {
                      if ($event.target.composing) {
                        return
                      }
                      _vm.$set(_vm.checked.price, "max", $event.target.value)
                    }
                  }
                })
              ])
            ]),
            _vm._v(" "),
            _c("li", [
              _c("span", { on: { click: _vm.toggle } }, [
                _vm._v("Sizes\n                    "),
                _c("i", { staticClass: "fas fa-chevron-down slide-down-hover" })
              ]),
              _vm._v(" "),
              _c("hr", { staticClass: "divider1" }),
              _vm._v(" "),
              _c(
                "p",
                { staticClass: "mt-3" },
                [
                  _c("label", { staticClass: "container" }, [
                    _c("span", [_vm._v("All Sizes")]),
                    _vm._v(" "),
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.checked.sizes,
                          expression: "checked.sizes"
                        }
                      ],
                      attrs: {
                        type: "checkbox",
                        value: "all",
                        checked: "checked"
                      },
                      domProps: {
                        checked: Array.isArray(_vm.checked.sizes)
                          ? _vm._i(_vm.checked.sizes, "all") > -1
                          : _vm.checked.sizes
                      },
                      on: {
                        click: function($event) {
                          return _vm.getFilterProductsPoly("sizes")
                        },
                        change: function($event) {
                          var $$a = _vm.checked.sizes,
                            $$el = $event.target,
                            $$c = $$el.checked ? true : false
                          if (Array.isArray($$a)) {
                            var $$v = "all",
                              $$i = _vm._i($$a, $$v)
                            if ($$el.checked) {
                              $$i < 0 &&
                                _vm.$set(
                                  _vm.checked,
                                  "sizes",
                                  $$a.concat([$$v])
                                )
                            } else {
                              $$i > -1 &&
                                _vm.$set(
                                  _vm.checked,
                                  "sizes",
                                  $$a.slice(0, $$i).concat($$a.slice($$i + 1))
                                )
                            }
                          } else {
                            _vm.$set(_vm.checked, "sizes", $$c)
                          }
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("span", { staticClass: "checkmark" })
                  ]),
                  _vm._v(" "),
                  _vm._l(_vm.filter.size, function(item) {
                    return _c("label", { staticClass: "container" }, [
                      _vm._v(
                        "\n                            " +
                          _vm._s(item.sizes) +
                          "\n                            "
                      ),
                      _c("input", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model",
                            value: _vm.checked.sizes,
                            expression: "checked.sizes"
                          }
                        ],
                        attrs: { type: "checkbox" },
                        domProps: {
                          value: item.id,
                          checked: Array.isArray(_vm.checked.sizes)
                            ? _vm._i(_vm.checked.sizes, item.id) > -1
                            : _vm.checked.sizes
                        },
                        on: {
                          click: function($event) {
                            return _vm.getFilterProducts("sizes")
                          },
                          change: function($event) {
                            var $$a = _vm.checked.sizes,
                              $$el = $event.target,
                              $$c = $$el.checked ? true : false
                            if (Array.isArray($$a)) {
                              var $$v = item.id,
                                $$i = _vm._i($$a, $$v)
                              if ($$el.checked) {
                                $$i < 0 &&
                                  _vm.$set(
                                    _vm.checked,
                                    "sizes",
                                    $$a.concat([$$v])
                                  )
                              } else {
                                $$i > -1 &&
                                  _vm.$set(
                                    _vm.checked,
                                    "sizes",
                                    $$a.slice(0, $$i).concat($$a.slice($$i + 1))
                                  )
                              }
                            } else {
                              _vm.$set(_vm.checked, "sizes", $$c)
                            }
                          }
                        }
                      }),
                      _vm._v(" "),
                      _c("span", { staticClass: "checkmark" })
                    ])
                  })
                ],
                2
              )
            ]),
            _vm._v(" "),
            !_vm.checkBrand()
              ? _c("li", [
                  _c("span", { on: { click: _vm.toggle } }, [
                    _vm._v("Brands\n                    "),
                    _c("i", {
                      staticClass: "fas fa-chevron-down slide-down-hover"
                    })
                  ]),
                  _vm._v(" "),
                  _c("hr", { staticClass: "divider1" }),
                  _vm._v(" "),
                  _c(
                    "p",
                    { staticClass: "mt-3" },
                    _vm._l(_vm.filter.brands, function(item) {
                      return _c("label", { staticClass: "container" }, [
                        _vm._v(
                          "\n                            " +
                            _vm._s(item.brands) +
                            "\n                            "
                        ),
                        _c("input", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.checked.brands,
                              expression: "checked.brands"
                            }
                          ],
                          attrs: { type: "checkbox" },
                          domProps: {
                            value: item.id,
                            checked: Array.isArray(_vm.checked.brands)
                              ? _vm._i(_vm.checked.brands, item.id) > -1
                              : _vm.checked.brands
                          },
                          on: {
                            click: function($event) {
                              return _vm.getFilterProducts("brands")
                            },
                            change: function($event) {
                              var $$a = _vm.checked.brands,
                                $$el = $event.target,
                                $$c = $$el.checked ? true : false
                              if (Array.isArray($$a)) {
                                var $$v = item.id,
                                  $$i = _vm._i($$a, $$v)
                                if ($$el.checked) {
                                  $$i < 0 &&
                                    _vm.$set(
                                      _vm.checked,
                                      "brands",
                                      $$a.concat([$$v])
                                    )
                                } else {
                                  $$i > -1 &&
                                    _vm.$set(
                                      _vm.checked,
                                      "brands",
                                      $$a
                                        .slice(0, $$i)
                                        .concat($$a.slice($$i + 1))
                                    )
                                }
                              } else {
                                _vm.$set(_vm.checked, "brands", $$c)
                              }
                            }
                          }
                        }),
                        _vm._v(" "),
                        _c("span", { staticClass: "checkmark" })
                      ])
                    }),
                    0
                  )
                ])
              : _vm._e(),
            _vm._v(" "),
            _c("li", [
              _c("span", { on: { click: _vm.toggle } }, [
                _vm._v("Colour\n                    "),
                _c("i", { staticClass: "fas fa-chevron-down slide-down-hover" })
              ]),
              _vm._v(" "),
              _c("hr", { staticClass: "divider1" }),
              _vm._v(" "),
              _c(
                "p",
                { staticClass: "mt-3" },
                [
                  _c("label", { staticClass: "container" }, [
                    _c("span", [_vm._v("All Colors")]),
                    _vm._v(" "),
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.checked.colors,
                          expression: "checked.colors"
                        }
                      ],
                      attrs: {
                        type: "checkbox",
                        checked: "checked",
                        value: "all"
                      },
                      domProps: {
                        checked: Array.isArray(_vm.checked.colors)
                          ? _vm._i(_vm.checked.colors, "all") > -1
                          : _vm.checked.colors
                      },
                      on: {
                        click: function($event) {
                          return _vm.getFilterProductsPoly("colors")
                        },
                        change: function($event) {
                          var $$a = _vm.checked.colors,
                            $$el = $event.target,
                            $$c = $$el.checked ? true : false
                          if (Array.isArray($$a)) {
                            var $$v = "all",
                              $$i = _vm._i($$a, $$v)
                            if ($$el.checked) {
                              $$i < 0 &&
                                _vm.$set(
                                  _vm.checked,
                                  "colors",
                                  $$a.concat([$$v])
                                )
                            } else {
                              $$i > -1 &&
                                _vm.$set(
                                  _vm.checked,
                                  "colors",
                                  $$a.slice(0, $$i).concat($$a.slice($$i + 1))
                                )
                            }
                          } else {
                            _vm.$set(_vm.checked, "colors", $$c)
                          }
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("span", { staticClass: "checkmark" })
                  ]),
                  _vm._v(" "),
                  _vm._l(_vm.filter.colors, function(item) {
                    return _c("label", { staticClass: "container" }, [
                      _vm._v(
                        "\n                            " +
                          _vm._s(item.colors) +
                          "\n                            "
                      ),
                      _c("input", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model",
                            value: _vm.checked.colors,
                            expression: "checked.colors"
                          }
                        ],
                        attrs: { type: "checkbox" },
                        domProps: {
                          value: item.id,
                          checked: Array.isArray(_vm.checked.colors)
                            ? _vm._i(_vm.checked.colors, item.id) > -1
                            : _vm.checked.colors
                        },
                        on: {
                          click: function($event) {
                            return _vm.getFilterProducts("colors")
                          },
                          change: function($event) {
                            var $$a = _vm.checked.colors,
                              $$el = $event.target,
                              $$c = $$el.checked ? true : false
                            if (Array.isArray($$a)) {
                              var $$v = item.id,
                                $$i = _vm._i($$a, $$v)
                              if ($$el.checked) {
                                $$i < 0 &&
                                  _vm.$set(
                                    _vm.checked,
                                    "colors",
                                    $$a.concat([$$v])
                                  )
                              } else {
                                $$i > -1 &&
                                  _vm.$set(
                                    _vm.checked,
                                    "colors",
                                    $$a.slice(0, $$i).concat($$a.slice($$i + 1))
                                  )
                              }
                            } else {
                              _vm.$set(_vm.checked, "colors", $$c)
                            }
                          }
                        }
                      }),
                      _vm._v(" "),
                      _c("span", { staticClass: "checkmark" })
                    ])
                  })
                ],
                2
              )
            ]),
            _vm._v(" "),
            _c("li", [
              _c("span", { on: { click: _vm.toggle } }, [
                _vm._v("Fabric/Material\n                    "),
                _c("i", { staticClass: "fas fa-chevron-down slide-down-hover" })
              ]),
              _vm._v(" "),
              _c("hr", { staticClass: "divider1" }),
              _vm._v(" "),
              _c(
                "p",
                { staticClass: "mt-3" },
                [
                  _c("label", { staticClass: "container" }, [
                    _c("span", [_vm._v("All Fabrics")]),
                    _vm._v(" "),
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.checked.fabrics,
                          expression: "checked.fabrics"
                        }
                      ],
                      attrs: {
                        type: "checkbox",
                        value: "all",
                        checked: "checked"
                      },
                      domProps: {
                        checked: Array.isArray(_vm.checked.fabrics)
                          ? _vm._i(_vm.checked.fabrics, "all") > -1
                          : _vm.checked.fabrics
                      },
                      on: {
                        click: function($event) {
                          return _vm.getFilterProductsPoly("fabrics")
                        },
                        change: function($event) {
                          var $$a = _vm.checked.fabrics,
                            $$el = $event.target,
                            $$c = $$el.checked ? true : false
                          if (Array.isArray($$a)) {
                            var $$v = "all",
                              $$i = _vm._i($$a, $$v)
                            if ($$el.checked) {
                              $$i < 0 &&
                                _vm.$set(
                                  _vm.checked,
                                  "fabrics",
                                  $$a.concat([$$v])
                                )
                            } else {
                              $$i > -1 &&
                                _vm.$set(
                                  _vm.checked,
                                  "fabrics",
                                  $$a.slice(0, $$i).concat($$a.slice($$i + 1))
                                )
                            }
                          } else {
                            _vm.$set(_vm.checked, "fabrics", $$c)
                          }
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("span", { staticClass: "checkmark" })
                  ]),
                  _vm._v(" "),
                  _vm._l(_vm.filter.fabrics, function(item) {
                    return _c("label", { staticClass: "container" }, [
                      _vm._v(
                        "\n                            " +
                          _vm._s(item.fabrics) +
                          "\n                            "
                      ),
                      _c("input", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model",
                            value: _vm.checked.fabrics,
                            expression: "checked.fabrics"
                          }
                        ],
                        attrs: { type: "checkbox" },
                        domProps: {
                          value: item.id,
                          checked: Array.isArray(_vm.checked.fabrics)
                            ? _vm._i(_vm.checked.fabrics, item.id) > -1
                            : _vm.checked.fabrics
                        },
                        on: {
                          click: function($event) {
                            return _vm.getFilterProducts("fabrics")
                          },
                          change: function($event) {
                            var $$a = _vm.checked.fabrics,
                              $$el = $event.target,
                              $$c = $$el.checked ? true : false
                            if (Array.isArray($$a)) {
                              var $$v = item.id,
                                $$i = _vm._i($$a, $$v)
                              if ($$el.checked) {
                                $$i < 0 &&
                                  _vm.$set(
                                    _vm.checked,
                                    "fabrics",
                                    $$a.concat([$$v])
                                  )
                              } else {
                                $$i > -1 &&
                                  _vm.$set(
                                    _vm.checked,
                                    "fabrics",
                                    $$a.slice(0, $$i).concat($$a.slice($$i + 1))
                                  )
                              }
                            } else {
                              _vm.$set(_vm.checked, "fabrics", $$c)
                            }
                          }
                        }
                      }),
                      _vm._v(" "),
                      _c("span", { staticClass: "checkmark" })
                    ])
                  })
                ],
                2
              )
            ]),
            _vm._v(" "),
            _c("li", [
              _c("span", { on: { click: _vm.toggle } }, [
                _vm._v("Occasion\n                    "),
                _c("i", { staticClass: "fas fa-chevron-down slide-down-hover" })
              ]),
              _vm._v(" "),
              _c("hr", { staticClass: "divider1" }),
              _vm._v(" "),
              _c(
                "p",
                { staticClass: "mt-3" },
                [
                  _c("label", { staticClass: "container" }, [
                    _c("span", [_vm._v("All Occasions")]),
                    _vm._v(" "),
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.checked.occasions,
                          expression: "checked.occasions"
                        }
                      ],
                      attrs: {
                        type: "checkbox",
                        checked: "checked",
                        value: "all"
                      },
                      domProps: {
                        checked: Array.isArray(_vm.checked.occasions)
                          ? _vm._i(_vm.checked.occasions, "all") > -1
                          : _vm.checked.occasions
                      },
                      on: {
                        click: function($event) {
                          return _vm.getFilterProductsPoly("occasions")
                        },
                        change: function($event) {
                          var $$a = _vm.checked.occasions,
                            $$el = $event.target,
                            $$c = $$el.checked ? true : false
                          if (Array.isArray($$a)) {
                            var $$v = "all",
                              $$i = _vm._i($$a, $$v)
                            if ($$el.checked) {
                              $$i < 0 &&
                                _vm.$set(
                                  _vm.checked,
                                  "occasions",
                                  $$a.concat([$$v])
                                )
                            } else {
                              $$i > -1 &&
                                _vm.$set(
                                  _vm.checked,
                                  "occasions",
                                  $$a.slice(0, $$i).concat($$a.slice($$i + 1))
                                )
                            }
                          } else {
                            _vm.$set(_vm.checked, "occasions", $$c)
                          }
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("span", { staticClass: "checkmark" })
                  ]),
                  _vm._v(" "),
                  _vm._l(_vm.filter.occasions, function(item) {
                    return _c("label", { staticClass: "container" }, [
                      _vm._v(
                        "\n                            " +
                          _vm._s(item.occasions) +
                          "\n                            "
                      ),
                      _c("input", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model",
                            value: _vm.checked.occasions,
                            expression: "checked.occasions"
                          }
                        ],
                        attrs: { type: "checkbox" },
                        domProps: {
                          value: item.id,
                          checked: Array.isArray(_vm.checked.occasions)
                            ? _vm._i(_vm.checked.occasions, item.id) > -1
                            : _vm.checked.occasions
                        },
                        on: {
                          click: function($event) {
                            return _vm.getFilterProducts("occasions")
                          },
                          change: function($event) {
                            var $$a = _vm.checked.occasions,
                              $$el = $event.target,
                              $$c = $$el.checked ? true : false
                            if (Array.isArray($$a)) {
                              var $$v = item.id,
                                $$i = _vm._i($$a, $$v)
                              if ($$el.checked) {
                                $$i < 0 &&
                                  _vm.$set(
                                    _vm.checked,
                                    "occasions",
                                    $$a.concat([$$v])
                                  )
                              } else {
                                $$i > -1 &&
                                  _vm.$set(
                                    _vm.checked,
                                    "occasions",
                                    $$a.slice(0, $$i).concat($$a.slice($$i + 1))
                                  )
                              }
                            } else {
                              _vm.$set(_vm.checked, "occasions", $$c)
                            }
                          }
                        }
                      }),
                      _vm._v(" "),
                      _c("span", { staticClass: "checkmark" })
                    ])
                  })
                ],
                2
              )
            ])
          ])
        ]
      ),
      _vm._v(" "),
      _c(
        "div",
        { staticClass: "col-xs-12 col-lg-9 filter-product-wrapper" },
        [
          _c(
            "div",
            { staticClass: "row" },
            [
              _c(
                "div",
                {
                  directives: [
                    {
                      name: "show",
                      rawName: "v-show",
                      value: _vm.loader,
                      expression: "loader"
                    }
                  ],
                  staticStyle: { height: "100%", width: "80%" }
                },
                [_vm._m(0)]
              ),
              _vm._v(" "),
              _vm.isEmpty(_vm.products.data)
                ? _c("div", [
                    _vm._v(
                      "\n                    No Result Found\n                "
                    )
                  ])
                : _vm._e(),
              _vm._v(" "),
              _vm._l(_vm.products.data, function(product, index) {
                return _c(
                  "div",
                  {
                    directives: [
                      {
                        name: "show",
                        rawName: "v-show",
                        value: !_vm.loader,
                        expression: "!loader"
                      }
                    ],
                    staticClass: "items-box col-sm-4 col-lg-3 col-6"
                  },
                  [
                    _c("div", { staticClass: "row pt-3" }, [
                      _c(
                        "div",
                        { staticClass: "card" },
                        [
                          _c(
                            "a",
                            {
                              staticClass: "similar",
                              on: {
                                click: function($event) {
                                  return _vm.viewSimilar(product.name, index)
                                }
                              }
                            },
                            [_vm._m(1, true)]
                          ),
                          _vm._v(" "),
                          _c(
                            "router-link",
                            {
                              attrs: {
                                to: {
                                  name: "singleProduct",
                                  params: { slug: product.slug, id: product.id }
                                }
                              }
                            },
                            [
                              _c("img", {
                                staticClass: "card-img-top",
                                attrs: { src: _vm.getImage(product.thumbnail) }
                              })
                            ]
                          ),
                          _vm._v(" "),
                          _c("div", { staticClass: "card-body" }, [
                            _c("div", { staticClass: "row" }, [
                              _c(
                                "div",
                                { staticClass: "col-12 d-flex desc-padd" },
                                [
                                  _c(
                                    "span",
                                    { staticClass: "brand mb-2 col-6" },
                                    [
                                      _vm._v(
                                        "\n                                                " +
                                          _vm._s(product.brand) +
                                          "\n                                            "
                                      )
                                    ]
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "span",
                                    { staticClass: "brand mb-2 col-6 heart" },
                                    [
                                      _c("WishList", {
                                        attrs: { product: product }
                                      })
                                    ],
                                    1
                                  )
                                ]
                              )
                            ]),
                            _vm._v(" "),
                            _c("div", { staticClass: "row" }, [
                              _c("div", { staticClass: "col-12 desc-padd" }, [
                                _c("span", { staticClass: "name mb-2" }, [
                                  _vm._v(
                                    "\n                                                " +
                                      _vm._s(product.name.substring(0, 22)) +
                                      "\n                                            "
                                  )
                                ])
                              ])
                            ]),
                            _vm._v(" "),
                            _c("div", { staticClass: "row" }, [
                              _c("div", { staticClass: "col-12 desc-padd" }, [
                                _c("h6", { staticClass: "name mt-2 mb-2" }, [
                                  _c(
                                    "span",
                                    { class: { "price-cut": product.rate } },
                                    [
                                      _vm._v(
                                        "Rs. " +
                                          _vm._s(_vm._f("price")(product.price))
                                      )
                                    ]
                                  ),
                                  _vm._v(" "),
                                  product.rate
                                    ? _c("span", { staticClass: "price-off" }, [
                                        _vm._v(
                                          "    \n                                            Rs. " +
                                            _vm._s(
                                              _vm._f("price")(
                                                _vm._f("round")(
                                                  product.price -
                                                    product.price * product.rate
                                                )
                                              )
                                            ) +
                                            "\n                                        "
                                        )
                                      ])
                                    : _vm._e()
                                ])
                              ])
                            ])
                          ])
                        ],
                        1
                      )
                    ])
                  ]
                )
              })
            ],
            2
          ),
          _vm._v(" "),
          !_vm.loader
            ? [
                _c("pagination", {
                  staticClass: "pagination-property",
                  attrs: { data: _vm.products },
                  on: { "pagination-change-page": _vm.getResults }
                })
              ]
            : _vm._e()
        ],
        2
      )
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "div",
      { staticStyle: { "margin-top": "10vh", "margin-left": "50%" } },
      [
        _c(
          "div",
          {
            staticClass: "spinner-border",
            staticStyle: { color: "#f4c2c2" },
            attrs: { role: "status" }
          },
          [_c("span", { staticClass: "sr-only" }, [_vm._v("Loading...")])]
        )
      ]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("i", { staticClass: "fas fa-layer-group" }, [
      _c("span", { staticClass: "text-box" }, [_vm._v("View Similar")])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/frontend/helpers/AddToWishList.vue?vue&type=template&id=045a22a5&scoped=true&":
/*!*********************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/frontend/helpers/AddToWishList.vue?vue&type=template&id=045a22a5&scoped=true& ***!
  \*********************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("i", {
    staticClass: "fa-heart",
    class: _vm.getHeart(),
    on: {
      click: function($event) {
        $event.preventDefault()
        return _vm.addToWish($event)
      }
    }
  })
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/components/frontend/Filter.vue":
/*!*****************************************************!*\
  !*** ./resources/js/components/frontend/Filter.vue ***!
  \*****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Filter_vue_vue_type_template_id_f06cd78c_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Filter.vue?vue&type=template&id=f06cd78c&scoped=true& */ "./resources/js/components/frontend/Filter.vue?vue&type=template&id=f06cd78c&scoped=true&");
/* harmony import */ var _Filter_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Filter.vue?vue&type=script&lang=js& */ "./resources/js/components/frontend/Filter.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _Filter_vue_vue_type_style_index_0_id_f06cd78c_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./Filter.vue?vue&type=style&index=0&id=f06cd78c&lang=scss&scoped=true& */ "./resources/js/components/frontend/Filter.vue?vue&type=style&index=0&id=f06cd78c&lang=scss&scoped=true&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _Filter_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Filter_vue_vue_type_template_id_f06cd78c_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Filter_vue_vue_type_template_id_f06cd78c_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "f06cd78c",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/frontend/Filter.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/frontend/Filter.vue?vue&type=script&lang=js&":
/*!******************************************************************************!*\
  !*** ./resources/js/components/frontend/Filter.vue?vue&type=script&lang=js& ***!
  \******************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Filter_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./Filter.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/frontend/Filter.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Filter_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/frontend/Filter.vue?vue&type=style&index=0&id=f06cd78c&lang=scss&scoped=true&":
/*!***************************************************************************************************************!*\
  !*** ./resources/js/components/frontend/Filter.vue?vue&type=style&index=0&id=f06cd78c&lang=scss&scoped=true& ***!
  \***************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_Filter_vue_vue_type_style_index_0_id_f06cd78c_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/style-loader!../../../../node_modules/css-loader!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--7-2!../../../../node_modules/sass-loader/dist/cjs.js??ref--7-3!../../../../node_modules/vue-loader/lib??vue-loader-options!./Filter.vue?vue&type=style&index=0&id=f06cd78c&lang=scss&scoped=true& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/frontend/Filter.vue?vue&type=style&index=0&id=f06cd78c&lang=scss&scoped=true&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_Filter_vue_vue_type_style_index_0_id_f06cd78c_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_Filter_vue_vue_type_style_index_0_id_f06cd78c_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_Filter_vue_vue_type_style_index_0_id_f06cd78c_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_Filter_vue_vue_type_style_index_0_id_f06cd78c_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_Filter_vue_vue_type_style_index_0_id_f06cd78c_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./resources/js/components/frontend/Filter.vue?vue&type=template&id=f06cd78c&scoped=true&":
/*!************************************************************************************************!*\
  !*** ./resources/js/components/frontend/Filter.vue?vue&type=template&id=f06cd78c&scoped=true& ***!
  \************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Filter_vue_vue_type_template_id_f06cd78c_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./Filter.vue?vue&type=template&id=f06cd78c&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/frontend/Filter.vue?vue&type=template&id=f06cd78c&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Filter_vue_vue_type_template_id_f06cd78c_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Filter_vue_vue_type_template_id_f06cd78c_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/frontend/helpers/AddToWishList.vue":
/*!********************************************************************!*\
  !*** ./resources/js/components/frontend/helpers/AddToWishList.vue ***!
  \********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _AddToWishList_vue_vue_type_template_id_045a22a5_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./AddToWishList.vue?vue&type=template&id=045a22a5&scoped=true& */ "./resources/js/components/frontend/helpers/AddToWishList.vue?vue&type=template&id=045a22a5&scoped=true&");
/* harmony import */ var _AddToWishList_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./AddToWishList.vue?vue&type=script&lang=js& */ "./resources/js/components/frontend/helpers/AddToWishList.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _AddToWishList_vue_vue_type_style_index_0_id_045a22a5_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./AddToWishList.vue?vue&type=style&index=0&id=045a22a5&scoped=true&lang=css& */ "./resources/js/components/frontend/helpers/AddToWishList.vue?vue&type=style&index=0&id=045a22a5&scoped=true&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _AddToWishList_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _AddToWishList_vue_vue_type_template_id_045a22a5_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _AddToWishList_vue_vue_type_template_id_045a22a5_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "045a22a5",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/frontend/helpers/AddToWishList.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/frontend/helpers/AddToWishList.vue?vue&type=script&lang=js&":
/*!*********************************************************************************************!*\
  !*** ./resources/js/components/frontend/helpers/AddToWishList.vue?vue&type=script&lang=js& ***!
  \*********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_AddToWishList_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./AddToWishList.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/frontend/helpers/AddToWishList.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_AddToWishList_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/frontend/helpers/AddToWishList.vue?vue&type=style&index=0&id=045a22a5&scoped=true&lang=css&":
/*!*****************************************************************************************************************************!*\
  !*** ./resources/js/components/frontend/helpers/AddToWishList.vue?vue&type=style&index=0&id=045a22a5&scoped=true&lang=css& ***!
  \*****************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_AddToWishList_vue_vue_type_style_index_0_id_045a22a5_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/style-loader!../../../../../node_modules/css-loader??ref--6-1!../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../node_modules/postcss-loader/src??ref--6-2!../../../../../node_modules/vue-loader/lib??vue-loader-options!./AddToWishList.vue?vue&type=style&index=0&id=045a22a5&scoped=true&lang=css& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/frontend/helpers/AddToWishList.vue?vue&type=style&index=0&id=045a22a5&scoped=true&lang=css&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_AddToWishList_vue_vue_type_style_index_0_id_045a22a5_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_AddToWishList_vue_vue_type_style_index_0_id_045a22a5_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_AddToWishList_vue_vue_type_style_index_0_id_045a22a5_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_AddToWishList_vue_vue_type_style_index_0_id_045a22a5_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_AddToWishList_vue_vue_type_style_index_0_id_045a22a5_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./resources/js/components/frontend/helpers/AddToWishList.vue?vue&type=template&id=045a22a5&scoped=true&":
/*!***************************************************************************************************************!*\
  !*** ./resources/js/components/frontend/helpers/AddToWishList.vue?vue&type=template&id=045a22a5&scoped=true& ***!
  \***************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_AddToWishList_vue_vue_type_template_id_045a22a5_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./AddToWishList.vue?vue&type=template&id=045a22a5&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/frontend/helpers/AddToWishList.vue?vue&type=template&id=045a22a5&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_AddToWishList_vue_vue_type_template_id_045a22a5_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_AddToWishList_vue_vue_type_template_id_045a22a5_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);