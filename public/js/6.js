(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[6],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/frontend/layout/Footer.vue?vue&type=script&lang=js&":
/*!*********************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/frontend/layout/Footer.vue?vue&type=script&lang=js& ***!
  \*********************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  name: "Footer",
  data: function data() {
    return {
      year: new Date().getFullYear()
    };
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/frontend/layout/Header.vue?vue&type=script&lang=js&":
/*!*********************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/frontend/layout/Header.vue?vue&type=script&lang=js& ***!
  \*********************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _helpers_header_cart__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../helpers/header_cart */ "./resources/js/components/frontend/helpers/header_cart.vue");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
 // import Wishlist from "../helpers/Wishlist_header";

/* harmony default export */ __webpack_exports__["default"] = ({
  name: "Header",
  components: {
    Header_cart: _helpers_header_cart__WEBPACK_IMPORTED_MODULE_0__["default"]
  },
  data: function data() {
    return {
      display: '',
      search: '',
      isMen: false,
      isWomen: true,
      allCategory: {},
      categories: {
        men: {},
        women: {},
        brand: {}
      },
      badge: '0',
      carts: [],
      quantity: '0',
      totalPrice: '0',
      brands: [],
      bannerImages: {},
      brandImage: ''
    };
  },
  created: function created() {
    var _this = this;

    axios.get('/api/getCategoryData').then(function (data) {
      _this.allCategory = data.data;
    });
  },
  methods: {
    openSearch: function openSearch() {
      this.display = 'block';
    },
    closeSearch: function closeSearch() {
      this.display = 'none';
    },
    logout: function logout() {
      this.$store.commit('logout');
      this.$router.go();
    },
    goSearch: function goSearch() {
      this.closeSearch();
      this.$router.push({
        name: 'filter',
        params: {
          gender: 'All'
        },
        query: {
          search: this.search
        }
      });
    },
    //go to filter page with defined parameter
    goFilter: function goFilter(gender, subCategory, category) {
      this.$router.push({
        name: 'filter',
        params: {
          gender: gender,
          subCategory: subCategory,
          category: category
        }
      });
    },
    goFilterBrand: function goFilterBrand(category, brand) {
      var val = this.isMen ? 'Men' : 'Women';

      if (category == 'All') {
        this.$router.push({
          name: 'filter',
          params: {
            gender: val
          },
          query: {
            brand: brand
          }
        });
      } else {
        this.$router.push({
          name: 'filter',
          params: {
            gender: val,
            category: category
          },
          query: {
            brand: brand
          }
        });
      }
    },
    goFilterSales: function goFilterSales(gender, category, sales) {
      if (category !== '') {
        this.$router.push({
          name: 'filter',
          params: {
            gender: gender,
            category: category
          },
          query: {
            sales: sales
          }
        });
      } else {
        this.$router.push({
          name: 'filter',
          params: {
            gender: gender
          },
          query: {
            sales: sales
          }
        });
      }
    },
    //end of filter section
    getLogo: function getLogo() {
      return "/image/logo.png";
    },
    getNavImg: function getNavImg() {
      return "/image/item8.png";
    },
    showMen: function showMen() {
      this.isMen = true;
      this.isWomen = false;
    },
    showWomen: function showWomen() {
      this.isMen = false;
      this.isWomen = true;
    },
    shownav: function shownav() {
      $(this).parent().toggleClass('open');
    },
    getCategoryData: function getCategoryData(name) {
      var _this2 = this;

      this.categories = this.allCategory[name];
      var gender = this.isWomen ? 'Women' : 'Men';
      axios.get('/api/brandDirectory/' + gender + '/' + name).then(function (data) {
        _this2.brands = data.data;
      });
      axios.get('/api/navigation-banner-image/' + gender + '/' + name).then(function (data) {
        _this2.bannerImages = data.data;
      });
      this.shownav();
    },
    getBrands: function getBrands(category) {
      var _this3 = this;

      var gender = this.isWomen ? 'Women' : 'Men';
      axios.get('/api/brandDirectory/' + gender + '/' + category).then(function (data) {
        _this3.brands = data.data;
      });
    },
    getBanner: function getBanner(category) {
      var _this4 = this;

      var gender = this.isWomen ? 'Women' : 'Men';
      axios.get('/api/navigation-banner-image/' + gender + '/' + category).then(function (data) {
        _this4.bannerImages = data.data;
      });
    },
    getBrand: function getBrand() {
      var _this5 = this;

      var gender = this.isWomen ? 'Women' : 'Men';
      axios.get('/api/featuredBrand/' + gender).then(function (data) {
        _this5.brandImage = data.data;
      });
    }
  },
  computed: {
    currentUser: function currentUser() {
      return this.$store.getters.currentUser;
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/frontend/layout/Layout.vue?vue&type=script&lang=js&":
/*!*********************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/frontend/layout/Layout.vue?vue&type=script&lang=js& ***!
  \*********************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Header__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Header */ "./resources/js/components/frontend/layout/Header.vue");
/* harmony import */ var _Footer__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Footer */ "./resources/js/components/frontend/layout/Footer.vue");
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  name: "Layout.vue",
  components: {
    Header: _Header__WEBPACK_IMPORTED_MODULE_0__["default"],
    Footer: _Footer__WEBPACK_IMPORTED_MODULE_1__["default"]
  },
  data: function data() {
    return {
      search: ''
    };
  },
  watch: {
    '$route': {
      handler: function handler(to, from) {
        document.title = to.meta.title + ' | The NextTrends';
      },
      immediate: true
    }
  }
});

/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/frontend/layout/Footer.vue?vue&type=style&index=0&id=c51d3ce4&lang=scss&scoped=true&":
/*!********************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/sass-loader/dist/cjs.js??ref--7-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/frontend/layout/Footer.vue?vue&type=style&index=0&id=c51d3ce4&lang=scss&scoped=true& ***!
  \********************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".page-footer[data-v-c51d3ce4] {\n  background: #232323;\n  color: whitesmoke;\n}\na[data-v-c51d3ce4] {\n  color: whitesmoke !important;\n  text-decoration: none;\n}\nhr[data-v-c51d3ce4] {\n  border-top: 1px solid whitesmoke;\n}\n.title[data-v-c51d3ce4] {\n  font-size: 17px;\n  font-weight: bold;\n  margin-bottom: 10px;\n}\n.title span[data-v-c51d3ce4] {\n  font-size: 26px;\n}\n@media screen and (max-width: 767px) {\n.title[data-v-c51d3ce4] {\n    font-size: 1rem;\n}\n.title span[data-v-c51d3ce4] {\n    font-size: 25px;\n}\n}", ""]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/frontend/layout/Header.vue?vue&type=style&index=0&id=82b87500&lang=scss&scoped=true&":
/*!********************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/sass-loader/dist/cjs.js??ref--7-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/frontend/layout/Header.vue?vue&type=style&index=0&id=82b87500&lang=scss&scoped=true& ***!
  \********************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".sub-header[data-v-82b87500] {\n  color: #020101;\n  font-size: 1rem;\n}\n.show_active[data-v-82b87500] {\n  border-bottom: 3px solid #f4c2c2;\n}\n.user-drop[data-v-82b87500] {\n  border-radius: 0;\n  z-index: 2000;\n}\n.user-drop .dropdown-item[data-v-82b87500] {\n  color: #232323;\n  margin-top: 0.5rem;\n}\n.user-drop .dropdown-item.active[data-v-82b87500], .user-drop .dropdown-item[data-v-82b87500]:active {\n  color: #ffffff !important;\n  text-decoration: none;\n  background-color: #545b62 !important;\n}\na[data-v-82b87500] {\n  text-decoration: none;\n  color: whitesmoke;\n}\n.category-a[data-v-82b87500] {\n  color: #4d4d4d;\n  margin: 0.3rem 0;\n  width: 50%;\n  font-family: Roboto;\n  font-size: 14px;\n}\n.category-a[data-v-82b87500]:hover {\n  color: cornflowerblue;\n}\n.wish-search[data-v-82b87500] {\n  display: flex;\n  /*margin-right: 1vw;*/\n  margin-left: 3vw;\n  align-items: center;\n}\n.wish-search .openBtn[data-v-82b87500] {\n  background: transparent;\n  border: none;\n  font-size: 20px;\n  cursor: pointer;\n  outline: none;\n}\n.openBtn[data-v-82b87500]:hover {\n  background: #bbb;\n}\n.overlay[data-v-82b87500] {\n  height: 8rem;\n  width: 100%;\n  display: none;\n  position: fixed;\n  z-index: 1;\n  top: 0;\n  left: 0;\n  background-color: #232323;\n}\n@media screen and (max-width: 767px) {\n.overlay[data-v-82b87500] {\n    height: 100%;\n    background-color: rgba(0, 0, 0, 0.9);\n}\n}\n.overlay-content[data-v-82b87500] {\n  position: relative;\n  top: 46%;\n  width: 80%;\n  text-align: center;\n  margin-top: 30px;\n  margin: auto;\n}\n@media screen and (max-width: 767px) {\n.overlay-content[data-v-82b87500] {\n    top: 6%;\n}\n}\n.overlay .closebtn[data-v-82b87500] {\n  z-index: 2;\n  position: absolute;\n  top: 45px;\n  left: 29%;\n  font-size: 45px;\n  cursor: pointer;\n  color: white;\n}\n@media screen and (max-width: 767px) {\n.overlay .closebtn[data-v-82b87500] {\n    top: 4%;\n    left: 8vw;\n}\n}\n.overlay .closebtn[data-v-82b87500]:hover {\n  color: #ccc;\n}\n.overlay input[type=text][data-v-82b87500] {\n  padding: 12px;\n  font-size: 13px;\n  border: none;\n  width: 40%;\n  background: white;\n}\n@media screen and (max-width: 767px) {\n.overlay input[type=text][data-v-82b87500] {\n    width: 80%;\n}\n}\n.overlay input[type=text][data-v-82b87500]:hover {\n  background: #f1f1f1;\n}\n.overlay button[data-v-82b87500] {\n  float: left;\n  width: 20%;\n  padding: 15px;\n  background: #ddd;\n  font-size: 17px;\n  border: none;\n  cursor: pointer;\n}\n.overlay button[data-v-82b87500]:hover {\n  background: #bbb;\n}\n\n/**Search**/\n/**middle-header**/\n.middle-header[data-v-82b87500] {\n  display: flex;\n  background: #232323;\n  justify-content: space-between;\n  align-items: center;\n  padding-top: 2rem;\n}\n@media screen and (max-width: 767px) {\n.middle-header[data-v-82b87500] {\n    padding-top: 1rem;\n}\n}\n.middle-header .search[data-v-82b87500] {\n  margin: 0 3vw;\n}\n.middle-header .shop-title[data-v-82b87500] {\n  display: flex;\n  align-items: center;\n}\n.middle-header .shop-title .title[data-v-82b87500] {\n  font-size: 30px;\n  font-weight: bold;\n  margin-left: 2vw;\n}\n.middle-header .shop-title .title span[data-v-82b87500] {\n  font-size: 40px;\n}\n@media screen and (max-width: 767px) {\n.middle-header .shop-title .title[data-v-82b87500] {\n    font-size: 1rem;\n}\n.middle-header .shop-title .title span[data-v-82b87500] {\n    font-size: 25px;\n}\n}\n.middle-header .profile-cart[data-v-82b87500] {\n  display: flex;\n  margin-right: 3vw;\n  align-items: center;\n}\n.middle-header .profile-cart .nav-item[data-v-82b87500] {\n  padding: 0.5rem 0;\n}\n.middle-header .profile-cart .nav-link[data-v-82b87500] {\n  padding: 0.5rem 0;\n}\n.middle-header .profile-cart .nav-link i[data-v-82b87500] {\n  font-size: 20px;\n}\n@media screen and (max-width: 767px) {\n.middle-header .profile-cart .nav-link i[data-v-82b87500] {\n    margin-left: 12px;\n}\n}\n.middle-header .profile-cart .cart-logged[data-v-82b87500] {\n  height: 2rem;\n  width: 2rem;\n  border-radius: 50%;\n  display: flex;\n  background: whitesmoke;\n  justify-content: center;\n  align-items: center;\n}\n.middle-header .profile-cart .cart[data-v-82b87500] {\n  margin: 0 1vw;\n}\n.middle-header .profile-cart .cart i[data-v-82b87500] {\n  color: black;\n}\n.middle-header .profile-cart .dropdown i[data-v-82b87500] {\n  font-size: 20px;\n  margin-bottom: 5px;\n}\n\n/**middle-header**/\n/**menu-header**/\n@media screen and (min-width: 768px) {\n.menu-header .cat-list[data-v-82b87500] {\n    display: flex;\n    flex-wrap: wrap;\n    position: static;\n}\n.menu-header .cat-list .dropdown-menu[data-v-82b87500] {\n    position: absolute;\n    width: 100%;\n    left: 0;\n    right: 0;\n    height: 100vh;\n}\n.menu-header .navbar-nav[data-v-82b87500] {\n    width: 55%;\n    display: flex;\n    justify-content: center;\n}\n.menu-header .navbar-nav .dropdown-menu[data-v-82b87500] {\n    width: 100%;\n    z-index: 2000;\n    left: 0;\n    right: 0;\n    margin: 0;\n}\n.menu-header .navbar-nav .navigation-menu-style .genre-list[data-v-82b87500] {\n    display: flex;\n    justify-content: center;\n}\n.menu-header .navbar-nav .navigation-menu-style .genre-list span[data-v-82b87500] {\n    font-weight: bold;\n    cursor: pointer;\n}\n.menu-header .navbar-nav .navigation-menu-style .genre-list .shop-women[data-v-82b87500] {\n    margin-right: 4vw;\n}\n.menu-header .navbar-nav .navigation-menu-style .category[data-v-82b87500] {\n    display: flex;\n}\n.menu-header .navbar-nav .navigation-menu-style .category .category-list[data-v-82b87500] {\n    padding: 1.5rem;\n    width: 39%;\n    display: flex;\n    flex-wrap: wrap;\n    flex-direction: column;\n}\n.menu-header .navbar-nav .navigation-menu-style .category .category-list div[data-v-82b87500] {\n    display: flex;\n}\n.menu-header .navbar-nav .navigation-menu-style .category .category-list div a[data-v-82b87500] {\n    cursor: pointer;\n}\n.menu-header .navbar-nav .navigation-menu-style .category .category-list div a[data-v-82b87500]:hover {\n    text-decoration: underline;\n}\n.menu-header .navbar-nav .navigation-menu-style .category .top-brands[data-v-82b87500] {\n    display: flex;\n    flex-wrap: wrap;\n    padding: 1.5rem;\n    width: 20%;\n    border-right: 1px solid grey;\n    border-left: 1px solid grey;\n    flex-direction: column;\n}\n.menu-header .navbar-nav .navigation-menu-style .category .top-brands a[data-v-82b87500] {\n    width: 100%;\n    cursor: pointer;\n}\n.menu-header .navbar-nav .navigation-menu-style .category .top-brands a[data-v-82b87500]:hover {\n    text-decoration: underline;\n}\n}\n@media screen and (max-width: 767px) {\n.menu-header .cat-list[data-v-82b87500] {\n    width: 100%;\n    display: flex;\n    flex-wrap: wrap;\n}\n.menu-header .genre-list[data-v-82b87500] {\n    display: flex;\n    justify-content: space-evenly;\n}\n.menu-header .category-list[data-v-82b87500], .menu-header .top-brands[data-v-82b87500] {\n    padding: 1.5rem;\n    display: flex;\n    flex-wrap: wrap;\n}\n.menu-header .category-list a[data-v-82b87500], .menu-header .top-brands a[data-v-82b87500] {\n    width: 50%;\n    cursor: pointer;\n    color: #232323;\n}\n}\n.menu-header .clothing-brand-title[data-v-82b87500] {\n  font-size: 1rem;\n  margin-bottom: 1rem;\n  width: 100%;\n  font-weight: bold;\n  font-family: \"Roboto\", sans-serif;\n  color: #4d4d4d;\n}\n.menu-header .photo-offer[data-v-82b87500] {\n  width: 60%;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n}\n.menu-header .photo-offer .photos[data-v-82b87500] {\n  display: flex;\n  flex-wrap: wrap;\n}\n.menu-header .photo-offer .image[data-v-82b87500] {\n  width: 24%;\n  margin-right: 0.5rem;\n  margin-bottom: 0.5rem;\n}\n.menu-header .photo-offer .image img[data-v-82b87500] {\n  height: 100%;\n  width: 100%;\n  -o-object-fit: contain;\n     object-fit: contain;\n}\n\n/**menu-header**/\n.home-shipping[data-v-82b87500] {\n  display: flex;\n  justify-content: center;\n  padding: 0.3rem;\n  align-items: center;\n  background: #f4f3f3;\n}\n.home-shipping span[data-v-82b87500] {\n  font-size: 0.8rem;\n  color: #020101;\n  font-weight: bold;\n  text-transform: uppercase;\n  font-family: Titillium Web;\n}\n.shop-women-men[data-v-82b87500] {\n  font-family: \"Roboto\", sans-serif;\n  color: #4d4d4d;\n  font-size: 0.9rem !important;\n}\n.brand-list a[data-v-82b87500] {\n  margin-bottom: 0.5rem;\n  color: #232323;\n}\n.brand-list div[data-v-82b87500] {\n  flex-direction: column;\n}\n.navbar-light .navbar-nav .nav-link[data-v-82b87500] {\n  font-family: \"Roboto\", sans-serif;\n  color: whitesmoke;\n  font-size: 1rem;\n}\n.dropdown-item[data-v-82b87500] {\n  color: black;\n}\n.profile-box[data-v-82b87500] {\n  left: -7rem;\n}\n.cat-list[data-v-82b87500] {\n  margin-right: 1rem;\n}\n@media screen and (min-width: 767px) {\n.cat-list[data-v-82b87500] {\n    margin-right: 2rem;\n}\n}\n.cart-logged[data-v-82b87500] {\n  margin-left: 1vw;\n  color: white;\n}\n@media screen and (max-width: 767px) {\n.mobile-navigation[data-v-82b87500] {\n    overflow-x: scroll;\n    padding: 0 1rem;\n}\n.nav-ul[data-v-82b87500] {\n    flex-direction: row;\n}\n.nav-ul .cat-list[data-v-82b87500] {\n    position: static;\n}\n.nav-ul .cat-list .dropdown-menu[data-v-82b87500] {\n    position: absolute;\n    width: 100%;\n    left: 0;\n    right: 0;\n    height: 100vh;\n    margin: 0;\n}\n.nav-ul .cat-list .nav-link[data-v-82b87500] {\n    font-size: 1em;\n}\n.mobile-navigation[data-v-82b87500]::-webkit-scrollbar {\n    display: none;\n}\n}\n.quantity[data-v-82b87500] {\n  position: relative;\n  background: #e3342f;\n  border-radius: 50%;\n  bottom: 1rem;\n  padding: 0.1rem 0.5rem;\n  color: white;\n}\n.wish-btn[data-v-82b87500] {\n  background: transparent;\n  border: none;\n  font-size: 20px;\n  cursor: pointer;\n  outline: none;\n}\n\n/*@media screen and (max-width: 768px) {*/\n/*.desktop-cart {*/\n/*display: none;*/\n/*}*/\n/*}*/\n/*@media screen and (min-width: 770px) {*/\n/*.mobile-cart {*/\n/*display: none;*/\n/*}*/\n/*}*/", ""]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/frontend/layout/Layout.vue?vue&type=style&index=0&lang=scss&":
/*!********************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/sass-loader/dist/cjs.js??ref--7-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/frontend/layout/Layout.vue?vue&type=style&index=0&lang=scss& ***!
  \********************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "html, body {\n  color: #707070;\n}", ""]);

// exports


/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/frontend/layout/Footer.vue?vue&type=style&index=0&id=c51d3ce4&lang=scss&scoped=true&":
/*!************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/sass-loader/dist/cjs.js??ref--7-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/frontend/layout/Footer.vue?vue&type=style&index=0&id=c51d3ce4&lang=scss&scoped=true& ***!
  \************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../../node_modules/css-loader!../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../node_modules/postcss-loader/src??ref--7-2!../../../../../node_modules/sass-loader/dist/cjs.js??ref--7-3!../../../../../node_modules/vue-loader/lib??vue-loader-options!./Footer.vue?vue&type=style&index=0&id=c51d3ce4&lang=scss&scoped=true& */ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/frontend/layout/Footer.vue?vue&type=style&index=0&id=c51d3ce4&lang=scss&scoped=true&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/frontend/layout/Header.vue?vue&type=style&index=0&id=82b87500&lang=scss&scoped=true&":
/*!************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/sass-loader/dist/cjs.js??ref--7-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/frontend/layout/Header.vue?vue&type=style&index=0&id=82b87500&lang=scss&scoped=true& ***!
  \************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../../node_modules/css-loader!../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../node_modules/postcss-loader/src??ref--7-2!../../../../../node_modules/sass-loader/dist/cjs.js??ref--7-3!../../../../../node_modules/vue-loader/lib??vue-loader-options!./Header.vue?vue&type=style&index=0&id=82b87500&lang=scss&scoped=true& */ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/frontend/layout/Header.vue?vue&type=style&index=0&id=82b87500&lang=scss&scoped=true&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/frontend/layout/Layout.vue?vue&type=style&index=0&lang=scss&":
/*!************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/sass-loader/dist/cjs.js??ref--7-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/frontend/layout/Layout.vue?vue&type=style&index=0&lang=scss& ***!
  \************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../../node_modules/css-loader!../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../node_modules/postcss-loader/src??ref--7-2!../../../../../node_modules/sass-loader/dist/cjs.js??ref--7-3!../../../../../node_modules/vue-loader/lib??vue-loader-options!./Layout.vue?vue&type=style&index=0&lang=scss& */ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/frontend/layout/Layout.vue?vue&type=style&index=0&lang=scss&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/frontend/layout/Footer.vue?vue&type=template&id=c51d3ce4&scoped=true&":
/*!*************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/frontend/layout/Footer.vue?vue&type=template&id=c51d3ce4&scoped=true& ***!
  \*************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _c("footer", { staticClass: "page-footer font-small mdb-color pt-4" }, [
      _c("div", { staticClass: "container text-center text-md-left" }, [
        _c(
          "div",
          {
            staticClass:
              "row text-center text-md-left mt-3 pb-3 justify-content-between"
          },
          [
            _c("div", { staticClass: "col-md-3 col-lg-3 col-xl-3 mt-2" }, [
              _vm._m(0),
              _vm._v(" "),
              _c(
                "p",
                [
                  _c(
                    "router-link",
                    { attrs: { to: { name: "AboutUs" }, tag: "a", exact: "" } },
                    [
                      _vm._v(
                        "\n                                About US\n                            "
                      )
                    ]
                  )
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "p",
                [
                  _c(
                    "router-link",
                    { attrs: { to: { name: "Careers" }, tag: "a", exact: "" } },
                    [
                      _vm._v(
                        "\n                                Careers\n                            "
                      )
                    ]
                  )
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "p",
                [
                  _c(
                    "router-link",
                    { attrs: { to: { name: "privacy" }, tag: "a", exact: "" } },
                    [
                      _vm._v(
                        "\n                                Privacy Policy\n                            "
                      )
                    ]
                  )
                ],
                1
              )
            ]),
            _vm._v(" "),
            _c("hr", { staticClass: "w-100 clearfix d-md-none" }),
            _vm._v(" "),
            _c("div", { staticClass: "col-md-3 col-lg-2 col-xl-2 mt-3" }, [
              _c(
                "h6",
                { staticClass: "text-uppercase mb-4 font-weight-bold" },
                [_vm._v("Help & Support")]
              ),
              _vm._v(" "),
              _c(
                "p",
                [
                  _c(
                    "router-link",
                    { attrs: { to: { name: "Contact" }, tag: "a", exact: "" } },
                    [
                      _vm._v(
                        "\n                                FAQs & Contact\n                            "
                      )
                    ]
                  )
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "p",
                [
                  _c(
                    "router-link",
                    {
                      attrs: { to: { name: "Delivery" }, tag: "a", exact: "" }
                    },
                    [
                      _vm._v(
                        "\n                                Delivery\n                            "
                      )
                    ]
                  )
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "p",
                [
                  _c(
                    "router-link",
                    { attrs: { to: { name: "Returns" }, tag: "a", exact: "" } },
                    [
                      _vm._v(
                        "\n                                Returns\n                            "
                      )
                    ]
                  )
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "p",
                [
                  _c(
                    "router-link",
                    {
                      attrs: { to: { name: "SizeGuide" }, tag: "a", exact: "" }
                    },
                    [
                      _vm._v(
                        "\n                                Size Guide\n                            "
                      )
                    ]
                  )
                ],
                1
              )
            ]),
            _vm._v(" "),
            _c("hr", { staticClass: "w-100 clearfix d-md-none" }),
            _vm._v(" "),
            _vm._m(1),
            _vm._v(" "),
            _c("hr", { staticClass: "w-100 clearfix d-md-none" }),
            _vm._v(" "),
            _vm._m(2)
          ]
        ),
        _vm._v(" "),
        _c("hr"),
        _vm._v(" "),
        _vm._m(3)
      ])
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "title" }, [
      _vm._v("THE NEX"),
      _c("span", [_vm._v("T")]),
      _vm._v("RENDS")
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "col-md-4 col-lg-3 col-xl-3 mt-3" }, [
      _c("h6", { staticClass: "text-uppercase mb-4 font-weight-bold" }, [
        _vm._v("Contact")
      ]),
      _vm._v(" "),
      _c("p", [
        _c("i", { staticClass: "fas fa-envelope mr-3" }),
        _vm._v(" info@thenextrends.com\n                        ")
      ]),
      _vm._v(" "),
      _c("p", [
        _c("i", { staticClass: "fas fa-phone mr-3" }),
        _vm._v(" +977 01 4701833\n                        ")
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "col-md-2 col-lg-2 col-xl-2 mt-3" }, [
      _c("h6", { staticClass: "text-uppercase mb-4 font-weight-bold" }, [
        _vm._v("Follow Us On")
      ]),
      _vm._v(" "),
      _c("p", [
        _c(
          "a",
          {
            attrs: {
              href: "https://www.facebook.com/Thenextrends-106513440722139/",
              target: "_blank"
            }
          },
          [
            _c("i", { staticClass: "fab fa-facebook-square mr-3" }),
            _vm._v(" Facebook\n                            ")
          ]
        )
      ]),
      _vm._v(" "),
      _c("p", [
        _c(
          "a",
          {
            attrs: {
              href:
                "https://www.instagram.com/p/B6aBkNlFIVI/?igshid=1gtqutmj6us4f",
              target: "_blank"
            }
          },
          [
            _c("i", { staticClass: "fab fa-instagram mr-3" }),
            _vm._v(" Instagram\n                            ")
          ]
        )
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "row d-flex align-items-center" }, [
      _c("div", { staticClass: "col-md-7 col-lg-8" }, [
        _c("p", { staticClass: "text-center text-md-left mr-5" }, [
          _vm._v(
            "© 2019 Copyright: All Rights Reserved By TheNextrends,\n                            Website Powered By\n                            "
          ),
          _c(
            "a",
            { attrs: { href: "https://yashrisoft.com/", target: "_blank" } },
            [
              _c("strong", { staticClass: "ml-3" }, [
                _vm._v("Yashri Soft Pvt.Ltd")
              ])
            ]
          )
        ])
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/frontend/layout/Header.vue?vue&type=template&id=82b87500&scoped=true&":
/*!*************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/frontend/layout/Header.vue?vue&type=template&id=82b87500&scoped=true& ***!
  \*************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _c(
      "div",
      { staticClass: "middle-header" },
      [
        _c(
          "div",
          { staticClass: "wish-search" },
          [
            _c(
              "router-link",
              { attrs: { to: { name: "Wishlist" } } },
              [
                _c("i", { staticClass: "far fa-heart white wish-btn" }),
                _vm._v(" "),
                this.$store.getters.getWish.length >= 1
                  ? [
                      _c("span", { staticClass: "quantity" }, [
                        _vm._v(_vm._s(this.$store.getters.getWish.length))
                      ])
                    ]
                  : _vm._e()
              ],
              2
            ),
            _vm._v(" "),
            _c(
              "div",
              {
                staticClass: "overlay",
                style: { display: _vm.display },
                attrs: { id: "myOverlay" }
              },
              [
                _c(
                  "span",
                  {
                    staticClass: "closebtn",
                    attrs: { title: "Close Overlay" },
                    on: {
                      click: function($event) {
                        $event.preventDefault()
                        return _vm.closeSearch($event)
                      }
                    }
                  },
                  [_vm._v("×")]
                ),
                _vm._v(" "),
                _c("div", { staticClass: "overlay-content" }, [
                  _c(
                    "form",
                    {
                      on: {
                        submit: function($event) {
                          $event.preventDefault()
                          return _vm.goSearch($event)
                        }
                      }
                    },
                    [
                      _c("input", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model",
                            value: _vm.search,
                            expression: "search"
                          }
                        ],
                        attrs: {
                          type: "text",
                          placeholder: "Search..",
                          name: "search"
                        },
                        domProps: { value: _vm.search },
                        on: {
                          input: function($event) {
                            if ($event.target.composing) {
                              return
                            }
                            _vm.search = $event.target.value
                          }
                        }
                      })
                    ]
                  )
                ])
              ]
            ),
            _vm._v(" "),
            _c(
              "button",
              {
                staticClass: "openBtn ml-2",
                on: {
                  click: function($event) {
                    $event.preventDefault()
                    return _vm.openSearch($event)
                  }
                }
              },
              [_c("i", { staticClass: "fa fa-search white" })]
            )
          ],
          1
        ),
        _vm._v(" "),
        _c("router-link", { attrs: { to: { name: "home" } } }, [
          _c("div", { staticClass: "shop-title" }, [
            _c("div", { staticClass: "title" }, [
              _vm._v("THE NEX"),
              _c("span", [_vm._v("T")]),
              _vm._v("RENDS")
            ])
          ])
        ]),
        _vm._v(" "),
        _c(
          "div",
          { staticClass: "profile-cart" },
          [
            _c(
              "div",
              { staticClass: "mobile-cart" },
              [
                _c(
                  "router-link",
                  { attrs: { to: { name: "cart" } } },
                  [
                    _c("i", { staticClass: "fa fa-shopping-cart wish-btn" }),
                    _vm._v(" "),
                    this.$store.getters.getCart.length >= 1
                      ? [
                          _c("span", { staticClass: "quantity" }, [
                            _vm._v(_vm._s(this.$store.getters.getCart.length))
                          ])
                        ]
                      : _vm._e()
                  ],
                  2
                )
              ],
              1
            ),
            _vm._v(" "),
            !_vm.currentUser
              ? [
                  _c(
                    "router-link",
                    {
                      staticClass: "nav-link",
                      attrs: {
                        to: { name: "login" },
                        tag: "a",
                        "active-class": "active",
                        exact: ""
                      }
                    },
                    [_c("i", { staticClass: "far fa-user" })]
                  )
                ]
              : [
                  _c("div", { staticClass: "nav-item dropdown" }, [
                    _vm._m(0),
                    _vm._v(" "),
                    _c(
                      "div",
                      {
                        staticClass: "dropdown-menu user-drop profile-box",
                        attrs: { "aria-labelledby": "navbarDropdown" }
                      },
                      [
                        _c(
                          "router-link",
                          {
                            staticClass: "dropdown-item",
                            attrs: {
                              to: { name: "userProfile" },
                              tag: "a",
                              "active-class": "active",
                              exact: ""
                            }
                          },
                          [
                            _c("i", { staticClass: "fas fa-user " }),
                            _vm._v(
                              "\n\n                            Profile\n                        "
                            )
                          ]
                        ),
                        _vm._v(" "),
                        _c(
                          "router-link",
                          {
                            staticClass: "dropdown-item",
                            attrs: {
                              to: { name: "userOrder" },
                              tag: "a",
                              "active-class": "active",
                              exact: ""
                            }
                          },
                          [
                            _c("i", { staticClass: "fas fa-dolly " }),
                            _vm._v(
                              "\n                            My Order\n                        "
                            )
                          ]
                        ),
                        _vm._v(" "),
                        _c("div", { staticClass: "dropdown-divider" }),
                        _vm._v(" "),
                        _c(
                          "a",
                          {
                            staticClass: "dropdown-item",
                            attrs: { href: "#" },
                            on: {
                              click: function($event) {
                                $event.preventDefault()
                                return _vm.logout($event)
                              }
                            }
                          },
                          [
                            _c("i", { staticClass: "fas fa-power-off red" }),
                            _vm._v(
                              "\n                            Logout\n                        "
                            )
                          ]
                        )
                      ],
                      1
                    )
                  ])
                ]
          ],
          2
        )
      ],
      1
    ),
    _vm._v(" "),
    _c("div", { staticClass: "menu-header" }, [
      _c(
        "nav",
        { staticClass: "navbar navbar-expand-md navbar-light fixed p-0" },
        [
          _c(
            "div",
            {
              staticClass:
                "collapse navbar-collapse justify-content-center mobile-navigation",
              staticStyle: { background: "#232323", display: "inline-block" },
              attrs: { id: "navbarNav" }
            },
            [
              _c("ul", { staticClass: "navbar-nav nav-ul" }, [
                _c("li", { staticClass: "nav-item dropdown cat-list" }, [
                  _c(
                    "a",
                    {
                      staticClass: "nav-link",
                      attrs: {
                        href: "#",
                        id: "clothing",
                        role: "button",
                        "data-toggle": "dropdown",
                        "aria-haspopup": "true",
                        "aria-expanded": "false"
                      },
                      on: {
                        click: function($event) {
                          return _vm.getCategoryData("Clothing")
                        }
                      }
                    },
                    [
                      _vm._v(
                        "\n                            CLOTHING\n                        "
                      )
                    ]
                  ),
                  _vm._v(" "),
                  _c(
                    "div",
                    {
                      staticClass:
                        "dropdown-menu clothing-drop navigation-menu-style",
                      attrs: { "aria-labelledby": "clothing" }
                    },
                    [
                      _c("div", { staticClass: "genre-list" }, [
                        _c(
                          "span",
                          {
                            staticClass: "shop-women shop-women-men",
                            class: { show_active: _vm.isWomen },
                            on: {
                              click: function($event) {
                                $event.stopPropagation()
                                _vm.showWomen()
                                _vm.getBrands("Clothing")
                                _vm.getBanner("Clothing")
                              }
                            }
                          },
                          [_vm._v("SHOP WOMEN")]
                        ),
                        _vm._v(" "),
                        _c(
                          "span",
                          {
                            staticClass: "shop-women-men",
                            class: { show_active: _vm.isMen },
                            on: {
                              click: function($event) {
                                $event.stopPropagation()
                                _vm.showMen()
                                _vm.getBrands("Clothing")
                                _vm.getBanner("Clothing")
                              }
                            }
                          },
                          [_vm._v("SHOP MEN")]
                        )
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "dropdown-divider" }),
                      _vm._v(" "),
                      _c("div", { staticClass: "category" }, [
                        _c("div", { staticClass: "category-list" }, [
                          _c("span", { staticClass: "clothing-brand-title" }, [
                            _vm._v("SHOP CLOTHING")
                          ]),
                          _vm._v(" "),
                          _c(
                            "div",
                            {
                              directives: [
                                {
                                  name: "show",
                                  rawName: "v-show",
                                  value: _vm.isMen,
                                  expression: "isMen"
                                }
                              ],
                              staticClass: "cat-list"
                            },
                            [
                              _vm._l(_vm.categories.men, function(sub) {
                                return [
                                  _c(
                                    "a",
                                    {
                                      staticClass: "category-a",
                                      on: {
                                        click: function($event) {
                                          return _vm.goFilter(
                                            "Men",
                                            sub.label,
                                            "Clothing"
                                          )
                                        }
                                      }
                                    },
                                    [_vm._v(_vm._s(sub.label))]
                                  )
                                ]
                              })
                            ],
                            2
                          ),
                          _vm._v(" "),
                          _c(
                            "div",
                            {
                              directives: [
                                {
                                  name: "show",
                                  rawName: "v-show",
                                  value: _vm.isWomen,
                                  expression: "isWomen"
                                }
                              ],
                              staticClass: "cat-list"
                            },
                            [
                              _vm._l(_vm.categories.women, function(sub) {
                                return [
                                  _c(
                                    "a",
                                    {
                                      staticClass: "category-a",
                                      on: {
                                        click: function($event) {
                                          return _vm.goFilter(
                                            "Women",
                                            sub.label,
                                            "Clothing"
                                          )
                                        }
                                      }
                                    },
                                    [_vm._v(_vm._s(sub.label))]
                                  )
                                ]
                              })
                            ],
                            2
                          )
                        ]),
                        _vm._v(" "),
                        _c("div", { staticClass: "top-brands" }, [
                          _c("span", { staticClass: "clothing-brand-title" }, [
                            _vm._v("TOP SELLING BRANDS")
                          ]),
                          _vm._v(" "),
                          _c(
                            "div",
                            { staticClass: "cat-list" },
                            [
                              _vm._l(_vm.brands, function(brand) {
                                return [
                                  _c(
                                    "a",
                                    {
                                      staticClass: "category-a",
                                      on: {
                                        click: function($event) {
                                          return _vm.goFilterBrand(
                                            brand.type,
                                            brand.name
                                          )
                                        }
                                      }
                                    },
                                    [_vm._v(_vm._s(brand.name))]
                                  )
                                ]
                              })
                            ],
                            2
                          )
                        ]),
                        _vm._v(" "),
                        _c("div", { staticClass: "photo-offer" }, [
                          _c(
                            "div",
                            { staticClass: "image" },
                            [
                              _vm._l(_vm.bannerImages, function(image) {
                                return [
                                  _c("img", {
                                    staticClass: "home_slider_image",
                                    attrs: { src: "/image/banner/" + image }
                                  })
                                ]
                              })
                            ],
                            2
                          )
                        ])
                      ])
                    ]
                  )
                ]),
                _vm._v(" "),
                _c("li", { staticClass: "nav-item dropdown cat-list" }, [
                  _c(
                    "a",
                    {
                      staticClass: "nav-link",
                      attrs: {
                        href: "#",
                        id: "shoes",
                        role: "button",
                        "data-toggle": "dropdown",
                        "aria-haspopup": "true",
                        "aria-expanded": "false"
                      },
                      on: {
                        click: function($event) {
                          return _vm.getCategoryData("Shoes")
                        }
                      }
                    },
                    [
                      _vm._v(
                        "\n                            SHOES\n                        "
                      )
                    ]
                  ),
                  _vm._v(" "),
                  _c(
                    "div",
                    {
                      staticClass:
                        "dropdown-menu shoes-drop navigation-menu-style",
                      attrs: { "aria-labelledby": "shoes" }
                    },
                    [
                      _c("div", { staticClass: "genre-list" }, [
                        _c(
                          "span",
                          {
                            staticClass: "shop-women shop-women-men",
                            class: { show_active: _vm.isWomen },
                            on: {
                              click: function($event) {
                                $event.stopPropagation()
                                _vm.showWomen()
                                _vm.getBrands("Shoes")
                                _vm.getBanner("Shoes")
                              }
                            }
                          },
                          [_vm._v("SHOP WOMEN")]
                        ),
                        _vm._v(" "),
                        _c(
                          "span",
                          {
                            staticClass: "shop-women-men",
                            class: { show_active: _vm.isMen },
                            on: {
                              click: function($event) {
                                $event.stopPropagation()
                                _vm.showMen()
                                _vm.getBrands("Shoes")
                                _vm.getBanner("Shoes")
                              }
                            }
                          },
                          [_vm._v("SHOP MEN")]
                        )
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "dropdown-divider" }),
                      _vm._v(" "),
                      _c("div", { staticClass: "category" }, [
                        _c("div", { staticClass: "category-list" }, [
                          _c("span", { staticClass: "clothing-brand-title" }, [
                            _vm._v("SHOP SHOES")
                          ]),
                          _vm._v(" "),
                          _c(
                            "div",
                            {
                              directives: [
                                {
                                  name: "show",
                                  rawName: "v-show",
                                  value: _vm.isMen,
                                  expression: "isMen"
                                }
                              ],
                              staticClass: "cat-list"
                            },
                            [
                              _vm._l(_vm.categories.men, function(sub) {
                                return [
                                  _c(
                                    "a",
                                    {
                                      staticClass: "category-a",
                                      on: {
                                        click: function($event) {
                                          return _vm.goFilter(
                                            "Men",
                                            sub.label,
                                            "Shoes"
                                          )
                                        }
                                      }
                                    },
                                    [_vm._v(_vm._s(sub.label))]
                                  )
                                ]
                              })
                            ],
                            2
                          ),
                          _vm._v(" "),
                          _c(
                            "div",
                            {
                              directives: [
                                {
                                  name: "show",
                                  rawName: "v-show",
                                  value: _vm.isWomen,
                                  expression: "isWomen"
                                }
                              ],
                              staticClass: "cat-list"
                            },
                            [
                              _vm._l(_vm.categories.women, function(sub) {
                                return [
                                  _c(
                                    "a",
                                    {
                                      staticClass: "category-a",
                                      on: {
                                        click: function($event) {
                                          return _vm.goFilter(
                                            "Women",
                                            sub.label,
                                            "Shoes"
                                          )
                                        }
                                      }
                                    },
                                    [_vm._v(_vm._s(sub.label))]
                                  )
                                ]
                              })
                            ],
                            2
                          )
                        ]),
                        _vm._v(" "),
                        _c(
                          "div",
                          { staticClass: "top-brands" },
                          [
                            _c(
                              "span",
                              { staticClass: "clothing-brand-title" },
                              [_vm._v("TOP SELLING BRANDS")]
                            ),
                            _vm._v(" "),
                            _vm._l(_vm.brands, function(brand) {
                              return [
                                _c(
                                  "a",
                                  {
                                    staticClass: "category-a",
                                    on: {
                                      click: function($event) {
                                        return _vm.goFilterBrand(
                                          brand.type,
                                          brand.name
                                        )
                                      }
                                    }
                                  },
                                  [_vm._v(_vm._s(brand.name))]
                                )
                              ]
                            })
                          ],
                          2
                        ),
                        _vm._v(" "),
                        _c("div", { staticClass: "photo-offer" }, [
                          _c(
                            "div",
                            { staticClass: "image" },
                            [
                              _vm._l(_vm.bannerImages, function(image) {
                                return [
                                  _c("img", {
                                    staticClass: "home_slider_image",
                                    attrs: { src: "/image/banner/" + image }
                                  })
                                ]
                              })
                            ],
                            2
                          )
                        ])
                      ])
                    ]
                  )
                ]),
                _vm._v(" "),
                _c("li", { staticClass: "nav-item dropdown cat-list" }, [
                  _c(
                    "a",
                    {
                      staticClass: "nav-link",
                      attrs: {
                        href: "#",
                        id: "accessories",
                        role: "button",
                        "data-toggle": "dropdown",
                        "aria-haspopup": "true",
                        "aria-expanded": "false"
                      },
                      on: {
                        click: function($event) {
                          return _vm.getCategoryData("Accessories")
                        }
                      }
                    },
                    [
                      _vm._v(
                        "\n                            ACCESSORIES\n                        "
                      )
                    ]
                  ),
                  _vm._v(" "),
                  _c(
                    "div",
                    {
                      staticClass:
                        "dropdown-menu accessories-drop navigation-menu-style",
                      attrs: { "aria-labelledby": "accessories" }
                    },
                    [
                      _c("div", { staticClass: "genre-list" }, [
                        _c(
                          "span",
                          {
                            staticClass: "shop-women shop-women-men",
                            class: { show_active: _vm.isWomen },
                            on: {
                              click: function($event) {
                                $event.stopPropagation()
                                _vm.showWomen()
                                _vm.getBrands("Accessories")
                                _vm.getBanner("Accessories")
                              }
                            }
                          },
                          [_vm._v("SHOP WOMEN")]
                        ),
                        _vm._v(" "),
                        _c(
                          "span",
                          {
                            staticClass: "shop-women-men",
                            class: { show_active: _vm.isMen },
                            on: {
                              click: function($event) {
                                $event.stopPropagation()
                                _vm.showMen()
                                _vm.getBrands("Accessories")
                                _vm.getBanner("Accessories")
                              }
                            }
                          },
                          [_vm._v("SHOP MEN")]
                        )
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "dropdown-divider" }),
                      _vm._v(" "),
                      _c("div", { staticClass: "category" }, [
                        _c("div", { staticClass: "category-list" }, [
                          _c("span", { staticClass: "clothing-brand-title" }, [
                            _vm._v("SHOP ACCESSORIES")
                          ]),
                          _vm._v(" "),
                          _c(
                            "div",
                            {
                              directives: [
                                {
                                  name: "show",
                                  rawName: "v-show",
                                  value: _vm.isMen,
                                  expression: "isMen"
                                }
                              ],
                              staticClass: "cat-list"
                            },
                            [
                              _vm._l(_vm.categories.men, function(sub) {
                                return [
                                  _c(
                                    "a",
                                    {
                                      staticClass: "category-a",
                                      on: {
                                        click: function($event) {
                                          return _vm.goFilter(
                                            "Men",
                                            sub.label,
                                            "Accessories"
                                          )
                                        }
                                      }
                                    },
                                    [_vm._v(_vm._s(sub.label))]
                                  )
                                ]
                              })
                            ],
                            2
                          ),
                          _vm._v(" "),
                          _c(
                            "div",
                            {
                              directives: [
                                {
                                  name: "show",
                                  rawName: "v-show",
                                  value: _vm.isWomen,
                                  expression: "isWomen"
                                }
                              ],
                              staticClass: "cat-list"
                            },
                            [
                              _vm._l(_vm.categories.women, function(sub) {
                                return [
                                  _c(
                                    "a",
                                    {
                                      staticClass: "category-a",
                                      on: {
                                        click: function($event) {
                                          return _vm.goFilter(
                                            "Women",
                                            sub.label,
                                            "Accessories"
                                          )
                                        }
                                      }
                                    },
                                    [_vm._v(_vm._s(sub.label))]
                                  )
                                ]
                              })
                            ],
                            2
                          )
                        ]),
                        _vm._v(" "),
                        _c(
                          "div",
                          { staticClass: "top-brands" },
                          [
                            _c(
                              "span",
                              { staticClass: "clothing-brand-title" },
                              [_vm._v("TOP SELLING BRANDS")]
                            ),
                            _vm._v(" "),
                            _vm._l(_vm.brands, function(brand) {
                              return [
                                _c(
                                  "a",
                                  {
                                    staticClass: "category-a",
                                    on: {
                                      click: function($event) {
                                        return _vm.goFilterBrand(
                                          brand.type,
                                          brand.name
                                        )
                                      }
                                    }
                                  },
                                  [_vm._v(_vm._s(brand.name))]
                                )
                              ]
                            })
                          ],
                          2
                        ),
                        _vm._v(" "),
                        _c("div", { staticClass: "photo-offer" }, [
                          _c(
                            "div",
                            { staticClass: "image" },
                            [
                              _vm._l(_vm.bannerImages, function(image) {
                                return [
                                  _c("img", {
                                    staticClass: "home_slider_image",
                                    attrs: { src: "/image/banner/" + image }
                                  })
                                ]
                              })
                            ],
                            2
                          )
                        ])
                      ])
                    ]
                  )
                ]),
                _vm._v(" "),
                _c("li", { staticClass: "nav-item dropdown cat-list" }, [
                  _c(
                    "a",
                    {
                      staticClass: "nav-link",
                      attrs: {
                        href: "#",
                        id: "sports",
                        role: "button",
                        "data-toggle": "dropdown",
                        "aria-haspopup": "true",
                        "aria-expanded": "false"
                      },
                      on: {
                        click: function($event) {
                          return _vm.getCategoryData("Sports")
                        }
                      }
                    },
                    [
                      _vm._v(
                        "\n                            SPORT\n                        "
                      )
                    ]
                  ),
                  _vm._v(" "),
                  _c(
                    "div",
                    {
                      staticClass:
                        "dropdown-menu sports-drop navigation-menu-style",
                      attrs: { "aria-labelledby": "sports" }
                    },
                    [
                      _c("div", { staticClass: "genre-list" }, [
                        _c(
                          "span",
                          {
                            staticClass: "shop-women shop-women-men",
                            class: { show_active: _vm.isWomen },
                            on: {
                              click: function($event) {
                                $event.stopPropagation()
                                _vm.showWomen()
                                _vm.getBrands("Sports")
                                _vm.getBanner("Sports")
                              }
                            }
                          },
                          [_vm._v("SHOP WOMEN")]
                        ),
                        _vm._v(" "),
                        _c(
                          "span",
                          {
                            staticClass: "shop-women-men",
                            class: { show_active: _vm.isMen },
                            on: {
                              click: function($event) {
                                $event.stopPropagation()
                                _vm.showMen()
                                _vm.getBrands("Sports")
                                _vm.getBanner("Sports")
                              }
                            }
                          },
                          [_vm._v("SHOP MEN")]
                        )
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "dropdown-divider" }),
                      _vm._v(" "),
                      _c("div", { staticClass: "category" }, [
                        _c("div", { staticClass: "category-list" }, [
                          _c("span", { staticClass: "clothing-brand-title" }, [
                            _vm._v("SHOP SPORT")
                          ]),
                          _vm._v(" "),
                          _c(
                            "div",
                            {
                              directives: [
                                {
                                  name: "show",
                                  rawName: "v-show",
                                  value: _vm.isMen,
                                  expression: "isMen"
                                }
                              ],
                              staticClass: "cat-list"
                            },
                            [
                              _vm._l(_vm.categories.men, function(sub) {
                                return [
                                  _c(
                                    "a",
                                    {
                                      staticClass: "category-a",
                                      on: {
                                        click: function($event) {
                                          return _vm.goFilter(
                                            "Men",
                                            sub.label,
                                            "Sports"
                                          )
                                        }
                                      }
                                    },
                                    [
                                      _vm._v(
                                        "\n                                                " +
                                          _vm._s(sub.label)
                                      )
                                    ]
                                  )
                                ]
                              })
                            ],
                            2
                          ),
                          _vm._v(" "),
                          _c(
                            "div",
                            {
                              directives: [
                                {
                                  name: "show",
                                  rawName: "v-show",
                                  value: _vm.isWomen,
                                  expression: "isWomen"
                                }
                              ],
                              staticClass: "cat-list"
                            },
                            [
                              _vm._l(_vm.categories.women, function(sub) {
                                return [
                                  _c(
                                    "a",
                                    {
                                      staticClass: "category-a",
                                      on: {
                                        click: function($event) {
                                          return _vm.goFilter(
                                            "Women",
                                            sub.label,
                                            "Sports"
                                          )
                                        }
                                      }
                                    },
                                    [
                                      _vm._v(
                                        "\n                                                " +
                                          _vm._s(sub.label)
                                      )
                                    ]
                                  )
                                ]
                              })
                            ],
                            2
                          )
                        ]),
                        _vm._v(" "),
                        _c(
                          "div",
                          { staticClass: "top-brands" },
                          [
                            _c(
                              "span",
                              { staticClass: "clothing-brand-title" },
                              [_vm._v("TOP SELLING BRANDS")]
                            ),
                            _vm._v(" "),
                            _vm._l(_vm.brands, function(brand) {
                              return [
                                _c(
                                  "a",
                                  {
                                    staticClass: "category-a",
                                    on: {
                                      click: function($event) {
                                        return _vm.goFilterBrand(
                                          brand.type,
                                          brand.name
                                        )
                                      }
                                    }
                                  },
                                  [_vm._v(_vm._s(brand.name))]
                                )
                              ]
                            })
                          ],
                          2
                        ),
                        _vm._v(" "),
                        _c("div", { staticClass: "photo-offer" }, [
                          _c(
                            "div",
                            { staticClass: "image" },
                            [
                              _vm._l(_vm.bannerImages, function(image) {
                                return [
                                  _c("img", {
                                    staticClass: "home_slider_image",
                                    attrs: { src: "/image/banner/" + image }
                                  })
                                ]
                              })
                            ],
                            2
                          )
                        ])
                      ])
                    ]
                  )
                ]),
                _vm._v(" "),
                _c("li", { staticClass: "nav-item dropdown cat-list" }, [
                  _c(
                    "a",
                    {
                      staticClass: "nav-link",
                      attrs: {
                        href: "#",
                        id: "brands",
                        role: "button",
                        "data-toggle": "dropdown",
                        "aria-haspopup": "true",
                        "aria-expanded": "false"
                      },
                      on: {
                        click: function($event) {
                          return _vm.getBrand()
                        }
                      }
                    },
                    [
                      _vm._v(
                        "\n                            BRANDS\n                        "
                      )
                    ]
                  ),
                  _vm._v(" "),
                  _c(
                    "div",
                    {
                      staticClass:
                        "dropdown-menu brands-drop navigation-menu-style",
                      attrs: { "aria-labelledby": "brands" }
                    },
                    [
                      _c("div", { staticClass: "genre-list" }, [
                        _c(
                          "span",
                          {
                            staticClass: "shop-women shop-women-men",
                            class: { show_active: _vm.isWomen },
                            on: {
                              click: function($event) {
                                $event.stopPropagation()
                                _vm.showWomen()
                                _vm.getBrand()
                              }
                            }
                          },
                          [_vm._v("SHOP WOMEN")]
                        ),
                        _vm._v(" "),
                        _c(
                          "span",
                          {
                            staticClass: "shop-women-men",
                            class: { show_active: _vm.isMen },
                            on: {
                              click: function($event) {
                                $event.stopPropagation()
                                _vm.showMen()
                                _vm.getBrand()
                              }
                            }
                          },
                          [_vm._v("SHOP MEN")]
                        )
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "dropdown-divider" }),
                      _vm._v(" "),
                      _c("div", { staticClass: "category" }, [
                        _c("div", { staticClass: "category-list brand-list" }, [
                          _c(
                            "span",
                            { staticClass: "clothing-brand-title mb-5" },
                            [_vm._v("BRANDS DIRECTORY")]
                          ),
                          _vm._v(" "),
                          _c(
                            "div",
                            {
                              directives: [
                                {
                                  name: "show",
                                  rawName: "v-show",
                                  value: _vm.isMen,
                                  expression: "isMen"
                                }
                              ]
                            },
                            [
                              _c(
                                "router-link",
                                {
                                  attrs: {
                                    to: {
                                      name: "brand-directory",
                                      query: {
                                        gender: "Men",
                                        category: "Clothing"
                                      }
                                    }
                                  }
                                },
                                [
                                  _vm._v(
                                    "\n                                            Clothing Brands\n                                        "
                                  )
                                ]
                              ),
                              _vm._v(" "),
                              _c(
                                "router-link",
                                {
                                  attrs: {
                                    to: {
                                      name: "brand-directory",
                                      query: {
                                        gender: "Men",
                                        category: "Shoes"
                                      }
                                    }
                                  }
                                },
                                [
                                  _vm._v(
                                    "\n                                            Shoes Brands\n                                        "
                                  )
                                ]
                              ),
                              _vm._v(" "),
                              _c(
                                "router-link",
                                {
                                  attrs: {
                                    to: {
                                      name: "brand-directory",
                                      query: {
                                        gender: "Men",
                                        category: "Accessories"
                                      }
                                    }
                                  }
                                },
                                [
                                  _vm._v(
                                    "\n                                            Accessories Brands\n                                        "
                                  )
                                ]
                              ),
                              _vm._v(" "),
                              _c(
                                "router-link",
                                {
                                  attrs: {
                                    to: {
                                      name: "brand-directory",
                                      query: {
                                        gender: "Men",
                                        category: "Sports"
                                      }
                                    }
                                  }
                                },
                                [
                                  _vm._v(
                                    "\n                                            Sports Brands\n                                        "
                                  )
                                ]
                              )
                            ],
                            1
                          ),
                          _vm._v(" "),
                          _c(
                            "div",
                            {
                              directives: [
                                {
                                  name: "show",
                                  rawName: "v-show",
                                  value: _vm.isWomen,
                                  expression: "isWomen"
                                }
                              ]
                            },
                            [
                              _c(
                                "router-link",
                                {
                                  attrs: {
                                    to: {
                                      name: "brand-directory",
                                      query: {
                                        gender: "Women",
                                        category: "Clothing"
                                      }
                                    }
                                  }
                                },
                                [
                                  _vm._v(
                                    "\n                                            Clothing Brands\n                                        "
                                  )
                                ]
                              ),
                              _vm._v(" "),
                              _c(
                                "router-link",
                                {
                                  attrs: {
                                    to: {
                                      name: "brand-directory",
                                      query: {
                                        gender: "Women",
                                        category: "Shoes"
                                      }
                                    }
                                  }
                                },
                                [
                                  _vm._v(
                                    "\n                                            Shoes Brands\n                                        "
                                  )
                                ]
                              ),
                              _vm._v(" "),
                              _c(
                                "router-link",
                                {
                                  attrs: {
                                    to: {
                                      name: "brand-directory",
                                      query: {
                                        gender: "Women",
                                        category: "Accessories"
                                      }
                                    }
                                  }
                                },
                                [
                                  _vm._v(
                                    "\n                                            Accessories Brands\n                                        "
                                  )
                                ]
                              ),
                              _vm._v(" "),
                              _c(
                                "router-link",
                                {
                                  attrs: {
                                    to: {
                                      name: "brand-directory",
                                      query: {
                                        gender: "Women",
                                        category: "Sports"
                                      }
                                    }
                                  }
                                },
                                [
                                  _vm._v(
                                    "\n                                            Sports Brands\n                                        "
                                  )
                                ]
                              )
                            ],
                            1
                          )
                        ]),
                        _vm._v(" "),
                        _c("div", { staticClass: "photo-offer" }, [
                          _c(
                            "div",
                            { staticClass: "photos" },
                            [
                              _vm._l(_vm.brandImage, function(image) {
                                return [
                                  _c(
                                    "div",
                                    {
                                      staticClass: "image",
                                      on: {
                                        click: function($event) {
                                          return _vm.goFilterBrand(
                                            "All",
                                            image.name
                                          )
                                        }
                                      }
                                    },
                                    [
                                      _c("img", {
                                        attrs: {
                                          src:
                                            "/image/featuredBrand/" +
                                            image.image
                                        }
                                      })
                                    ]
                                  )
                                ]
                              })
                            ],
                            2
                          )
                        ])
                      ])
                    ]
                  )
                ]),
                _vm._v(" "),
                _c("li", { staticClass: "nav-item dropdown cat-list" }, [
                  _c(
                    "a",
                    {
                      staticClass: "nav-link",
                      attrs: {
                        href: "#",
                        id: "sales",
                        role: "button",
                        "data-toggle": "dropdown",
                        "aria-haspopup": "true",
                        "aria-expanded": "false"
                      },
                      on: {
                        click: function($event) {
                          return _vm.getCategoryData("Sports")
                        }
                      }
                    },
                    [
                      _c("span", { staticClass: "text-danger" }, [
                        _vm._v("SALE")
                      ])
                    ]
                  ),
                  _vm._v(" "),
                  _c(
                    "div",
                    {
                      staticClass:
                        "dropdown-menu sales-drop navigation-menu-style",
                      attrs: { "aria-labelledby": "sales" }
                    },
                    [
                      _c("div", { staticClass: "genre-list" }, [
                        _c(
                          "span",
                          {
                            staticClass: "shop-women shop-women-men",
                            class: { show_active: _vm.isWomen },
                            on: {
                              click: function($event) {
                                $event.stopPropagation()
                                return _vm.showWomen($event)
                              }
                            }
                          },
                          [_vm._v("SHOP WOMEN")]
                        ),
                        _vm._v(" "),
                        _c(
                          "span",
                          {
                            staticClass: "shop-women-men",
                            class: { show_active: _vm.isMen },
                            on: {
                              click: function($event) {
                                $event.stopPropagation()
                                return _vm.showMen($event)
                              }
                            }
                          },
                          [_vm._v("SHOP MEN")]
                        )
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "dropdown-divider" }),
                      _vm._v(" "),
                      _c("div", { staticClass: "category" }, [
                        _c("div", { staticClass: "category-list" }, [
                          _c("h5", { staticClass: "clothing-brand-title" }, [
                            _vm._v("SALE OFFER")
                          ]),
                          _vm._v(" "),
                          _c(
                            "div",
                            {
                              directives: [
                                {
                                  name: "show",
                                  rawName: "v-show",
                                  value: _vm.isMen,
                                  expression: "isMen"
                                }
                              ],
                              staticClass: "cat-list"
                            },
                            [
                              _c(
                                "a",
                                {
                                  staticClass: "category-a",
                                  on: {
                                    click: function($event) {
                                      return _vm.goFilterSales(
                                        "Men",
                                        "Clothing",
                                        "yes"
                                      )
                                    }
                                  }
                                },
                                [
                                  _vm._v(
                                    "Sales\n                                            Clothing"
                                  )
                                ]
                              ),
                              _vm._v(" "),
                              _c(
                                "a",
                                {
                                  staticClass: "category-a",
                                  on: {
                                    click: function($event) {
                                      return _vm.goFilterSales("Men", "", "20")
                                    }
                                  }
                                },
                                [_vm._v("Under 20%")]
                              ),
                              _vm._v(" "),
                              _c(
                                "a",
                                {
                                  staticClass: "category-a",
                                  on: {
                                    click: function($event) {
                                      return _vm.goFilterSales(
                                        "Men",
                                        "Shoes",
                                        "yes"
                                      )
                                    }
                                  }
                                },
                                [
                                  _vm._v(
                                    "Sales\n                                            Shoes"
                                  )
                                ]
                              ),
                              _vm._v(" "),
                              _c(
                                "a",
                                {
                                  staticClass: "category-a",
                                  on: {
                                    click: function($event) {
                                      return _vm.goFilterSales("Men", "", "30")
                                    }
                                  }
                                },
                                [_vm._v("Under 30%")]
                              ),
                              _vm._v(" "),
                              _c(
                                "a",
                                {
                                  staticClass: "category-a",
                                  on: {
                                    click: function($event) {
                                      return _vm.goFilterSales(
                                        "Men",
                                        "Accessories",
                                        "yes"
                                      )
                                    }
                                  }
                                },
                                [
                                  _vm._v(
                                    "Sales\n                                            Accessories"
                                  )
                                ]
                              ),
                              _vm._v(" "),
                              _c(
                                "a",
                                {
                                  staticClass: "category-a",
                                  on: {
                                    click: function($event) {
                                      return _vm.goFilterSales("Men", "", "40")
                                    }
                                  }
                                },
                                [_vm._v("Upto 40%")]
                              ),
                              _vm._v(" "),
                              _c(
                                "a",
                                {
                                  staticClass: "category-a",
                                  on: {
                                    click: function($event) {
                                      return _vm.goFilterSales(
                                        "Men",
                                        "Sports",
                                        "yes"
                                      )
                                    }
                                  }
                                },
                                [
                                  _vm._v(
                                    "Sales\n                                            Sports"
                                  )
                                ]
                              )
                            ]
                          ),
                          _vm._v(" "),
                          _c(
                            "div",
                            {
                              directives: [
                                {
                                  name: "show",
                                  rawName: "v-show",
                                  value: _vm.isWomen,
                                  expression: "isWomen"
                                }
                              ],
                              staticClass: "cat-list"
                            },
                            [
                              _c(
                                "a",
                                {
                                  staticClass: "category-a",
                                  on: {
                                    click: function($event) {
                                      return _vm.goFilterSales(
                                        "Women",
                                        "Clothing",
                                        "yes"
                                      )
                                    }
                                  }
                                },
                                [
                                  _vm._v(
                                    "Sales\n                                            Clothing"
                                  )
                                ]
                              ),
                              _vm._v(" "),
                              _c(
                                "a",
                                {
                                  staticClass: "category-a",
                                  on: {
                                    click: function($event) {
                                      return _vm.goFilterSales(
                                        "Women",
                                        "",
                                        "20"
                                      )
                                    }
                                  }
                                },
                                [
                                  _vm._v(
                                    "Under\n                                            20%"
                                  )
                                ]
                              ),
                              _vm._v(" "),
                              _c(
                                "a",
                                {
                                  staticClass: "category-a",
                                  on: {
                                    click: function($event) {
                                      return _vm.goFilterSales(
                                        "Women",
                                        "Shoes",
                                        "yes"
                                      )
                                    }
                                  }
                                },
                                [
                                  _vm._v(
                                    "Sales\n                                            Shoes"
                                  )
                                ]
                              ),
                              _vm._v(" "),
                              _c(
                                "a",
                                {
                                  staticClass: "category-a",
                                  on: {
                                    click: function($event) {
                                      return _vm.goFilterSales(
                                        "Women",
                                        "",
                                        "30"
                                      )
                                    }
                                  }
                                },
                                [
                                  _vm._v(
                                    "Under\n                                            30%"
                                  )
                                ]
                              ),
                              _vm._v(" "),
                              _c(
                                "a",
                                {
                                  staticClass: "category-a",
                                  on: {
                                    click: function($event) {
                                      return _vm.goFilterSales(
                                        "Women",
                                        "Accessories",
                                        "yes"
                                      )
                                    }
                                  }
                                },
                                [
                                  _vm._v(
                                    "Sales\n                                            Accessories"
                                  )
                                ]
                              ),
                              _vm._v(" "),
                              _c(
                                "a",
                                {
                                  staticClass: "category-a",
                                  on: {
                                    click: function($event) {
                                      return _vm.goFilterSales(
                                        "Women",
                                        "",
                                        "40"
                                      )
                                    }
                                  }
                                },
                                [_vm._v("Upto 40%")]
                              ),
                              _vm._v(" "),
                              _c(
                                "a",
                                {
                                  staticClass: "category-a",
                                  on: {
                                    click: function($event) {
                                      return _vm.goFilterSales(
                                        "Women",
                                        "Sports",
                                        "yes"
                                      )
                                    }
                                  }
                                },
                                [
                                  _vm._v(
                                    "Sales\n                                            Sports"
                                  )
                                ]
                              )
                            ]
                          )
                        ]),
                        _vm._v(" "),
                        _c("div", { staticClass: "photo-offer" }, [
                          _c(
                            "div",
                            { staticClass: "image" },
                            [
                              _vm._l(_vm.bannerImages, function(image) {
                                return [
                                  _c("img", {
                                    staticClass: "home_slider_image",
                                    attrs: { src: "/image/banner/" + image }
                                  })
                                ]
                              })
                            ],
                            2
                          )
                        ])
                      ])
                    ]
                  )
                ])
              ])
            ]
          )
        ]
      )
    ]),
    _vm._v(" "),
    _c("div", { staticClass: "home-shipping" }, [
      _c(
        "span",
        { staticStyle: { "margin-right": "2rem" } },
        [
          _c(
            "router-link",
            {
              staticClass: "sub-header",
              attrs: { to: { name: "Delivery" }, tag: "a", exact: "" }
            },
            [_vm._v("\n                Delivery\n            ")]
          )
        ],
        1
      ),
      _vm._v(" "),
      _c(
        "span",
        [
          _c(
            "router-link",
            {
              staticClass: "sub-header",
              attrs: { to: { name: "Returns" }, tag: "a", exact: "" }
            },
            [_vm._v("\n                Returns\n            ")]
          )
        ],
        1
      )
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "span",
      {
        staticClass: "cart-logged nav-link dropdown-toggle",
        staticStyle: { background: "#232323" },
        attrs: {
          id: "navbarDropdown",
          role: "button",
          "data-toggle": "dropdown",
          "aria-haspopup": "true",
          "aria-expanded": "false"
        }
      },
      [_c("i", { staticClass: "fas fa-user " })]
    )
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/frontend/layout/Layout.vue?vue&type=template&id=5d22479d&":
/*!*************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/frontend/layout/Layout.vue?vue&type=template&id=5d22479d& ***!
  \*************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [_c("Header"), _vm._v(" "), _c("router-view"), _vm._v(" "), _c("Footer")],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js":
/*!********************************************************************!*\
  !*** ./node_modules/vue-loader/lib/runtime/componentNormalizer.js ***!
  \********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return normalizeComponent; });
/* globals __VUE_SSR_CONTEXT__ */

// IMPORTANT: Do NOT use ES2015 features in this file (except for modules).
// This module is a runtime utility for cleaner component module output and will
// be included in the final webpack user bundle.

function normalizeComponent (
  scriptExports,
  render,
  staticRenderFns,
  functionalTemplate,
  injectStyles,
  scopeId,
  moduleIdentifier, /* server only */
  shadowMode /* vue-cli only */
) {
  // Vue.extend constructor export interop
  var options = typeof scriptExports === 'function'
    ? scriptExports.options
    : scriptExports

  // render functions
  if (render) {
    options.render = render
    options.staticRenderFns = staticRenderFns
    options._compiled = true
  }

  // functional template
  if (functionalTemplate) {
    options.functional = true
  }

  // scopedId
  if (scopeId) {
    options._scopeId = 'data-v-' + scopeId
  }

  var hook
  if (moduleIdentifier) { // server build
    hook = function (context) {
      // 2.3 injection
      context =
        context || // cached call
        (this.$vnode && this.$vnode.ssrContext) || // stateful
        (this.parent && this.parent.$vnode && this.parent.$vnode.ssrContext) // functional
      // 2.2 with runInNewContext: true
      if (!context && typeof __VUE_SSR_CONTEXT__ !== 'undefined') {
        context = __VUE_SSR_CONTEXT__
      }
      // inject component styles
      if (injectStyles) {
        injectStyles.call(this, context)
      }
      // register component module identifier for async chunk inferrence
      if (context && context._registeredComponents) {
        context._registeredComponents.add(moduleIdentifier)
      }
    }
    // used by ssr in case component is cached and beforeCreate
    // never gets called
    options._ssrRegister = hook
  } else if (injectStyles) {
    hook = shadowMode
      ? function () { injectStyles.call(this, this.$root.$options.shadowRoot) }
      : injectStyles
  }

  if (hook) {
    if (options.functional) {
      // for template-only hot-reload because in that case the render fn doesn't
      // go through the normalizer
      options._injectStyles = hook
      // register for functioal component in vue file
      var originalRender = options.render
      options.render = function renderWithStyleInjection (h, context) {
        hook.call(context)
        return originalRender(h, context)
      }
    } else {
      // inject component registration as beforeCreate hook
      var existing = options.beforeCreate
      options.beforeCreate = existing
        ? [].concat(existing, hook)
        : [hook]
    }
  }

  return {
    exports: scriptExports,
    options: options
  }
}


/***/ }),

/***/ "./resources/js/components/frontend/helpers/header_cart.vue":
/*!******************************************************************!*\
  !*** ./resources/js/components/frontend/helpers/header_cart.vue ***!
  \******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");
var render, staticRenderFns
var script = {}


/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_0__["default"])(
  script,
  render,
  staticRenderFns,
  false,
  null,
  null,
  null
  
)

component.options.__file = "resources/js/components/frontend/helpers/header_cart.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/frontend/layout/Footer.vue":
/*!************************************************************!*\
  !*** ./resources/js/components/frontend/layout/Footer.vue ***!
  \************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Footer_vue_vue_type_template_id_c51d3ce4_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Footer.vue?vue&type=template&id=c51d3ce4&scoped=true& */ "./resources/js/components/frontend/layout/Footer.vue?vue&type=template&id=c51d3ce4&scoped=true&");
/* harmony import */ var _Footer_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Footer.vue?vue&type=script&lang=js& */ "./resources/js/components/frontend/layout/Footer.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _Footer_vue_vue_type_style_index_0_id_c51d3ce4_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./Footer.vue?vue&type=style&index=0&id=c51d3ce4&lang=scss&scoped=true& */ "./resources/js/components/frontend/layout/Footer.vue?vue&type=style&index=0&id=c51d3ce4&lang=scss&scoped=true&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _Footer_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Footer_vue_vue_type_template_id_c51d3ce4_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Footer_vue_vue_type_template_id_c51d3ce4_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "c51d3ce4",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/frontend/layout/Footer.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/frontend/layout/Footer.vue?vue&type=script&lang=js&":
/*!*************************************************************************************!*\
  !*** ./resources/js/components/frontend/layout/Footer.vue?vue&type=script&lang=js& ***!
  \*************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Footer_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./Footer.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/frontend/layout/Footer.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Footer_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/frontend/layout/Footer.vue?vue&type=style&index=0&id=c51d3ce4&lang=scss&scoped=true&":
/*!**********************************************************************************************************************!*\
  !*** ./resources/js/components/frontend/layout/Footer.vue?vue&type=style&index=0&id=c51d3ce4&lang=scss&scoped=true& ***!
  \**********************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_Footer_vue_vue_type_style_index_0_id_c51d3ce4_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/style-loader!../../../../../node_modules/css-loader!../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../node_modules/postcss-loader/src??ref--7-2!../../../../../node_modules/sass-loader/dist/cjs.js??ref--7-3!../../../../../node_modules/vue-loader/lib??vue-loader-options!./Footer.vue?vue&type=style&index=0&id=c51d3ce4&lang=scss&scoped=true& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/frontend/layout/Footer.vue?vue&type=style&index=0&id=c51d3ce4&lang=scss&scoped=true&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_Footer_vue_vue_type_style_index_0_id_c51d3ce4_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_Footer_vue_vue_type_style_index_0_id_c51d3ce4_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_Footer_vue_vue_type_style_index_0_id_c51d3ce4_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_Footer_vue_vue_type_style_index_0_id_c51d3ce4_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_Footer_vue_vue_type_style_index_0_id_c51d3ce4_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./resources/js/components/frontend/layout/Footer.vue?vue&type=template&id=c51d3ce4&scoped=true&":
/*!*******************************************************************************************************!*\
  !*** ./resources/js/components/frontend/layout/Footer.vue?vue&type=template&id=c51d3ce4&scoped=true& ***!
  \*******************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Footer_vue_vue_type_template_id_c51d3ce4_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./Footer.vue?vue&type=template&id=c51d3ce4&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/frontend/layout/Footer.vue?vue&type=template&id=c51d3ce4&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Footer_vue_vue_type_template_id_c51d3ce4_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Footer_vue_vue_type_template_id_c51d3ce4_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/frontend/layout/Header.vue":
/*!************************************************************!*\
  !*** ./resources/js/components/frontend/layout/Header.vue ***!
  \************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Header_vue_vue_type_template_id_82b87500_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Header.vue?vue&type=template&id=82b87500&scoped=true& */ "./resources/js/components/frontend/layout/Header.vue?vue&type=template&id=82b87500&scoped=true&");
/* harmony import */ var _Header_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Header.vue?vue&type=script&lang=js& */ "./resources/js/components/frontend/layout/Header.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _Header_vue_vue_type_style_index_0_id_82b87500_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./Header.vue?vue&type=style&index=0&id=82b87500&lang=scss&scoped=true& */ "./resources/js/components/frontend/layout/Header.vue?vue&type=style&index=0&id=82b87500&lang=scss&scoped=true&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _Header_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Header_vue_vue_type_template_id_82b87500_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Header_vue_vue_type_template_id_82b87500_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "82b87500",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/frontend/layout/Header.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/frontend/layout/Header.vue?vue&type=script&lang=js&":
/*!*************************************************************************************!*\
  !*** ./resources/js/components/frontend/layout/Header.vue?vue&type=script&lang=js& ***!
  \*************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Header_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./Header.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/frontend/layout/Header.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Header_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/frontend/layout/Header.vue?vue&type=style&index=0&id=82b87500&lang=scss&scoped=true&":
/*!**********************************************************************************************************************!*\
  !*** ./resources/js/components/frontend/layout/Header.vue?vue&type=style&index=0&id=82b87500&lang=scss&scoped=true& ***!
  \**********************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_Header_vue_vue_type_style_index_0_id_82b87500_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/style-loader!../../../../../node_modules/css-loader!../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../node_modules/postcss-loader/src??ref--7-2!../../../../../node_modules/sass-loader/dist/cjs.js??ref--7-3!../../../../../node_modules/vue-loader/lib??vue-loader-options!./Header.vue?vue&type=style&index=0&id=82b87500&lang=scss&scoped=true& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/frontend/layout/Header.vue?vue&type=style&index=0&id=82b87500&lang=scss&scoped=true&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_Header_vue_vue_type_style_index_0_id_82b87500_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_Header_vue_vue_type_style_index_0_id_82b87500_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_Header_vue_vue_type_style_index_0_id_82b87500_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_Header_vue_vue_type_style_index_0_id_82b87500_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_Header_vue_vue_type_style_index_0_id_82b87500_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./resources/js/components/frontend/layout/Header.vue?vue&type=template&id=82b87500&scoped=true&":
/*!*******************************************************************************************************!*\
  !*** ./resources/js/components/frontend/layout/Header.vue?vue&type=template&id=82b87500&scoped=true& ***!
  \*******************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Header_vue_vue_type_template_id_82b87500_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./Header.vue?vue&type=template&id=82b87500&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/frontend/layout/Header.vue?vue&type=template&id=82b87500&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Header_vue_vue_type_template_id_82b87500_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Header_vue_vue_type_template_id_82b87500_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/frontend/layout/Layout.vue":
/*!************************************************************!*\
  !*** ./resources/js/components/frontend/layout/Layout.vue ***!
  \************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Layout_vue_vue_type_template_id_5d22479d___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Layout.vue?vue&type=template&id=5d22479d& */ "./resources/js/components/frontend/layout/Layout.vue?vue&type=template&id=5d22479d&");
/* harmony import */ var _Layout_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Layout.vue?vue&type=script&lang=js& */ "./resources/js/components/frontend/layout/Layout.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _Layout_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./Layout.vue?vue&type=style&index=0&lang=scss& */ "./resources/js/components/frontend/layout/Layout.vue?vue&type=style&index=0&lang=scss&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _Layout_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Layout_vue_vue_type_template_id_5d22479d___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Layout_vue_vue_type_template_id_5d22479d___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/frontend/layout/Layout.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/frontend/layout/Layout.vue?vue&type=script&lang=js&":
/*!*************************************************************************************!*\
  !*** ./resources/js/components/frontend/layout/Layout.vue?vue&type=script&lang=js& ***!
  \*************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Layout_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./Layout.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/frontend/layout/Layout.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Layout_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/frontend/layout/Layout.vue?vue&type=style&index=0&lang=scss&":
/*!**********************************************************************************************!*\
  !*** ./resources/js/components/frontend/layout/Layout.vue?vue&type=style&index=0&lang=scss& ***!
  \**********************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_Layout_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/style-loader!../../../../../node_modules/css-loader!../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../node_modules/postcss-loader/src??ref--7-2!../../../../../node_modules/sass-loader/dist/cjs.js??ref--7-3!../../../../../node_modules/vue-loader/lib??vue-loader-options!./Layout.vue?vue&type=style&index=0&lang=scss& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/frontend/layout/Layout.vue?vue&type=style&index=0&lang=scss&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_Layout_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_Layout_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_Layout_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_Layout_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_Layout_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./resources/js/components/frontend/layout/Layout.vue?vue&type=template&id=5d22479d&":
/*!*******************************************************************************************!*\
  !*** ./resources/js/components/frontend/layout/Layout.vue?vue&type=template&id=5d22479d& ***!
  \*******************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Layout_vue_vue_type_template_id_5d22479d___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./Layout.vue?vue&type=template&id=5d22479d& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/frontend/layout/Layout.vue?vue&type=template&id=5d22479d&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Layout_vue_vue_type_template_id_5d22479d___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Layout_vue_vue_type_template_id_5d22479d___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);