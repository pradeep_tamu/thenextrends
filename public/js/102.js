(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[102],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/frontend/user/UserOrderDetails.vue?vue&type=script&lang=js&":
/*!*****************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/frontend/user/UserOrderDetails.vue?vue&type=script&lang=js& ***!
  \*****************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  name: "user-order-details",
  data: function data() {
    return {
      order: {},
      user: {},
      orderDetails: {},
      shipping: 50,
      total: 0
    };
  },
  computed: {
    grandTotal: function grandTotal() {
      console.log(this.orderDetails[0].products);

      for (var i = 0; i < this.orderDetails.length; i++) {
        this.total += this.orderDetails[i].products.price * this.orderDetails[i].quantity;
      }

      return this.total;
    }
  },
  beforeMount: function beforeMount() {
    this.getData(this.$route.params.id);
  },
  methods: {
    getImage: function getImage(image) {
      return "/image/thumbnail/" + image;
    },
    getData: function getData(id) {
      var _this = this;

      this.loading = true;
      axios.get('/api/getOrderDetail/' + id).then(function (response) {
        _this.order = response.data.order;
        _this.user = response.data.user;
        _this.orderDetails = response.data.orderDetails;
      })["catch"](function (error) {});
    }
  }
});

/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/frontend/user/UserOrderDetails.vue?vue&type=style&index=0&id=1b637e31&scoped=true&lang=scss&":
/*!****************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/sass-loader/dist/cjs.js??ref--7-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/frontend/user/UserOrderDetails.vue?vue&type=style&index=0&id=1b637e31&scoped=true&lang=scss& ***!
  \****************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "@media screen and (min-width: 1100px) {\n.single-product-details[data-v-1b637e31] {\n    height: 100%;\n    border-right: 1px solid grey;\n    padding-right: 5vw;\n}\n.single-product-details .thumbnail[data-v-1b637e31] {\n    height: 25vh;\n}\n.single-product-details .thumbnail img[data-v-1b637e31] {\n    height: 100%;\n}\n}\n@media screen and (max-width: 769px) {\n.single-product-details[data-v-1b637e31] {\n    flex-direction: column;\n}\n.single-product-details .thumbnail[data-v-1b637e31] {\n    height: 45vh;\n}\n.single-product-details .thumbnail img[data-v-1b637e31] {\n    height: 100%;\n}\n}\n@media screen and (max-width: 1025px) {\n.thumbnail[data-v-1b637e31] {\n    height: 30vh;\n}\n.thumbnail img[data-v-1b637e31] {\n    height: 100%;\n}\n}\n.single-product h5[data-v-1b637e31] {\n  margin-right: 1vw;\n  font-weight: bold;\n}\n.product-price[data-v-1b637e31] {\n  margin-right: 5vw;\n}\n.title[data-v-1b637e31] {\n  font-weight: bolder;\n}\n.order-padding[data-v-1b637e31] {\n  padding: 2rem;\n}\n.single-product-store[data-v-1b637e31] {\n  padding-left: 3vw;\n}\n.total-amount[data-v-1b637e31] {\n  padding-right: 4rem;\n}\n.total-amount h5[data-v-1b637e31] {\n  color: black;\n  font-weight: bold;\n}\n.total-amount h5 span[data-v-1b637e31] {\n  color: grey;\n}\n.total-amount hr[data-v-1b637e31] {\n  display: flex;\n  width: 30%;\n  background-color: #0c0c0c;\n  margin: 1rem 0;\n}", ""]);

// exports


/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/frontend/user/UserOrderDetails.vue?vue&type=style&index=0&id=1b637e31&scoped=true&lang=scss&":
/*!********************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/sass-loader/dist/cjs.js??ref--7-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/frontend/user/UserOrderDetails.vue?vue&type=style&index=0&id=1b637e31&scoped=true&lang=scss& ***!
  \********************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../../node_modules/css-loader!../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../node_modules/postcss-loader/src??ref--7-2!../../../../../node_modules/sass-loader/dist/cjs.js??ref--7-3!../../../../../node_modules/vue-loader/lib??vue-loader-options!./UserOrderDetails.vue?vue&type=style&index=0&id=1b637e31&scoped=true&lang=scss& */ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/frontend/user/UserOrderDetails.vue?vue&type=style&index=0&id=1b637e31&scoped=true&lang=scss&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/frontend/user/UserOrderDetails.vue?vue&type=template&id=1b637e31&scoped=true&":
/*!*********************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/frontend/user/UserOrderDetails.vue?vue&type=template&id=1b637e31&scoped=true& ***!
  \*********************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "order" }, [
    _c("div", { staticClass: "order-information order-padding" }, [
      _c("h1", [_vm._v("Order Information")]),
      _vm._v(" "),
      _c("hr"),
      _vm._v(" "),
      _c("div", { staticClass: "status" }, [
        _c("span", [_vm._v("Order Status")]),
        _vm._v(":\n            "),
        _vm.order.status === 2
          ? _c("span", { staticClass: "bg-success px-2 rounded-pill" }, [
              _vm._v("Received")
            ])
          : _vm.order.status === 1
          ? _c("span", { staticClass: "bg-primary px-2 rounded-pill" }, [
              _vm._v("Processing")
            ])
          : _c("span", { staticClass: "bg-warning px-2 rounded-pill" }, [
              _vm._v("Pending")
            ])
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "date" }, [
        _c("span", [_vm._v("Order Date")]),
        _vm._v(": " + _vm._s(_vm.order.created_at) + "\n        ")
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "ship-name" }, [
        _c("span", [_vm._v("Billed To")]),
        _vm._v(": " + _vm._s(_vm.order.name) + "\n        ")
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "ship-address" }, [
        _c("span", [_vm._v("Ship To")]),
        _vm._v(": " + _vm._s(_vm.order.address) + "\n        ")
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "ship-contact" }, [
        _c("span", [_vm._v("Contact Number")]),
        _vm._v(": " + _vm._s(_vm.order.mobile) + "\n        ")
      ])
    ]),
    _vm._v(" "),
    _c(
      "div",
      { staticClass: "Product Details order-padding" },
      [
        _c("h1", [_vm._v("Products Ordered")]),
        _vm._v(" "),
        _c("hr"),
        _vm._v(" "),
        _vm._l(_vm.orderDetails, function(product) {
          return [
            _c("div", { staticClass: "single-product row mb-2 d-flex" }, [
              _c(
                "div",
                {
                  staticClass: "single-product-details d-flex col-12 col-xl-6"
                },
                [
                  _c("div", { staticClass: "thumbnail mr-4" }, [
                    _c("img", { attrs: { src: _vm.getImage(product.image) } })
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: " product-details" }, [
                    _c(
                      "div",
                      {
                        staticClass:
                          "d-flex flex-column justify-content-between"
                      },
                      [
                        _c(
                          "div",
                          {
                            staticClass:
                              "form-row my-2 justify-content-between flex-wrap ml-0 pl-0"
                          },
                          [
                            _c("div", { staticClass: "product-name pl-0" }, [
                              _c("h5", [_vm._v("Product Name:")]),
                              _vm._v(" "),
                              _c("h6", { staticClass: "text-dark" }, [
                                _vm._v(_vm._s(product.products.name))
                              ])
                            ])
                          ]
                        ),
                        _vm._v(" "),
                        _c("div", { staticClass: " mt-2 mb-1" }, [
                          _c("div", { staticClass: "size-details text-dark" }, [
                            _c("span", { staticClass: "title" }, [
                              _vm._v("Size:")
                            ]),
                            _vm._v(" "),
                            _c("span", [_vm._v(_vm._s(product.size))])
                          ])
                        ]),
                        _vm._v(" "),
                        _c("div", { staticClass: " my-1" }, [
                          _c(
                            "div",
                            { staticClass: "color-details text-dark" },
                            [
                              _c("span", { staticClass: "title" }, [
                                _vm._v("Color:")
                              ]),
                              _vm._v(" "),
                              _c("span", [_vm._v(_vm._s(product.color))])
                            ]
                          )
                        ]),
                        _vm._v(" "),
                        _c("div", { staticClass: " my-1" }, [
                          _c(
                            "div",
                            { staticClass: "color-details text-dark" },
                            [
                              _c("span", { staticClass: "title" }, [
                                _vm._v("Quantity:")
                              ]),
                              _vm._v(" "),
                              _c("span", [_vm._v(_vm._s(product.quantity))])
                            ]
                          )
                        ]),
                        _vm._v(" "),
                        _c("div", { staticClass: " my-1 d-flex" }, [
                          _c(
                            "div",
                            { staticClass: "product-price text-dark" },
                            [
                              _c("span", { staticClass: "title" }, [
                                _vm._v("Price:")
                              ]),
                              _vm._v(" "),
                              _c("span", [
                                _vm._v("Rs. " + _vm._s(product.products.price))
                              ])
                            ]
                          ),
                          _vm._v(" "),
                          _c(
                            "div",
                            { staticClass: "color-details text-dark " },
                            [
                              _c("span", { staticClass: "title" }, [
                                _vm._v("Sub-Total:")
                              ]),
                              _vm._v(" "),
                              _c("span", [
                                _vm._v(
                                  "Rs. " +
                                    _vm._s(
                                      product.products.price * product.quantity
                                    ) +
                                    "  "
                                )
                              ])
                            ]
                          )
                        ])
                      ]
                    )
                  ])
                ]
              ),
              _vm._v(" "),
              _c(
                "div",
                { staticClass: "single-product-store col-12 col-xl-6" },
                [
                  _c(
                    "div",
                    {
                      staticClass: "d-flex flex-column justify-content-between"
                    },
                    [
                      _c("h4", [_vm._v("Store Detail")]),
                      _vm._v(" "),
                      _c("div", { staticClass: " mt-2 mb-1" }, [
                        _c(
                          "div",
                          { staticClass: "size-details text-dark d-flex" },
                          [
                            _c("h5", [_vm._v("Owner Name:")]),
                            _vm._v(" "),
                            _c("span", [
                              _vm._v(_vm._s(product.stores[0].owner_name))
                            ])
                          ]
                        )
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: " my-1" }, [
                        _c(
                          "div",
                          { staticClass: "size-details text-dark d-flex" },
                          [
                            _c("h5", [_vm._v("Store Name:")]),
                            _vm._v(" "),
                            _c("span", [
                              _vm._v(_vm._s(product.stores[0].store_name))
                            ])
                          ]
                        )
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: " my-1" }, [
                        _c(
                          "div",
                          { staticClass: "color-details text-dark d-flex" },
                          [
                            _c("h5", [_vm._v("Email:")]),
                            _vm._v(" "),
                            _c("span", [
                              _vm._v(_vm._s(product.stores[0].email))
                            ])
                          ]
                        )
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: " my-1" }, [
                        _c(
                          "div",
                          { staticClass: "color-details text-dark d-flex" },
                          [
                            _c("h5", [_vm._v("Address:")]),
                            _vm._v(" "),
                            _c("span", [
                              _vm._v(_vm._s(product.stores[0].address))
                            ])
                          ]
                        )
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: " my-1" }, [
                        _c(
                          "div",
                          { staticClass: "color-details text-dark d-flex" },
                          [
                            _c("h5", [_vm._v("Phone Number:")]),
                            _vm._v(" "),
                            _c("span", [
                              _vm._v(_vm._s(product.stores[0].phone))
                            ])
                          ]
                        )
                      ])
                    ]
                  )
                ]
              )
            ]),
            _vm._v(" "),
            _c("hr")
          ]
        }),
        _vm._v(" "),
        _c("div", { staticClass: "total-amount d-flex flex-column mt-5" }, [
          _c("h5", [
            _vm._v("Total Amount: "),
            _c("span", [_vm._v(_vm._s(_vm.grandTotal))])
          ]),
          _vm._v(" "),
          _c("h5", [
            _vm._v("Shipping Charge: "),
            _c("span", [_vm._v(_vm._s(_vm.shipping))])
          ]),
          _vm._v(" "),
          _c("h5", [
            _vm._v("Voucher Amount: "),
            _c("span", [_vm._v(_vm._s(_vm.order.voucher_amount))])
          ]),
          _vm._v(" "),
          _c("hr"),
          _vm._v(" "),
          _c("h5", [
            _vm._v("Grand Total: "),
            _c("span", [
              _vm._v(
                _vm._s(_vm.grandTotal + _vm.shipping - _vm.order.voucher_amount)
              )
            ])
          ])
        ])
      ],
      2
    )
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/components/frontend/user/UserOrderDetails.vue":
/*!********************************************************************!*\
  !*** ./resources/js/components/frontend/user/UserOrderDetails.vue ***!
  \********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _UserOrderDetails_vue_vue_type_template_id_1b637e31_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./UserOrderDetails.vue?vue&type=template&id=1b637e31&scoped=true& */ "./resources/js/components/frontend/user/UserOrderDetails.vue?vue&type=template&id=1b637e31&scoped=true&");
/* harmony import */ var _UserOrderDetails_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./UserOrderDetails.vue?vue&type=script&lang=js& */ "./resources/js/components/frontend/user/UserOrderDetails.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _UserOrderDetails_vue_vue_type_style_index_0_id_1b637e31_scoped_true_lang_scss___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./UserOrderDetails.vue?vue&type=style&index=0&id=1b637e31&scoped=true&lang=scss& */ "./resources/js/components/frontend/user/UserOrderDetails.vue?vue&type=style&index=0&id=1b637e31&scoped=true&lang=scss&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _UserOrderDetails_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _UserOrderDetails_vue_vue_type_template_id_1b637e31_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _UserOrderDetails_vue_vue_type_template_id_1b637e31_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "1b637e31",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/frontend/user/UserOrderDetails.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/frontend/user/UserOrderDetails.vue?vue&type=script&lang=js&":
/*!*********************************************************************************************!*\
  !*** ./resources/js/components/frontend/user/UserOrderDetails.vue?vue&type=script&lang=js& ***!
  \*********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_UserOrderDetails_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./UserOrderDetails.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/frontend/user/UserOrderDetails.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_UserOrderDetails_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/frontend/user/UserOrderDetails.vue?vue&type=style&index=0&id=1b637e31&scoped=true&lang=scss&":
/*!******************************************************************************************************************************!*\
  !*** ./resources/js/components/frontend/user/UserOrderDetails.vue?vue&type=style&index=0&id=1b637e31&scoped=true&lang=scss& ***!
  \******************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_UserOrderDetails_vue_vue_type_style_index_0_id_1b637e31_scoped_true_lang_scss___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/style-loader!../../../../../node_modules/css-loader!../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../node_modules/postcss-loader/src??ref--7-2!../../../../../node_modules/sass-loader/dist/cjs.js??ref--7-3!../../../../../node_modules/vue-loader/lib??vue-loader-options!./UserOrderDetails.vue?vue&type=style&index=0&id=1b637e31&scoped=true&lang=scss& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/frontend/user/UserOrderDetails.vue?vue&type=style&index=0&id=1b637e31&scoped=true&lang=scss&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_UserOrderDetails_vue_vue_type_style_index_0_id_1b637e31_scoped_true_lang_scss___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_UserOrderDetails_vue_vue_type_style_index_0_id_1b637e31_scoped_true_lang_scss___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_UserOrderDetails_vue_vue_type_style_index_0_id_1b637e31_scoped_true_lang_scss___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_UserOrderDetails_vue_vue_type_style_index_0_id_1b637e31_scoped_true_lang_scss___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_UserOrderDetails_vue_vue_type_style_index_0_id_1b637e31_scoped_true_lang_scss___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./resources/js/components/frontend/user/UserOrderDetails.vue?vue&type=template&id=1b637e31&scoped=true&":
/*!***************************************************************************************************************!*\
  !*** ./resources/js/components/frontend/user/UserOrderDetails.vue?vue&type=template&id=1b637e31&scoped=true& ***!
  \***************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_UserOrderDetails_vue_vue_type_template_id_1b637e31_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./UserOrderDetails.vue?vue&type=template&id=1b637e31&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/frontend/user/UserOrderDetails.vue?vue&type=template&id=1b637e31&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_UserOrderDetails_vue_vue_type_template_id_1b637e31_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_UserOrderDetails_vue_vue_type_template_id_1b637e31_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);