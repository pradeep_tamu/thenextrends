(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[84],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backend/reports/SalesReport.vue?vue&type=script&lang=js&":
/*!**************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/backend/reports/SalesReport.vue?vue&type=script&lang=js& ***!
  \**************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'Report',
  components: {
    'Excel': function Excel() {
      return __webpack_require__.e(/*! import() */ 0).then(__webpack_require__.bind(null, /*! vue-json-excel */ "./node_modules/vue-json-excel/JsonExcel.vue"));
    }
  },
  data: function data() {
    return {
      isLoading: true,
      isStore: false,
      isSize: false,
      isColor: false,
      isGender: false,
      isCategory: false,
      isSubCategory: false,
      isRegion: false,
      isTrending: false,
      start: '',
      end: '',
      trending: '',
      title: '',
      filename: 'sales-report',
      type: 'xls',
      data: [],
      fields: {},
      subCategories: [],
      filterData: {
        stores: [],
        colors: [],
        sizes: [],
        subCategories: [],
        region: []
      },
      filter: {
        store: [],
        color: [],
        size: [],
        category: [],
        subCategory: [],
        gender: [],
        region: []
      },
      search: [{
        prop: ['product_id', 'product'],
        value: ''
      }]
    };
  },
  created: function created() {
    var date = new Date();
    this.start = this.formatDate(new Date(date.getFullYear(), 0, 1));
    this.end = this.formatDate(date);
    this.getFilterData();
    this.getData();
  },
  methods: {
    checkFields: function checkFields() {
      this.isStore = false;
      this.isSize = false;
      this.isColor = false;
      this.isGender = false;
      this.isCategory = false;
      this.isSubCategory = false;
      this.isRegion = false;
      this.filename = 'sales-report';
      this.fields = {
        'ID': 'product_id',
        'Product Name': 'product',
        'Purchase Price': 'cp',
        'Sales Qty': 'quantity',
        'Selling Price': 'sp',
        'Gross Sales': 'gross_sales',
        'Discount': 'discount',
        'Net Sales': 'net_sales',
        'Gross Profit': 'gross_profit',
        'GP %': 'profit'
      };

      if (this.trending !== '') {
        this.isTrending = true;
        this.filename = this.trending + '-trending-product-report';
      }

      if (this.filter.region.length !== 0) {
        this.isRegion = true;
        this.fields.Region = 'region';
        this.filename = this.filename + '-' + this.filter.region;
      }

      if (this.filter.store.length !== 0) {
        this.isStore = true;
        this.fields.Store = 'store';
        this.filename = this.filename + '-' + this.filter.store;
      }

      if (this.filter.size.length !== 0) {
        this.isSize = true;
        this.fields.Size = 'size';
        this.filename = this.filename + '-' + this.filter.size;
      }

      if (this.filter.color.length !== 0) {
        this.isColor = true;
        this.fields.Color = 'color';
        this.filename = this.filename + '-' + this.filter.color;
      }

      if (this.filter.gender.length !== 0) {
        this.isGender = true;
        this.fields.Gender = 'gender';
        this.filename = this.filename + '-' + this.filter.gender;
      }

      if (this.filter.category.length !== 0) {
        this.isCategory = true;
        this.fields.Category = 'category';
        this.filename = this.filename + '-' + this.filter.category;
      }

      if (this.filter.subCategory.length !== 0) {
        this.isSubCategory = true;
        this.fields['Sub Category'] = 'sub';
        this.filename = this.filename + '-' + this.filter.subCategory;
      }
    },
    clearFilter: function clearFilter() {
      this.filter.gender = [];
      this.filter.category = [];
      this.filter.color = [];
      this.filter.size = [];
      this.filter.store = [];
      this.filter.region = [];
      this.getData();
    },
    formatDate: function formatDate(date) {
      var d = new Date(date),
          month = '' + (d.getMonth() + 1),
          day = '' + d.getDate(),
          year = d.getFullYear();
      if (month.length < 2) month = '0' + month;
      if (day.length < 2) day = '0' + day;
      return [year, month, day].join('-');
    },
    getData: function getData() {
      var _this = this;

      this.isLoading = true;
      var filter = JSON.stringify(this.filter);
      axios.get('/api/detailReport/' + filter + '/' + this.start + '/' + this.end + '/' + this.trending).then(function (response) {
        _this.data = response.data.items;
        _this.title = 'From ' + response.data.start + ' to ' + response.data.end;
        _this.isLoading = false;
      });
      this.checkFields();
    },
    getFilterData: function getFilterData() {
      var _this2 = this;

      axios.get('/api/getReportFilter').then(function (response) {
        _this2.filterData.stores = response.data.stores;
        _this2.filterData.sizes = response.data.sizes;
        _this2.filterData.colors = response.data.colors;
        _this2.filterData.region = response.data.region;
        _this2.filterData.subCategories = [];
        _this2.subCategories = response.data.subCategories;
      });
    },
    toggle: function toggle(event) {
      var parent = event.target.parentElement;

      if (parent.className === 'filter-title') {
        parent = parent.parentElement;
      }

      var val = parent.childNodes[2].style.display;

      if (val === "block") {
        parent.childNodes[2].style.display = "none";
      } else {
        parent.childNodes[2].style.display = "block";
      }
    }
  }
});

/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backend/reports/SalesReport.vue?vue&type=style&index=0&id=75c4d77a&lang=scss&scoped=true&":
/*!*************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/sass-loader/dist/cjs.js??ref--7-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/backend/reports/SalesReport.vue?vue&type=style&index=0&id=75c4d77a&lang=scss&scoped=true& ***!
  \*************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "input[data-v-75c4d77a], select[data-v-75c4d77a], .btn[data-v-75c4d77a], .input-group-text[data-v-75c4d77a] {\n  border-radius: 0;\n  outline: none;\n  border: 1px solid #343a40;\n  text-transform: uppercase;\n  font-weight: 700;\n}\n.filter-content .filter-title[data-v-75c4d77a] {\n  display: flex;\n  justify-content: space-between;\n  align-items: center;\n  border: 1px solid #232323;\n  padding: 0.5rem 1rem;\n  text-transform: uppercase;\n  font-weight: 700;\n}\n.filter-content .filter-items[data-v-75c4d77a] {\n  display: none;\n  border: 1px solid #232323;\n  border-top: none;\n  /* Hide the browser's default checkbox */\n  /* Create a custom checkbox */\n  /* On mouse-over, add a grey background color */\n  /* When the checkbox is checked, add a black background */\n  /* Create the check-mark/indicator (hidden when not checked) */\n  /* Show the check-mark when checked */\n  /* Style the check-mark/indicator */\n}\n.filter-content .filter-items .container[data-v-75c4d77a] {\n  display: block;\n  position: relative;\n  padding-left: 20px;\n  cursor: pointer;\n  font-weight: 500 !important;\n  -webkit-user-select: none;\n  -moz-user-select: none;\n  -ms-user-select: none;\n  user-select: none;\n}\n.filter-content .filter-items .container input[data-v-75c4d77a] {\n  position: absolute;\n  opacity: 0;\n  cursor: pointer;\n  height: 0;\n  width: 0;\n}\n.filter-content .filter-items .check-mark[data-v-75c4d77a] {\n  position: absolute;\n  top: 4px;\n  left: 0;\n  height: 15px;\n  width: 15px;\n  background-color: #fff;\n  border: solid 1px black;\n}\n.filter-content .filter-items .container:hover input ~ .check-mark[data-v-75c4d77a] {\n  background-color: #ccc;\n}\n.filter-content .filter-items .container input:checked ~ .check-mark[data-v-75c4d77a] {\n  background-color: #232323;\n}\n.filter-content .filter-items .check-mark[data-v-75c4d77a]:after {\n  content: \"\";\n  position: absolute;\n  display: none;\n}\n.filter-content .filter-items .container input:checked ~ .check-mark[data-v-75c4d77a]:after {\n  display: block;\n}\n.filter-content .filter-items .container .check-mark[data-v-75c4d77a]:after {\n  left: 5px;\n  top: 1px;\n  width: 5px;\n  height: 10px;\n  border: solid white;\n  border-width: 0 3px 3px 0;\n  transform: rotate(45deg);\n}", ""]);

// exports


/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backend/reports/SalesReport.vue?vue&type=style&index=0&id=75c4d77a&lang=scss&scoped=true&":
/*!*****************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/sass-loader/dist/cjs.js??ref--7-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/backend/reports/SalesReport.vue?vue&type=style&index=0&id=75c4d77a&lang=scss&scoped=true& ***!
  \*****************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../../node_modules/css-loader!../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../node_modules/postcss-loader/src??ref--7-2!../../../../../node_modules/sass-loader/dist/cjs.js??ref--7-3!../../../../../node_modules/vue-loader/lib??vue-loader-options!./SalesReport.vue?vue&type=style&index=0&id=75c4d77a&lang=scss&scoped=true& */ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backend/reports/SalesReport.vue?vue&type=style&index=0&id=75c4d77a&lang=scss&scoped=true&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backend/reports/SalesReport.vue?vue&type=template&id=75c4d77a&scoped=true&":
/*!******************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/backend/reports/SalesReport.vue?vue&type=template&id=75c4d77a&scoped=true& ***!
  \******************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "pt-1" }, [
    _c("h2", { staticClass: "text-center" }, [_vm._v("Sales Reports")]),
    _vm._v(" "),
    _c("hr"),
    _vm._v(" "),
    _c("div", { staticClass: "row mt-3 justify-content-center" }, [
      _c("div", { staticClass: "form-group col-6 text-center" }, [
        _c("div", { staticClass: "input-group" }, [
          _vm._m(0),
          _vm._v(" "),
          _vm._m(1),
          _vm._v(" "),
          _c("input", {
            directives: [
              {
                name: "model",
                rawName: "v-model",
                value: _vm.start,
                expression: "start"
              }
            ],
            staticClass: "form-control",
            attrs: { id: "start", type: "date", name: "start" },
            domProps: { value: _vm.start },
            on: {
              input: function($event) {
                if ($event.target.composing) {
                  return
                }
                _vm.start = $event.target.value
              }
            }
          })
        ])
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "form-group col-6 text-center" }, [
        _c("div", { staticClass: "input-group" }, [
          _vm._m(2),
          _vm._v(" "),
          _vm._m(3),
          _vm._v(" "),
          _c("input", {
            directives: [
              {
                name: "model",
                rawName: "v-model",
                value: _vm.end,
                expression: "end"
              }
            ],
            staticClass: "form-control",
            attrs: { id: "end", type: "date", name: "end" },
            domProps: { value: _vm.end },
            on: {
              input: function($event) {
                if ($event.target.composing) {
                  return
                }
                _vm.end = $event.target.value
              }
            }
          })
        ])
      ])
    ]),
    _vm._v(" "),
    _c("hr"),
    _vm._v(" "),
    _c("div", { staticClass: "row justify-content-center" }, [
      _c("div", { staticClass: "col-4" }, [
        _c(
          "select",
          {
            directives: [
              {
                name: "model",
                rawName: "v-model",
                value: _vm.trending,
                expression: "trending"
              }
            ],
            staticClass: "custom-select",
            attrs: { name: "trending", id: "trending" },
            on: {
              change: function($event) {
                var $$selectedVal = Array.prototype.filter
                  .call($event.target.options, function(o) {
                    return o.selected
                  })
                  .map(function(o) {
                    var val = "_value" in o ? o._value : o.value
                    return val
                  })
                _vm.trending = $event.target.multiple
                  ? $$selectedVal
                  : $$selectedVal[0]
              }
            }
          },
          [
            _c("option", { attrs: { value: "" } }, [
              _vm._v("Choose Trending Option")
            ]),
            _vm._v(" "),
            _c("option", { attrs: { value: "Top" } }, [_vm._v("Top 10")]),
            _vm._v(" "),
            _c("option", { attrs: { value: "Worst" } }, [_vm._v("Worst 10")])
          ]
        )
      ])
    ]),
    _vm._v(" "),
    _c("hr"),
    _vm._v(" "),
    _c("div", { staticClass: "row" }, [
      _c("div", { staticClass: "col-2 filter-content" }, [
        _c("div", { staticClass: "filter-title", on: { click: _vm.toggle } }, [
          _vm._v("Gender\n                "),
          _c("i", { staticClass: "fas fa-chevron-down slide-down-hover" })
        ]),
        _vm._v(" "),
        _c("p", { staticClass: "p-2 filter-items" }, [
          _c("label", { staticClass: "container" }, [
            _c("span", [_vm._v("All Gender")]),
            _vm._v(" "),
            _c("input", {
              directives: [
                {
                  name: "model",
                  rawName: "v-model",
                  value: _vm.filter.gender,
                  expression: "filter.gender"
                }
              ],
              attrs: { type: "checkbox", value: "all", checked: "checked" },
              domProps: {
                checked: Array.isArray(_vm.filter.gender)
                  ? _vm._i(_vm.filter.gender, "all") > -1
                  : _vm.filter.gender
              },
              on: {
                change: function($event) {
                  var $$a = _vm.filter.gender,
                    $$el = $event.target,
                    $$c = $$el.checked ? true : false
                  if (Array.isArray($$a)) {
                    var $$v = "all",
                      $$i = _vm._i($$a, $$v)
                    if ($$el.checked) {
                      $$i < 0 &&
                        _vm.$set(_vm.filter, "gender", $$a.concat([$$v]))
                    } else {
                      $$i > -1 &&
                        _vm.$set(
                          _vm.filter,
                          "gender",
                          $$a.slice(0, $$i).concat($$a.slice($$i + 1))
                        )
                    }
                  } else {
                    _vm.$set(_vm.filter, "gender", $$c)
                  }
                }
              }
            }),
            _vm._v(" "),
            _c("span", { staticClass: "check-mark" })
          ]),
          _vm._v(" "),
          _c("label", { staticClass: "container" }, [
            _c("span", [_vm._v("Women")]),
            _vm._v(" "),
            _c("input", {
              directives: [
                {
                  name: "model",
                  rawName: "v-model",
                  value: _vm.filter.gender,
                  expression: "filter.gender"
                }
              ],
              attrs: { type: "checkbox", value: "Women", checked: "checked" },
              domProps: {
                checked: Array.isArray(_vm.filter.gender)
                  ? _vm._i(_vm.filter.gender, "Women") > -1
                  : _vm.filter.gender
              },
              on: {
                change: function($event) {
                  var $$a = _vm.filter.gender,
                    $$el = $event.target,
                    $$c = $$el.checked ? true : false
                  if (Array.isArray($$a)) {
                    var $$v = "Women",
                      $$i = _vm._i($$a, $$v)
                    if ($$el.checked) {
                      $$i < 0 &&
                        _vm.$set(_vm.filter, "gender", $$a.concat([$$v]))
                    } else {
                      $$i > -1 &&
                        _vm.$set(
                          _vm.filter,
                          "gender",
                          $$a.slice(0, $$i).concat($$a.slice($$i + 1))
                        )
                    }
                  } else {
                    _vm.$set(_vm.filter, "gender", $$c)
                  }
                }
              }
            }),
            _vm._v(" "),
            _c("span", { staticClass: "check-mark" })
          ]),
          _vm._v(" "),
          _c("label", { staticClass: "container" }, [
            _c("span", [_vm._v("Men")]),
            _vm._v(" "),
            _c("input", {
              directives: [
                {
                  name: "model",
                  rawName: "v-model",
                  value: _vm.filter.gender,
                  expression: "filter.gender"
                }
              ],
              attrs: { type: "checkbox", value: "Men", checked: "checked" },
              domProps: {
                checked: Array.isArray(_vm.filter.gender)
                  ? _vm._i(_vm.filter.gender, "Men") > -1
                  : _vm.filter.gender
              },
              on: {
                change: function($event) {
                  var $$a = _vm.filter.gender,
                    $$el = $event.target,
                    $$c = $$el.checked ? true : false
                  if (Array.isArray($$a)) {
                    var $$v = "Men",
                      $$i = _vm._i($$a, $$v)
                    if ($$el.checked) {
                      $$i < 0 &&
                        _vm.$set(_vm.filter, "gender", $$a.concat([$$v]))
                    } else {
                      $$i > -1 &&
                        _vm.$set(
                          _vm.filter,
                          "gender",
                          $$a.slice(0, $$i).concat($$a.slice($$i + 1))
                        )
                    }
                  } else {
                    _vm.$set(_vm.filter, "gender", $$c)
                  }
                }
              }
            }),
            _vm._v(" "),
            _c("span", { staticClass: "check-mark" })
          ])
        ])
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "col-2 filter-content" }, [
        _c("div", { staticClass: "filter-title", on: { click: _vm.toggle } }, [
          _vm._v("Category\n                "),
          _c("i", { staticClass: "fas fa-chevron-down slide-down-hover" })
        ]),
        _vm._v(" "),
        _c("p", { staticClass: "p-2 filter-items" }, [
          _c("label", { staticClass: "container" }, [
            _c("span", [_vm._v("All Category")]),
            _vm._v(" "),
            _c("input", {
              directives: [
                {
                  name: "model",
                  rawName: "v-model",
                  value: _vm.filter.category,
                  expression: "filter.category"
                }
              ],
              attrs: { type: "checkbox", value: "all", checked: "checked" },
              domProps: {
                checked: Array.isArray(_vm.filter.category)
                  ? _vm._i(_vm.filter.category, "all") > -1
                  : _vm.filter.category
              },
              on: {
                change: function($event) {
                  var $$a = _vm.filter.category,
                    $$el = $event.target,
                    $$c = $$el.checked ? true : false
                  if (Array.isArray($$a)) {
                    var $$v = "all",
                      $$i = _vm._i($$a, $$v)
                    if ($$el.checked) {
                      $$i < 0 &&
                        _vm.$set(_vm.filter, "category", $$a.concat([$$v]))
                    } else {
                      $$i > -1 &&
                        _vm.$set(
                          _vm.filter,
                          "category",
                          $$a.slice(0, $$i).concat($$a.slice($$i + 1))
                        )
                    }
                  } else {
                    _vm.$set(_vm.filter, "category", $$c)
                  }
                }
              }
            }),
            _vm._v(" "),
            _c("span", { staticClass: "check-mark" })
          ]),
          _vm._v(" "),
          _c("label", { staticClass: "container" }, [
            _c("span", [_vm._v("Clothing")]),
            _vm._v(" "),
            _c("input", {
              directives: [
                {
                  name: "model",
                  rawName: "v-model",
                  value: _vm.filter.category,
                  expression: "filter.category"
                }
              ],
              attrs: {
                type: "checkbox",
                value: "Clothing",
                checked: "checked"
              },
              domProps: {
                checked: Array.isArray(_vm.filter.category)
                  ? _vm._i(_vm.filter.category, "Clothing") > -1
                  : _vm.filter.category
              },
              on: {
                change: function($event) {
                  var $$a = _vm.filter.category,
                    $$el = $event.target,
                    $$c = $$el.checked ? true : false
                  if (Array.isArray($$a)) {
                    var $$v = "Clothing",
                      $$i = _vm._i($$a, $$v)
                    if ($$el.checked) {
                      $$i < 0 &&
                        _vm.$set(_vm.filter, "category", $$a.concat([$$v]))
                    } else {
                      $$i > -1 &&
                        _vm.$set(
                          _vm.filter,
                          "category",
                          $$a.slice(0, $$i).concat($$a.slice($$i + 1))
                        )
                    }
                  } else {
                    _vm.$set(_vm.filter, "category", $$c)
                  }
                }
              }
            }),
            _vm._v(" "),
            _c("span", { staticClass: "check-mark" })
          ]),
          _vm._v(" "),
          _c("label", { staticClass: "container" }, [
            _c("span", [_vm._v("Shoes")]),
            _vm._v(" "),
            _c("input", {
              directives: [
                {
                  name: "model",
                  rawName: "v-model",
                  value: _vm.filter.category,
                  expression: "filter.category"
                }
              ],
              attrs: { type: "checkbox", value: "Shoes", checked: "checked" },
              domProps: {
                checked: Array.isArray(_vm.filter.category)
                  ? _vm._i(_vm.filter.category, "Shoes") > -1
                  : _vm.filter.category
              },
              on: {
                change: function($event) {
                  var $$a = _vm.filter.category,
                    $$el = $event.target,
                    $$c = $$el.checked ? true : false
                  if (Array.isArray($$a)) {
                    var $$v = "Shoes",
                      $$i = _vm._i($$a, $$v)
                    if ($$el.checked) {
                      $$i < 0 &&
                        _vm.$set(_vm.filter, "category", $$a.concat([$$v]))
                    } else {
                      $$i > -1 &&
                        _vm.$set(
                          _vm.filter,
                          "category",
                          $$a.slice(0, $$i).concat($$a.slice($$i + 1))
                        )
                    }
                  } else {
                    _vm.$set(_vm.filter, "category", $$c)
                  }
                }
              }
            }),
            _vm._v(" "),
            _c("span", { staticClass: "check-mark" })
          ]),
          _vm._v(" "),
          _c("label", { staticClass: "container" }, [
            _c("span", [_vm._v("Accessories")]),
            _vm._v(" "),
            _c("input", {
              directives: [
                {
                  name: "model",
                  rawName: "v-model",
                  value: _vm.filter.category,
                  expression: "filter.category"
                }
              ],
              attrs: {
                type: "checkbox",
                value: "Accessories",
                checked: "checked"
              },
              domProps: {
                checked: Array.isArray(_vm.filter.category)
                  ? _vm._i(_vm.filter.category, "Accessories") > -1
                  : _vm.filter.category
              },
              on: {
                change: function($event) {
                  var $$a = _vm.filter.category,
                    $$el = $event.target,
                    $$c = $$el.checked ? true : false
                  if (Array.isArray($$a)) {
                    var $$v = "Accessories",
                      $$i = _vm._i($$a, $$v)
                    if ($$el.checked) {
                      $$i < 0 &&
                        _vm.$set(_vm.filter, "category", $$a.concat([$$v]))
                    } else {
                      $$i > -1 &&
                        _vm.$set(
                          _vm.filter,
                          "category",
                          $$a.slice(0, $$i).concat($$a.slice($$i + 1))
                        )
                    }
                  } else {
                    _vm.$set(_vm.filter, "category", $$c)
                  }
                }
              }
            }),
            _vm._v(" "),
            _c("span", { staticClass: "check-mark" })
          ]),
          _vm._v(" "),
          _c("label", { staticClass: "container" }, [
            _c("span", [_vm._v("Sports")]),
            _vm._v(" "),
            _c("input", {
              directives: [
                {
                  name: "model",
                  rawName: "v-model",
                  value: _vm.filter.category,
                  expression: "filter.category"
                }
              ],
              attrs: { type: "checkbox", value: "Sports", checked: "checked" },
              domProps: {
                checked: Array.isArray(_vm.filter.category)
                  ? _vm._i(_vm.filter.category, "Sports") > -1
                  : _vm.filter.category
              },
              on: {
                change: function($event) {
                  var $$a = _vm.filter.category,
                    $$el = $event.target,
                    $$c = $$el.checked ? true : false
                  if (Array.isArray($$a)) {
                    var $$v = "Sports",
                      $$i = _vm._i($$a, $$v)
                    if ($$el.checked) {
                      $$i < 0 &&
                        _vm.$set(_vm.filter, "category", $$a.concat([$$v]))
                    } else {
                      $$i > -1 &&
                        _vm.$set(
                          _vm.filter,
                          "category",
                          $$a.slice(0, $$i).concat($$a.slice($$i + 1))
                        )
                    }
                  } else {
                    _vm.$set(_vm.filter, "category", $$c)
                  }
                }
              }
            }),
            _vm._v(" "),
            _c("span", { staticClass: "check-mark" })
          ])
        ])
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "col-2 filter-content" }, [
        _c("div", { staticClass: "filter-title", on: { click: _vm.toggle } }, [
          _vm._v("Store\n                "),
          _c("i", { staticClass: "fas fa-chevron-down slide-down-hover" })
        ]),
        _vm._v(" "),
        _c(
          "p",
          { staticClass: "p-2 filter-items" },
          [
            _c("label", { staticClass: "container" }, [
              _c("span", [_vm._v("All Stores")]),
              _vm._v(" "),
              _c("input", {
                directives: [
                  {
                    name: "model",
                    rawName: "v-model",
                    value: _vm.filter.store,
                    expression: "filter.store"
                  }
                ],
                attrs: { type: "checkbox", value: "all", checked: "checked" },
                domProps: {
                  checked: Array.isArray(_vm.filter.store)
                    ? _vm._i(_vm.filter.store, "all") > -1
                    : _vm.filter.store
                },
                on: {
                  change: function($event) {
                    var $$a = _vm.filter.store,
                      $$el = $event.target,
                      $$c = $$el.checked ? true : false
                    if (Array.isArray($$a)) {
                      var $$v = "all",
                        $$i = _vm._i($$a, $$v)
                      if ($$el.checked) {
                        $$i < 0 &&
                          _vm.$set(_vm.filter, "store", $$a.concat([$$v]))
                      } else {
                        $$i > -1 &&
                          _vm.$set(
                            _vm.filter,
                            "store",
                            $$a.slice(0, $$i).concat($$a.slice($$i + 1))
                          )
                      }
                    } else {
                      _vm.$set(_vm.filter, "store", $$c)
                    }
                  }
                }
              }),
              _vm._v(" "),
              _c("span", { staticClass: "check-mark" })
            ]),
            _vm._v(" "),
            _vm._l(_vm.filterData.stores, function(item) {
              return _c("label", { staticClass: "container" }, [
                _vm._v(
                  "\n                    " +
                    _vm._s(item.store_name) +
                    "\n                    "
                ),
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.filter.store,
                      expression: "filter.store"
                    }
                  ],
                  key: item.id,
                  attrs: { type: "checkbox" },
                  domProps: {
                    value: item.store_name,
                    checked: Array.isArray(_vm.filter.store)
                      ? _vm._i(_vm.filter.store, item.store_name) > -1
                      : _vm.filter.store
                  },
                  on: {
                    change: function($event) {
                      var $$a = _vm.filter.store,
                        $$el = $event.target,
                        $$c = $$el.checked ? true : false
                      if (Array.isArray($$a)) {
                        var $$v = item.store_name,
                          $$i = _vm._i($$a, $$v)
                        if ($$el.checked) {
                          $$i < 0 &&
                            _vm.$set(_vm.filter, "store", $$a.concat([$$v]))
                        } else {
                          $$i > -1 &&
                            _vm.$set(
                              _vm.filter,
                              "store",
                              $$a.slice(0, $$i).concat($$a.slice($$i + 1))
                            )
                        }
                      } else {
                        _vm.$set(_vm.filter, "store", $$c)
                      }
                    }
                  }
                }),
                _vm._v(" "),
                _c("span", { staticClass: "check-mark" })
              ])
            })
          ],
          2
        )
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "col-2 filter-content" }, [
        _c("div", { staticClass: "filter-title", on: { click: _vm.toggle } }, [
          _vm._v("Color\n                "),
          _c("i", { staticClass: "fas fa-chevron-down slide-down-hover" })
        ]),
        _vm._v(" "),
        _c(
          "p",
          { staticClass: "p-2 filter-items" },
          [
            _c("label", { staticClass: "container" }, [
              _c("span", [_vm._v("All Colors")]),
              _vm._v(" "),
              _c("input", {
                directives: [
                  {
                    name: "model",
                    rawName: "v-model",
                    value: _vm.filter.color,
                    expression: "filter.color"
                  }
                ],
                attrs: { type: "checkbox", value: "all", checked: "checked" },
                domProps: {
                  checked: Array.isArray(_vm.filter.color)
                    ? _vm._i(_vm.filter.color, "all") > -1
                    : _vm.filter.color
                },
                on: {
                  change: function($event) {
                    var $$a = _vm.filter.color,
                      $$el = $event.target,
                      $$c = $$el.checked ? true : false
                    if (Array.isArray($$a)) {
                      var $$v = "all",
                        $$i = _vm._i($$a, $$v)
                      if ($$el.checked) {
                        $$i < 0 &&
                          _vm.$set(_vm.filter, "color", $$a.concat([$$v]))
                      } else {
                        $$i > -1 &&
                          _vm.$set(
                            _vm.filter,
                            "color",
                            $$a.slice(0, $$i).concat($$a.slice($$i + 1))
                          )
                      }
                    } else {
                      _vm.$set(_vm.filter, "color", $$c)
                    }
                  }
                }
              }),
              _vm._v(" "),
              _c("span", { staticClass: "check-mark" })
            ]),
            _vm._v(" "),
            _vm._l(_vm.filterData.colors, function(item) {
              return _c("label", { staticClass: "container" }, [
                _vm._v(
                  "\n                    " +
                    _vm._s(item.name) +
                    "\n                    "
                ),
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.filter.color,
                      expression: "filter.color"
                    }
                  ],
                  key: item.id,
                  attrs: { type: "checkbox" },
                  domProps: {
                    value: item.name,
                    checked: Array.isArray(_vm.filter.color)
                      ? _vm._i(_vm.filter.color, item.name) > -1
                      : _vm.filter.color
                  },
                  on: {
                    change: function($event) {
                      var $$a = _vm.filter.color,
                        $$el = $event.target,
                        $$c = $$el.checked ? true : false
                      if (Array.isArray($$a)) {
                        var $$v = item.name,
                          $$i = _vm._i($$a, $$v)
                        if ($$el.checked) {
                          $$i < 0 &&
                            _vm.$set(_vm.filter, "color", $$a.concat([$$v]))
                        } else {
                          $$i > -1 &&
                            _vm.$set(
                              _vm.filter,
                              "color",
                              $$a.slice(0, $$i).concat($$a.slice($$i + 1))
                            )
                        }
                      } else {
                        _vm.$set(_vm.filter, "color", $$c)
                      }
                    }
                  }
                }),
                _vm._v(" "),
                _c("span", { staticClass: "check-mark" })
              ])
            })
          ],
          2
        )
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "col-2 filter-content" }, [
        _c("div", { staticClass: "filter-title", on: { click: _vm.toggle } }, [
          _vm._v("Sizes\n                "),
          _c("i", { staticClass: "fas fa-chevron-down slide-down-hover" })
        ]),
        _vm._v(" "),
        _c(
          "p",
          { staticClass: "p-2 filter-items" },
          [
            _c("label", { staticClass: "container" }, [
              _c("span", [_vm._v("All Sizes")]),
              _vm._v(" "),
              _c("input", {
                directives: [
                  {
                    name: "model",
                    rawName: "v-model",
                    value: _vm.filter.size,
                    expression: "filter.size"
                  }
                ],
                attrs: { type: "checkbox", value: "all", checked: "checked" },
                domProps: {
                  checked: Array.isArray(_vm.filter.size)
                    ? _vm._i(_vm.filter.size, "all") > -1
                    : _vm.filter.size
                },
                on: {
                  change: function($event) {
                    var $$a = _vm.filter.size,
                      $$el = $event.target,
                      $$c = $$el.checked ? true : false
                    if (Array.isArray($$a)) {
                      var $$v = "all",
                        $$i = _vm._i($$a, $$v)
                      if ($$el.checked) {
                        $$i < 0 &&
                          _vm.$set(_vm.filter, "size", $$a.concat([$$v]))
                      } else {
                        $$i > -1 &&
                          _vm.$set(
                            _vm.filter,
                            "size",
                            $$a.slice(0, $$i).concat($$a.slice($$i + 1))
                          )
                      }
                    } else {
                      _vm.$set(_vm.filter, "size", $$c)
                    }
                  }
                }
              }),
              _vm._v(" "),
              _c("span", { staticClass: "check-mark" })
            ]),
            _vm._v(" "),
            _vm._l(_vm.filterData.sizes, function(item) {
              return _c("label", { staticClass: "container" }, [
                _vm._v(
                  "\n                    " +
                    _vm._s(item.name) +
                    "\n                    "
                ),
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.filter.size,
                      expression: "filter.size"
                    }
                  ],
                  key: item.id,
                  attrs: { type: "checkbox" },
                  domProps: {
                    value: item.name,
                    checked: Array.isArray(_vm.filter.size)
                      ? _vm._i(_vm.filter.size, item.name) > -1
                      : _vm.filter.size
                  },
                  on: {
                    change: function($event) {
                      var $$a = _vm.filter.size,
                        $$el = $event.target,
                        $$c = $$el.checked ? true : false
                      if (Array.isArray($$a)) {
                        var $$v = item.name,
                          $$i = _vm._i($$a, $$v)
                        if ($$el.checked) {
                          $$i < 0 &&
                            _vm.$set(_vm.filter, "size", $$a.concat([$$v]))
                        } else {
                          $$i > -1 &&
                            _vm.$set(
                              _vm.filter,
                              "size",
                              $$a.slice(0, $$i).concat($$a.slice($$i + 1))
                            )
                        }
                      } else {
                        _vm.$set(_vm.filter, "size", $$c)
                      }
                    }
                  }
                }),
                _vm._v(" "),
                _c("span", { staticClass: "check-mark" })
              ])
            })
          ],
          2
        )
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "col-2 filter-content" }, [
        _c("div", { staticClass: "filter-title", on: { click: _vm.toggle } }, [
          _vm._v("Region\n                "),
          _c("i", { staticClass: "fas fa-chevron-down slide-down-hover" })
        ]),
        _vm._v(" "),
        _c(
          "p",
          { staticClass: "p-2 filter-items" },
          [
            _c("label", { staticClass: "container" }, [
              _c("span", [_vm._v("All Region")]),
              _vm._v(" "),
              _c("input", {
                directives: [
                  {
                    name: "model",
                    rawName: "v-model",
                    value: _vm.filter.region,
                    expression: "filter.region"
                  }
                ],
                attrs: { type: "checkbox", value: "all", checked: "checked" },
                domProps: {
                  checked: Array.isArray(_vm.filter.region)
                    ? _vm._i(_vm.filter.region, "all") > -1
                    : _vm.filter.region
                },
                on: {
                  change: function($event) {
                    var $$a = _vm.filter.region,
                      $$el = $event.target,
                      $$c = $$el.checked ? true : false
                    if (Array.isArray($$a)) {
                      var $$v = "all",
                        $$i = _vm._i($$a, $$v)
                      if ($$el.checked) {
                        $$i < 0 &&
                          _vm.$set(_vm.filter, "region", $$a.concat([$$v]))
                      } else {
                        $$i > -1 &&
                          _vm.$set(
                            _vm.filter,
                            "region",
                            $$a.slice(0, $$i).concat($$a.slice($$i + 1))
                          )
                      }
                    } else {
                      _vm.$set(_vm.filter, "region", $$c)
                    }
                  }
                }
              }),
              _vm._v(" "),
              _c("span", { staticClass: "check-mark" })
            ]),
            _vm._v(" "),
            _vm._l(_vm.filterData.region, function(item) {
              return _c("label", { staticClass: "container" }, [
                _vm._v(
                  "\n                    " +
                    _vm._s(item.region) +
                    "\n                    "
                ),
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.filter.region,
                      expression: "filter.region"
                    }
                  ],
                  key: item.region,
                  attrs: { type: "checkbox" },
                  domProps: {
                    value: item.region,
                    checked: Array.isArray(_vm.filter.region)
                      ? _vm._i(_vm.filter.region, item.region) > -1
                      : _vm.filter.region
                  },
                  on: {
                    change: function($event) {
                      var $$a = _vm.filter.region,
                        $$el = $event.target,
                        $$c = $$el.checked ? true : false
                      if (Array.isArray($$a)) {
                        var $$v = item.region,
                          $$i = _vm._i($$a, $$v)
                        if ($$el.checked) {
                          $$i < 0 &&
                            _vm.$set(_vm.filter, "region", $$a.concat([$$v]))
                        } else {
                          $$i > -1 &&
                            _vm.$set(
                              _vm.filter,
                              "region",
                              $$a.slice(0, $$i).concat($$a.slice($$i + 1))
                            )
                        }
                      } else {
                        _vm.$set(_vm.filter, "region", $$c)
                      }
                    }
                  }
                }),
                _vm._v(" "),
                _c("span", { staticClass: "check-mark" })
              ])
            })
          ],
          2
        )
      ])
    ]),
    _vm._v(" "),
    _c("div", { staticClass: "row mt-4" }, [
      _c("div", { staticClass: "col-12 filter-content" }, [
        _c("div", { staticClass: "filter-title", on: { click: _vm.toggle } }, [
          _vm._v("Sub-Categories\n                "),
          _c("i", { staticClass: "fas fa-chevron-down slide-down-hover" })
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "p-2 filter-items" }, [
          _c(
            "div",
            { staticClass: "row m-0" },
            [
              _vm._l(_vm.subCategories, function(gender) {
                return [
                  _c("div", { staticClass: "col-6 pl-2" }, [
                    _c("strong", { staticClass: "text-uppercase" }, [
                      _c("u", [_vm._v(" " + _vm._s(gender.label) + " ")])
                    ]),
                    _vm._v(" "),
                    _c(
                      "div",
                      { staticClass: "row m-0" },
                      [
                        _vm._l(gender.childs, function(category) {
                          return [
                            _c(
                              "div",
                              { staticClass: "col-6 pt-2" },
                              [
                                _c(
                                  "strong",
                                  { staticClass: "text-uppercase" },
                                  [_c("i", [_vm._v(_vm._s(category.label))])]
                                ),
                                _vm._v(" "),
                                _vm._l(category.grandchilds, function(item) {
                                  return _c(
                                    "label",
                                    { staticClass: "container" },
                                    [
                                      _vm._v(
                                        "\n                                            " +
                                          _vm._s(item.label) +
                                          "\n                                            "
                                      ),
                                      _c("input", {
                                        directives: [
                                          {
                                            name: "model",
                                            rawName: "v-model",
                                            value: _vm.filter.subCategory,
                                            expression: "filter.subCategory"
                                          }
                                        ],
                                        key: item.id,
                                        attrs: { type: "checkbox" },
                                        domProps: {
                                          value: item.label,
                                          checked: Array.isArray(
                                            _vm.filter.subCategory
                                          )
                                            ? _vm._i(
                                                _vm.filter.subCategory,
                                                item.label
                                              ) > -1
                                            : _vm.filter.subCategory
                                        },
                                        on: {
                                          change: function($event) {
                                            var $$a = _vm.filter.subCategory,
                                              $$el = $event.target,
                                              $$c = $$el.checked ? true : false
                                            if (Array.isArray($$a)) {
                                              var $$v = item.label,
                                                $$i = _vm._i($$a, $$v)
                                              if ($$el.checked) {
                                                $$i < 0 &&
                                                  _vm.$set(
                                                    _vm.filter,
                                                    "subCategory",
                                                    $$a.concat([$$v])
                                                  )
                                              } else {
                                                $$i > -1 &&
                                                  _vm.$set(
                                                    _vm.filter,
                                                    "subCategory",
                                                    $$a
                                                      .slice(0, $$i)
                                                      .concat(
                                                        $$a.slice($$i + 1)
                                                      )
                                                  )
                                              }
                                            } else {
                                              _vm.$set(
                                                _vm.filter,
                                                "subCategory",
                                                $$c
                                              )
                                            }
                                          }
                                        }
                                      }),
                                      _vm._v(" "),
                                      _c("span", { staticClass: "check-mark" })
                                    ]
                                  )
                                })
                              ],
                              2
                            )
                          ]
                        })
                      ],
                      2
                    )
                  ])
                ]
              })
            ],
            2
          )
        ])
      ])
    ]),
    _vm._v(" "),
    _c("hr"),
    _vm._v(" "),
    _c("div", { staticClass: "row justify-content-center mt-3 mb-3" }, [
      _c(
        "button",
        {
          staticClass: "btn btn-outline-dark col-2 text-bold",
          on: { click: _vm.getData }
        },
        [_vm._v("Get Report")]
      ),
      _vm._v(" "),
      _c(
        "button",
        {
          staticClass: "btn btn-outline-dark col-2 ml-5 text-bold",
          on: { click: _vm.clearFilter }
        },
        [_vm._v("Clear Filters")]
      )
    ]),
    _vm._v(" "),
    _c("hr"),
    _vm._v(" "),
    _c("h4", { staticClass: "text-center" }, [
      _vm._v("Detailed Sales Report "),
      _vm.isTrending
        ? _c("span", [
            _vm._v(" of " + _vm._s(_vm.trending) + " Trending Products")
          ])
        : _vm._e()
    ]),
    _vm._v(" "),
    _c("h6", { staticClass: "text-center mt-n1" }, [
      _vm.isGender
        ? _c("span", [_vm._v(" - Gender: " + _vm._s(_vm.filter.gender))])
        : _vm._e(),
      _vm._v(" "),
      _vm.isCategory
        ? _c("span", [_vm._v(" - Categories: " + _vm._s(_vm.filter.category))])
        : _vm._e(),
      _vm._v(" "),
      _vm.isSubCategory
        ? _c("span", [
            _vm._v(" - Sub-Categories: " + _vm._s(_vm.filter.subCategory))
          ])
        : _vm._e(),
      _vm._v(" "),
      _vm.isStore
        ? _c("span", [_vm._v(" - Stores: " + _vm._s(_vm.filter.store))])
        : _vm._e(),
      _vm._v(" "),
      _vm.isColor
        ? _c("span", [_vm._v(" - Colors: " + _vm._s(_vm.filter.color))])
        : _vm._e(),
      _vm._v(" "),
      _vm.isSize
        ? _c("span", [_vm._v(" - Sizes: " + _vm._s(_vm.filter.size))])
        : _vm._e(),
      _vm._v(" "),
      _vm.isRegion
        ? _c("span", [_vm._v(" - Region: " + _vm._s(_vm.filter.region))])
        : _vm._e(),
      _vm._v("\n        -\n    ")
    ]),
    _vm._v(" "),
    _c("hr"),
    _vm._v(" "),
    _vm.isLoading
      ? _c("div", { staticStyle: { height: "100vh" } }, [_vm._m(4)])
      : _c(
          "div",
          { staticClass: "report-data" },
          [
            _c("div", { staticClass: "mt-2 row justify-content-center" }, [
              _c("div", { staticClass: "form-group col-5 mb-0" }, [
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.search[0].value,
                      expression: "search[0].value"
                    }
                  ],
                  staticClass: "form-control",
                  attrs: { placeholder: "Search" },
                  domProps: { value: _vm.search[0].value },
                  on: {
                    input: function($event) {
                      if ($event.target.composing) {
                        return
                      }
                      _vm.$set(_vm.search[0], "value", $event.target.value)
                    }
                  }
                })
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "form-group col-5 ml-5 mb-0" }, [
                _c("div", { staticClass: "input-group" }, [
                  _c(
                    "select",
                    {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.type,
                          expression: "type"
                        }
                      ],
                      staticClass: "custom-select",
                      attrs: {
                        id: "filetype",
                        "aria-label": "Select file type to export"
                      },
                      on: {
                        change: function($event) {
                          var $$selectedVal = Array.prototype.filter
                            .call($event.target.options, function(o) {
                              return o.selected
                            })
                            .map(function(o) {
                              var val = "_value" in o ? o._value : o.value
                              return val
                            })
                          _vm.type = $event.target.multiple
                            ? $$selectedVal
                            : $$selectedVal[0]
                        }
                      }
                    },
                    [
                      _c("option", { attrs: { value: "csv" } }, [
                        _vm._v("CSV")
                      ]),
                      _vm._v(" "),
                      _c("option", { attrs: { value: "xls" } }, [
                        _vm._v("Excel")
                      ])
                    ]
                  ),
                  _vm._v(" "),
                  _c(
                    "div",
                    { staticClass: "input-group-append" },
                    [
                      _c(
                        "excel",
                        {
                          staticClass: "btn btn-outline-dark",
                          attrs: {
                            data: _vm.data,
                            name: _vm.filename,
                            type: _vm.type,
                            title: _vm.title,
                            fields: _vm.fields
                          }
                        },
                        [
                          _vm._v(
                            "\n                            Export / Download\n                        "
                          )
                        ]
                      )
                    ],
                    1
                  )
                ])
              ])
            ]),
            _vm._v(" "),
            _c("hr"),
            _vm._v(" "),
            _c(
              "data-tables",
              {
                attrs: {
                  data: _vm.data,
                  filters: _vm.search,
                  "pagination-props": {
                    background: true,
                    pageSizes: [10, 20, 50]
                  }
                }
              },
              [
                _c("el-table-column", {
                  attrs: {
                    prop: "product_id",
                    label: "ID",
                    sortable: "custom",
                    "min-width": "50%"
                  }
                }),
                _vm._v(" "),
                _c("el-table-column", {
                  attrs: {
                    prop: "product",
                    label: "Product",
                    sortable: "custom"
                  }
                }),
                _vm._v(" "),
                _c("el-table-column", {
                  attrs: {
                    prop: "cp",
                    label: "Purchase Price",
                    sortable: "custom"
                  }
                }),
                _vm._v(" "),
                _c("el-table-column", {
                  attrs: {
                    prop: "sp",
                    label: "Sales Price",
                    sortable: "custom"
                  }
                }),
                _vm._v(" "),
                _c("el-table-column", {
                  attrs: {
                    prop: "quantity",
                    label: "Quantity",
                    sortable: "custom"
                  }
                }),
                _vm._v(" "),
                _c("el-table-column", {
                  attrs: {
                    prop: "gross_sales",
                    label: "Gross Sales",
                    sortable: "custom"
                  }
                }),
                _vm._v(" "),
                _c("el-table-column", {
                  attrs: {
                    prop: "discount",
                    label: "Discount",
                    sortable: "custom"
                  }
                }),
                _vm._v(" "),
                _c("el-table-column", {
                  attrs: {
                    prop: "net_sales",
                    label: "Net Sales",
                    sortable: "custom"
                  }
                }),
                _vm._v(" "),
                _c("el-table-column", {
                  attrs: {
                    prop: "gross_profit",
                    label: "Gross Profit",
                    sortable: "custom"
                  }
                }),
                _vm._v(" "),
                _c("el-table-column", {
                  attrs: {
                    prop: "profit",
                    label: "Profit %",
                    sortable: "custom"
                  },
                  scopedSlots: _vm._u([
                    {
                      key: "default",
                      fn: function(scope) {
                        return [
                          _vm._v(
                            "\n                    " +
                              _vm._s(scope.row.profit) +
                              "%\n                "
                          )
                        ]
                      }
                    }
                  ])
                }),
                _vm._v(" "),
                _vm.isGender
                  ? _c("el-table-column", {
                      attrs: { prop: "gender", label: "Gender" }
                    })
                  : _vm._e(),
                _vm._v(" "),
                _vm.isCategory
                  ? _c("el-table-column", {
                      attrs: { prop: "category", label: "Category" }
                    })
                  : _vm._e(),
                _vm._v(" "),
                _vm.isSubCategory
                  ? _c("el-table-column", {
                      attrs: { prop: "sub", label: "Sub Category" }
                    })
                  : _vm._e(),
                _vm._v(" "),
                _vm.isStore
                  ? _c("el-table-column", {
                      attrs: { prop: "store", label: "Store" }
                    })
                  : _vm._e(),
                _vm._v(" "),
                _vm.isColor
                  ? _c("el-table-column", {
                      attrs: { prop: "color", label: "Color" }
                    })
                  : _vm._e(),
                _vm._v(" "),
                _vm.isSize
                  ? _c("el-table-column", {
                      attrs: { prop: "size", label: "Size" }
                    })
                  : _vm._e(),
                _vm._v(" "),
                _vm.isRegion
                  ? _c("el-table-column", {
                      attrs: { prop: "region", label: "Region" }
                    })
                  : _vm._e()
              ],
              1
            )
          ],
          1
        )
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "input-group-prepend" }, [
      _c(
        "label",
        { staticClass: "input-group-text", attrs: { for: "start" } },
        [_vm._v("Start Date")]
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "input-group-prepend" }, [
      _c("span", { staticClass: "input-group-text" }, [
        _c("i", { staticClass: "far fa-calendar-alt" })
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "input-group-prepend" }, [
      _c("label", { staticClass: "input-group-text", attrs: { for: "end" } }, [
        _vm._v("End Date")
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "input-group-prepend" }, [
      _c("span", { staticClass: "input-group-text" }, [
        _c("i", { staticClass: "far fa-calendar-alt" })
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticStyle: { "margin-left": "50%" } }, [
      _c(
        "div",
        {
          staticClass: "spinner-border",
          staticStyle: { color: "#343a40" },
          attrs: { role: "status" }
        },
        [_c("span", { staticClass: "sr-only" }, [_vm._v("Loading...")])]
      )
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/components/backend/reports/SalesReport.vue":
/*!*****************************************************************!*\
  !*** ./resources/js/components/backend/reports/SalesReport.vue ***!
  \*****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _SalesReport_vue_vue_type_template_id_75c4d77a_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./SalesReport.vue?vue&type=template&id=75c4d77a&scoped=true& */ "./resources/js/components/backend/reports/SalesReport.vue?vue&type=template&id=75c4d77a&scoped=true&");
/* harmony import */ var _SalesReport_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./SalesReport.vue?vue&type=script&lang=js& */ "./resources/js/components/backend/reports/SalesReport.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _SalesReport_vue_vue_type_style_index_0_id_75c4d77a_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./SalesReport.vue?vue&type=style&index=0&id=75c4d77a&lang=scss&scoped=true& */ "./resources/js/components/backend/reports/SalesReport.vue?vue&type=style&index=0&id=75c4d77a&lang=scss&scoped=true&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _SalesReport_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _SalesReport_vue_vue_type_template_id_75c4d77a_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _SalesReport_vue_vue_type_template_id_75c4d77a_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "75c4d77a",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/backend/reports/SalesReport.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/backend/reports/SalesReport.vue?vue&type=script&lang=js&":
/*!******************************************************************************************!*\
  !*** ./resources/js/components/backend/reports/SalesReport.vue?vue&type=script&lang=js& ***!
  \******************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_SalesReport_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./SalesReport.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backend/reports/SalesReport.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_SalesReport_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/backend/reports/SalesReport.vue?vue&type=style&index=0&id=75c4d77a&lang=scss&scoped=true&":
/*!***************************************************************************************************************************!*\
  !*** ./resources/js/components/backend/reports/SalesReport.vue?vue&type=style&index=0&id=75c4d77a&lang=scss&scoped=true& ***!
  \***************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_SalesReport_vue_vue_type_style_index_0_id_75c4d77a_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/style-loader!../../../../../node_modules/css-loader!../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../node_modules/postcss-loader/src??ref--7-2!../../../../../node_modules/sass-loader/dist/cjs.js??ref--7-3!../../../../../node_modules/vue-loader/lib??vue-loader-options!./SalesReport.vue?vue&type=style&index=0&id=75c4d77a&lang=scss&scoped=true& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backend/reports/SalesReport.vue?vue&type=style&index=0&id=75c4d77a&lang=scss&scoped=true&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_SalesReport_vue_vue_type_style_index_0_id_75c4d77a_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_SalesReport_vue_vue_type_style_index_0_id_75c4d77a_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_SalesReport_vue_vue_type_style_index_0_id_75c4d77a_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_SalesReport_vue_vue_type_style_index_0_id_75c4d77a_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_SalesReport_vue_vue_type_style_index_0_id_75c4d77a_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./resources/js/components/backend/reports/SalesReport.vue?vue&type=template&id=75c4d77a&scoped=true&":
/*!************************************************************************************************************!*\
  !*** ./resources/js/components/backend/reports/SalesReport.vue?vue&type=template&id=75c4d77a&scoped=true& ***!
  \************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_SalesReport_vue_vue_type_template_id_75c4d77a_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./SalesReport.vue?vue&type=template&id=75c4d77a&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backend/reports/SalesReport.vue?vue&type=template&id=75c4d77a&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_SalesReport_vue_vue_type_template_id_75c4d77a_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_SalesReport_vue_vue_type_template_id_75c4d77a_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);