(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[54],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backend/invoice/InvoiceDetails.vue?vue&type=script&lang=js&":
/*!*****************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/backend/invoice/InvoiceDetails.vue?vue&type=script&lang=js& ***!
  \*****************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'admin-invoice-details',
  data: function data() {
    return {
      invoice: {},
      order: {},
      items: {},
      loading: true
    };
  },
  created: function created() {
    this.getData(this.$route.params.id);
  },
  methods: {
    getData: function getData(id) {
      var _this = this;

      axios.get('/api/getItemDetails/' + id).then(function (response) {
        _this.invoice = response.data.invoice;
        _this.order = response.data.order;
        _this.items = response.data.items;
        _this.loading = false;
        console.log(response.data);
      });
    },
    getEachItemTotal: function getEachItemTotal(index) {
      return this.items[index].quantity * this.items[index].products.price;
    },
    subTotal: function subTotal() {
      var amt = 0;

      for (var i = 0; i < this.items.length; i++) {
        amt += this.items[i].quantity * this.items[i].products.price;
      }

      return amt;
    },
    completeOrder: function completeOrder() {
      var _this2 = this;

      axios.get('/api/complete-order/' + this.invoice.id + '/' + this.invoice.order_id).then(function (response) {
        _this2.loading = true;

        _this2.getData(_this2.invoice.order_id);
      })["catch"](function (errors) {});
    }
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backend/invoice/InvoiceDetails.vue?vue&type=template&id=58becb7d&":
/*!*********************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/backend/invoice/InvoiceDetails.vue?vue&type=template&id=58becb7d& ***!
  \*********************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _vm.loading
    ? _c("div", { staticStyle: { height: "100vh" } }, [_vm._m(0)])
    : _c("div", { staticClass: "container-fluid mt-3" }, [
        _c("h4", { staticClass: "text-right" }, [
          _vm._v("Invoice ID: #" + _vm._s(_vm.invoice.id))
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "d-flex justify-content-between" }, [
          _c("div", { staticClass: "col-lg-3 pl-0" }, [
            _vm._m(1),
            _vm._v(" "),
            _c("p", [
              _c("b", [_vm._v(_vm._s(_vm.order.name))]),
              _c("br"),
              _vm._v(_vm._s(_vm.order.address)),
              _c("br"),
              _vm._v(_vm._s(_vm.order.mobile))
            ])
          ]),
          _vm._v(" "),
          _vm._m(2)
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "d-flex justify-content-between" }, [
          _c("div", { staticClass: "col-lg-6 pl-0" }, [
            _c("p", { staticClass: "mb-0 mt-5" }, [
              _vm._v("Invoice Date : " + _vm._s(_vm.invoice.created_at))
            ]),
            _vm._v(" "),
            _c("p", [_vm._v("Ordered Date : " + _vm._s(_vm.order.created_at))])
          ])
        ]),
        _vm._v(" "),
        _c(
          "div",
          {
            staticClass:
              "container-fluid mt-5 d-flex justify-content-center w-100"
          },
          [
            _c("div", { staticClass: "table-responsive w-100" }, [
              _c("table", { staticClass: "table" }, [
                _vm._m(3),
                _vm._v(" "),
                _c(
                  "tbody",
                  _vm._l(_vm.items, function(item, index) {
                    return _c("tr", { staticClass: "text-right" }, [
                      _c("td", { staticClass: "text-left" }, [
                        _vm._v(_vm._s(index + 1))
                      ]),
                      _vm._v(" "),
                      _c("td", { staticClass: "text-left" }, [
                        _vm._v(
                          _vm._s(item.products.name) +
                            " | " +
                            _vm._s(item.color) +
                            " | " +
                            _vm._s(item.size)
                        )
                      ]),
                      _vm._v(" "),
                      _c("td", [_vm._v(_vm._s(item.quantity))]),
                      _vm._v(" "),
                      _c("td", [_vm._v("Rs. " + _vm._s(item.products.price))]),
                      _vm._v(" "),
                      _c("td", [
                        _vm._v("Rs. " + _vm._s(_vm.getEachItemTotal(index)))
                      ])
                    ])
                  }),
                  0
                )
              ])
            ])
          ]
        ),
        _vm._v(" "),
        _c("div", { staticClass: "container-fluid mt-5 w-100" }, [
          _c("p", { staticClass: "text-right mb-2" }, [
            _vm._v("Total Amount : Rs. " + _vm._s(_vm.subTotal()))
          ]),
          _vm._v(" "),
          _c("p", { staticClass: "text-right" }, [_vm._v("Shipping : Rs. 50")]),
          _vm._v(" "),
          _c("p", { staticClass: "text-right" }, [
            _vm._v("Voucher Amount : Rs. " + _vm._s(_vm.order.voucher_amount))
          ]),
          _vm._v(" "),
          _c("h4", { staticClass: "text-right mb-5" }, [
            _vm._v(
              "Grand Total : Rs. " +
                _vm._s(_vm.subTotal() + 50 - _vm.order.voucher_amount)
            )
          ]),
          _vm._v(" "),
          _c("hr")
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "my-5 text-center" }, [
          _c(
            "button",
            {
              staticClass: "btn btn-success",
              attrs: { type: "button", disabled: _vm.invoice.status === 1 },
              on: { click: _vm.completeOrder }
            },
            [_vm._v("Mark as Complete")]
          )
        ])
      ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "div",
      { staticClass: "mt-5", staticStyle: { "margin-left": "50%" } },
      [
        _c(
          "div",
          {
            staticClass: "spinner-border",
            staticStyle: { width: "3rem", height: "3rem" },
            attrs: { role: "status" }
          },
          [_c("span", { staticClass: "sr-only" }, [_vm._v("Loading...")])]
        )
      ]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("p", { staticClass: "mt-5 mb-2" }, [
      _c("b", [_c("strong", [_vm._v("Invoice to:")])])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "col-lg-3 pr-0" }, [
      _c("p", { staticClass: "mt-5 mb-2 text-right" }, [
        _c("b", [_vm._v("TheNexTrend")])
      ]),
      _vm._v(" "),
      _c("p", { staticClass: "text-right" }, [
        _vm._v("Gaala & Sons,"),
        _c("br"),
        _vm._v(" C-201, Beykoz-34800,"),
        _c("br"),
        _vm._v(" Canada, K1A 0G9.")
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("thead", [
      _c("tr", [
        _c("th", [_vm._v("#")]),
        _vm._v(" "),
        _c("th", [_vm._v("Product")]),
        _vm._v(" "),
        _c("th", { staticClass: "text-right" }, [_vm._v("Quantity")]),
        _vm._v(" "),
        _c("th", { staticClass: "text-right" }, [_vm._v("Unit cost")]),
        _vm._v(" "),
        _c("th", { staticClass: "text-right" }, [_vm._v("Total")])
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js":
/*!********************************************************************!*\
  !*** ./node_modules/vue-loader/lib/runtime/componentNormalizer.js ***!
  \********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return normalizeComponent; });
/* globals __VUE_SSR_CONTEXT__ */

// IMPORTANT: Do NOT use ES2015 features in this file (except for modules).
// This module is a runtime utility for cleaner component module output and will
// be included in the final webpack user bundle.

function normalizeComponent (
  scriptExports,
  render,
  staticRenderFns,
  functionalTemplate,
  injectStyles,
  scopeId,
  moduleIdentifier, /* server only */
  shadowMode /* vue-cli only */
) {
  // Vue.extend constructor export interop
  var options = typeof scriptExports === 'function'
    ? scriptExports.options
    : scriptExports

  // render functions
  if (render) {
    options.render = render
    options.staticRenderFns = staticRenderFns
    options._compiled = true
  }

  // functional template
  if (functionalTemplate) {
    options.functional = true
  }

  // scopedId
  if (scopeId) {
    options._scopeId = 'data-v-' + scopeId
  }

  var hook
  if (moduleIdentifier) { // server build
    hook = function (context) {
      // 2.3 injection
      context =
        context || // cached call
        (this.$vnode && this.$vnode.ssrContext) || // stateful
        (this.parent && this.parent.$vnode && this.parent.$vnode.ssrContext) // functional
      // 2.2 with runInNewContext: true
      if (!context && typeof __VUE_SSR_CONTEXT__ !== 'undefined') {
        context = __VUE_SSR_CONTEXT__
      }
      // inject component styles
      if (injectStyles) {
        injectStyles.call(this, context)
      }
      // register component module identifier for async chunk inferrence
      if (context && context._registeredComponents) {
        context._registeredComponents.add(moduleIdentifier)
      }
    }
    // used by ssr in case component is cached and beforeCreate
    // never gets called
    options._ssrRegister = hook
  } else if (injectStyles) {
    hook = shadowMode
      ? function () { injectStyles.call(this, this.$root.$options.shadowRoot) }
      : injectStyles
  }

  if (hook) {
    if (options.functional) {
      // for template-only hot-reload because in that case the render fn doesn't
      // go through the normalizer
      options._injectStyles = hook
      // register for functioal component in vue file
      var originalRender = options.render
      options.render = function renderWithStyleInjection (h, context) {
        hook.call(context)
        return originalRender(h, context)
      }
    } else {
      // inject component registration as beforeCreate hook
      var existing = options.beforeCreate
      options.beforeCreate = existing
        ? [].concat(existing, hook)
        : [hook]
    }
  }

  return {
    exports: scriptExports,
    options: options
  }
}


/***/ }),

/***/ "./resources/js/components/backend/invoice/InvoiceDetails.vue":
/*!********************************************************************!*\
  !*** ./resources/js/components/backend/invoice/InvoiceDetails.vue ***!
  \********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _InvoiceDetails_vue_vue_type_template_id_58becb7d___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./InvoiceDetails.vue?vue&type=template&id=58becb7d& */ "./resources/js/components/backend/invoice/InvoiceDetails.vue?vue&type=template&id=58becb7d&");
/* harmony import */ var _InvoiceDetails_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./InvoiceDetails.vue?vue&type=script&lang=js& */ "./resources/js/components/backend/invoice/InvoiceDetails.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _InvoiceDetails_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _InvoiceDetails_vue_vue_type_template_id_58becb7d___WEBPACK_IMPORTED_MODULE_0__["render"],
  _InvoiceDetails_vue_vue_type_template_id_58becb7d___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/backend/invoice/InvoiceDetails.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/backend/invoice/InvoiceDetails.vue?vue&type=script&lang=js&":
/*!*********************************************************************************************!*\
  !*** ./resources/js/components/backend/invoice/InvoiceDetails.vue?vue&type=script&lang=js& ***!
  \*********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_InvoiceDetails_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./InvoiceDetails.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backend/invoice/InvoiceDetails.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_InvoiceDetails_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/backend/invoice/InvoiceDetails.vue?vue&type=template&id=58becb7d&":
/*!***************************************************************************************************!*\
  !*** ./resources/js/components/backend/invoice/InvoiceDetails.vue?vue&type=template&id=58becb7d& ***!
  \***************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_InvoiceDetails_vue_vue_type_template_id_58becb7d___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./InvoiceDetails.vue?vue&type=template&id=58becb7d& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backend/invoice/InvoiceDetails.vue?vue&type=template&id=58becb7d&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_InvoiceDetails_vue_vue_type_template_id_58becb7d___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_InvoiceDetails_vue_vue_type_template_id_58becb7d___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);