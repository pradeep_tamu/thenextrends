(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[65],{

/***/ "./node_modules/khalti-web/lib/index.js":
/*!**********************************************!*\
  !*** ./node_modules/khalti-web/lib/index.js ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


function unwrapExports (x) {
	return x && x.__esModule ? x['default'] : x;
}

function createCommonjsModule(fn, module) {
	return module = { exports: {} }, fn(module, module.exports), module.exports;
}

var lib = createCommonjsModule(function (module, exports) {

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol ? "symbol" : typeof obj; };

var SkipValidation = exports.SkipValidation = function SkipValidation(message) {
	this.name = "SkipValidation";
	this.message = message;
};

var validateSingle = exports.validateSingle = function validateSingle(data, validators, multipleErrors, all, key) {
	var errors = [];

	if (typeof validators === "function") {
		validators = [validators];
	}

	for (var i = 0; i < validators.length; i++) {
		try {
			var error = validators[i](data, all);
			if (typeof error === "string") {
				errors.push(error.replace("{value}", data).replace("{key}", key));
			}
		} catch (err) {
			if (err instanceof SkipValidation) {
				break;
			}
		}
	}

	if (multipleErrors === true) return errors;

	if (errors.length > 0) return errors[0];
};

var validate = exports.validate = function validate(data, validators, multipleErrors) {
	if (!validators) return;

	var errors = {};
	var noError = true;

	if ((typeof validators === "undefined" ? "undefined" : _typeof(validators)) === "object" && !validators.length) {
		for (var prop in validators) {
			if (validators.hasOwnProperty(prop)) {
				var error = validateSingle(data[prop], validators[prop], multipleErrors, data, prop);

				if (error !== undefined) {
					noError = false;
				}

				errors[prop] = error;
			}
		}

		return noError ? undefined : errors;
	}

	errors = validateSingle(data, validators, multipleErrors);
	return errors;
};

var required = exports.required = function required(flag, error) {
	function isNullLike(value) {
		return value === undefined || value === "" || value === null;
	}

	return function (value) {
		if (flag && isNullLike(value)) {
			return error || "This field is required.";
		} else if (!flag && isNullLike(value)) {
			// skip rest of the validators
			throw new SkipValidation();
		}
	};
};

var isNumber = exports.isNumber = function isNumber(error) {
	return function (value) {
		if (typeof value !== "number" || isNaN(value)) {
			return error || "'{value}' is not a valid number.";
		}
	};
};

var isString = exports.isString = function isString(error) {
	return function (value) {
		if (typeof value !== "string") {
			return error || "'{value}' is not a valid string.";
		}
	};
};

var isFunction = exports.isFunction = function isFunction(error) {
	return function (value) {
		if (typeof value !== "function") {
			return error || "Expected a function.";
		}
	};
};

var isObject = exports.isObject = function isObject(error) {
	return function (value) {
		if (value !== Object(value)) {
			return error || "Expected an object.";
		}
	};
};

var isArray = exports.isArray = function isArray(error) {
	return function (value) {
		if (Object.prototype.toString.call(value) !== "[object Array]") {
			return error || "Expected an array.";
		}
	};
};

var length = exports.length = function length(_length, error) {
	return function (value) {
		var str = value + "";
		if (str.length !== _length) {
			return error || "It must be " + _length + " characters long.";
		}
	};
};

var isEmail = exports.isEmail = function isEmail(error) {
	return function (value) {
		var pattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		if (!pattern.test(value)) {
			return error || "Invalid email id.";
		}
	};
};

var equalsTo = exports.equalsTo = function equalsTo(key, error) {
	return function (value, all) {
		if (value !== all[key]) {
			return error || "'{key}' and '" + key + "' do not match.";
		}
	};
};

var minLength = exports.minLength = function minLength(length, error) {
	return function (value) {
		var str = value + "";
		if (str.length < length) {
			return error || "It must be at least " + length + " characters long.";
		}
	};
};

var maxLength = exports.maxLength = function maxLength(length, error) {
	return function (value) {
		var str = value + "";
		if (str.length > length) {
			return error || "It must be at most " + length + " characters long.";
		}
	};
};

var isBoolean = exports.isBoolean = function isBoolean(error) {
	return function (value) {
		if (value !== true && value !== false) {
			return error || "Invalid boolean value.";
		}
	};
};

var within = exports.within = function within(list, error) {
	return function (value) {
		if (!(value instanceof Array)) {
			value = [value];
		}

		var odds = [];

		for (var index = 0; index < value.length; index++) {
			if (list.indexOf(value[index]) === -1) {
				odds.push(value[index]);
			}
		}

		if (odds.length > 0) {
			return error || "[" + odds + "] do not fall under the allowed list.";
		}
	};
};

var excludes = exports.excludes = function excludes(list, error) {
	return function (value) {
		if (!(value instanceof Array)) {
			value = [value];
		}

		var odds = [];

		for (var index = 0; index < value.length; index++) {
			if (list.indexOf(value[index]) !== -1) {
				odds.push(value[index]);
			}
		}

		if (odds.length > 0) {
			return error || "[" + odds + "] fall under restricted values.";
		}
	};
};

var pattern = exports.pattern = function pattern(regex, error) {
	return function (value) {
		if (!regex.test(value)) {
			return error || "'{value}' does not match with the pattern.";
		}
	};
};
});

unwrapExports(lib);
var lib_1 = lib.SkipValidation;
var lib_2 = lib.validateSingle;
var lib_3 = lib.validate;
var lib_4 = lib.required;
var lib_5 = lib.isNumber;
var lib_6 = lib.isString;
var lib_7 = lib.isFunction;
var lib_8 = lib.isObject;
var lib_9 = lib.isArray;
var lib_10 = lib.length;
var lib_11 = lib.isEmail;
var lib_12 = lib.equalsTo;
var lib_13 = lib.minLength;
var lib_14 = lib.maxLength;
var lib_15 = lib.isBoolean;
var lib_16 = lib.within;
var lib_17 = lib.excludes;
var lib_18 = lib.pattern;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var WIDGET_URL = undefined || "https://khalti.com/payment/widget/";

var forEach = function forEach(data, iteratee) {
	if (Array.isArray(data)) {
		return data.forEach(iteratee);
	}
	return Object.keys(data).map(function (key) {
		return iteratee(data[key], key);
	});
};

var clone = function clone(data) {
	if (!data) return data;
	return JSON.parse(JSON.stringify(data));
};

var eventHandlerSchema = {
	onSuccess: [lib_4(true), lib_7()],
	onError: [lib_4(false), lib_7()],
	onClose: [lib_4(false), lib_7()]
};

var configSchema = {
	publicKey: lib_4(true),
	productUrl: lib_4(true),
	productIdentity: lib_4(true),
	productName: lib_4(true),
	eventHandler: lib_4(true),
	amount: lib_4(true),
	merchantData: [lib_4(false), lib_8()]
};

var KhaltiCheckout = function () {
	function KhaltiCheckout(config) {
		_classCallCheck(this, KhaltiCheckout);

		this._widgetId = "khalti-widget-" + Date.now();
		this._config = config;
		this._widget = this.attachWidget();
		this.listenToWidget();
	}

	_createClass(KhaltiCheckout, [{
		key: "listenToWidget",
		value: function listenToWidget() {
			var _this = this;

			window.addEventListener("message", function (e) {
				if (!e.data.realm) return;
				if (e.data.realm === 'widgetInit') {
					_this.widgetInit(e.data.payload);
				} else if (!e.data.payload || e.data.payload.widget_id !== _this._widgetId) {
					return;
				} else {
					var handler = "handle_msg_" + e.data.realm;
					_this[handler](e.data.payload);
				}
			}, false);
		}
	}, {
		key: "msgWidget",
		value: function msgWidget(realm, payload) {
			payload = clone(payload);
			payload.widgetId = this._widgetId;
			this._widget.contentWindow.postMessage({ realm: realm, payload: payload }, "*");
		}
	}, {
		key: "handle_msg_widgetInit",
		value: function handle_msg_widgetInit() {
			this.widgetInit();
		}
	}, {
		key: "widgetInit",
		value: function widgetInit() {
			var paymentInfo = clone(this._config);
			delete paymentInfo.eventHandler;
			this.msgWidget("paymentInfo", paymentInfo);
		}
	}, {
		key: "validateConfig",
		value: function validateConfig() {
			var errors = lib_3(this._config, configSchema);
			if (errors) {
				throw new Error(JSON.stringify(errors));
			} else {
				var _errors = lib_3(this._config.eventHandler, eventHandlerSchema);
				if (_errors) {
					throw new Error(JSON.stringify({ eventHandler: _errors }));
				}
			}
		}
	}, {
		key: "handle_msg_walletPaymentVerification",
		value: function handle_msg_walletPaymentVerification(paymentInfo) {
			this._config.eventHandler.onSuccess(paymentInfo);
			this.hide();
		}
	}, {
		key: "handle_msg_widgetError",
		value: function handle_msg_widgetError(error) {
			var errorHandler = this._config.eventHandler.onError;
			errorHandler && errorHandler(error);
		}
	}, {
		key: "disableParentScrollbar",
		value: function disableParentScrollbar() {
			this.parentOverflowValue = window.document.body.style.overflowY;
			window.document.body.style.overflowY = "hidden";
		}
	}, {
		key: "enableParentScrollbar",
		value: function enableParentScrollbar() {
			window.document.body.style.overflowY = this.parentOverflowValue;
			this.parentOverflowValue = null;
		}
	}, {
		key: "show",
		value: function show(updates) {
			this._config.source = "web";
			this._widget.setAttribute("src", WIDGET_URL);
			Object.assign(this._config, updates);
			this.validateConfig();
			this.disableParentScrollbar();
			this._widget.style.display = "block";
			this.widgetInit();
		}
	}, {
		key: "handle_msg_hide",
		value: function handle_msg_hide() {
			this.hide();
			var closeHandler = this._config.eventHandler.onClose;
			closeHandler && closeHandler();
		}
	}, {
		key: "hide",
		value: function hide() {
			this.enableParentScrollbar();
			this._widget.style.display = "none";
		}
	}, {
		key: "attachWidget",
		value: function attachWidget() {
			var widget = window.document.createElement("iframe");
			widget.setAttribute("id", this._widgetId);
			widget.style.position = "fixed";
			widget.style.display = "none";
			widget.style.top = "0";
			widget.style.left = "0";
			widget.width = "100%";
			widget.height = window.innerHeight + "px";
			// widget.setAttribute("src", undefined);
			widget.style.zIndex = 999999999;
			widget.setAttribute("frameborder", 0);
			widget.setAttribute("allowtransparency", true);

			window.document.body.appendChild(widget);

			return widget;
		}
	}, {
		key: "postAtURL",
		value: function postAtURL(payload) {
			var khaltiEbankingFormId = "khalti-ebanking-form-" + Date.now();
			// remove earlier form if exists
			if (this.ebankingForm) window.document.body.removeChild(this.ebankingForm);

			// create new form
			var form = window.document.createElement("form");
			form.setAttribute("id", khaltiEbankingFormId);
			form.setAttribute("action", payload.url);
			form.setAttribute("target", "_blank");
			form.setAttribute("method", "POST");
			form.style.display = "none";

			// attach inputs to form
			forEach(payload.paymentInfo, function (value, key) {
				var input = window.document.createElement("input");
				input.setAttribute("name", key);
				input.value = value;
				form.appendChild(input);
			});

			// attach form to body
			window.document.body.appendChild(form);
			this.ebankingForm = form;

			form.submit();
		}
	}]);

	return KhaltiCheckout;
}();

module.exports = KhaltiCheckout;


/***/ }),

/***/ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js":
/*!********************************************************************!*\
  !*** ./node_modules/vue-loader/lib/runtime/componentNormalizer.js ***!
  \********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return normalizeComponent; });
/* globals __VUE_SSR_CONTEXT__ */

// IMPORTANT: Do NOT use ES2015 features in this file (except for modules).
// This module is a runtime utility for cleaner component module output and will
// be included in the final webpack user bundle.

function normalizeComponent (
  scriptExports,
  render,
  staticRenderFns,
  functionalTemplate,
  injectStyles,
  scopeId,
  moduleIdentifier, /* server only */
  shadowMode /* vue-cli only */
) {
  // Vue.extend constructor export interop
  var options = typeof scriptExports === 'function'
    ? scriptExports.options
    : scriptExports

  // render functions
  if (render) {
    options.render = render
    options.staticRenderFns = staticRenderFns
    options._compiled = true
  }

  // functional template
  if (functionalTemplate) {
    options.functional = true
  }

  // scopedId
  if (scopeId) {
    options._scopeId = 'data-v-' + scopeId
  }

  var hook
  if (moduleIdentifier) { // server build
    hook = function (context) {
      // 2.3 injection
      context =
        context || // cached call
        (this.$vnode && this.$vnode.ssrContext) || // stateful
        (this.parent && this.parent.$vnode && this.parent.$vnode.ssrContext) // functional
      // 2.2 with runInNewContext: true
      if (!context && typeof __VUE_SSR_CONTEXT__ !== 'undefined') {
        context = __VUE_SSR_CONTEXT__
      }
      // inject component styles
      if (injectStyles) {
        injectStyles.call(this, context)
      }
      // register component module identifier for async chunk inferrence
      if (context && context._registeredComponents) {
        context._registeredComponents.add(moduleIdentifier)
      }
    }
    // used by ssr in case component is cached and beforeCreate
    // never gets called
    options._ssrRegister = hook
  } else if (injectStyles) {
    hook = shadowMode
      ? function () { injectStyles.call(this, this.$root.$options.shadowRoot) }
      : injectStyles
  }

  if (hook) {
    if (options.functional) {
      // for template-only hot-reload because in that case the render fn doesn't
      // go through the normalizer
      options._injectStyles = hook
      // register for functioal component in vue file
      var originalRender = options.render
      options.render = function renderWithStyleInjection (h, context) {
        hook.call(context)
        return originalRender(h, context)
      }
    } else {
      // inject component registration as beforeCreate hook
      var existing = options.beforeCreate
      options.beforeCreate = existing
        ? [].concat(existing, hook)
        : [hook]
    }
  }

  return {
    exports: scriptExports,
    options: options
  }
}


/***/ })

}]);