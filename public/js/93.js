(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[93],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/frontend/Privacy.vue?vue&type=script&lang=js&":
/*!***************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/frontend/Privacy.vue?vue&type=script&lang=js& ***!
  \***************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  name: "Privacy",
  created: function created() {
    this.scroll();
  },
  methods: {
    scroll: function scroll() {
      window.scrollTo(0, 0);
    }
  }
});

/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/frontend/Privacy.vue?vue&type=style&index=0&id=02b08536&scoped=true&lang=scss&":
/*!**************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/sass-loader/dist/cjs.js??ref--7-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/frontend/Privacy.vue?vue&type=style&index=0&id=02b08536&scoped=true&lang=scss& ***!
  \**************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".privacy-wrapper[data-v-02b08536] {\n  padding: 5%;\n  color: #464643;\n  background: white;\n  text-align: justify;\n}\n.privacy-wrapper p[data-v-02b08536] {\n  font-size: 16px;\n}\n.privacy-wrapper li[data-v-02b08536] {\n  font-size: 15px;\n}\n.title[data-v-02b08536] {\n  font-family: Roboto;\n  font-size: 26px;\n  font-weight: bold;\n  margin-bottom: 1rem;\n  color: #232323;\n  font-weight: bold;\n  text-align: center;\n}\n.title span[data-v-02b08536] {\n  font-size: 33px;\n}\n@media screen and (max-width: 767px) {\n.title[data-v-02b08536] {\n    font-size: 1rem;\n}\n.title span[data-v-02b08536] {\n    font-size: 21px;\n}\n}\n.privacy-title[data-v-02b08536] {\n  font-family: Roboto;\n  margin: 3rem 0 1rem 0;\n}", ""]);

// exports


/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/frontend/Privacy.vue?vue&type=style&index=0&id=02b08536&scoped=true&lang=scss&":
/*!******************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/sass-loader/dist/cjs.js??ref--7-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/frontend/Privacy.vue?vue&type=style&index=0&id=02b08536&scoped=true&lang=scss& ***!
  \******************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../node_modules/css-loader!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--7-2!../../../../node_modules/sass-loader/dist/cjs.js??ref--7-3!../../../../node_modules/vue-loader/lib??vue-loader-options!./Privacy.vue?vue&type=style&index=0&id=02b08536&scoped=true&lang=scss& */ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/frontend/Privacy.vue?vue&type=style&index=0&id=02b08536&scoped=true&lang=scss&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/frontend/Privacy.vue?vue&type=template&id=02b08536&scoped=true&":
/*!*******************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/frontend/Privacy.vue?vue&type=template&id=02b08536&scoped=true& ***!
  \*******************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "privacy-wrapper" }, [
    _vm._m(0),
    _vm._v(" "),
    _vm._m(1),
    _vm._v(" "),
    _c("p", [_vm._v("Here are some of our promises to you:")]),
    _vm._v(" "),
    _vm._m(2),
    _vm._v(" "),
    _c("p", [
      _vm._v(
        "You have a lot of rights relating to your personal information. You can control nearly all aspects of your\n        personal information through\n        "
      ),
      _c(
        "span",
        [
          _c(
            "router-link",
            {
              attrs: {
                to: { name: "userProfile" },
                tag: "a",
                "active-class": "active",
                exact: ""
              }
            },
            [_vm._v("\n            My Account\n        ")]
          )
        ],
        1
      ),
      _vm._v("\n        and any others by contacting us.")
    ]),
    _vm._v(" "),
    _c("ul", [
      _c("li", [
        _vm._v("Be informed about how your personal information is being used!")
      ]),
      _vm._v(" "),
      _c("li", [_vm._v("Access the personal information we hold about you")]),
      _vm._v(" "),
      _c("li", [
        _vm._v(
          "Request any data correction of inaccurate personal information we hold about you "
        )
      ]),
      _vm._v(" "),
      _c("li", [
        _vm._v(
          "Request that we delete your data, or stop processing it or collecting it, in some circumstances"
        )
      ]),
      _vm._v(" "),
      _c("li", [
        _vm._v(
          "Stop direct marketing messages, which you can do through \n            "
        ),
        _c(
          "span",
          [
            _c(
              "router-link",
              {
                attrs: {
                  to: { name: "userProfile" },
                  tag: "a",
                  "active-class": "active",
                  exact: ""
                }
              },
              [_vm._v("\n                My Account\n            ")]
            )
          ],
          1
        ),
        _vm._v(
          "\n            , and to withdraw\n            consent for other consent-based processing at any time\n        "
        )
      ]),
      _vm._v(" "),
      _c("li", [
        _vm._v(
          "Request that we transfer elements of your data either to you or another service provider"
        )
      ])
    ]),
    _vm._v(" "),
    _c("p", [
      _vm._v(
        "If you want to exercise your rights, have a complaint, or have any questions, please contact us. You can\n        find our details in the How to Contact Us section below.\n        You can choose not to provide any personal information, however you may not be able to buy from the site and\n        you are unlikely to receive our optimal customer experience.\n        At THE NEXTRENDS we will only keep your information as long as you are a customer with us. Our systems will\n        delete your information if you are inactive for 3 years. If you no longer wish to be a customer, you can\n        contact our support team to request your information be removed from our system. \n        If required to meet legal or regulation requirements, resolve disputes, prevent fraud and abuse, or enforce\n        our Terms & Conditions, we may also keep some of your information as reasonably necessary, even after\n        you have closed your account or your account is no longer needed."
      )
    ]),
    _vm._v(" "),
    _vm._m(3),
    _vm._v(" "),
    _c("p", [
      _vm._v(
        "We do not, and will not, sell any of your personal information to any third parties. We consider\n        your privacy hugely important and want you to be protected. In order to do that we follow all privacy laws,\n        and take extra care regarding your personal information."
      )
    ]),
    _vm._v(" "),
    _c("p", [
      _vm._v(
        "However, we must share your data with the following service companies to be able to provide our services to\n        you."
      )
    ]),
    _vm._v(" "),
    _c("p", [
      _vm._v(
        "Companies within THE NEXTRENDS group, as sometimes different portions of our group as different groups have\n        different responsibilities."
      )
    ]),
    _vm._v(" "),
    _vm._m(4),
    _vm._v(" "),
    _c("p", [
      _vm._v(
        "We may also anonymise and combine personal information (so that it does not identify you) and use it for\n        purposes such as testing our IT systems, research, data analysis, improving our site and applications, and\n        developing new products and services. We may also share this anonymous information with third parties."
      )
    ]),
    _vm._v(" "),
    _vm._m(5),
    _vm._v(" "),
    _c("p", [
      _vm._v(
        "THE NEXTRENDS is a global business with operations inside and outside of Nepal. We use suppliers and services\n        located across the world. If we have to transfer your information to one of these locations to provide a\n        service to you, we will take steps to ensure that your personal information is protected using approved\n        methods within the relevant Data Protection laws."
      )
    ]),
    _vm._v(" "),
    _vm._m(6),
    _vm._v(" "),
    _vm._m(7),
    _vm._v(" "),
    _c("p", [
      _vm._v(
        "You may notice changes on this page from time to time, this is to reflect any updates on how we are\n        processing your data."
      )
    ]),
    _vm._v(" "),
    _c("p", [
      _vm._v(
        "If any significant changes are made, we will make that clear to you on our website, email, or any other THE\n        NEXTRENDS services so you are able to review the changes and make an informed decision to purchase with us. "
      )
    ]),
    _vm._v(" "),
    _vm._m(8),
    _vm._v(" "),
    _vm._m(9),
    _vm._v(" "),
    _vm._m(10),
    _vm._v(" "),
    _c("p", [
      _vm._v(
        "If you have any questions or concerns about this notice, or would like to exercise your rights as set out\n        above, please contact our customer service at the below email address."
      )
    ]),
    _vm._v(" "),
    _vm._m(11)
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "title" }, [
      _vm._v("THE NEX"),
      _c("span", [_vm._v("T")]),
      _vm._v("RENDS "),
      _c("strong", [_vm._v("Privacy Policy")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("p", [
      _vm._v("At "),
      _c("b", [_vm._v("THE NEXTRENDS")]),
      _vm._v(
        " we are committed to protecting the privacy and security of our customers and site\n        visitors. This is to help you understand how."
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("ul", [
      _c("li", [_vm._v("We will protect your info like it’s our own")]),
      _vm._v(" "),
      _c("li", [
        _vm._v(
          "We won’t hang onto your information – if we don’t need it, we’ll delete it."
        )
      ]),
      _vm._v(" "),
      _c("li", [_vm._v("You are in full control of the content you receive")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("h5", { staticClass: "privacy-title" }, [
      _c("strong", [_vm._v("THIRD PARTY")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("ul", [
      _c("li", [
        _vm._v(
          "Companies that complete a service to get your purchases to you. These could include payment service\n            providers, warehouses, order packers, and delivery companies\n        "
        )
      ]),
      _vm._v(" "),
      _c("li", [
        _vm._v(
          "Professional service providers, such as website hosts, marketing agencies, and advertising\n            partners. These companies help us run our business.\n        "
        )
      ]),
      _vm._v(" "),
      _c("li", [
        _vm._v(
          "Law enforcement, credit reference agencies and fraud prevention agencies"
        )
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("ul", [
      _c("li", [
        _vm._v(
          "Note This Privacy Policy also does not apply to the practices of organisations that THE NEXTRENDS does\n            not own or control, or to people that THE NEXTRENDS does not employ or oversee even if offers, coupons\n            or links to their websites appear within our Platforms or technology.\n        "
        )
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("p", [
      _vm._v(
        "We also engage in online advertising, to help keep you aware of what we’re up to and to help you find\n        our products."
      ),
      _c("br"),
      _c("br"),
      _vm._v(
        "Like many companies, we target our ads to you when you are on other websites and\n        apps. To do this we use a range of digital marketing networks and ad exchanges. We also use a variety of\n        advertising technologies like web beacons, pixels, ad tags, cookies, and mobile identifiers, along with\n        specific services offered by some sites and social networks."
      ),
      _c("br"),
      _c("br"),
      _vm._v(
        "The banners and ads you see will be\n        based on information we hold about you, or your previous use of THE NEXTRENDS or any associated\n        applications. Some of these might include your THE NEXTRENDS search history, or any of our advertisements\n        you have previously clicked on."
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("h5", { staticClass: "privacy-title" }, [
      _c("strong", [_vm._v("Changes to how we protect your privacy ")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("h5", { staticClass: "privacy-title" }, [
      _c("strong", [_vm._v("Cookies ")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("p", [
      _vm._v(
        "We use cookies on our website. For more information on cookies, please see our "
      ),
      _c("a", { attrs: { href: "" } }, [_vm._v("cookie\n        notice.")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("h5", { staticClass: "privacy-title" }, [
      _c("strong", [_vm._v("How to contact us")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("p", [
      _c("a", { attrs: { href: "" } }, [_vm._v("info@thenextrends.com")])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/components/frontend/Privacy.vue":
/*!******************************************************!*\
  !*** ./resources/js/components/frontend/Privacy.vue ***!
  \******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Privacy_vue_vue_type_template_id_02b08536_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Privacy.vue?vue&type=template&id=02b08536&scoped=true& */ "./resources/js/components/frontend/Privacy.vue?vue&type=template&id=02b08536&scoped=true&");
/* harmony import */ var _Privacy_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Privacy.vue?vue&type=script&lang=js& */ "./resources/js/components/frontend/Privacy.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _Privacy_vue_vue_type_style_index_0_id_02b08536_scoped_true_lang_scss___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./Privacy.vue?vue&type=style&index=0&id=02b08536&scoped=true&lang=scss& */ "./resources/js/components/frontend/Privacy.vue?vue&type=style&index=0&id=02b08536&scoped=true&lang=scss&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _Privacy_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Privacy_vue_vue_type_template_id_02b08536_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Privacy_vue_vue_type_template_id_02b08536_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "02b08536",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/frontend/Privacy.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/frontend/Privacy.vue?vue&type=script&lang=js&":
/*!*******************************************************************************!*\
  !*** ./resources/js/components/frontend/Privacy.vue?vue&type=script&lang=js& ***!
  \*******************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Privacy_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./Privacy.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/frontend/Privacy.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Privacy_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/frontend/Privacy.vue?vue&type=style&index=0&id=02b08536&scoped=true&lang=scss&":
/*!****************************************************************************************************************!*\
  !*** ./resources/js/components/frontend/Privacy.vue?vue&type=style&index=0&id=02b08536&scoped=true&lang=scss& ***!
  \****************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_Privacy_vue_vue_type_style_index_0_id_02b08536_scoped_true_lang_scss___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/style-loader!../../../../node_modules/css-loader!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--7-2!../../../../node_modules/sass-loader/dist/cjs.js??ref--7-3!../../../../node_modules/vue-loader/lib??vue-loader-options!./Privacy.vue?vue&type=style&index=0&id=02b08536&scoped=true&lang=scss& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/frontend/Privacy.vue?vue&type=style&index=0&id=02b08536&scoped=true&lang=scss&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_Privacy_vue_vue_type_style_index_0_id_02b08536_scoped_true_lang_scss___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_Privacy_vue_vue_type_style_index_0_id_02b08536_scoped_true_lang_scss___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_Privacy_vue_vue_type_style_index_0_id_02b08536_scoped_true_lang_scss___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_Privacy_vue_vue_type_style_index_0_id_02b08536_scoped_true_lang_scss___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_Privacy_vue_vue_type_style_index_0_id_02b08536_scoped_true_lang_scss___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./resources/js/components/frontend/Privacy.vue?vue&type=template&id=02b08536&scoped=true&":
/*!*************************************************************************************************!*\
  !*** ./resources/js/components/frontend/Privacy.vue?vue&type=template&id=02b08536&scoped=true& ***!
  \*************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Privacy_vue_vue_type_template_id_02b08536_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./Privacy.vue?vue&type=template&id=02b08536&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/frontend/Privacy.vue?vue&type=template&id=02b08536&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Privacy_vue_vue_type_template_id_02b08536_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Privacy_vue_vue_type_template_id_02b08536_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);