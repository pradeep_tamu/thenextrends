(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[91],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/frontend/Contact.vue?vue&type=script&lang=js&":
/*!***************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/frontend/Contact.vue?vue&type=script&lang=js& ***!
  \***************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  name: "Contact",
  data: function data() {
    return {
      form: new Form({
        first_name: '',
        last_name: '',
        email: '',
        message: ''
      })
    };
  },
  created: function created() {
    window.scrollTo(0, 0);
  },
  methods: {
    toggle: function toggle(event) {
      var parent = event.target.parentElement;

      if (parent.className === 'question') {
        parent = parent.parentElement;
      }

      var val = parent.childNodes[4].style.display;

      if (val === "block") {
        parent.childNodes[4].style.display = "none";
        parent.childNodes[0].lastChild.innerHTML = "+";
      } else {
        parent.childNodes[4].style.display = "block";
        parent.childNodes[0].lastChild.innerHTML = "-";
      }
    },
    enquiry: function enquiry() {
      var _this = this;

      this.$Progress.start();
      this.form.post('/api/enquiry').then(function () {
        _this.form.reset();

        Toast.fire({
          type: 'success',
          title: 'Your issue has been registered!!!'
        });

        _this.$Progress.finish();
      })["catch"](function (e) {
        _this.$Progress.fail();

        console.log(e);
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/frontend/Contact.vue?vue&type=style&index=0&id=a8f70c64&scoped=true&lang=scss&":
/*!**************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/sass-loader/dist/cjs.js??ref--7-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/frontend/Contact.vue?vue&type=style&index=0&id=a8f70c64&scoped=true&lang=scss& ***!
  \**************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".faq-section h3[data-v-a8f70c64] {\n  color: #232323;\n  padding: 3rem 0;\n}\n.question[data-v-a8f70c64] {\n  display: flex;\n  justify-content: space-between;\n  align-items: center;\n  color: #232323;\n  cursor: pointer;\n}\n.question h5[data-v-a8f70c64] {\n  margin: 0;\n}\n.question span[data-v-a8f70c64] {\n  font-size: 30px;\n}\n.answer[data-v-a8f70c64] {\n  font-size: 15px;\n  margin-bottom: 2rem;\n  text-align: justify;\n  display: none;\n  color: #4d4d4d;\n}\n.contact-email-section[data-v-a8f70c64] {\n  color: #232323;\n}\n.contact-email-section h3[data-v-a8f70c64] {\n  padding: 2rem 0;\n  text-align: center;\n}\n.contact-email-section h6[data-v-a8f70c64] {\n  margin-bottom: 2rem;\n  color: #4d4d4d;\n}\n.submit-button[data-v-a8f70c64] {\n  display: flex;\n  justify-content: center;\n  margin: 3rem 0;\n}\n.submit-button button[data-v-a8f70c64] {\n  padding: 0.6rem 10rem;\n  background: #e5aeae;\n  border: none;\n  outline: none;\n  color: white;\n  font-size: 14px;\n  text-transform: uppercase;\n  font-weight: bold;\n}", ""]);

// exports


/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/frontend/Contact.vue?vue&type=style&index=0&id=a8f70c64&scoped=true&lang=scss&":
/*!******************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/sass-loader/dist/cjs.js??ref--7-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/frontend/Contact.vue?vue&type=style&index=0&id=a8f70c64&scoped=true&lang=scss& ***!
  \******************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../node_modules/css-loader!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--7-2!../../../../node_modules/sass-loader/dist/cjs.js??ref--7-3!../../../../node_modules/vue-loader/lib??vue-loader-options!./Contact.vue?vue&type=style&index=0&id=a8f70c64&scoped=true&lang=scss& */ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/frontend/Contact.vue?vue&type=style&index=0&id=a8f70c64&scoped=true&lang=scss&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/frontend/Contact.vue?vue&type=template&id=a8f70c64&scoped=true&":
/*!*******************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/frontend/Contact.vue?vue&type=template&id=a8f70c64&scoped=true& ***!
  \*******************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _c("div", { staticClass: "contact-wrapper container" }, [
      _c("div", { staticClass: "faq-section" }, [
        _c("h3", [_vm._v("Frequently Asked Questions")]),
        _vm._v(" "),
        _c("div", { staticClass: "faq" }, [
          _c("div", { staticClass: "question", on: { click: _vm.toggle } }, [
            _c("h5", [
              _vm._v("How does your The NexTrends Delivery Service work? ")
            ]),
            _vm._v(" "),
            _c("span", [_vm._v("+")])
          ]),
          _vm._v(" "),
          _c("hr"),
          _vm._v(" "),
          _vm._m(0)
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "faq" }, [
          _c("div", { staticClass: "question", on: { click: _vm.toggle } }, [
            _c("h5", [_vm._v("How can i track my order? ")]),
            _vm._v(" "),
            _c("span", [_vm._v("+")])
          ]),
          _vm._v(" "),
          _c("hr"),
          _vm._v(" "),
          _vm._m(1)
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "faq" }, [
          _c("div", { staticClass: "question", on: { click: _vm.toggle } }, [
            _c("h5", [_vm._v("What happens on the day of the delivery?")]),
            _vm._v(" "),
            _c("span", [_vm._v("+")])
          ]),
          _vm._v(" "),
          _c("hr"),
          _vm._v(" "),
          _vm._m(2)
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "faq" }, [
          _c("div", { staticClass: "question", on: { click: _vm.toggle } }, [
            _c("h5", [_vm._v("What is your Returns Policy? ")]),
            _vm._v(" "),
            _c("span", [_vm._v("+")])
          ]),
          _vm._v(" "),
          _c("hr"),
          _vm._v(" "),
          _vm._m(3)
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "faq" }, [
          _c("div", { staticClass: "question", on: { click: _vm.toggle } }, [
            _c("h5", [
              _vm._v(
                "How can I return products if they are not suitable for me? "
              )
            ]),
            _vm._v(" "),
            _c("span", [_vm._v("+")])
          ]),
          _vm._v(" "),
          _c("hr"),
          _vm._v(" "),
          _vm._m(4)
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "faq" }, [
          _c("div", { staticClass: "question", on: { click: _vm.toggle } }, [
            _c("h5", [_vm._v("Returning a faulty or incorrect item? ")]),
            _vm._v(" "),
            _c("span", [_vm._v("+")])
          ]),
          _vm._v(" "),
          _c("hr"),
          _vm._v(" "),
          _c("div", { staticClass: "answer" }, [
            _vm._v(
              "\n                    If you've received an item which isn't quite simply return to us, the products need to be in the\n                    original condition.\n                "
            )
          ])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "faq" }, [
          _c("div", { staticClass: "question", on: { click: _vm.toggle } }, [
            _c("h5", [_vm._v("Returning an unwanted item? ")]),
            _vm._v(" "),
            _c("span", [_vm._v("+")])
          ]),
          _vm._v(" "),
          _c("hr"),
          _vm._v(" "),
          _vm._m(5)
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "faq" }, [
          _c("div", { staticClass: "question", on: { click: _vm.toggle } }, [
            _c("h5", [_vm._v("Want to exchange an item?")]),
            _vm._v(" "),
            _c("span", [_vm._v("+")])
          ]),
          _vm._v(" "),
          _c("hr"),
          _vm._v(" "),
          _vm._m(6)
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "faq" }, [
          _c("div", { staticClass: "question", on: { click: _vm.toggle } }, [
            _c("h5", [
              _vm._v("What should I do if my order hasn't been delivered yet? ")
            ]),
            _vm._v(" "),
            _c("span", [_vm._v("+")])
          ]),
          _vm._v(" "),
          _c("hr"),
          _vm._v(" "),
          _vm._m(7)
        ])
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "contact-email-section" }, [
        _c("h3", [_vm._v("CAN'T FIND WHAT YOU'RE LOOKING FOR?")]),
        _vm._v(" "),
        _c("h5", [_vm._v("Email Us")]),
        _vm._v(" "),
        _c("h6", [
          _vm._v(
            "If your enquiry is urgent, please call us during our opening hours. Our\n                current response time through our contact form is 2 business days.\n            "
          )
        ]),
        _vm._v(" "),
        _c(
          "form",
          {
            on: {
              submit: function($event) {
                $event.preventDefault()
                return _vm.enquiry($event)
              }
            }
          },
          [
            _c("div", { staticClass: "row" }, [
              _c(
                "div",
                { staticClass: "form-group col" },
                [
                  _c("label", { attrs: { for: "firstname" } }, [
                    _vm._v("First Name")
                  ]),
                  _vm._v(" "),
                  _c("input", {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.form.first_name,
                        expression: "form.first_name"
                      }
                    ],
                    staticClass: "form-control",
                    class: { "is-invalid": _vm.form.errors.has("firstname") },
                    attrs: {
                      type: "text",
                      id: "firstname",
                      placeholder: "First Name"
                    },
                    domProps: { value: _vm.form.first_name },
                    on: {
                      input: function($event) {
                        if ($event.target.composing) {
                          return
                        }
                        _vm.$set(_vm.form, "first_name", $event.target.value)
                      }
                    }
                  }),
                  _vm._v(" "),
                  _c("has-error", {
                    attrs: { form: _vm.form, field: "firstname" }
                  })
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "div",
                { staticClass: "form-group col" },
                [
                  _c("label", { attrs: { for: "lastname" } }, [
                    _vm._v("Last Name")
                  ]),
                  _vm._v(" "),
                  _c("input", {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.form.last_name,
                        expression: "form.last_name"
                      }
                    ],
                    staticClass: "form-control",
                    class: { "is-invalid": _vm.form.errors.has("lastname") },
                    attrs: {
                      type: "text",
                      id: "lastname",
                      placeholder: "Last Name"
                    },
                    domProps: { value: _vm.form.last_name },
                    on: {
                      input: function($event) {
                        if ($event.target.composing) {
                          return
                        }
                        _vm.$set(_vm.form, "last_name", $event.target.value)
                      }
                    }
                  }),
                  _vm._v(" "),
                  _c("has-error", {
                    attrs: { form: _vm.form, field: "lastname" }
                  })
                ],
                1
              )
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "row" }, [
              _c(
                "div",
                { staticClass: "form-group col" },
                [
                  _c("label", { attrs: { for: "email" } }, [
                    _vm._v("Email address")
                  ]),
                  _vm._v(" "),
                  _c("input", {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.form.email,
                        expression: "form.email"
                      }
                    ],
                    staticClass: "form-control",
                    class: { "is-invalid": _vm.form.errors.has("email") },
                    attrs: {
                      type: "email",
                      id: "email",
                      "aria-describedby": "emailHelp",
                      placeholder: "Enter your email address"
                    },
                    domProps: { value: _vm.form.email },
                    on: {
                      input: function($event) {
                        if ($event.target.composing) {
                          return
                        }
                        _vm.$set(_vm.form, "email", $event.target.value)
                      }
                    }
                  }),
                  _vm._v(" "),
                  _c("has-error", {
                    attrs: { form: _vm.form, field: "email" }
                  }),
                  _vm._v(" "),
                  _c(
                    "small",
                    {
                      staticClass: "form-text text-muted",
                      attrs: { id: "emailHelp" }
                    },
                    [
                      _vm._v(
                        "We'll never share your email with anyone\n                            else.\n                        "
                      )
                    ]
                  )
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "div",
                { staticClass: "form-group col" },
                [
                  _c("label", { attrs: { for: "message" } }, [
                    _vm._v("Message")
                  ]),
                  _vm._v(" "),
                  _c("textarea", {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.form.message,
                        expression: "form.message"
                      }
                    ],
                    staticClass: "form-control",
                    class: { "is-invalid": _vm.form.errors.has("message") },
                    attrs: { id: "message", rows: "3" },
                    domProps: { value: _vm.form.message },
                    on: {
                      input: function($event) {
                        if ($event.target.composing) {
                          return
                        }
                        _vm.$set(_vm.form, "message", $event.target.value)
                      }
                    }
                  }),
                  _vm._v(" "),
                  _c("has-error", {
                    attrs: { form: _vm.form, field: "message" }
                  })
                ],
                1
              )
            ]),
            _vm._v(" "),
            _vm._m(8)
          ]
        )
      ])
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "answer" }, [
      _c("p", [
        _vm._v(
          "General delivery fees are Rs,50 within Kathmandu valley and Rs 100 outside of the valley."
        )
      ]),
      _vm._v(" "),
      _c("p", [
        _vm._v(
          "Delivery can take up to 7-10 working days* from the moment your order is submitted."
        )
      ]),
      _vm._v(" "),
      _c("p", [
        _vm._v(
          "Delivery is Sunday to Friday (excluding Saturday and bank holidays)."
        )
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "answer" }, [
      _c("p", [
        _vm._v(
          "Delivery notifications and tracking are provided by our delivery partner."
        )
      ]),
      _vm._v(" "),
      _c("p", [
        _vm._v(
          " We will email you a link to your tracking information once your parcel has been shipped from\n                        our\n                        warehouse.\n                        You can also track your order by logging into 'My Account' and viewing your most recent\n                        orders.\n                        Just click 'Track This Order' and you'll be directed to our delivery partner's tracking\n                        page."
        )
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "answer" }, [
      _c("p", [_vm._v("A signature may be required on receipt.")]),
      _vm._v(" "),
      _c("p", [
        _vm._v(
          "Dependant on the delivery partner, you might be presented with additional delivery features\n                        including\n                        specified delivery window slots and follow my parcel GPS tracking, so you'll know exactly\n                        when\n                        to\n                        expect your delivery and the location of your order -\n                        just keep an eye on your tracking, or for an email from our delivery partner, for the latest\n                        updates."
        )
      ]),
      _vm._v(" "),
      _c("p", [
        _vm._v(
          " If you're not available to take delivery of your parcel, our delivery partner will leave a\n                        calling\n                        card advising if your parcel has been delivered to a safe location, or how to arrange\n                        collection\n                        of\n                        your delivery.\n                    "
        )
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "answer" }, [
      _c("p", [
        _vm._v(
          "We don't accept returns for unwanted items after the relevant returns period above. If you\n                        try to\n                        make a return, we may have to send it back to your default delivery address and ask you to\n                        cover\n                        the\n                        delivery costs."
        )
      ]),
      _vm._v(" "),
      _c("p", [
        _c("b", [_vm._v("Proof of postage")]),
        _c("br"),
        _vm._v(
          "\n                        As the parcel remains your responsibility until it arrives back with us, ensure that you get\n                        proof\n                        of postage in case you need to contact us about your return."
        )
      ]),
      _vm._v(" "),
      _c("p", [
        _c("b", [_vm._v("Keep it clean")]),
        _c("br"),
        _vm._v(
          "\n                        Some items can't be returned, like earrings or swimwear and underwear for hygienic purposes.\n                        "
        ),
        _c("br"),
        _vm._v(
          "\n                        Please check product descriptions before you order. If you do want to return underwear,\n                        swimwear,\n                        makeup/Face + Body products or any items marked with a '+' next to the product name, please\n                        do\n                        not\n                        remove the original wrapping/seal or we will not be able to accept your return."
        )
      ]),
      _vm._v(" "),
      _c("p", [
        _c("b", [_vm._v("Original condition")]),
        _c("br"),
        _vm._v(
          "\n                        Of course, it's fine to try an item on like you would in a shop, but please don't actually\n                        wear\n                        it.\n                        If an item is returned to us damaged, worn or in an unsuitable condition, we won't be able\n                        to\n                        give\n                        you a refund and we may have to send it back to you (and ask you to cover the delivery\n                        costs).\n                        All\n                        items are inspected on return."
        )
      ]),
      _vm._v(" "),
      _c("p", [
        _vm._v(
          "All returned items should be sent in their original condition and packaging where possible,\n                        including\n                        tags (e.g., shoes should be returned with the original shoe box)."
        )
      ]),
      _vm._v(" "),
      _c("p", [
        _c("b", [_vm._v("Responsibility")]),
        _c("br"),
        _vm._v(
          "\n                        Returned items are your responsibility until they reach us, so make sure they're packed up\n                        properly\n                        and can't get damaged on the way!"
        )
      ]),
      _vm._v(" "),
      _c("p", [
        _vm._v(
          "We're not responsible for any items that are returned to us by mistake (it happens!). If\n                        we're\n                        able\n                        to locate the items (it's not always possible) and you'd like these returned to you, we may\n                        ask\n                        you\n                        to cover the delivery cost."
        )
      ]),
      _vm._v(" "),
      _c("p", [
        _c("b", [_vm._v("Fair use")]),
        _c("br"),
        _vm._v(
          "\n                        If we think there is any suspicious behaviour regarding your returns, we might have to\n                        deactivate\n                        your account and any associated accounts. Some examples of these behaviours could be if we\n                        suspect\n                        someone is actually wearing their purchases and then returning them, ordering and returning\n                        items\n                        frequently, or the items returned don't match what you ordered."
        ),
        _c("br"),
        _vm._v(
          "\n                        If this happens to you and you think we've made a mistake, please get in touch with us and\n                        we\n                        will\n                        be happy to discuss it with you."
        ),
        _c("br"),
        _vm._v(
          "\n                        Please note, we reserve the right to take legal action against you if the items you return\n                        don't\n                        match what you ordered."
        )
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "answer" }, [
      _c("p", [
        _vm._v(
          "It can take up to 7 working days (excluding weekends and bank holidays) from the day after\n                        the\n                        date\n                        of your return, for your parcel to be delivered back to our warehouse and processed."
        ),
        _c("br"),
        _vm._v(
          "\n                        We'll send you an email as soon as we've completed your return, letting you know whether a\n                        refund or\n                        exchange has been processed.\n                    "
        )
      ]),
      _vm._v(" "),
      _c("p", [
        _vm._v(
          "Any refund will be automatically issued to the payment method you used to place your original\n                        order.\n                        This typically takes 10 working days in Nepal, depending on your payment method issuer.\n                    "
        )
      ]),
      _vm._v(" "),
      _c("p", [
        _vm._v(
          "You have the choice of a refund or a credit. If you choose not to be refunded, a credit will\n                        be\n                        added\n                        in your account. We will add Rs 50 more for returns over Rs 5000 and Rs 100 for returns over\n                        Rs10,000 order value. This credit can be used for your future purchases.\n                    "
        )
      ]),
      _vm._v(" "),
      _c("p", [
        _vm._v(
          "If you've returned more than one order in the same parcel, please allow 24 hours for all your\n                        returns\n                        to be completed.\n                    "
        )
      ]),
      _vm._v(" "),
      _c("p", [
        _vm._v(
          "If your returns haven't reached us after the returns timeframe, please get in touch using one\n                        of\n                        the\n                        contact options below so we can help you further.\n                    "
        )
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "answer" }, [
      _c("p", [
        _vm._v(
          "We get it, sometimes something just doesn't work for you and you want your money back.\n                        Don't worry, as long as an item is still in its original condition, we accept returns for\n                        free,\n                        subject to the rules below, which includes rules around fair use. None of these rules affect\n                        your\n                        statutory rights."
        )
      ]),
      _vm._v(" "),
      _c("p", [
        _vm._v(
          "If you return an item requesting a refund within 15 days of receiving it, we'll give you a\n                        full\n                        refund\n                        by way of the original payment method."
        )
      ]),
      _vm._v(" "),
      _c("p", [
        _vm._v(
          "We aim to refund you within 14 days of receiving the returned item."
        )
      ]),
      _vm._v(" "),
      _c("p", [
        _vm._v(
          "If you request a refund for an item during the above time frames but you can't return it to\n                        us\n                        for\n                        some reason, please get in touch - but any refund will be at our discretion."
        )
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "answer" }, [
      _c("p", [
        _vm._v(
          "You can also exchange your item within 15 days of delivery, as long as the new item has the\n                        same\n                        product code as the original thing you ordered. If you want to exchange for a different\n                        colour,\n                        the\n                        price of both colours must be the same as the price at which you bought the original\n                        product."
        )
      ]),
      _vm._v(" "),
      _c("p", [
        _vm._v(
          "You'll know the new item has the same product code as your original, if the size or colour is\n                        available in the drop-down menu on the product page of the original piece you ordered."
        )
      ]),
      _vm._v(" "),
      _c("p", [
        _vm._v(
          "We'll send your replacement to the same delivery address on the same delivery method as your\n                        original\n                        order. To check or update your address details, log into My Account and click 'edit' below\n                        your\n                        Address Book (remember to click 'Save' when you're done)."
        )
      ]),
      _vm._v(" "),
      _c("p", [
        _vm._v(
          "If the item is sold out in the size or colour you have chosen, a refund will be issued to\n                        your\n                        original payment method. "
        ),
        _c("br"),
        _vm._v(
          "\n                        If you want a different item, the item you want is sold under a different product code, or\n                        the\n                        item\n                        you want is a different price to the price at which you bought the original, you'll need to\n                        return\n                        the unwanted item for a refund and place a new order."
        ),
        _c("br")
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "answer" }, [
      _c("p", [
        _vm._v(
          "Your estimated delivery date is in your Order Confirmation email – please allow until\n                        this\n                        date\n                        for your order to arrive."
        )
      ]),
      _vm._v(" "),
      _c("p", [
        _vm._v(
          "You’ll be able to get the latest updates on your order by clicking the tracking link in\n                        your\n                        shipping confirmation email. Alternatively, you can log into ‘My Account’ and\n                        click\n                        ‘Track This Order’."
        )
      ]),
      _vm._v(" "),
      _c("p", [
        _vm._v(
          "Your tracking link will be able to provide up to date information on the status of your\n                        order."
        )
      ]),
      _vm._v(" "),
      _c("p", [
        _vm._v(
          "If your estimated delivery date has passed and you haven’t received your order, please\n                        get\n                        in\n                        touch with our Customer Care team on one of the options below so we can help you\n                        further."
        )
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "submit-button" }, [
      _c("button", { staticClass: "btn", attrs: { type: "submit" } }, [
        _vm._v("Submit")
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/components/frontend/Contact.vue":
/*!******************************************************!*\
  !*** ./resources/js/components/frontend/Contact.vue ***!
  \******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Contact_vue_vue_type_template_id_a8f70c64_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Contact.vue?vue&type=template&id=a8f70c64&scoped=true& */ "./resources/js/components/frontend/Contact.vue?vue&type=template&id=a8f70c64&scoped=true&");
/* harmony import */ var _Contact_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Contact.vue?vue&type=script&lang=js& */ "./resources/js/components/frontend/Contact.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _Contact_vue_vue_type_style_index_0_id_a8f70c64_scoped_true_lang_scss___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./Contact.vue?vue&type=style&index=0&id=a8f70c64&scoped=true&lang=scss& */ "./resources/js/components/frontend/Contact.vue?vue&type=style&index=0&id=a8f70c64&scoped=true&lang=scss&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _Contact_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Contact_vue_vue_type_template_id_a8f70c64_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Contact_vue_vue_type_template_id_a8f70c64_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "a8f70c64",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/frontend/Contact.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/frontend/Contact.vue?vue&type=script&lang=js&":
/*!*******************************************************************************!*\
  !*** ./resources/js/components/frontend/Contact.vue?vue&type=script&lang=js& ***!
  \*******************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Contact_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./Contact.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/frontend/Contact.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Contact_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/frontend/Contact.vue?vue&type=style&index=0&id=a8f70c64&scoped=true&lang=scss&":
/*!****************************************************************************************************************!*\
  !*** ./resources/js/components/frontend/Contact.vue?vue&type=style&index=0&id=a8f70c64&scoped=true&lang=scss& ***!
  \****************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_Contact_vue_vue_type_style_index_0_id_a8f70c64_scoped_true_lang_scss___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/style-loader!../../../../node_modules/css-loader!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--7-2!../../../../node_modules/sass-loader/dist/cjs.js??ref--7-3!../../../../node_modules/vue-loader/lib??vue-loader-options!./Contact.vue?vue&type=style&index=0&id=a8f70c64&scoped=true&lang=scss& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/frontend/Contact.vue?vue&type=style&index=0&id=a8f70c64&scoped=true&lang=scss&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_Contact_vue_vue_type_style_index_0_id_a8f70c64_scoped_true_lang_scss___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_Contact_vue_vue_type_style_index_0_id_a8f70c64_scoped_true_lang_scss___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_Contact_vue_vue_type_style_index_0_id_a8f70c64_scoped_true_lang_scss___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_Contact_vue_vue_type_style_index_0_id_a8f70c64_scoped_true_lang_scss___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_Contact_vue_vue_type_style_index_0_id_a8f70c64_scoped_true_lang_scss___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./resources/js/components/frontend/Contact.vue?vue&type=template&id=a8f70c64&scoped=true&":
/*!*************************************************************************************************!*\
  !*** ./resources/js/components/frontend/Contact.vue?vue&type=template&id=a8f70c64&scoped=true& ***!
  \*************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Contact_vue_vue_type_template_id_a8f70c64_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./Contact.vue?vue&type=template&id=a8f70c64&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/frontend/Contact.vue?vue&type=template&id=a8f70c64&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Contact_vue_vue_type_template_id_a8f70c64_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Contact_vue_vue_type_template_id_a8f70c64_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);