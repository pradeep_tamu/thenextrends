(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[72],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/frontend/Home.vue?vue&type=script&lang=js&":
/*!************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/frontend/Home.vue?vue&type=script&lang=js& ***!
  \************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _helpers_AddToWishList__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./helpers/AddToWishList */ "./resources/js/components/frontend/helpers/AddToWishList.vue");


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  name: "Home",
  components: {
    WishList: _helpers_AddToWishList__WEBPACK_IMPORTED_MODULE_1__["default"]
  },
  data: function data() {
    return {
      isMen: false,
      isWomen: true,
      brandImage: '',
      trendingProducts: {
        men: {},
        women: {}
      },
      form: new Form({
        email: ''
      })
    };
  },
  created: function created() {
    var _this = this;

    axios.get('/api/getTrending').then(function (data) {
      _this.trendingProducts.men = data.data[0];
      _this.trendingProducts.women = data.data[1];
      console.log('here');
      console.log(_this.trendingProducts.men);
    });
  },
  mounted: function mounted() {
    this.loadModal();
  },
  methods: {
    loadModal: function () {
      var _loadModal = _asyncToGenerator(
      /*#__PURE__*/
      _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                if (sessionStorage.getItem('isInitial') != '1') {
                  setTimeout(function () {
                    return swal.fire({
                      imageUrl: '/image/banner.jpg',
                      imageWidth: 500,
                      imageHeight: 200,
                      imageAlt: 'Custom image',
                      showClass: {
                        popup: 'animated wobble slower '
                      },
                      hideClass: {
                        popup: 'animated fadeOutUp '
                      },
                      html: '<span>Sign up to get</span> <br><br>' + '<span style="font-size: 1.5rem; font-family: serif; font-weight: bold">Rs.2,000 off your next order</span> <br><br>  ' + '<span style="font-size: 0.8rem">Min spend Rs10,000. Full price items only. One use per customer </span><br> ' + '<span style="text-decoration: underline; font-size: 0.8rem">terms and conditions apply</span>',
                      showCloseButton: true,
                      showCancelButton: true,
                      focusConfirm: false,
                      input: 'email',
                      inputAttributes: {
                        autocapitalize: 'off'
                      },
                      confirmButtonText: 'Subscribe',
                      cancelButtonText: 'No Thanks',
                      showLoaderOnConfirm: true,
                      inputPlaceholder: 'Enter your email address'
                    }).then(function (result) {
                      // Send request to the server
                      if (result.value) {
                        axios.post('/api/newsletter', {
                          email: result.value
                        }).then(function () {
                          Toast.fire({
                            type: 'success',
                            title: 'Thank You For Your Subscription!!!'
                          });
                        })["catch"](function (e) {
                          console.log(e);
                        });
                      }
                    });
                  }, 5000);
                  sessionStorage.setItem('isInitial', '1');
                }

              case 1:
              case "end":
                return _context.stop();
            }
          }
        }, _callee);
      }));

      function loadModal() {
        return _loadModal.apply(this, arguments);
      }

      return loadModal;
    }(),
    goFilter: function goFilter(gender, subCategory, category) {
      this.$router.push({
        name: 'filter',
        params: {
          gender: gender,
          subCategory: subCategory,
          category: category
        }
      });
    },
    getImage: function getImage(src) {
      return "/image/thumbnail/" + src;
    },
    getCoverImage: function getCoverImage() {
      return "/image/women-home.jpg";
    },
    getCoverImage1: function getCoverImage1() {
      return "/image/men-home.jpg";
    },
    getOffer1: function getOffer1() {
      return "/image/Denim.jpg";
    },
    getOffer2: function getOffer2() {
      return "/image/winter.jpg";
    },
    showMens: function showMens() {
      this.isMen = true;
      this.isWomen = false;
    },
    showWomens: function showWomens() {
      this.isMen = false;
      this.isWomen = true;
    },
    subscribe: function subscribe() {
      var _this2 = this;

      this.form.post('/api/newsletter').then(function () {
        _this2.form.reset();

        Toast.fire({
          type: 'success',
          title: 'Thank You For Your Subscription!!!'
        });
      })["catch"](function (e) {
        console.log(e);
      });
    },
    goFilterBrand: function goFilterBrand() {
      this.$router.push({
        name: 'filter',
        params: {
          gender: 'All'
        },
        query: {
          brand: 'Denim'
        }
      });
    },
    goFilterSales: function goFilterSales(gender, category, sales) {
      this.$router.push({
        name: 'filter',
        params: {
          gender: gender
        },
        query: {
          sales: sales
        }
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/frontend/helpers/AddToWishList.vue?vue&type=script&lang=js&":
/*!*****************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/frontend/helpers/AddToWishList.vue?vue&type=script&lang=js& ***!
  \*****************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'add-to-wishList',
  props: {
    product: Object
  },
  data: function data() {
    return {
      item: {}
    };
  },
  methods: {
    addToWish: function addToWish(e) {
      var _this = this;

      this.item['productId'] = this.product.id;
      this.item['slug'] = this.product.slug;
      this.item['icon'] = 'fas';
      var found = this.$store.getters.getWish.find(function (product) {
        return product.productId == _this.product.id;
      });

      if (found) {
        this.$store.commit("removeWish", this.item);
        e.target.classList.remove('fas');
        e.target.classList.add('far');
        Toast.fire({
          type: 'error',
          title: 'You have removed the product from your wish list !!!'
        });
      } else {
        this.$store.commit("addToWish", this.item);
        e.target.classList.remove('far');
        e.target.classList.add('fas');
        Toast.fire({
          type: 'success',
          title: 'You have successfully added your product to your wish list !!!'
        });
      }
    },
    getHeart: function getHeart() {
      var _this2 = this;

      var found = this.$store.getters.getWish.find(function (product) {
        return product.productId === _this2.product.id;
      });

      if (found) {
        return 'fas';
      } else {
        return 'far';
      }
    }
  }
});

/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/frontend/Home.vue?vue&type=style&index=0&id=42d334a1&lang=scss&scoped=true&":
/*!***********************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/sass-loader/dist/cjs.js??ref--7-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/frontend/Home.vue?vue&type=style&index=0&id=42d334a1&lang=scss&scoped=true& ***!
  \***********************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".sales-offer[data-v-42d334a1] {\n  height: 10rem;\n  background-color: grey;\n  margin: 0 6.5%;\n  margin-bottom: 3rem;\n}\n.swal2-image[data-v-42d334a1] {\n  width: 100% !important;\n}\n.swal2-content[data-v-42d334a1] {\n  font-size: 0.8rem !important;\n}\n.show_active[data-v-42d334a1] {\n  border-bottom: 3px solid #42abc8;\n}\nh2[data-v-42d334a1] {\n  font-family: Roboto;\n}\n.shop-by-category[data-v-42d334a1] {\n  padding: 5rem;\n}\n.shop-by-category h3[data-v-42d334a1] {\n  padding-left: 0.8rem;\n  font-family: Roboto;\n  font-weight: bold;\n  margin-bottom: 1rem;\n}\n.shop-by-category .shop-by-category-title[data-v-42d334a1] {\n  display: flex;\n  justify-content: center;\n  margin-bottom: 1.5rem;\n}\n.shop-by-category .shop-by-category-title .category-link[data-v-42d334a1] {\n  cursor: pointer;\n  font-size: 1.5rem;\n  font-family: Roboto;\n}\n.shop-by-category .shop-by-category-body .card-box[data-v-42d334a1] {\n  height: 10rem;\n  margin-bottom: 1rem;\n}\n.shop-by-category .shop-by-category-body .card-box p[data-v-42d334a1] {\n  border: 1px solid #dadada;\n  width: 100%;\n  margin: 0;\n  height: 100%;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  background: #dadada;\n  color: rgba(0, 0, 0, 0.7);\n  font-size: 20px;\n  font-family: none;\n  font-weight: bold;\n  cursor: pointer;\n}\n.shop-by-category .shop-by-category-body .card-box p[data-v-42d334a1]:hover {\n  box-shadow: 0 3px 5px 0 rgba(0, 0, 0, 0.08);\n  transition: box-shadow 0.3s ease-in-out;\n  transform: scale(1.1);\n}\na[data-v-42d334a1] {\n  text-decoration: none;\n}\n.row[data-v-42d334a1] {\n  margin: 0;\n}\n.home-slider[data-v-42d334a1] {\n  display: flex;\n  background: black;\n  height: 40vh;\n}\n@media screen and (min-width: 1100px) {\n.home-slider[data-v-42d334a1] {\n    height: 70vh;\n}\n}\n.home-slider img[data-v-42d334a1] {\n  opacity: 0.5;\n}\n.home-slider .men-cover[data-v-42d334a1] {\n  width: 50%;\n}\n.home-slider .women-cover[data-v-42d334a1] {\n  width: 50%;\n}\n.home-slider .shop-men[data-v-42d334a1], .home-slider .shop-women[data-v-42d334a1] {\n  display: flex;\n  justify-content: center;\n  font-size: 3rem;\n  position: relative;\n  top: -50%;\n}\n@media screen and (max-width: 767px) {\n.home-slider .shop-men[data-v-42d334a1], .home-slider .shop-women[data-v-42d334a1] {\n    font-size: 1rem;\n}\n}\n.home-slider .shop-men a[data-v-42d334a1], .home-slider .shop-women a[data-v-42d334a1] {\n  color: white !important;\n}\n.home-slider .home_slider_image[data-v-42d334a1] {\n  height: 100%;\n  width: 100%;\n  -o-object-fit: cover;\n     object-fit: cover;\n}\n.trending-link[data-v-42d334a1]:hover {\n  cursor: pointer;\n  color: #38A1DA;\n}\n.offer-banner .row[data-v-42d334a1] {\n  margin: 0;\n}\n.offer-banner .row span[data-v-42d334a1] {\n  font-size: 1.5rem;\n}\n@media screen and (max-width: 996px) {\n.offer-banner .row span[data-v-42d334a1] {\n    font-size: 0.8rem;\n}\n}\n.offer-banner .row .kathmandu[data-v-42d334a1] {\n  font-size: 1.3rem;\n}\n@media screen and (max-width: 996px) {\n.offer-banner .row .kathmandu[data-v-42d334a1] {\n    font-size: 0.8rem;\n}\n}\n.offer-banner .row .shop-now[data-v-42d334a1] {\n  color: whitesmoke;\n  /*font-weight: bold;*/\n  background: #63af97;\n}\n.offer-banner .row .order-return-banner[data-v-42d334a1] {\n  color: #435061;\n  /*font-weight: bold;*/\n  background: #e8e2c8;\n}\n.offer-banner .row .shipping-banner[data-v-42d334a1] {\n  color: whitesmoke;\n  /*font-weight: bold;*/\n  background: #da5938;\n}\n.offer-banner .row .shop-now[data-v-42d334a1], .offer-banner .row .order-return-banner[data-v-42d334a1], .offer-banner .row .shipping-banner[data-v-42d334a1] {\n  display: flex;\n  flex-direction: column;\n  justify-content: center;\n  align-items: center;\n  padding: 2.5rem;\n  font-family: Titillium web;\n}\n@media screen and (max-width: 767px) {\n.offer-banner .row .shop-now[data-v-42d334a1], .offer-banner .row .order-return-banner[data-v-42d334a1], .offer-banner .row .shipping-banner[data-v-42d334a1] {\n    padding: 1rem;\n}\n}\n@media screen and (min-width: 768px) and (max-width: 1025px) {\n.offer-banner .row .shop-now[data-v-42d334a1], .offer-banner .row .order-return-banner[data-v-42d334a1], .offer-banner .row .shipping-banner[data-v-42d334a1] {\n    padding: 1.5rem;\n}\n}\n.home-trending .trending-menu span[data-v-42d334a1] {\n  font-size: 1.5rem;\n}\n.product-padding[data-v-42d334a1] {\n  padding: 0 5rem;\n}\n@media screen and (max-width: 1100px) {\n.product-padding[data-v-42d334a1] {\n    padding: 0 3rem;\n}\n}\n@media screen and (max-width: 676px) {\n.product-padding[data-v-42d334a1] {\n    padding: 0 0.5rem;\n}\n}\n.collection-offer[data-v-42d334a1] {\n  margin-bottom: 2rem;\n}\n@media screen and (max-width: 676px) {\n.collection-offer[data-v-42d334a1] {\n    flex-direction: column;\n}\n}\n.collection-offer .image-section[data-v-42d334a1] {\n  height: 30rem;\n  width: 50%;\n  margin-right: 3%;\n  margin-left: 7%;\n  background: black;\n}\n@media screen and (max-width: 676px) {\n.collection-offer .image-section[data-v-42d334a1] {\n    height: 40vh;\n    width: 90%;\n}\n}\n.collection-offer .image-section .text[data-v-42d334a1] {\n  position: relative;\n  top: -100%;\n  display: flex;\n  align-items: center;\n  flex-direction: column;\n  justify-content: center;\n  height: 100%;\n}\n.collection-offer .image-section .text button[data-v-42d334a1] {\n  font-size: 1.5rem;\n  color: #232323;\n  padding: 0.5rem 6rem;\n  font-weight: bold;\n  font-family: Roboto;\n}\n@media only screen and (max-width: 767px) {\n.collection-offer .image-section .text button[data-v-42d334a1] {\n    font-size: 10px;\n    padding: 4px 20px;\n}\n}\n.collection-offer .image-section img[data-v-42d334a1] {\n  height: 100%;\n  width: 100%;\n  -o-object-fit: cover;\n     object-fit: cover;\n}\n.collection-content[data-v-42d334a1] {\n  height: 30rem;\n  width: 33%;\n  background: black;\n}\n@media screen and (max-width: 676px) {\n.collection-content[data-v-42d334a1] {\n    height: 40vh;\n    width: 90%;\n    margin: 5%;\n}\n}\n.collection-content .text[data-v-42d334a1] {\n  position: relative;\n  top: -35%;\n  display: flex;\n  align-items: center;\n  flex-direction: column;\n  justify-content: center;\n}\n.collection-content img[data-v-42d334a1] {\n  height: 100%;\n  width: 100%;\n  opacity: 0.9;\n  -o-object-fit: cover;\n     object-fit: cover;\n}\n.top-1[data-v-42d334a1] {\n  font-size: 3rem;\n  color: white;\n  font-family: Roboto;\n  text-align: center;\n  letter-spacing: 10px;\n  line-height: 1.1;\n}\n.top-1 .denim[data-v-42d334a1] {\n  font-size: 3rem;\n  font-family: serif;\n}\n@media screen and (max-width: 676px) {\n.top-1[data-v-42d334a1] {\n    font-size: 1.5rem;\n    letter-spacing: 4px;\n}\n.top-1 .denim[data-v-42d334a1] {\n    font-size: 1rem;\n    font-family: serif;\n}\n}\n.top-2[data-v-42d334a1] {\n  font-size: 3rem;\n  color: white;\n  font-family: Roboto;\n}\n@media screen and (max-width: 676px) {\n.top-2[data-v-42d334a1] {\n    font-size: 1.5rem;\n}\n}\n.top-3[data-v-42d334a1] {\n  font-size: 2rem;\n  color: white;\n}\n@media screen and (max-width: 676px) {\n.top-3[data-v-42d334a1] {\n    font-size: 1rem;\n}\n}\n.top-4[data-v-42d334a1] {\n  margin-right: 3rem;\n  font-family: cursive;\n  font-size: 2rem;\n  color: white;\n  font-weight: bold;\n}\n@media screen and (max-width: 676px) {\n.top-4[data-v-42d334a1] {\n    margin-right: 0;\n    font-size: 1.5rem;\n}\n}\n.sales-offer[data-v-42d334a1] {\n  display: flex;\n  justify-content: center;\n  align-items: center;\n}\n@media screen and (max-width: 676px) {\n.sales-offer[data-v-42d334a1] {\n    flex-direction: column;\n}\n}\n.latest-arrival-menu[data-v-42d334a1] {\n  display: flex;\n  justify-content: space-evenly;\n  font-size: 1rem;\n  font-weight: bolder;\n}\n.home-notification[data-v-42d334a1] {\n  background: #7e6f70;\n  padding: 2rem 0;\n}\n.home-notification .notification-title[data-v-42d334a1] {\n  text-align: center;\n}\n.home-notification .notification-title span[data-v-42d334a1] {\n  font-family: none;\n  font-size: 2.5rem;\n  margin: 2rem;\n  font-weight: bolder;\n  text-align: center;\n  color: whitesmoke;\n}\n@media screen and (max-width: 767px) {\n.home-notification .notification-title span[data-v-42d334a1] {\n    font-size: 1.3rem;\n    margin: 1rem;\n}\n}\n.home-notification .notification-title button[data-v-42d334a1] {\n  border-radius: 25px;\n  padding: 1rem 2.5rem;\n  outline: none;\n  border: none;\n  font-weight: bold;\n  background: #ff7a00;\n  color: whitesmoke;\n}\n@media screen and (max-width: 996px) {\n.home-notification .notification-title button[data-v-42d334a1] {\n    padding: 1rem 1rem;\n}\n}\n.card[data-v-42d334a1] {\n  border: none;\n  box-shadow: none;\n  background: transparent;\n}\n.container[data-v-42d334a1] {\n  max-width: 80%;\n}\n.brand[data-v-42d334a1], .name[data-v-42d334a1] {\n  font-style: normal;\n  color: #4d4d4d;\n  padding: 0;\n  font-family: Titillium Web;\n}\n.brand[data-v-42d334a1] {\n  font-weight: bold;\n}\n.card-body[data-v-42d334a1] {\n  padding: 0.5rem;\n}\n.heart[data-v-42d334a1] {\n  display: flex;\n  justify-content: flex-end;\n}\n.desc-padd[data-v-42d334a1] {\n  padding: 0;\n}\n.price-cut[data-v-42d334a1] {\n  background-color: transparent;\n  background-image: gradient(linear, 19.1% -7.9%, 81% 107.9%, color-stop(0, transparent), color-stop(0.48, transparent), color-stop(0.5, #000), color-stop(0.52, transparent), color-stop(1, transparent));\n  background-image: repeating-linear-gradient(172deg, transparent 0%, transparent 46%, black 50%, transparent 54%, transparent 100%);\n}\n.price-off[data-v-42d334a1] {\n  color: orangered;\n}\n.home-notification .row[data-v-42d334a1] {\n  justify-content: center;\n}\n.top-brands-container[data-v-42d334a1] {\n  font-family: Roboto;\n  font-weight: bold;\n  margin: 0 6rem;\n}\n.top-brands-container .brand-body[data-v-42d334a1] {\n  flex-wrap: wrap;\n}\n.top-brands-container .brand-body .image-brand .image[data-v-42d334a1] {\n  margin-bottom: 1rem;\n  position: relative;\n  cursor: pointer;\n}\n.top-brands-container .brand-body .image-brand .image img[data-v-42d334a1] {\n  opacity: 1;\n  display: block;\n  width: 100%;\n  height: auto;\n  transition: 0.5s ease;\n  -webkit-backface-visibility: hidden;\n          backface-visibility: hidden;\n}\n.top-brands-container .brand-body .image-brand .image .middle[data-v-42d334a1] {\n  transition: 0.5s ease;\n  opacity: 0;\n  position: absolute;\n  top: 50%;\n  left: 50%;\n  transform: translate(-50%, -50%);\n  -ms-transform: translate(-50%, -50%);\n  text-align: center;\n}\n.top-brands-container .brand-body .image-brand .image .middle .text[data-v-42d334a1] {\n  color: black;\n  font-size: 1rem;\n  font-weight: bold;\n}\n.top-brands-container .brand-body .image-brand .image:hover img[data-v-42d334a1] {\n  opacity: 0.3;\n}\n.top-brands-container .brand-body .image-brand .image:hover .middle[data-v-42d334a1] {\n  opacity: 1;\n}\n.shop-by-category-title[data-v-42d334a1] {\n  display: flex;\n  justify-content: center;\n  margin-bottom: 1.5rem;\n}\n.shop-by-category-title .category-link[data-v-42d334a1] {\n  cursor: pointer;\n  font-size: 1.5rem;\n  font-family: Roboto;\n}\n@media only screen and (max-width: 767px) {\n.desktop-view[data-v-42d334a1] {\n    display: none;\n}\n}\n@media only screen and (min-width: 767px) {\n.mobile-view[data-v-42d334a1] {\n    display: none;\n}\n}\n.top-5[data-v-42d334a1] {\n  font-size: 23px;\n  color: white;\n  padding: 1px 8px;\n  border: 2px solid grey;\n  background: #343534;\n  margin-bottom: 2rem;\n}\n@media only screen and (max-width: 767px) {\n.top-5[data-v-42d334a1] {\n    font-size: 12px;\n    padding: 1px 8px;\n}\n}", ""]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/frontend/helpers/AddToWishList.vue?vue&type=style&index=0&id=045a22a5&scoped=true&lang=css&":
/*!************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/frontend/helpers/AddToWishList.vue?vue&type=style&index=0&id=045a22a5&scoped=true&lang=css& ***!
  \************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\ni[data-v-045a22a5]:hover{\n    cursor: pointer;\n}\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/frontend/Home.vue?vue&type=style&index=0&id=42d334a1&lang=scss&scoped=true&":
/*!***************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/sass-loader/dist/cjs.js??ref--7-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/frontend/Home.vue?vue&type=style&index=0&id=42d334a1&lang=scss&scoped=true& ***!
  \***************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../node_modules/css-loader!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--7-2!../../../../node_modules/sass-loader/dist/cjs.js??ref--7-3!../../../../node_modules/vue-loader/lib??vue-loader-options!./Home.vue?vue&type=style&index=0&id=42d334a1&lang=scss&scoped=true& */ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/frontend/Home.vue?vue&type=style&index=0&id=42d334a1&lang=scss&scoped=true&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/frontend/helpers/AddToWishList.vue?vue&type=style&index=0&id=045a22a5&scoped=true&lang=css&":
/*!****************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/frontend/helpers/AddToWishList.vue?vue&type=style&index=0&id=045a22a5&scoped=true&lang=css& ***!
  \****************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../../node_modules/css-loader??ref--6-1!../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../node_modules/postcss-loader/src??ref--6-2!../../../../../node_modules/vue-loader/lib??vue-loader-options!./AddToWishList.vue?vue&type=style&index=0&id=045a22a5&scoped=true&lang=css& */ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/frontend/helpers/AddToWishList.vue?vue&type=style&index=0&id=045a22a5&scoped=true&lang=css&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/frontend/Home.vue?vue&type=template&id=42d334a1&scoped=true&":
/*!****************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/frontend/Home.vue?vue&type=template&id=42d334a1&scoped=true& ***!
  \****************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _c("div", { staticClass: "home-slider" }, [
      _c("div", { staticClass: "women-cover" }, [
        _c("img", {
          staticClass: "home_slider_image",
          attrs: { src: _vm.getCoverImage() }
        }),
        _vm._v(" "),
        _c(
          "div",
          { staticClass: "shop-women" },
          [
            _c(
              "router-link",
              {
                attrs: { to: { name: "filter", params: { gender: "Women" } } }
              },
              [_vm._v("SHOP WOMEN")]
            )
          ],
          1
        )
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "men-cover" }, [
        _c("img", {
          staticClass: "home_slider_image",
          attrs: { src: _vm.getCoverImage1() }
        }),
        _vm._v(" "),
        _c(
          "div",
          { staticClass: "shop-men" },
          [
            _c(
              "router-link",
              { attrs: { to: { name: "filter", params: { gender: "Men" } } } },
              [_vm._v("SHOP MEN")]
            )
          ],
          1
        )
      ])
    ]),
    _vm._v(" "),
    _vm._m(0),
    _vm._v(" "),
    _c("div", { staticClass: "home-trending" }, [
      _c(
        "h2",
        { staticClass: "d-flex justify-content-center mt-5 font-weight-bold" },
        [_vm._v("New Arrivals")]
      ),
      _vm._v(" "),
      _c("div", { staticClass: "trending-menu mt-5 ml-3 product-padding" }, [
        _c(
          "span",
          {
            staticClass: "trending-link",
            class: { show_active: _vm.isMen },
            staticStyle: { "margin-right": "2rem" },
            on: { click: _vm.showMens }
          },
          [_vm._v("Men")]
        ),
        _vm._v(" "),
        _c(
          "span",
          {
            staticClass: "trending-link",
            class: { show_active: _vm.isWomen },
            on: { click: _vm.showWomens }
          },
          [_vm._v("Women")]
        )
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "desktop-view" }, [
        _c(
          "div",
          {
            directives: [
              {
                name: "show",
                rawName: "v-show",
                value: _vm.isMen,
                expression: "isMen"
              }
            ],
            staticClass: "trending-items"
          },
          [
            _c(
              "div",
              { staticClass: "row product-padding" },
              [
                _vm._l(_vm.trendingProducts.men, function(parent) {
                  return [
                    _vm._l(parent.child, function(child) {
                      return [
                        _vm._l(child.grand_child, function(grand) {
                          return _vm._l(grand.products, function(product) {
                            return _c(
                              "div",
                              {
                                key: product.id,
                                staticClass: "items-box col-sm-4 col-lg-3 p-3"
                              },
                              [
                                _c("div", { staticClass: "row pt-3" }, [
                                  _c(
                                    "div",
                                    { staticClass: "card" },
                                    [
                                      _c(
                                        "router-link",
                                        {
                                          attrs: {
                                            to: {
                                              name: "singleProduct",
                                              params: {
                                                id: product.id,
                                                slug: product.slug
                                              }
                                            }
                                          }
                                        },
                                        [
                                          _c("img", {
                                            staticClass: "card-img-top",
                                            attrs: {
                                              src: _vm.getImage(
                                                product.thumbnail
                                              )
                                            }
                                          })
                                        ]
                                      ),
                                      _vm._v(" "),
                                      _c("div", { staticClass: "card-body" }, [
                                        _c("div", { staticClass: "row" }, [
                                          _c(
                                            "div",
                                            {
                                              staticClass:
                                                "col-12 d-flex desc-padd"
                                            },
                                            [
                                              _c(
                                                "span",
                                                {
                                                  staticClass:
                                                    "brand mb-2 col-6"
                                                },
                                                [
                                                  _vm._v(
                                                    "\n                                                            " +
                                                      _vm._s(
                                                        product.brand.name
                                                      ) +
                                                      "\n                                                        "
                                                  )
                                                ]
                                              ),
                                              _vm._v(" "),
                                              _c(
                                                "span",
                                                {
                                                  staticClass:
                                                    "brand mb-2 col-6 heart"
                                                },
                                                [
                                                  _c("WishList", {
                                                    attrs: { product: product }
                                                  })
                                                ],
                                                1
                                              )
                                            ]
                                          )
                                        ]),
                                        _vm._v(" "),
                                        _c("div", { staticClass: "row" }, [
                                          _c(
                                            "div",
                                            { staticClass: "col-12 desc-padd" },
                                            [
                                              _c(
                                                "span",
                                                { staticClass: "name mb-2" },
                                                [
                                                  _vm._v(
                                                    "\n                                                            " +
                                                      _vm._s(product.name) +
                                                      "\n                                                        "
                                                  )
                                                ]
                                              )
                                            ]
                                          )
                                        ]),
                                        _vm._v(" "),
                                        _c("div", { staticClass: "row" }, [
                                          _c(
                                            "div",
                                            { staticClass: "col-12 desc-padd" },
                                            [
                                              _c(
                                                "h6",
                                                {
                                                  staticClass: "name mt-2 mb-2"
                                                },
                                                [
                                                  _c(
                                                    "span",
                                                    {
                                                      class: {
                                                        "price-cut":
                                                          product.sales
                                                      }
                                                    },
                                                    [
                                                      _vm._v(
                                                        "Rs. " +
                                                          _vm._s(
                                                            _vm._f("price")(
                                                              product.price
                                                            )
                                                          )
                                                      )
                                                    ]
                                                  ),
                                                  _vm._v(" "),
                                                  product.sales
                                                    ? _c(
                                                        "span",
                                                        {
                                                          staticClass:
                                                            "price-off"
                                                        },
                                                        [
                                                          _vm._v(
                                                            "    \n                                                                Rs. " +
                                                              _vm._s(
                                                                _vm._f("price")(
                                                                  _vm._f(
                                                                    "round"
                                                                  )(
                                                                    product.price -
                                                                      product.price *
                                                                        product
                                                                          .sales
                                                                          .rate
                                                                  )
                                                                )
                                                              ) +
                                                              "\n                                                            "
                                                          )
                                                        ]
                                                      )
                                                    : _vm._e()
                                                ]
                                              )
                                            ]
                                          )
                                        ])
                                      ])
                                    ],
                                    1
                                  )
                                ])
                              ]
                            )
                          })
                        })
                      ]
                    })
                  ]
                })
              ],
              2
            )
          ]
        ),
        _vm._v(" "),
        _c(
          "div",
          {
            directives: [
              {
                name: "show",
                rawName: "v-show",
                value: _vm.isWomen,
                expression: "isWomen"
              }
            ],
            staticClass: "trending-items"
          },
          [
            _c(
              "div",
              { staticClass: "row product-padding" },
              [
                _vm._l(_vm.trendingProducts.women, function(parent) {
                  return [
                    _vm._l(parent.child, function(child) {
                      return [
                        _vm._l(child.grand_child, function(grand) {
                          return _vm._l(grand.products, function(product) {
                            return _c(
                              "div",
                              {
                                key: product.id,
                                staticClass: "items-box col-sm-4 col-lg-3 p-3"
                              },
                              [
                                _c("div", { staticClass: "row pt-3" }, [
                                  _c(
                                    "div",
                                    { staticClass: "card" },
                                    [
                                      _c(
                                        "router-link",
                                        {
                                          attrs: {
                                            to: {
                                              name: "singleProduct",
                                              params: {
                                                id: product.id,
                                                slug: product.slug
                                              }
                                            }
                                          }
                                        },
                                        [
                                          _c("img", {
                                            staticClass: "card-img-top",
                                            attrs: {
                                              src: _vm.getImage(
                                                product.thumbnail
                                              )
                                            }
                                          })
                                        ]
                                      ),
                                      _vm._v(" "),
                                      _c("div", { staticClass: "card-body" }, [
                                        _c("div", { staticClass: "row" }, [
                                          _c(
                                            "div",
                                            {
                                              staticClass:
                                                "col-12 d-flex desc-padd"
                                            },
                                            [
                                              _c(
                                                "span",
                                                {
                                                  staticClass:
                                                    "brand mb-2 col-6"
                                                },
                                                [
                                                  _vm._v(
                                                    "\n                                                            " +
                                                      _vm._s(
                                                        product.brand.name
                                                      ) +
                                                      "\n                                                        "
                                                  )
                                                ]
                                              ),
                                              _vm._v(" "),
                                              _c(
                                                "span",
                                                {
                                                  staticClass:
                                                    "brand mb-2 col-6 heart"
                                                },
                                                [
                                                  _c("WishList", {
                                                    attrs: { product: product }
                                                  })
                                                ],
                                                1
                                              )
                                            ]
                                          )
                                        ]),
                                        _vm._v(" "),
                                        _c("div", { staticClass: "row" }, [
                                          _c(
                                            "div",
                                            { staticClass: "col-12 desc-padd" },
                                            [
                                              _c(
                                                "span",
                                                { staticClass: "name mb-2" },
                                                [
                                                  _vm._v(
                                                    "\n                                                            " +
                                                      _vm._s(product.name) +
                                                      "\n                                                        "
                                                  )
                                                ]
                                              )
                                            ]
                                          )
                                        ]),
                                        _vm._v(" "),
                                        _c("div", { staticClass: "row" }, [
                                          _c(
                                            "div",
                                            { staticClass: "col-12 desc-padd" },
                                            [
                                              _c(
                                                "h6",
                                                {
                                                  staticClass: "name mt-2 mb-2"
                                                },
                                                [
                                                  _c(
                                                    "span",
                                                    {
                                                      class: {
                                                        "price-cut":
                                                          product.sales
                                                      }
                                                    },
                                                    [
                                                      _vm._v(
                                                        "Rs. " +
                                                          _vm._s(
                                                            _vm._f("price")(
                                                              product.price
                                                            )
                                                          )
                                                      )
                                                    ]
                                                  ),
                                                  _vm._v(" "),
                                                  product.sales
                                                    ? _c(
                                                        "span",
                                                        {
                                                          staticClass:
                                                            "price-off"
                                                        },
                                                        [
                                                          _vm._v(
                                                            "    \n                                                                Rs. " +
                                                              _vm._s(
                                                                _vm._f("price")(
                                                                  _vm._f(
                                                                    "round"
                                                                  )(
                                                                    product.price -
                                                                      product.price *
                                                                        product
                                                                          .sales
                                                                          .rate
                                                                  )
                                                                )
                                                              ) +
                                                              "\n                                                           "
                                                          )
                                                        ]
                                                      )
                                                    : _vm._e()
                                                ]
                                              )
                                            ]
                                          )
                                        ])
                                      ])
                                    ],
                                    1
                                  )
                                ])
                              ]
                            )
                          })
                        })
                      ]
                    })
                  ]
                })
              ],
              2
            )
          ]
        )
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "mobile-view" }, [
        _c(
          "div",
          {
            directives: [
              {
                name: "show",
                rawName: "v-show",
                value: _vm.isMen,
                expression: "isMen"
              }
            ],
            staticClass: "trending-items"
          },
          [
            _c(
              "div",
              { staticClass: "row product-padding" },
              [
                _c(
                  "carousel",
                  {
                    staticStyle: { width: "100%" },
                    attrs: {
                      "per-page": 1,
                      autoplay: true,
                      loop: true,
                      autoplayHoverPause: true,
                      scrollPerPage: false
                    }
                  },
                  [
                    _vm._l(_vm.trendingProducts.men, function(parent) {
                      return [
                        _vm._l(parent.child, function(child) {
                          return [
                            _vm._l(child.grand_child, function(grand) {
                              return _vm._l(grand.products, function(product) {
                                return _c("slide", { key: product.id }, [
                                  _c(
                                    "div",
                                    { staticClass: "items-box col-12" },
                                    [
                                      _c("div", { staticClass: "row pt-3" }, [
                                        _c(
                                          "div",
                                          { staticClass: "card" },
                                          [
                                            _c(
                                              "router-link",
                                              {
                                                attrs: {
                                                  to: {
                                                    name: "singleProduct",
                                                    params: {
                                                      id: product.id,
                                                      slug: product.slug
                                                    }
                                                  }
                                                }
                                              },
                                              [
                                                _c("img", {
                                                  staticClass: "card-img-top",
                                                  attrs: {
                                                    src: _vm.getImage(
                                                      product.thumbnail
                                                    )
                                                  }
                                                })
                                              ]
                                            ),
                                            _vm._v(" "),
                                            _c(
                                              "div",
                                              { staticClass: "card-body" },
                                              [
                                                _c(
                                                  "div",
                                                  { staticClass: "row" },
                                                  [
                                                    _c(
                                                      "div",
                                                      {
                                                        staticClass:
                                                          "col-12 d-flex desc-padd"
                                                      },
                                                      [
                                                        _c(
                                                          "span",
                                                          {
                                                            staticClass:
                                                              "brand mb-2 col-6"
                                                          },
                                                          [
                                                            _vm._v(
                                                              "\n                                                                    " +
                                                                _vm._s(
                                                                  product.brand
                                                                    .name
                                                                ) +
                                                                "\n                                                                "
                                                            )
                                                          ]
                                                        ),
                                                        _vm._v(" "),
                                                        _c(
                                                          "span",
                                                          {
                                                            staticClass:
                                                              "brand mb-2 col-6 heart"
                                                          },
                                                          [
                                                            _c("WishList", {
                                                              attrs: {
                                                                product: product
                                                              }
                                                            })
                                                          ],
                                                          1
                                                        )
                                                      ]
                                                    )
                                                  ]
                                                ),
                                                _vm._v(" "),
                                                _c(
                                                  "div",
                                                  { staticClass: "row" },
                                                  [
                                                    _c(
                                                      "div",
                                                      {
                                                        staticClass:
                                                          "col-12 desc-padd"
                                                      },
                                                      [
                                                        _c(
                                                          "span",
                                                          {
                                                            staticClass:
                                                              "name mb-2"
                                                          },
                                                          [
                                                            _vm._v(
                                                              "\n                                                                    " +
                                                                _vm._s(
                                                                  product.name
                                                                ) +
                                                                "\n                                                                "
                                                            )
                                                          ]
                                                        )
                                                      ]
                                                    )
                                                  ]
                                                ),
                                                _vm._v(" "),
                                                _c(
                                                  "div",
                                                  { staticClass: "row" },
                                                  [
                                                    _c(
                                                      "div",
                                                      {
                                                        staticClass:
                                                          "col-12 desc-padd"
                                                      },
                                                      [
                                                        _c(
                                                          "h6",
                                                          {
                                                            staticClass:
                                                              "name mt-2 mb-2"
                                                          },
                                                          [
                                                            _c(
                                                              "span",
                                                              {
                                                                class: {
                                                                  "price-cut":
                                                                    product.sales
                                                                }
                                                              },
                                                              [
                                                                _vm._v(
                                                                  "Rs. " +
                                                                    _vm._s(
                                                                      _vm._f(
                                                                        "price"
                                                                      )(
                                                                        product.price
                                                                      )
                                                                    )
                                                                )
                                                              ]
                                                            ),
                                                            _vm._v(" "),
                                                            product.sales
                                                              ? _c(
                                                                  "span",
                                                                  {
                                                                    staticClass:
                                                                      "price-off"
                                                                  },
                                                                  [
                                                                    _vm._v(
                                                                      "    \n                                                                        Rs. " +
                                                                        _vm._s(
                                                                          _vm._f(
                                                                            "price"
                                                                          )(
                                                                            _vm._f(
                                                                              "round"
                                                                            )(
                                                                              product.price -
                                                                                product.price *
                                                                                  product
                                                                                    .sales
                                                                                    .rate
                                                                            )
                                                                          )
                                                                        ) +
                                                                        "\n                                                                    "
                                                                    )
                                                                  ]
                                                                )
                                                              : _vm._e()
                                                          ]
                                                        )
                                                      ]
                                                    )
                                                  ]
                                                )
                                              ]
                                            )
                                          ],
                                          1
                                        )
                                      ])
                                    ]
                                  )
                                ])
                              })
                            })
                          ]
                        })
                      ]
                    })
                  ],
                  2
                )
              ],
              1
            )
          ]
        ),
        _vm._v(" "),
        _c(
          "div",
          {
            directives: [
              {
                name: "show",
                rawName: "v-show",
                value: _vm.isWomen,
                expression: "isWomen"
              }
            ],
            staticClass: "trending-items"
          },
          [
            _c(
              "div",
              { staticClass: "row product-padding" },
              [
                _c(
                  "carousel",
                  {
                    staticStyle: { width: "100%" },
                    attrs: {
                      "per-page": 1,
                      autoplay: true,
                      loop: true,
                      autoplayHoverPause: true,
                      scrollPerPage: false
                    }
                  },
                  [
                    _vm._l(_vm.trendingProducts.women, function(parent) {
                      return [
                        _vm._l(parent.child, function(child) {
                          return [
                            _vm._l(child.grand_child, function(grand) {
                              return _vm._l(grand.products, function(product) {
                                return _c("slide", { key: product.id }, [
                                  _c(
                                    "div",
                                    { staticClass: "items-box col-12" },
                                    [
                                      _c("div", { staticClass: "row pt-3" }, [
                                        _c(
                                          "div",
                                          { staticClass: "card" },
                                          [
                                            _c(
                                              "router-link",
                                              {
                                                attrs: {
                                                  to: {
                                                    name: "singleProduct",
                                                    params: {
                                                      id: product.id,
                                                      slug: product.slug
                                                    }
                                                  }
                                                }
                                              },
                                              [
                                                _c("img", {
                                                  staticClass: "card-img-top",
                                                  attrs: {
                                                    src: _vm.getImage(
                                                      product.thumbnail
                                                    )
                                                  }
                                                })
                                              ]
                                            ),
                                            _vm._v(" "),
                                            _c(
                                              "div",
                                              { staticClass: "card-body" },
                                              [
                                                _c(
                                                  "div",
                                                  { staticClass: "row" },
                                                  [
                                                    _c(
                                                      "div",
                                                      {
                                                        staticClass:
                                                          "col-12 d-flex desc-padd"
                                                      },
                                                      [
                                                        _c(
                                                          "span",
                                                          {
                                                            staticClass:
                                                              "brand mb-2 col-6"
                                                          },
                                                          [
                                                            _vm._v(
                                                              "\n                                                                    " +
                                                                _vm._s(
                                                                  product.brand
                                                                    .name
                                                                ) +
                                                                "\n                                                                "
                                                            )
                                                          ]
                                                        ),
                                                        _vm._v(" "),
                                                        _c(
                                                          "span",
                                                          {
                                                            staticClass:
                                                              "brand mb-2 col-6 heart"
                                                          },
                                                          [
                                                            _c("WishList", {
                                                              attrs: {
                                                                product: product
                                                              }
                                                            })
                                                          ],
                                                          1
                                                        )
                                                      ]
                                                    )
                                                  ]
                                                ),
                                                _vm._v(" "),
                                                _c(
                                                  "div",
                                                  { staticClass: "row" },
                                                  [
                                                    _c(
                                                      "div",
                                                      {
                                                        staticClass:
                                                          "col-12 desc-padd"
                                                      },
                                                      [
                                                        _c(
                                                          "span",
                                                          {
                                                            staticClass:
                                                              "name mb-2"
                                                          },
                                                          [
                                                            _vm._v(
                                                              "\n                                                                    " +
                                                                _vm._s(
                                                                  product.name
                                                                ) +
                                                                "\n                                                                "
                                                            )
                                                          ]
                                                        )
                                                      ]
                                                    )
                                                  ]
                                                ),
                                                _vm._v(" "),
                                                _c(
                                                  "div",
                                                  { staticClass: "row" },
                                                  [
                                                    _c(
                                                      "div",
                                                      {
                                                        staticClass:
                                                          "col-12 desc-padd"
                                                      },
                                                      [
                                                        _c(
                                                          "h6",
                                                          {
                                                            staticClass:
                                                              "name mt-2 mb-2"
                                                          },
                                                          [
                                                            _c(
                                                              "span",
                                                              {
                                                                class: {
                                                                  "price-cut":
                                                                    product.sales
                                                                }
                                                              },
                                                              [
                                                                _vm._v(
                                                                  "Rs. " +
                                                                    _vm._s(
                                                                      _vm._f(
                                                                        "price"
                                                                      )(
                                                                        product.price
                                                                      )
                                                                    )
                                                                )
                                                              ]
                                                            ),
                                                            _vm._v(" "),
                                                            product.sales
                                                              ? _c(
                                                                  "span",
                                                                  {
                                                                    staticClass:
                                                                      "price-off"
                                                                  },
                                                                  [
                                                                    _vm._v(
                                                                      "    \n                                                                          Rs. " +
                                                                        _vm._s(
                                                                          _vm._f(
                                                                            "price"
                                                                          )(
                                                                            _vm._f(
                                                                              "round"
                                                                            )(
                                                                              product.price -
                                                                                product.price *
                                                                                  product
                                                                                    .sales
                                                                                    .rate
                                                                            )
                                                                          )
                                                                        ) +
                                                                        "\n                                                                    "
                                                                    )
                                                                  ]
                                                                )
                                                              : _vm._e()
                                                          ]
                                                        )
                                                      ]
                                                    )
                                                  ]
                                                )
                                              ]
                                            )
                                          ],
                                          1
                                        )
                                      ])
                                    ]
                                  )
                                ])
                              })
                            })
                          ]
                        })
                      ]
                    })
                  ],
                  2
                )
              ],
              1
            )
          ]
        )
      ])
    ]),
    _vm._v(" "),
    _c("div", [
      _c("div", { staticClass: "collection-offer d-flex" }, [
        _c("div", { staticClass: "image-section" }, [
          _c("img", { attrs: { src: _vm.getOffer1() } }),
          _vm._v(" "),
          _c("div", { staticClass: "text" }, [
            _vm._m(1),
            _vm._v(" "),
            _c("span", { staticClass: "top-5" }, [
              _vm._v("DENIM  FROM  Rs.2199")
            ]),
            _vm._v(" "),
            _c(
              "button",
              {
                staticClass: "top-6",
                on: {
                  click: function($event) {
                    return _vm.goFilter("Men", "Jeans", "Clothing")
                  }
                }
              },
              [_vm._v("Shop Now")]
            )
          ])
        ]),
        _vm._v(" "),
        _c(
          "div",
          {
            staticClass: "collection-content",
            on: {
              click: function($event) {
                return _vm.goFilterSales("All", "", "30")
              }
            }
          },
          [
            _c("img", { attrs: { src: _vm.getOffer2() } }),
            _vm._v(" "),
            _vm._m(2)
          ]
        )
      ])
    ]),
    _vm._v(" "),
    _c("div", { staticClass: "home-notification" }, [
      _c("div", { staticClass: "notification-title" }, [
        _c("span", [_vm._v("Keep up to date with the next trends")]),
        _c("br"),
        _vm._v(" "),
        _c(
          "form",
          {
            on: {
              submit: function($event) {
                $event.preventDefault()
                return _vm.subscribe($event)
              }
            }
          },
          [
            _c("div", { staticClass: "row" }, [
              _c(
                "div",
                { staticClass: "form-group col-md-4" },
                [
                  _c("input", {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.form.email,
                        expression: "form.email"
                      }
                    ],
                    staticClass: "form-control",
                    class: { "is-invalid": _vm.form.errors.has("email") },
                    attrs: {
                      type: "email",
                      name: "email",
                      placeholder: "Enter your email address...."
                    },
                    domProps: { value: _vm.form.email },
                    on: {
                      input: function($event) {
                        if ($event.target.composing) {
                          return
                        }
                        _vm.$set(_vm.form, "email", $event.target.value)
                      }
                    }
                  }),
                  _vm._v(" "),
                  _c("has-error", { attrs: { form: _vm.form, field: "email" } })
                ],
                1
              )
            ]),
            _vm._v(" "),
            _vm._m(3)
          ]
        )
      ])
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "offer-banner" }, [
      _c("div", { staticClass: "row" }, [
        _c("div", { staticClass: "shop-now col-sm-4" }, [
          _c("span", [_vm._v("Sale")]),
          _vm._v(" "),
          _c("span", [_vm._v("Upto 40% Off")])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "order-return-banner col-sm-4" }, [
          _c("span", [_vm._v("ORDER RETURN")]),
          _vm._v(" "),
          _c("span", [_vm._v("Return within 15 Days")])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "shipping-banner col-sm-4 " }, [
          _c("span", [_vm._v("FREE SHIPPING")]),
          _vm._v(" "),
          _c("span", [_vm._v("On Orders Over Rs.5000")]),
          _vm._v(" "),
          _c("span", { staticClass: "kathmandu" }, [
            _vm._v("in Kathmandu Valley")
          ])
        ])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("span", { staticClass: "top-1 mb-5" }, [
      _c("span", [_vm._v("NEW STYLE")]),
      _c("br"),
      _vm._v(" "),
      _c("span", { staticClass: "denim" }, [_vm._v("DENIM")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "text" }, [
      _c("span", { staticClass: "top-2" }, [_vm._v("Winter Sale")]),
      _vm._v(" "),
      _c("span", { staticClass: "top-3" }, [_vm._v("Upto 30% Off")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "row" }, [
      _c("div", { staticClass: "form-group col-md-4" }, [
        _c(
          "button",
          { staticClass: "btn btn-success", attrs: { type: "submit" } },
          [_vm._v("SUBSCRIBE")]
        )
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/frontend/helpers/AddToWishList.vue?vue&type=template&id=045a22a5&scoped=true&":
/*!*********************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/frontend/helpers/AddToWishList.vue?vue&type=template&id=045a22a5&scoped=true& ***!
  \*********************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("i", {
    staticClass: "fa-heart",
    class: _vm.getHeart(),
    on: {
      click: function($event) {
        $event.preventDefault()
        return _vm.addToWish($event)
      }
    }
  })
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/components/frontend/Home.vue":
/*!***************************************************!*\
  !*** ./resources/js/components/frontend/Home.vue ***!
  \***************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Home_vue_vue_type_template_id_42d334a1_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Home.vue?vue&type=template&id=42d334a1&scoped=true& */ "./resources/js/components/frontend/Home.vue?vue&type=template&id=42d334a1&scoped=true&");
/* harmony import */ var _Home_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Home.vue?vue&type=script&lang=js& */ "./resources/js/components/frontend/Home.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _Home_vue_vue_type_style_index_0_id_42d334a1_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./Home.vue?vue&type=style&index=0&id=42d334a1&lang=scss&scoped=true& */ "./resources/js/components/frontend/Home.vue?vue&type=style&index=0&id=42d334a1&lang=scss&scoped=true&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _Home_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Home_vue_vue_type_template_id_42d334a1_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Home_vue_vue_type_template_id_42d334a1_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "42d334a1",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/frontend/Home.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/frontend/Home.vue?vue&type=script&lang=js&":
/*!****************************************************************************!*\
  !*** ./resources/js/components/frontend/Home.vue?vue&type=script&lang=js& ***!
  \****************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Home_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./Home.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/frontend/Home.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Home_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/frontend/Home.vue?vue&type=style&index=0&id=42d334a1&lang=scss&scoped=true&":
/*!*************************************************************************************************************!*\
  !*** ./resources/js/components/frontend/Home.vue?vue&type=style&index=0&id=42d334a1&lang=scss&scoped=true& ***!
  \*************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_Home_vue_vue_type_style_index_0_id_42d334a1_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/style-loader!../../../../node_modules/css-loader!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--7-2!../../../../node_modules/sass-loader/dist/cjs.js??ref--7-3!../../../../node_modules/vue-loader/lib??vue-loader-options!./Home.vue?vue&type=style&index=0&id=42d334a1&lang=scss&scoped=true& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/frontend/Home.vue?vue&type=style&index=0&id=42d334a1&lang=scss&scoped=true&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_Home_vue_vue_type_style_index_0_id_42d334a1_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_Home_vue_vue_type_style_index_0_id_42d334a1_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_Home_vue_vue_type_style_index_0_id_42d334a1_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_Home_vue_vue_type_style_index_0_id_42d334a1_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_Home_vue_vue_type_style_index_0_id_42d334a1_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./resources/js/components/frontend/Home.vue?vue&type=template&id=42d334a1&scoped=true&":
/*!**********************************************************************************************!*\
  !*** ./resources/js/components/frontend/Home.vue?vue&type=template&id=42d334a1&scoped=true& ***!
  \**********************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Home_vue_vue_type_template_id_42d334a1_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./Home.vue?vue&type=template&id=42d334a1&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/frontend/Home.vue?vue&type=template&id=42d334a1&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Home_vue_vue_type_template_id_42d334a1_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Home_vue_vue_type_template_id_42d334a1_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/frontend/helpers/AddToWishList.vue":
/*!********************************************************************!*\
  !*** ./resources/js/components/frontend/helpers/AddToWishList.vue ***!
  \********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _AddToWishList_vue_vue_type_template_id_045a22a5_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./AddToWishList.vue?vue&type=template&id=045a22a5&scoped=true& */ "./resources/js/components/frontend/helpers/AddToWishList.vue?vue&type=template&id=045a22a5&scoped=true&");
/* harmony import */ var _AddToWishList_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./AddToWishList.vue?vue&type=script&lang=js& */ "./resources/js/components/frontend/helpers/AddToWishList.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _AddToWishList_vue_vue_type_style_index_0_id_045a22a5_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./AddToWishList.vue?vue&type=style&index=0&id=045a22a5&scoped=true&lang=css& */ "./resources/js/components/frontend/helpers/AddToWishList.vue?vue&type=style&index=0&id=045a22a5&scoped=true&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _AddToWishList_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _AddToWishList_vue_vue_type_template_id_045a22a5_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _AddToWishList_vue_vue_type_template_id_045a22a5_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "045a22a5",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/frontend/helpers/AddToWishList.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/frontend/helpers/AddToWishList.vue?vue&type=script&lang=js&":
/*!*********************************************************************************************!*\
  !*** ./resources/js/components/frontend/helpers/AddToWishList.vue?vue&type=script&lang=js& ***!
  \*********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_AddToWishList_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./AddToWishList.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/frontend/helpers/AddToWishList.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_AddToWishList_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/frontend/helpers/AddToWishList.vue?vue&type=style&index=0&id=045a22a5&scoped=true&lang=css&":
/*!*****************************************************************************************************************************!*\
  !*** ./resources/js/components/frontend/helpers/AddToWishList.vue?vue&type=style&index=0&id=045a22a5&scoped=true&lang=css& ***!
  \*****************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_AddToWishList_vue_vue_type_style_index_0_id_045a22a5_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/style-loader!../../../../../node_modules/css-loader??ref--6-1!../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../node_modules/postcss-loader/src??ref--6-2!../../../../../node_modules/vue-loader/lib??vue-loader-options!./AddToWishList.vue?vue&type=style&index=0&id=045a22a5&scoped=true&lang=css& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/frontend/helpers/AddToWishList.vue?vue&type=style&index=0&id=045a22a5&scoped=true&lang=css&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_AddToWishList_vue_vue_type_style_index_0_id_045a22a5_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_AddToWishList_vue_vue_type_style_index_0_id_045a22a5_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_AddToWishList_vue_vue_type_style_index_0_id_045a22a5_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_AddToWishList_vue_vue_type_style_index_0_id_045a22a5_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_AddToWishList_vue_vue_type_style_index_0_id_045a22a5_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./resources/js/components/frontend/helpers/AddToWishList.vue?vue&type=template&id=045a22a5&scoped=true&":
/*!***************************************************************************************************************!*\
  !*** ./resources/js/components/frontend/helpers/AddToWishList.vue?vue&type=template&id=045a22a5&scoped=true& ***!
  \***************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_AddToWishList_vue_vue_type_template_id_045a22a5_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./AddToWishList.vue?vue&type=template&id=045a22a5&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/frontend/helpers/AddToWishList.vue?vue&type=template&id=045a22a5&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_AddToWishList_vue_vue_type_template_id_045a22a5_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_AddToWishList_vue_vue_type_template_id_045a22a5_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);