(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[94],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/frontend/Returns.vue?vue&type=script&lang=js&":
/*!***************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/frontend/Returns.vue?vue&type=script&lang=js& ***!
  \***************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _helpers_auth__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./helpers/auth */ "./resources/js/components/frontend/helpers/auth.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  name: "Returns",
  data: function data() {
    return {
      form: {
        email: '',
        password: ''
      },
      url: '',
      loginError: '',
      message: '',
      props: ['url']
    };
  },
  beforeRouteEnter: function beforeRouteEnter(to, from, next) {
    next(function (vm) {
      vm.url = from.path;
    });
  },
  created: function created() {
    this.scroll();
  },
  methods: {
    scroll: function scroll() {
      window.scrollTo(0, 0);
    },
    authenticate: function authenticate() {
      var _this = this;

      this.$Progress.start();

      if (!this.form.email) {
        this.loginError = 'Please enter an email address.';
        this.$Progress.fail();
      } else if (!this.validEmail(this.form.email)) {
        this.loginError = 'Invalid email address';
        this.$Progress.fail();
      } else if (!this.form.password) {
        this.loginError = 'Please enter your password.';
        this.$Progress.fail();
      } else if (this.form.password.length < 6) {
        this.loginError = 'Password must be at least 6 characters.';
        this.$Progress.fail();
      } else {
        this.$store.dispatch('login');
        Object(_helpers_auth__WEBPACK_IMPORTED_MODULE_0__["login"])(this.$data.form).then(function (res) {
          _this.$Progress.finish();

          _this.$store.commit("loginSuccess", res);

          if (_this.url) {
            _this.$router.push(_this.url);
          } else {
            _this.$router.push('/');
          }
        })["catch"](function (error) {
          _this.$Progress.fail();

          _this.loginError = error.message;

          _this.$store.commit("loginFailed", {
            error: error
          });
        });
      }
    },
    validEmail: function validEmail(email) {
      var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      return re.test(email);
    }
  }
});

/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/frontend/Returns.vue?vue&type=style&index=0&id=51897571&scoped=true&lang=scss&":
/*!**************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/sass-loader/dist/cjs.js??ref--7-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/frontend/Returns.vue?vue&type=style&index=0&id=51897571&scoped=true&lang=scss& ***!
  \**************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".return-contents[data-v-51897571] {\n  display: flex;\n  text-align: justify;\n  max-width: 90% !important;\n}\n@media screen and (max-width: 768px) {\n.return-contents[data-v-51897571] {\n    flex-direction: column;\n}\n}\ni[data-v-51897571] {\n  display: flex;\n  justify-content: center;\n  font-size: 4rem;\n}\n.title[data-v-51897571] {\n  padding: 6% 0 0 6%;\n  color: #232323;\n  text-align: justify;\n}\n.title h3[data-v-51897571] {\n  font-weight: bold;\n}\n@media screen and (max-width: 768px) {\n.title h3[data-v-51897571] {\n    font-size: 1rem;\n}\n}\n.return-login[data-v-51897571] {\n  padding: 5rem 0 5rem 5rem;\n  width: 50%;\n}\n@media screen and (max-width: 768px) {\n.return-login[data-v-51897571] {\n    width: 100%;\n    padding: 0;\n    margin-bottom: 2rem;\n}\n}\n.return-info[data-v-51897571] {\n  margin: 6rem 0;\n  width: 50%;\n}\n@media screen and (max-width: 768px) {\n.return-info[data-v-51897571] {\n    width: 100%;\n    margin-bottom: 0;\n}\n}\n.return-info-1[data-v-51897571], .return-info-2[data-v-51897571], .return-info-3[data-v-51897571] {\n  margin-bottom: 2rem;\n  display: flex;\n  flex-direction: column;\n  justify-content: center;\n  align-items: center;\n}\n.return-info-1 i[data-v-51897571], .return-info-2 i[data-v-51897571], .return-info-3 i[data-v-51897571] {\n  margin-bottom: 1rem;\n  color: #232323;\n  display: flex;\n  justify-content: center;\n  height: 7rem;\n  width: 7rem;\n  background: rgba(0, 0, 0, 0.2);\n  align-items: center;\n  border-radius: 50%;\n}\nform[data-v-51897571] {\n  width: 100%;\n  display: flex;\n  flex-direction: column;\n  align-items: center;\n  margin-bottom: 2rem;\n}\n.title-size[data-v-51897571] {\n  font-size: 2rem;\n  color: #232323;\n  font-weight: bold;\n  margin-bottom: 1rem;\n}\nbutton[data-v-51897571] {\n  border-radius: 25px;\n  border: 1px solid #232323;\n  background-color: #232323;\n  color: #ffffff;\n  font-size: 12px;\n  font-weight: bold;\n  padding: 12px 45px;\n  letter-spacing: 1px;\n  text-transform: uppercase;\n  outline: none;\n  margin-bottom: 0.5rem;\n}\n.mobile[data-v-51897571] {\n  display: flex;\n  align-items: center;\n  margin-top: 0.5rem;\n}", ""]);

// exports


/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/frontend/Returns.vue?vue&type=style&index=0&id=51897571&scoped=true&lang=scss&":
/*!******************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/sass-loader/dist/cjs.js??ref--7-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/frontend/Returns.vue?vue&type=style&index=0&id=51897571&scoped=true&lang=scss& ***!
  \******************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../node_modules/css-loader!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--7-2!../../../../node_modules/sass-loader/dist/cjs.js??ref--7-3!../../../../node_modules/vue-loader/lib??vue-loader-options!./Returns.vue?vue&type=style&index=0&id=51897571&scoped=true&lang=scss& */ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/frontend/Returns.vue?vue&type=style&index=0&id=51897571&scoped=true&lang=scss&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/frontend/Returns.vue?vue&type=template&id=51897571&scoped=true&":
/*!*******************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/frontend/Returns.vue?vue&type=template&id=51897571&scoped=true& ***!
  \*******************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _vm._m(0),
    _vm._v(" "),
    _c("div", { staticClass: "return-contents container" }, [
      _vm._m(1),
      _vm._v(" "),
      _c(
        "div",
        { staticClass: "return-login " },
        [
          _c(
            "form",
            {
              on: {
                submit: function($event) {
                  $event.preventDefault()
                  return _vm.authenticate($event)
                }
              }
            },
            [
              _vm.$route.query.account
                ? _c("div", { staticClass: "text-center text-success" }, [
                    _vm._v(
                      "Your account has been activated.\n                    Now you can proceed to login.\n                "
                    )
                  ])
                : _vm._e(),
              _vm._v(" "),
              _vm.$route.query.reset
                ? _c("div", { staticClass: "text-center text-success" }, [
                    _vm._v(
                      "Password reset is successful. Now\n                    you can proceed to login.\n                "
                    )
                  ])
                : _vm._e(),
              _vm._v(" "),
              _c("div", { staticClass: "text-center text-success" }, [
                _vm._v(
                  "\n                    " +
                    _vm._s(_vm.message) +
                    "\n                "
                )
              ]),
              _vm._v(" "),
              _c("span", { staticClass: "title-size" }, [_vm._v("Sign In")]),
              _vm._v(" "),
              _c(
                "div",
                { staticClass: "form-group row justify-content-center w-100" },
                [
                  _c("input", {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.form.email,
                        expression: "form.email"
                      }
                    ],
                    staticClass: "form-control col-12 ",
                    attrs: { type: "text", placeholder: "Email Address" },
                    domProps: { value: _vm.form.email },
                    on: {
                      input: function($event) {
                        if ($event.target.composing) {
                          return
                        }
                        _vm.$set(_vm.form, "email", $event.target.value)
                      }
                    }
                  })
                ]
              ),
              _vm._v(" "),
              _c(
                "div",
                { staticClass: "form-group justify-content-center row w-100" },
                [
                  _c("input", {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.form.password,
                        expression: "form.password"
                      }
                    ],
                    staticClass: "form-control col-12 ",
                    attrs: { type: "password", placeholder: "Password" },
                    domProps: { value: _vm.form.password },
                    on: {
                      input: function($event) {
                        if ($event.target.composing) {
                          return
                        }
                        _vm.$set(_vm.form, "password", $event.target.value)
                      }
                    }
                  })
                ]
              ),
              _vm._v(" "),
              _c(
                "div",
                { staticClass: "d-block text-center invalid-feedback mb-1" },
                [_vm._v(_vm._s(_vm.loginError))]
              ),
              _vm._v(" "),
              _vm._m(2),
              _vm._v(" "),
              _c("button", [_vm._v("Sign In")]),
              _vm._v(" "),
              _c(
                "router-link",
                { attrs: { to: { name: "password-reset-form" } } },
                [
                  _vm._v(
                    "\n                    Forgot your password?\n                "
                  )
                ]
              ),
              _vm._v(" "),
              _c(
                "div",
                { staticClass: "mobile" },
                [
                  _c("h6", { staticClass: "mr-2" }, [
                    _vm._v("Don't have a account?")
                  ]),
                  _vm._v(" "),
                  _c(
                    "router-link",
                    {
                      attrs: { to: { name: "login", query: { signUp: "yes" } } }
                    },
                    [_c("h6", [_vm._v("Sign Up")])]
                  )
                ],
                1
              )
            ],
            1
          ),
          _vm._v(" "),
          _c("social-login", {
            staticClass: "social-login",
            class: {
              "in-login-box-on": !_vm.isSignUpActive,
              "in-login-box-off": _vm.isSignUpActive
            },
            attrs: { url: _vm.url }
          })
        ],
        1
      )
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "title" }, [
      _c("h3", { staticClass: "mb-4" }, [
        _vm._v("Please login to start your return")
      ]),
      _vm._v(" "),
      _c("h3", [_vm._v("15 days free returns. It's as easy as that.")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "return-info" }, [
      _c("div", { staticClass: "return-info-1" }, [
        _c("i", { staticClass: "far fa-calendar-check" }),
        _vm._v(" "),
        _c("span", [
          _vm._v(
            "You can return any item within 15 days of purchase if the item is unused with the\n                        original tags still attached; and in the original packaging which must be in the original\n                        condition. We'll even pay for the return delivery\n                "
          )
        ])
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "return-info-2" }, [
        _c("i", { staticClass: "fas fa-shopping-bag" }),
        _vm._v(" "),
        _c("span", [
          _vm._v(
            "Simply log in to My Account, select the item(s) you would like to return and fill out the\n                        information required.\n                "
          )
        ])
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "return-info-3" }, [
        _c("i", { staticClass: "fas fa-truck-moving" }),
        _vm._v(" "),
        _c("span", [
          _vm._v(
            "Print off the label and stick it on the “THE NexTrends” packaging or a similar satchel.\n                        Then drop it off at your nearest post office or drop off one our affiliated shops (only in\n                        Kathmandu valley). For more information please also have a look at our FAQ page\n                "
          )
        ])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "form-group row w-100" }, [
      _c("div", { staticClass: "col-12 text-center checkbox-container" }, [
        _c("input", {
          staticClass: "logged-in-checkbox",
          attrs: { type: "checkbox", id: "loggedIn" }
        }),
        _vm._v(" "),
        _c("label", { attrs: { for: "loggedIn" } }, [
          _vm._v("KEEP ME LOGGED IN")
        ])
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/components/frontend/Returns.vue":
/*!******************************************************!*\
  !*** ./resources/js/components/frontend/Returns.vue ***!
  \******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Returns_vue_vue_type_template_id_51897571_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Returns.vue?vue&type=template&id=51897571&scoped=true& */ "./resources/js/components/frontend/Returns.vue?vue&type=template&id=51897571&scoped=true&");
/* harmony import */ var _Returns_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Returns.vue?vue&type=script&lang=js& */ "./resources/js/components/frontend/Returns.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _Returns_vue_vue_type_style_index_0_id_51897571_scoped_true_lang_scss___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./Returns.vue?vue&type=style&index=0&id=51897571&scoped=true&lang=scss& */ "./resources/js/components/frontend/Returns.vue?vue&type=style&index=0&id=51897571&scoped=true&lang=scss&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _Returns_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Returns_vue_vue_type_template_id_51897571_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Returns_vue_vue_type_template_id_51897571_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "51897571",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/frontend/Returns.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/frontend/Returns.vue?vue&type=script&lang=js&":
/*!*******************************************************************************!*\
  !*** ./resources/js/components/frontend/Returns.vue?vue&type=script&lang=js& ***!
  \*******************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Returns_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./Returns.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/frontend/Returns.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Returns_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/frontend/Returns.vue?vue&type=style&index=0&id=51897571&scoped=true&lang=scss&":
/*!****************************************************************************************************************!*\
  !*** ./resources/js/components/frontend/Returns.vue?vue&type=style&index=0&id=51897571&scoped=true&lang=scss& ***!
  \****************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_Returns_vue_vue_type_style_index_0_id_51897571_scoped_true_lang_scss___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/style-loader!../../../../node_modules/css-loader!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--7-2!../../../../node_modules/sass-loader/dist/cjs.js??ref--7-3!../../../../node_modules/vue-loader/lib??vue-loader-options!./Returns.vue?vue&type=style&index=0&id=51897571&scoped=true&lang=scss& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/frontend/Returns.vue?vue&type=style&index=0&id=51897571&scoped=true&lang=scss&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_Returns_vue_vue_type_style_index_0_id_51897571_scoped_true_lang_scss___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_Returns_vue_vue_type_style_index_0_id_51897571_scoped_true_lang_scss___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_Returns_vue_vue_type_style_index_0_id_51897571_scoped_true_lang_scss___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_Returns_vue_vue_type_style_index_0_id_51897571_scoped_true_lang_scss___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_Returns_vue_vue_type_style_index_0_id_51897571_scoped_true_lang_scss___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./resources/js/components/frontend/Returns.vue?vue&type=template&id=51897571&scoped=true&":
/*!*************************************************************************************************!*\
  !*** ./resources/js/components/frontend/Returns.vue?vue&type=template&id=51897571&scoped=true& ***!
  \*************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Returns_vue_vue_type_template_id_51897571_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./Returns.vue?vue&type=template&id=51897571&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/frontend/Returns.vue?vue&type=template&id=51897571&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Returns_vue_vue_type_template_id_51897571_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Returns_vue_vue_type_template_id_51897571_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);