(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[80],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backend/product/AddProduct.vue?vue&type=script&lang=js&":
/*!*************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/backend/product/AddProduct.vue?vue&type=script&lang=js& ***!
  \*************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  name: "AddProduct",
  components: {
    'VueTrix': function VueTrix() {
      return __webpack_require__.e(/*! import() */ 61).then(__webpack_require__.bind(null, /*! vue-trix */ "./node_modules/vue-trix/dist/vue-trix.esm.js"));
    }
  },
  data: function data() {
    return {
      categories: {
        childs: {
          grandchilds: {}
        }
      },
      brands: {},
      fabrics: {},
      occasions: {},
      colors: '',
      selectedColor: [],
      colorsForPhoto: [],
      sizes: '',
      editMode: this.$store.getters.isEdit,
      editId: '',
      image: false,
      imageCount: [],
      imageError: {
        status: false,
        val: ''
      },
      catError: {
        status: false,
        val: ''
      },
      stores: '',
      form: new Form({
        name: '',
        brand: '',
        fabric: '',
        occasion: '',
        category: '',
        description: '',
        store: '',
        purchase_price: '',
        price: '',
        color: '',
        thumbnail: '',
        phototype: '',
        cimages: [{
          image: [],
          color: ''
        }],
        //array of object for dynamic field
        fields: [{
          color: '',
          size: '',
          quantity: '',
          minimum: ''
        }]
      })
    };
  },
  created: function created() {
    var _this = this;

    if (typeof this.$route.params.id !== 'undefined') {
      this.$store.dispatch('setEditMode', true); //api call section for general field section

      axios.get('/api/productInfo/' + this.$route.params.id).then(function (data) {
        var productInfo = data.data;
        _this.form.name = productInfo.name;
        _this.form.price = productInfo.price;
        _this.form.description = productInfo.description;
        _this.form.category = productInfo.category;
        _this.form.store = productInfo.store_id;
        _this.form.brand = productInfo.brand_id;
        _this.form.fabric = productInfo.fabric_id;
        _this.form.occasion = productInfo.occasion_id;
        _this.form.purchase_price = productInfo.purchase_price;
      })["catch"](function () {}); //api call section for multiple color size

      axios.get('/api/color_sizeInfo/' + this.$route.params.id).then(function (data) {
        var color_size = data.data;

        _this.form.fields.splice(0, 1);

        color_size.forEach(function (item, index) {
          _this.form.fields.push({
            color: item.color_id,
            size: item.size_id,
            quantity: item.quantity,
            minimum: item.minimum_stock
          });
        });
      })["catch"](function (e) {
        console.log(e);
      }); //api call section for multiple Images section

      axios.get('/api/getEditImages/' + this.$route.params.id).then(function (data) {
        var color = data.data;

        _this.form.cimages.splice(0, 1);

        color.forEach(function (item, index) {
          _this.form.cimages.push({
            image: ['chosen'],
            color: item.color_id,
            id: item.id
          });
        });
      })["catch"](function (e) {
        console.log(e);
      });
    } //end of edit param


    axios.get('/api/parent').then(function (data) {
      _this.categories = data.data;
    });
    axios.get('/api/getfabrics').then(function (data) {
      _this.fabrics = data.data;
    });
    axios.get('/api/getoccasions').then(function (data) {
      _this.occasions = data.data;
    });
    axios.get('/api/getcolor').then(function (data) {
      _this.colors = data.data;
    });
    axios.get('/api/getsizes').then(function (data) {
      _this.sizes = data.data;
    });
    axios.get('/api/getAllStores').then(function (data) {
      _this.stores = data.data;
    });
  },
  beforeMount: function beforeMount() {
    if (typeof this.$route.params.id == 'undefined') {
      //this section is removing required attribute
      var input = $('input[type="file"]');
      input.prop('required', true);
    }
  },
  methods: {
    //this section is for image upload
    loadUploadedImage: function loadUploadedImage(e) {
      var _this2 = this;

      var file = e.target.files[0];
      var reader = new FileReader();

      reader.onloadend = function (file) {
        _this2.form.thumbnail = reader.result;
      };

      reader.readAsDataURL(file);
    },
    toggle: function toggle(id) {
      if (document.getElementById(id).style.display == "none") {
        document.getElementById(id).style.display = "inline";
      } else {
        document.getElementById(id).style.display = "none";
      }
    },
    setColor: function setColor(color) {
      if (!this.colorsForPhoto.includes(color)) {
        this.colorsForPhoto.push(color);
      }
    },
    addProduct: function addProduct() {
      var _this3 = this;

      this.$Progress.start();
      this.form.post('/api/product').then(function () {
        _this3.form.reset();

        var input = $('input[type="file"]');
        input.val('');
        Toast.fire({
          type: 'success',
          title: 'Product Added!!!'
        });
        _this3.imageError.status = false;
        _this3.catError.status = false;
        _this3.colorsForPhoto = [];

        _this3.$Progress.finish();
      })["catch"](function (error) {
        _this3.$Progress.fail();

        var imageEr = error.response.data.message; //checking image error type

        if (imageEr.substr(0, 5) === 'image') {
          _this3.imageError.status = true;
          _this3.imageError.val = imageEr;
        } else {
          _this3.imageError.status = false;
        }

        if (error.response.data.errors === undefined) {
          _this3.catError.status = false;
        } else if (error.response.data.errors.category.length != 0) {
          _this3.catError.status = true;
          _this3.catError.val = "The Category Field is Required";
        }
      });
    },
    editProduct: function editProduct(id) {
      var _this4 = this;

      this.$Progress.start();
      this.form.put('/api/update_product/' + id).then(function (id) {
        //success
        swal.fire('Updated!', 'Product has been updated.', 'success');

        _this4.$Progress.finish();

        _this4.form.reset();

        _this4.$router.push({
          name: 'view-product'
        });
      })["catch"](function () {
        //fail
        _this4.$Progress.fail(); //changes the color of vue progress bar

      });
    },
    getBrands: function getBrands(gender, type) {
      var _this5 = this;

      axios.get('/api/getBrands/' + gender + '/' + type).then(function (data) {
        _this5.brands = data.data;
      });
    },
    AddField: function AddField() {
      this.form.fields.push({
        color: '',
        size: '',
        quantity: ''
      });
    },
    removeField: function removeField(index) {
      this.form.fields.splice(index, 1);
    },
    addImage: function addImage() {
      this.form.cimages.push({
        image: [],
        color: ''
      });
    },
    checkSelected: function checkSelected(event) {
      if (this.selectedColor.includes(event.target.value)) {} else {
        this.selectedColor.push(event.target.value);
      }
    },
    removeImageField: function removeImageField(index) {
      this.form.cimages.splice(index, 1);
    },
    imageSave: function imageSave(files, index, e) {
      var _this6 = this;

      //using base64 for converting image into text and vice versa
      //let files = e.target.files;
      for (var i = 0; i < files.length; i++) {
        this.imageCount = files.length;

        var _loop = function _loop() {
          var reader = new FileReader();
          var file = files[i];

          if (!file.type.match('image.*')) {
            e.target.value = '';
            _this6.form.cimages.image = [];
            alert('Select an image');
            return {
              v: void 0
            };
          }

          reader.onloadend = function (file) {
            _this6.form.cimages[index].image.push(reader.result); //stores image in format of text

          };

          reader.readAsDataURL(file);
        };

        for (var i = 0; i < files.length; i++) {
          var _ret = _loop();

          if (_typeof(_ret) === "object") return _ret.v;
        }
      }
    },
    onChange: function onChange(e) {
      var files = e.target.files; //this index to know where to insert the image

      var index = e.target.name[e.target.name.length - 1];
      this.form.cimages[index].image.splice(0, 1);
      this.imageSave(files, index, e); //this.createFile(files[index]);
    },
    createFile: function createFile(file) {
      if (!file.type.match('image.*')) {
        alert('Select an image');
        return;
      }

      var img = new Image();
      var reader = new FileReader();
      var vm = this;

      reader.onload = function (e) {
        vm.image = e.target.result;
      };

      reader.readAsDataURL(file);
    },
    removeFile: function removeFile() {
      this.form.cimages.image = [];
      this.image = ''; //image preview variable

      this.imageCount = 0;
    },
    getPreviewImage: function getPreviewImage() {
      var arrImg = this.form.cimages.image.split(",");
      this.image = "/image/product/" + arrImg[0];
      this.imageCount = arrImg.length;
    }
  }
});

/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backend/product/AddProduct.vue?vue&type=style&index=0&id=0530b898&scoped=true&lang=scss&":
/*!************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/sass-loader/dist/cjs.js??ref--7-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/backend/product/AddProduct.vue?vue&type=style&index=0&id=0530b898&scoped=true&lang=scss& ***!
  \************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".category input[type=radio][data-v-0530b898] {\n  display: none;\n}\n.category input + label[data-v-0530b898] {\n  border: 1px solid #EBECF0;\n  color: black;\n  border-radius: 5px;\n}\n.category input:checked + label[data-v-0530b898] {\n  background-color: #38A1DA;\n  color: white;\n  border-radius: 5px;\n}\n@media (min-width: 676px) {\n.select-container[data-v-0530b898] {\n    display: flex;\n}\n}\n.image-upload[data-v-0530b898] {\n  width: 40%;\n}\n@media (max-width: 676px) {\n.image-upload[data-v-0530b898] {\n    width: 100%;\n}\n}\n.image-upload img[data-v-0530b898] {\n  width: 100%;\n}", ""]);

// exports


/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backend/product/AddProduct.vue?vue&type=style&index=0&id=0530b898&scoped=true&lang=scss&":
/*!****************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/sass-loader/dist/cjs.js??ref--7-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/backend/product/AddProduct.vue?vue&type=style&index=0&id=0530b898&scoped=true&lang=scss& ***!
  \****************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../../node_modules/css-loader!../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../node_modules/postcss-loader/src??ref--7-2!../../../../../node_modules/sass-loader/dist/cjs.js??ref--7-3!../../../../../node_modules/vue-loader/lib??vue-loader-options!./AddProduct.vue?vue&type=style&index=0&id=0530b898&scoped=true&lang=scss& */ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backend/product/AddProduct.vue?vue&type=style&index=0&id=0530b898&scoped=true&lang=scss&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backend/product/AddProduct.vue?vue&type=template&id=0530b898&scoped=true&":
/*!*****************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/backend/product/AddProduct.vue?vue&type=template&id=0530b898&scoped=true& ***!
  \*****************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _c("div", { staticClass: "container-fluid mt-4 border mx-1 bg-light" }, [
      _c("div", { staticClass: "col-12 mx-1" }, [
        _c("div", { staticClass: "mt-4 mb-3" }, [
          _c(
            "div",
            { staticClass: "section-title text-dark" },
            [
              _vm.$store.getters.isEdit
                ? [
                    _c("h3", { staticClass: "font-weight-bold" }, [
                      _vm._v("Update a Product")
                    ])
                  ]
                : [
                    _c("h3", { staticClass: "font-weight-bold" }, [
                      _vm._v("Add a Product")
                    ])
                  ]
            ],
            2
          )
        ])
      ])
    ]),
    _vm._v(" "),
    _c(
      "form",
      {
        attrs: { enctype: "multipart/form-data" },
        on: {
          submit: function($event) {
            $event.preventDefault()
            _vm.$store.getters.isEdit
              ? _vm.editProduct(_vm.$route.params.id)
              : _vm.addProduct()
          }
        }
      },
      [
        _c(
          "div",
          { staticClass: "container-fluid my-2 border mx-1 bg-light" },
          [
            _c("div", { staticClass: "col-12 mx-1" }, [
              _c(
                "div",
                { staticClass: "my-2 mb-3 d-flex " },
                _vm._l(_vm.categories, function(category) {
                  return _c("div", { staticClass: "col-sm-6" }, [
                    _c("i", { attrs: { color: "green" } }, [
                      _vm._v("---Click Here for Category Selection---")
                    ]),
                    _c("br"),
                    _vm._v(" "),
                    _c("i", { staticClass: "fas fa-user" }),
                    _vm._v(" "),
                    _c(
                      "span",
                      {
                        staticStyle: { cursor: "pointer" },
                        on: {
                          click: function($event) {
                            return _vm.toggle(category.id)
                          }
                        }
                      },
                      [_vm._v(_vm._s(category.label))]
                    ),
                    _vm._v(" "),
                    _vm.catError.status
                      ? _c("span", { staticStyle: { color: "red" } }, [
                          _vm._v("*" + _vm._s(_vm.catError.val))
                        ])
                      : _vm._e(),
                    _vm._v(" "),
                    _c(
                      "div",
                      {
                        directives: [
                          {
                            name: "show",
                            rawName: "v-show",
                            value: false,
                            expression: "false"
                          }
                        ],
                        attrs: { id: category.id }
                      },
                      _vm._l(category.childs, function(child) {
                        return _c(
                          "div",
                          {
                            staticStyle: {
                              "margin-left": "20px",
                              cursor: "pointer"
                            }
                          },
                          [
                            _c("i", { staticClass: "fas fa-user" }),
                            _vm._v(" "),
                            _c(
                              "span",
                              {
                                on: {
                                  click: function($event) {
                                    return _vm.toggle(child.id)
                                  }
                                }
                              },
                              [_vm._v(_vm._s(child.label))]
                            ),
                            _vm._v(" "),
                            _c(
                              "div",
                              {
                                directives: [
                                  {
                                    name: "show",
                                    rawName: "v-show",
                                    value: false,
                                    expression: "false"
                                  }
                                ],
                                attrs: { id: child.id }
                              },
                              [
                                _vm._l(child.grandchilds, function(grand) {
                                  return _c(
                                    "div",
                                    {
                                      staticStyle: {
                                        "margin-left": "30px",
                                        cursor: "pointer"
                                      }
                                    },
                                    [
                                      _c("input", {
                                        directives: [
                                          {
                                            name: "model",
                                            rawName: "v-model",
                                            value: _vm.form.category,
                                            expression: "form.category"
                                          }
                                        ],
                                        staticClass: "form-check-input",
                                        class: {
                                          "is-invalid": _vm.form.errors.has(
                                            "category"
                                          )
                                        },
                                        attrs: {
                                          type: "radio",
                                          id: grand.label + grand.id,
                                          text: grand.label,
                                          name: "category"
                                        },
                                        domProps: {
                                          value: grand.id,
                                          checked: _vm._q(
                                            _vm.form.category,
                                            grand.id
                                          )
                                        },
                                        on: {
                                          input: function($event) {
                                            return _vm.getBrands(
                                              category.label,
                                              child.label
                                            )
                                          },
                                          change: function($event) {
                                            return _vm.$set(
                                              _vm.form,
                                              "category",
                                              grand.id
                                            )
                                          }
                                        }
                                      }),
                                      _vm._v(" "),
                                      _c(
                                        "label",
                                        {
                                          staticClass:
                                            "form-check-label py-1 px-2 ",
                                          attrs: { for: grand.label + grand.id }
                                        },
                                        [
                                          _vm._v(
                                            "\n                                            " +
                                              _vm._s(grand.label) +
                                              "\n                                        "
                                          )
                                        ]
                                      )
                                    ]
                                  )
                                }),
                                _vm._v(" "),
                                _c("has-error", {
                                  attrs: { form: _vm.form, field: "category" }
                                })
                              ],
                              2
                            )
                          ]
                        )
                      }),
                      0
                    )
                  ])
                }),
                0
              )
            ])
          ]
        ),
        _vm._v(" "),
        _c(
          "div",
          {
            staticClass: "container-fluid mt-4 border bg-light select-container"
          },
          [
            _c(
              "div",
              { staticClass: "mt-4 mb-3 col-md-4 col-12 " },
              [
                _c(
                  "label",
                  { staticClass: "font-weight-light", attrs: { for: "brand" } },
                  [_vm._v("Product Brand:")]
                ),
                _vm._v(" "),
                _c(
                  "select",
                  {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.form.brand,
                        expression: "form.brand"
                      }
                    ],
                    staticClass: "form-control",
                    class: { "is-invalid": _vm.form.errors.has("brand") },
                    attrs: { name: "brand", id: "brand" },
                    on: {
                      change: function($event) {
                        var $$selectedVal = Array.prototype.filter
                          .call($event.target.options, function(o) {
                            return o.selected
                          })
                          .map(function(o) {
                            var val = "_value" in o ? o._value : o.value
                            return val
                          })
                        _vm.$set(
                          _vm.form,
                          "brand",
                          $event.target.multiple
                            ? $$selectedVal
                            : $$selectedVal[0]
                        )
                      }
                    }
                  },
                  [
                    _c("option", { attrs: { disabled: "", value: "" } }, [
                      _vm._v("Select Brand")
                    ]),
                    _vm._v(" "),
                    _vm._l(_vm.brands, function(brand) {
                      return _c("option", { domProps: { value: brand.id } }, [
                        _vm._v(_vm._s(brand.name))
                      ])
                    })
                  ],
                  2
                ),
                _vm._v(" "),
                _c("has-error", { attrs: { form: _vm.form, field: "brand" } }),
                _vm._v(" "),
                _c("small", [
                  _vm._v(
                    "The brands are shown according to the category selected above."
                  )
                ])
              ],
              1
            ),
            _vm._v(" "),
            _c(
              "div",
              { staticClass: "mt-4 mb-3 col-md-4 col-12" },
              [
                _c(
                  "label",
                  {
                    staticClass: "font-weight-light",
                    attrs: { for: "fabric" }
                  },
                  [_vm._v("Product Fabric:")]
                ),
                _vm._v(" "),
                _c(
                  "select",
                  {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.form.fabric,
                        expression: "form.fabric"
                      }
                    ],
                    staticClass: "form-control",
                    class: { "is-invalid": _vm.form.errors.has("fabric") },
                    attrs: { id: "fabric", name: "fabric" },
                    on: {
                      change: function($event) {
                        var $$selectedVal = Array.prototype.filter
                          .call($event.target.options, function(o) {
                            return o.selected
                          })
                          .map(function(o) {
                            var val = "_value" in o ? o._value : o.value
                            return val
                          })
                        _vm.$set(
                          _vm.form,
                          "fabric",
                          $event.target.multiple
                            ? $$selectedVal
                            : $$selectedVal[0]
                        )
                      }
                    }
                  },
                  [
                    _c("option", { attrs: { disabled: "", value: "" } }, [
                      _vm._v("Select fabric")
                    ]),
                    _vm._v(" "),
                    _vm._l(_vm.fabrics, function(fabric) {
                      return _c("option", { domProps: { value: fabric.id } }, [
                        _vm._v(_vm._s(fabric.name))
                      ])
                    })
                  ],
                  2
                ),
                _vm._v(" "),
                _c("has-error", { attrs: { form: _vm.form, field: "fabric" } })
              ],
              1
            ),
            _vm._v(" "),
            _c(
              "div",
              { staticClass: "mt-4 mb-3 col-md-4 col-12" },
              [
                _c(
                  "label",
                  {
                    staticClass: "font-weight-light",
                    attrs: { for: "occasion" }
                  },
                  [_vm._v("Occasion:")]
                ),
                _vm._v(" "),
                _c(
                  "select",
                  {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.form.occasion,
                        expression: "form.occasion"
                      }
                    ],
                    staticClass: "form-control",
                    class: { "is-invalid": _vm.form.errors.has("fabric") },
                    attrs: { id: "occasion", name: "occasion" },
                    on: {
                      change: function($event) {
                        var $$selectedVal = Array.prototype.filter
                          .call($event.target.options, function(o) {
                            return o.selected
                          })
                          .map(function(o) {
                            var val = "_value" in o ? o._value : o.value
                            return val
                          })
                        _vm.$set(
                          _vm.form,
                          "occasion",
                          $event.target.multiple
                            ? $$selectedVal
                            : $$selectedVal[0]
                        )
                      }
                    }
                  },
                  [
                    _c("option", { attrs: { disabled: "", value: "" } }, [
                      _vm._v("Select the occasion")
                    ]),
                    _vm._v(" "),
                    _vm._l(_vm.occasions, function(occasion) {
                      return _c(
                        "option",
                        { domProps: { value: occasion.id } },
                        [_vm._v(_vm._s(occasion.name))]
                      )
                    })
                  ],
                  2
                ),
                _vm._v(" "),
                _c("has-error", {
                  attrs: { form: _vm.form, field: "occasion" }
                })
              ],
              1
            )
          ]
        ),
        _vm._v(" "),
        _c(
          "div",
          { staticClass: "container-fluid mt-4 border mx-1 bg-light" },
          [
            _c(
              "div",
              { staticClass: "mt-4 mb-3" },
              [
                _c(
                  "div",
                  { staticClass: "d-flex mb-3" },
                  [
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.form.name,
                          expression: "form.name"
                        }
                      ],
                      staticClass: "form-control col-sm-6 mr-4",
                      class: { "is-invalid": _vm.form.errors.has("name") },
                      attrs: { name: "name", placeholder: "Product name" },
                      domProps: { value: _vm.form.name },
                      on: {
                        input: function($event) {
                          if ($event.target.composing) {
                            return
                          }
                          _vm.$set(_vm.form, "name", $event.target.value)
                        }
                      }
                    }),
                    _c("br"),
                    _vm._v(" "),
                    _c("has-error", {
                      attrs: { form: _vm.form, field: "name" }
                    }),
                    _vm._v(" "),
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.form.purchase_price,
                          expression: "form.purchase_price"
                        }
                      ],
                      staticClass: "form-control col-sm-2 mr-4",
                      class: {
                        "is-invalid": _vm.form.errors.has("purchase_price")
                      },
                      attrs: {
                        type: "number",
                        name: "purchase_price",
                        placeholder: "Purchase Price"
                      },
                      domProps: { value: _vm.form.purchase_price },
                      on: {
                        input: function($event) {
                          if ($event.target.composing) {
                            return
                          }
                          _vm.$set(
                            _vm.form,
                            "purchase_price",
                            $event.target.value
                          )
                        }
                      }
                    }),
                    _c("br"),
                    _vm._v(" "),
                    _c("has-error", {
                      attrs: { form: _vm.form, field: "purchase_price" }
                    }),
                    _vm._v(" "),
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.form.price,
                          expression: "form.price"
                        }
                      ],
                      staticClass: "form-control col-sm-2",
                      class: { "is-invalid": _vm.form.errors.has("price") },
                      attrs: {
                        type: "number",
                        name: "price",
                        placeholder: "Selling Price"
                      },
                      domProps: { value: _vm.form.price },
                      on: {
                        input: function($event) {
                          if ($event.target.composing) {
                            return
                          }
                          _vm.$set(_vm.form, "price", $event.target.value)
                        }
                      }
                    }),
                    _c("br"),
                    _vm._v(" "),
                    _c("has-error", {
                      attrs: { form: _vm.form, field: "price" }
                    })
                  ],
                  1
                ),
                _vm._v(" "),
                _c("VueTrix", {
                  model: {
                    value: _vm.form.description,
                    callback: function($$v) {
                      _vm.$set(_vm.form, "description", $$v)
                    },
                    expression: "form.description"
                  }
                })
              ],
              1
            )
          ]
        ),
        _vm._v(" "),
        _c(
          "div",
          { staticClass: "container-fluid mt-4 border mx-1 bg-light " },
          [
            _c(
              "label",
              {
                staticClass: "font-weight-light mt-3 col-md-3 col-12",
                attrs: { for: "field" }
              },
              [_vm._v("Product option field:")]
            ),
            _vm._v(" "),
            _vm._l(_vm.form.fields, function(field, index) {
              return _c(
                "div",
                { staticClass: "select-container", attrs: { id: "field" } },
                [
                  _c(
                    "select",
                    {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: field.color,
                          expression: "field.color"
                        }
                      ],
                      staticClass: "form-control col-md-3 col-12 m-2",
                      class: { "is-invalid": _vm.form.errors.has("color") },
                      attrs: { name: "color", id: "color", required: "" },
                      on: {
                        change: [
                          function($event) {
                            var $$selectedVal = Array.prototype.filter
                              .call($event.target.options, function(o) {
                                return o.selected
                              })
                              .map(function(o) {
                                var val = "_value" in o ? o._value : o.value
                                return val
                              })
                            _vm.$set(
                              field,
                              "color",
                              $event.target.multiple
                                ? $$selectedVal
                                : $$selectedVal[0]
                            )
                          },
                          function($event) {
                            return _vm.setColor(field.color)
                          }
                        ]
                      }
                    },
                    [
                      _c("option", { attrs: { disabled: "", value: "" } }, [
                        _vm._v("Select Color")
                      ]),
                      _vm._v(" "),
                      _vm._l(_vm.colors, function(color) {
                        return _c("option", { domProps: { value: color.id } }, [
                          _vm._v(_vm._s(color.name))
                        ])
                      })
                    ],
                    2
                  ),
                  _vm._v(" "),
                  _c("has-error", {
                    attrs: { form: _vm.form, field: "color" }
                  }),
                  _vm._v(" "),
                  _c(
                    "select",
                    {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: field.size,
                          expression: "field.size"
                        }
                      ],
                      staticClass: "form-control col-md-3 col-12 m-2",
                      attrs: { id: "size", required: "" },
                      on: {
                        change: function($event) {
                          var $$selectedVal = Array.prototype.filter
                            .call($event.target.options, function(o) {
                              return o.selected
                            })
                            .map(function(o) {
                              var val = "_value" in o ? o._value : o.value
                              return val
                            })
                          _vm.$set(
                            field,
                            "size",
                            $event.target.multiple
                              ? $$selectedVal
                              : $$selectedVal[0]
                          )
                        }
                      }
                    },
                    [
                      _c("option", { attrs: { disabled: "", value: "" } }, [
                        _vm._v("Select Size")
                      ]),
                      _vm._v(" "),
                      _vm._l(_vm.sizes, function(size) {
                        return _c("option", { domProps: { value: size.id } }, [
                          _vm._v(_vm._s(size.name))
                        ])
                      })
                    ],
                    2
                  ),
                  _vm._v(" "),
                  _c("input", {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: field.quantity,
                        expression: "field.quantity"
                      }
                    ],
                    staticClass: "form-control col-md-2 col-12 m-2",
                    attrs: {
                      type: "number",
                      placeholder: "Product quantity",
                      required: ""
                    },
                    domProps: { value: field.quantity },
                    on: {
                      input: function($event) {
                        if ($event.target.composing) {
                          return
                        }
                        _vm.$set(field, "quantity", $event.target.value)
                      }
                    }
                  }),
                  _vm._v(" "),
                  _c("input", {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: field.minimum,
                        expression: "field.minimum"
                      }
                    ],
                    staticClass: "form-control col-md-2 col-12 m-2",
                    attrs: {
                      type: "number",
                      placeholder: "Minimum stock",
                      required: ""
                    },
                    domProps: { value: field.minimum },
                    on: {
                      input: function($event) {
                        if ($event.target.composing) {
                          return
                        }
                        _vm.$set(field, "minimum", $event.target.value)
                      }
                    }
                  }),
                  _vm._v(" "),
                  index != 0
                    ? _c("i", {
                        staticClass: "fas fa-times",
                        on: {
                          click: function($event) {
                            return _vm.removeField(index)
                          }
                        }
                      })
                    : _vm._e()
                ],
                1
              )
            }),
            _vm._v(" "),
            _c(
              "span",
              {
                staticClass: "btn btn-outline-primary m-2",
                on: { click: _vm.AddField }
              },
              [_vm._v("New Field")]
            )
          ],
          2
        ),
        _vm._v(" "),
        _c(
          "div",
          { staticClass: "container-fluid mt-4 border mx-1 bg-light " },
          [
            _c(
              "label",
              {
                staticClass: "font-weight-light mt-3 col-md-3 col-12",
                attrs: { for: "field" }
              },
              [_vm._v("Store:")]
            ),
            _vm._v(" "),
            _c(
              "select",
              {
                directives: [
                  {
                    name: "model",
                    rawName: "v-model",
                    value: _vm.form.store,
                    expression: "form.store"
                  }
                ],
                staticClass: "form-control col-md-3 col-12 m-2",
                class: { "is-invalid": _vm.form.errors.has("store") },
                attrs: { name: "store", id: "store", required: "" },
                on: {
                  change: function($event) {
                    var $$selectedVal = Array.prototype.filter
                      .call($event.target.options, function(o) {
                        return o.selected
                      })
                      .map(function(o) {
                        var val = "_value" in o ? o._value : o.value
                        return val
                      })
                    _vm.$set(
                      _vm.form,
                      "store",
                      $event.target.multiple ? $$selectedVal : $$selectedVal[0]
                    )
                  }
                }
              },
              [
                _c("option", { attrs: { disabled: "", value: "" } }, [
                  _vm._v("Select Store")
                ]),
                _vm._v(" "),
                _vm._l(_vm.stores, function(store) {
                  return _c("option", { domProps: { value: store.id } }, [
                    _vm._v(_vm._s(store.store_name))
                  ])
                })
              ],
              2
            ),
            _vm._v(" "),
            _c("has-error", { attrs: { form: _vm.form, field: "store" } })
          ],
          1
        ),
        _vm._v(" "),
        _c(
          "div",
          { staticClass: "container-fluid mt-4 border mx-1 bg-light " },
          [
            _c(
              "label",
              {
                staticClass: "font-weight-light mt-3 col-md-3 col-12",
                attrs: { for: "thumbnail" }
              },
              [_vm._v("Thumbnail:")]
            ),
            _vm._v(" "),
            _c("input", {
              attrs: { type: "file", name: "thumbnail", id: "thumbnail" },
              on: { change: _vm.loadUploadedImage }
            })
          ]
        ),
        _vm._v(" "),
        _c(
          "div",
          { staticClass: "container-fluid mt-4 border mx-1 bg-light " },
          [
            _c(
              "label",
              {
                staticClass: "font-weight-light mt-3 col-md-3 col-12",
                attrs: { for: "phototype" }
              },
              [_vm._v("Image Type (Camera Or Web):")]
            ),
            _vm._v(" "),
            _c(
              "select",
              {
                directives: [
                  {
                    name: "model",
                    rawName: "v-model",
                    value: _vm.form.phototype,
                    expression: "form.phototype"
                  }
                ],
                staticClass: "form-control col-md-3 col-12 m-2",
                class: { "is-invalid": _vm.form.errors.has("phototype") },
                attrs: { name: "phototype", id: "phototype", required: "" },
                on: {
                  change: function($event) {
                    var $$selectedVal = Array.prototype.filter
                      .call($event.target.options, function(o) {
                        return o.selected
                      })
                      .map(function(o) {
                        var val = "_value" in o ? o._value : o.value
                        return val
                      })
                    _vm.$set(
                      _vm.form,
                      "phototype",
                      $event.target.multiple ? $$selectedVal : $$selectedVal[0]
                    )
                  }
                }
              },
              [
                _c("option", { attrs: { disabled: "", value: "" } }, [
                  _vm._v("Select image type")
                ]),
                _vm._v(" "),
                _c("option", { attrs: { value: "camera" } }, [
                  _vm._v("Camera")
                ]),
                _vm._v(" "),
                _c("option", { attrs: { value: "web" } }, [_vm._v("Web")])
              ]
            ),
            _vm._v(" "),
            _c("has-error", { attrs: { form: _vm.form, field: "phototype" } })
          ],
          1
        ),
        _vm._v(" "),
        _c(
          "div",
          { staticClass: "container-fluid my-2 py-3 mx-1 border bg-light" },
          [
            _c("div", { staticClass: "col-12 mx-1" }, [
              _c(
                "div",
                { staticClass: "my-3" },
                [
                  _vm._m(0),
                  _vm._v(" "),
                  _vm.imageError.status
                    ? _c("span", { staticStyle: { color: "red" } }, [
                        _vm._v("*" + _vm._s(_vm.imageError.val))
                      ])
                    : _vm._e(),
                  _vm._v(" "),
                  _vm._l(_vm.form.cimages, function(field, index) {
                    return _c("div", [
                      _c("div", { staticClass: "form-row my-4" }, [
                        _c(
                          "select",
                          {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: field.color,
                                expression: "field.color"
                              }
                            ],
                            staticClass: "form-control col-sm",
                            attrs: { id: "color_image", required: "" },
                            on: {
                              change: [
                                function($event) {
                                  var $$selectedVal = Array.prototype.filter
                                    .call($event.target.options, function(o) {
                                      return o.selected
                                    })
                                    .map(function(o) {
                                      var val =
                                        "_value" in o ? o._value : o.value
                                      return val
                                    })
                                  _vm.$set(
                                    field,
                                    "color",
                                    $event.target.multiple
                                      ? $$selectedVal
                                      : $$selectedVal[0]
                                  )
                                },
                                function($event) {
                                  return _vm.checkSelected($event)
                                }
                              ]
                            }
                          },
                          [
                            _c(
                              "option",
                              { attrs: { disabled: "", value: "" } },
                              [_vm._v("Select Color")]
                            ),
                            _vm._v(" "),
                            _vm._l(_vm.colorsForPhoto, function(sColor) {
                              return [
                                _vm._l(_vm.colors, function(color) {
                                  return [
                                    color.id == sColor
                                      ? _c(
                                          "option",
                                          { domProps: { value: color.id } },
                                          [
                                            _vm._v(
                                              "\n                                            " +
                                                _vm._s(color.name) +
                                                "\n                                        "
                                            )
                                          ]
                                        )
                                      : _vm._e()
                                  ]
                                })
                              ]
                            })
                          ],
                          2
                        ),
                        _vm._v(" "),
                        _c(
                          "div",
                          { staticClass: "col-sm justify-content-center" },
                          [
                            !_vm.image
                              ? _c(
                                  "label",
                                  { staticClass: "btn display-inline" },
                                  [
                                    _c("input", {
                                      attrs: {
                                        type: "file",
                                        name: "images" + index,
                                        multiple: ""
                                      },
                                      on: { change: _vm.onChange }
                                    })
                                  ]
                                )
                              : _vm._e()
                          ]
                        ),
                        _vm._v(" "),
                        _c("div", { staticClass: "col-sm" }, [
                          index != 0
                            ? _c("i", {
                                staticClass: "fas fa-times",
                                on: {
                                  click: function($event) {
                                    return _vm.removeImageField(index)
                                  }
                                }
                              })
                            : _vm._e()
                        ])
                      ])
                    ])
                  }),
                  _vm._v(" "),
                  _c(
                    "span",
                    {
                      staticClass: "btn btn-outline-primary m-2",
                      on: { click: _vm.addImage }
                    },
                    [_vm._v("New Field")]
                  )
                ],
                2
              )
            ])
          ]
        ),
        _vm._v(" "),
        _c(
          "div",
          { staticClass: "d-flex justify-content-center mb-2" },
          [
            _vm.$store.getters.isEdit
              ? [
                  _c("input", {
                    staticClass: "btn btn-primary",
                    attrs: {
                      type: "submit",
                      name: "submit",
                      value: "Update Product"
                    }
                  })
                ]
              : [
                  _c("input", {
                    staticClass: "btn btn-primary",
                    attrs: {
                      type: "submit",
                      name: "submit",
                      value: "Add Product"
                    }
                  })
                ]
          ],
          2
        )
      ]
    )
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "section-title text-dark" }, [
      _c("h5", { staticClass: "font-weight-bold" }, [
        _vm._v("Upload Photos : ")
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/components/backend/product/AddProduct.vue":
/*!****************************************************************!*\
  !*** ./resources/js/components/backend/product/AddProduct.vue ***!
  \****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _AddProduct_vue_vue_type_template_id_0530b898_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./AddProduct.vue?vue&type=template&id=0530b898&scoped=true& */ "./resources/js/components/backend/product/AddProduct.vue?vue&type=template&id=0530b898&scoped=true&");
/* harmony import */ var _AddProduct_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./AddProduct.vue?vue&type=script&lang=js& */ "./resources/js/components/backend/product/AddProduct.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _AddProduct_vue_vue_type_style_index_0_id_0530b898_scoped_true_lang_scss___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./AddProduct.vue?vue&type=style&index=0&id=0530b898&scoped=true&lang=scss& */ "./resources/js/components/backend/product/AddProduct.vue?vue&type=style&index=0&id=0530b898&scoped=true&lang=scss&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _AddProduct_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _AddProduct_vue_vue_type_template_id_0530b898_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _AddProduct_vue_vue_type_template_id_0530b898_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "0530b898",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/backend/product/AddProduct.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/backend/product/AddProduct.vue?vue&type=script&lang=js&":
/*!*****************************************************************************************!*\
  !*** ./resources/js/components/backend/product/AddProduct.vue?vue&type=script&lang=js& ***!
  \*****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_AddProduct_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./AddProduct.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backend/product/AddProduct.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_AddProduct_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/backend/product/AddProduct.vue?vue&type=style&index=0&id=0530b898&scoped=true&lang=scss&":
/*!**************************************************************************************************************************!*\
  !*** ./resources/js/components/backend/product/AddProduct.vue?vue&type=style&index=0&id=0530b898&scoped=true&lang=scss& ***!
  \**************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_AddProduct_vue_vue_type_style_index_0_id_0530b898_scoped_true_lang_scss___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/style-loader!../../../../../node_modules/css-loader!../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../node_modules/postcss-loader/src??ref--7-2!../../../../../node_modules/sass-loader/dist/cjs.js??ref--7-3!../../../../../node_modules/vue-loader/lib??vue-loader-options!./AddProduct.vue?vue&type=style&index=0&id=0530b898&scoped=true&lang=scss& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backend/product/AddProduct.vue?vue&type=style&index=0&id=0530b898&scoped=true&lang=scss&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_AddProduct_vue_vue_type_style_index_0_id_0530b898_scoped_true_lang_scss___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_AddProduct_vue_vue_type_style_index_0_id_0530b898_scoped_true_lang_scss___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_AddProduct_vue_vue_type_style_index_0_id_0530b898_scoped_true_lang_scss___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_AddProduct_vue_vue_type_style_index_0_id_0530b898_scoped_true_lang_scss___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_AddProduct_vue_vue_type_style_index_0_id_0530b898_scoped_true_lang_scss___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./resources/js/components/backend/product/AddProduct.vue?vue&type=template&id=0530b898&scoped=true&":
/*!***********************************************************************************************************!*\
  !*** ./resources/js/components/backend/product/AddProduct.vue?vue&type=template&id=0530b898&scoped=true& ***!
  \***********************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_AddProduct_vue_vue_type_template_id_0530b898_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./AddProduct.vue?vue&type=template&id=0530b898&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backend/product/AddProduct.vue?vue&type=template&id=0530b898&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_AddProduct_vue_vue_type_template_id_0530b898_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_AddProduct_vue_vue_type_template_id_0530b898_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);