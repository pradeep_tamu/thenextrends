(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[56],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backend/sales/AddSales.vue?vue&type=script&lang=js&":
/*!*********************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/backend/sales/AddSales.vue?vue&type=script&lang=js& ***!
  \*********************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  name: "add-sales",
  data: function data() {
    return {
      loading: true,
      form: new Form({
        product_id: '',
        rate: ''
      }),
      store: 'Choose Store...',
      stores: [],
      data: [],
      filters: [{
        prop: ['id', 'name', 'price', 'category'],
        value: ''
      }]
    };
  },
  created: function created() {
    var _this = this;

    axios.get('/api/getStores').then(function (response) {
      _this.stores = response.data;
    });
  },
  methods: {
    getProducts: function getProducts() {
      var _this2 = this;

      this.loading = true;
      axios.get('/api/getStoreProducts/' + this.store).then(function (response) {
        _this2.data = response.data;
        _this2.loading = false;
      });
    },
    setProduct: function setProduct(id, rate) {
      this.form.reset();
      this.form.product_id = id;

      if (rate !== '') {
        this.form.rate = rate * 100;
      }
    },
    addSales: function addSales() {
      var _this3 = this;

      this.$Progress.start();
      this.form.post('/api/setSales').then(function (response) {
        _this3.form.reset();

        $('#addSales').modal('hide');
        Toast.fire({
          type: 'success',
          title: 'Sales Added!!!'
        });

        _this3.$Progress.finish();
      })["catch"](function (error) {
        _this3.$Progress.fail();
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backend/sales/AddSales.vue?vue&type=template&id=28f8a714&scoped=true&":
/*!*************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/backend/sales/AddSales.vue?vue&type=template&id=28f8a714&scoped=true& ***!
  \*************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _c(
      "div",
      {
        staticClass: "modal fade",
        attrs: {
          id: "addSales",
          tabindex: "-1",
          role: "dialog",
          "aria-labelledby": "modal-label",
          "aria-hidden": "true"
        }
      },
      [
        _c(
          "div",
          { staticClass: "modal-dialog", attrs: { role: "document" } },
          [
            _c("div", { staticClass: "modal-content" }, [
              _vm._m(0),
              _vm._v(" "),
              _c(
                "form",
                {
                  on: {
                    submit: function($event) {
                      $event.preventDefault()
                      return _vm.addSales($event)
                    }
                  }
                },
                [
                  _c("div", { staticClass: "modal-body" }, [
                    _c(
                      "div",
                      { staticClass: "form-group" },
                      [
                        _c("input", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.form.rate,
                              expression: "form.rate"
                            }
                          ],
                          staticClass: "form-control",
                          class: { "is-invalid": _vm.form.errors.has("rate") },
                          attrs: {
                            name: "rate",
                            id: "rate",
                            placeholder: "Rate",
                            required: ""
                          },
                          domProps: { value: _vm.form.rate },
                          on: {
                            input: function($event) {
                              if ($event.target.composing) {
                                return
                              }
                              _vm.$set(_vm.form, "rate", $event.target.value)
                            }
                          }
                        }),
                        _vm._v(" "),
                        _c(
                          "small",
                          {
                            staticClass: "form-text text-muted",
                            attrs: { id: "rateHelp" }
                          },
                          [
                            _vm._v(
                              "\n                                For Example: If rate is 12.5% then 12.5\n                            "
                            )
                          ]
                        ),
                        _vm._v(" "),
                        _c("has-error", {
                          attrs: { form: _vm.form, field: "rate" }
                        })
                      ],
                      1
                    )
                  ]),
                  _vm._v(" "),
                  _vm._m(1)
                ]
              )
            ])
          ]
        )
      ]
    ),
    _vm._v(" "),
    _c("div", { staticClass: "col-5 mt-5" }, [
      _c("div", { staticClass: "input-group" }, [
        _c(
          "select",
          {
            directives: [
              {
                name: "model",
                rawName: "v-model",
                value: _vm.store,
                expression: "store"
              }
            ],
            staticClass: "custom-select",
            attrs: {
              id: "inputGroupSelect04",
              "aria-label": "Example select with button addon"
            },
            on: {
              change: function($event) {
                var $$selectedVal = Array.prototype.filter
                  .call($event.target.options, function(o) {
                    return o.selected
                  })
                  .map(function(o) {
                    var val = "_value" in o ? o._value : o.value
                    return val
                  })
                _vm.store = $event.target.multiple
                  ? $$selectedVal
                  : $$selectedVal[0]
              }
            }
          },
          [
            _c("option", { attrs: { selected: "" } }, [
              _vm._v("Choose Store...")
            ]),
            _vm._v(" "),
            _vm._l(_vm.stores, function(store) {
              return [
                _c("option", { domProps: { value: store.id } }, [
                  _vm._v(_vm._s(store.store_name))
                ])
              ]
            })
          ],
          2
        ),
        _vm._v(" "),
        _c("div", { staticClass: "input-group-append" }, [
          _c(
            "button",
            {
              staticClass: "btn btn-outline-secondary",
              attrs: { type: "button" },
              on: { click: _vm.getProducts }
            },
            [_vm._v("Select")]
          )
        ])
      ])
    ]),
    _vm._v(" "),
    _c("hr"),
    _vm._v(" "),
    _vm.loading
      ? _c("div", { staticStyle: { height: "100vh" } }, [
          _c(
            "div",
            { staticClass: "mt-5", staticStyle: { "margin-left": "45%" } },
            [
              _vm.data.length > 0
                ? _c("span", [_vm._v("Loading....")])
                : _c("span", [_vm._v("Select a store first")]),
              _vm._v(" "),
              _vm._m(2)
            ]
          )
        ])
      : _c(
          "div",
          { staticClass: "pt-3 pb-5" },
          [
            _c("h4", [_vm._v("Products")]),
            _vm._v(" "),
            _c(
              "div",
              { staticClass: "mt-3 mb-2" },
              [
                _c(
                  "el-row",
                  [
                    _c(
                      "el-col",
                      { attrs: { span: 8 } },
                      [
                        _c("el-input", {
                          attrs: { placeholder: "Search" },
                          model: {
                            value: _vm.filters[0].value,
                            callback: function($$v) {
                              _vm.$set(_vm.filters[0], "value", $$v)
                            },
                            expression: "filters[0].value"
                          }
                        })
                      ],
                      1
                    )
                  ],
                  1
                )
              ],
              1
            ),
            _vm._v(" "),
            _c(
              "data-tables",
              {
                attrs: {
                  data: _vm.data,
                  filters: _vm.filters,
                  "pagination-props": {
                    background: true,
                    pageSizes: [10, 20, 50]
                  }
                }
              },
              [
                _c("el-table-column", {
                  attrs: { prop: "id", label: "ID", sortable: "custom" }
                }),
                _vm._v(" "),
                _c("el-table-column", {
                  attrs: {
                    prop: "name",
                    label: "Product Name",
                    sortable: "custom"
                  }
                }),
                _vm._v(" "),
                _c("el-table-column", {
                  attrs: { prop: "price", label: "Price", sortable: "custom" }
                }),
                _vm._v(" "),
                _c("el-table-column", {
                  attrs: {
                    prop: "category.label",
                    label: "Category",
                    sortable: "custom"
                  }
                }),
                _vm._v(" "),
                _c("el-table-column", {
                  attrs: { label: "Action" },
                  scopedSlots: _vm._u([
                    {
                      key: "default",
                      fn: function(scope) {
                        return [
                          _c(
                            "button",
                            {
                              staticClass: "btn btn-outline-dark btn-sm",
                              attrs: {
                                type: "button",
                                "data-toggle": "modal",
                                "data-target": "#addSales"
                              },
                              on: {
                                click: function($event) {
                                  scope.row.sales !== null
                                    ? _vm.setProduct(
                                        scope.row.id,
                                        scope.row.sales.rate
                                      )
                                    : _vm.setProduct(scope.row.id, "")
                                }
                              }
                            },
                            [
                              _c("i", { staticClass: "fas fa-plus-circle" }),
                              _vm._v(" "),
                              scope.row.sales !== null
                                ? _c("span", [_vm._v("Edit ")])
                                : _c("span", [_vm._v("Add ")]),
                              _vm._v(
                                "\n                        Sales\n                    "
                              )
                            ]
                          )
                        ]
                      }
                    }
                  ])
                })
              ],
              1
            )
          ],
          1
        )
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "modal-header" }, [
      _c("h5", { staticClass: "modal-title", attrs: { id: "modal-label" } }, [
        _vm._v("Add Sales")
      ]),
      _vm._v(" "),
      _c(
        "button",
        {
          staticClass: "close",
          attrs: {
            type: "button",
            "data-dismiss": "modal",
            "aria-label": "Close"
          }
        },
        [_c("span", { attrs: { "aria-hidden": "true" } }, [_vm._v("×")])]
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "modal-footer" }, [
      _c(
        "button",
        {
          staticClass: "btn btn-secondary",
          attrs: { type: "button", "data-dismiss": "modal" }
        },
        [_vm._v("Close")]
      ),
      _vm._v(" "),
      _c(
        "button",
        { staticClass: "btn btn-outline-primary", attrs: { type: "submit" } },
        [_vm._v("Submit")]
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "div",
      { staticClass: "spinner-border", attrs: { role: "status" } },
      [_c("span", { staticClass: "sr-only" }, [_vm._v("Loading...")])]
    )
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js":
/*!********************************************************************!*\
  !*** ./node_modules/vue-loader/lib/runtime/componentNormalizer.js ***!
  \********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return normalizeComponent; });
/* globals __VUE_SSR_CONTEXT__ */

// IMPORTANT: Do NOT use ES2015 features in this file (except for modules).
// This module is a runtime utility for cleaner component module output and will
// be included in the final webpack user bundle.

function normalizeComponent (
  scriptExports,
  render,
  staticRenderFns,
  functionalTemplate,
  injectStyles,
  scopeId,
  moduleIdentifier, /* server only */
  shadowMode /* vue-cli only */
) {
  // Vue.extend constructor export interop
  var options = typeof scriptExports === 'function'
    ? scriptExports.options
    : scriptExports

  // render functions
  if (render) {
    options.render = render
    options.staticRenderFns = staticRenderFns
    options._compiled = true
  }

  // functional template
  if (functionalTemplate) {
    options.functional = true
  }

  // scopedId
  if (scopeId) {
    options._scopeId = 'data-v-' + scopeId
  }

  var hook
  if (moduleIdentifier) { // server build
    hook = function (context) {
      // 2.3 injection
      context =
        context || // cached call
        (this.$vnode && this.$vnode.ssrContext) || // stateful
        (this.parent && this.parent.$vnode && this.parent.$vnode.ssrContext) // functional
      // 2.2 with runInNewContext: true
      if (!context && typeof __VUE_SSR_CONTEXT__ !== 'undefined') {
        context = __VUE_SSR_CONTEXT__
      }
      // inject component styles
      if (injectStyles) {
        injectStyles.call(this, context)
      }
      // register component module identifier for async chunk inferrence
      if (context && context._registeredComponents) {
        context._registeredComponents.add(moduleIdentifier)
      }
    }
    // used by ssr in case component is cached and beforeCreate
    // never gets called
    options._ssrRegister = hook
  } else if (injectStyles) {
    hook = shadowMode
      ? function () { injectStyles.call(this, this.$root.$options.shadowRoot) }
      : injectStyles
  }

  if (hook) {
    if (options.functional) {
      // for template-only hot-reload because in that case the render fn doesn't
      // go through the normalizer
      options._injectStyles = hook
      // register for functioal component in vue file
      var originalRender = options.render
      options.render = function renderWithStyleInjection (h, context) {
        hook.call(context)
        return originalRender(h, context)
      }
    } else {
      // inject component registration as beforeCreate hook
      var existing = options.beforeCreate
      options.beforeCreate = existing
        ? [].concat(existing, hook)
        : [hook]
    }
  }

  return {
    exports: scriptExports,
    options: options
  }
}


/***/ }),

/***/ "./resources/js/components/backend/sales/AddSales.vue":
/*!************************************************************!*\
  !*** ./resources/js/components/backend/sales/AddSales.vue ***!
  \************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _AddSales_vue_vue_type_template_id_28f8a714_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./AddSales.vue?vue&type=template&id=28f8a714&scoped=true& */ "./resources/js/components/backend/sales/AddSales.vue?vue&type=template&id=28f8a714&scoped=true&");
/* harmony import */ var _AddSales_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./AddSales.vue?vue&type=script&lang=js& */ "./resources/js/components/backend/sales/AddSales.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _AddSales_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _AddSales_vue_vue_type_template_id_28f8a714_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _AddSales_vue_vue_type_template_id_28f8a714_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "28f8a714",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/backend/sales/AddSales.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/backend/sales/AddSales.vue?vue&type=script&lang=js&":
/*!*************************************************************************************!*\
  !*** ./resources/js/components/backend/sales/AddSales.vue?vue&type=script&lang=js& ***!
  \*************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_AddSales_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./AddSales.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backend/sales/AddSales.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_AddSales_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/backend/sales/AddSales.vue?vue&type=template&id=28f8a714&scoped=true&":
/*!*******************************************************************************************************!*\
  !*** ./resources/js/components/backend/sales/AddSales.vue?vue&type=template&id=28f8a714&scoped=true& ***!
  \*******************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_AddSales_vue_vue_type_template_id_28f8a714_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./AddSales.vue?vue&type=template&id=28f8a714&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backend/sales/AddSales.vue?vue&type=template&id=28f8a714&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_AddSales_vue_vue_type_template_id_28f8a714_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_AddSales_vue_vue_type_template_id_28f8a714_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);