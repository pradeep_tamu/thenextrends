(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[33],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/frontend/auth/Login.vue?vue&type=script&lang=js&":
/*!******************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/frontend/auth/Login.vue?vue&type=script&lang=js& ***!
  \******************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _helpers_auth__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../helpers/auth */ "./resources/js/components/frontend/helpers/auth.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'login-register',
  components: {
    'SocialLogin': function SocialLogin() {
      return Promise.all(/*! import() */[__webpack_require__.e(68), __webpack_require__.e(43)]).then(__webpack_require__.bind(null, /*! ./SocialLogin */ "./resources/js/components/frontend/auth/SocialLogin.vue"));
    }
  },
  data: function data() {
    return {
      form: {
        email: '',
        password: ''
      },
      url: '',
      registrationForm: new Form({
        name: '',
        email: '',
        password: '',
        password_confirmation: '',
        birthday: '',
        address: '',
        phone: '',
        gender: 'Male'
      }),
      loginError: '',
      message: '',
      isSignUpActive: false,
      props: ['url']
    };
  },
  beforeRouteEnter: function beforeRouteEnter(to, from, next) {
    next(function (vm) {
      vm.url = from.path;
    });
  },
  created: function created() {
    if (this.$route.query.signUp) {
      this.isSignUpActive = true;
    }
  },
  methods: {
    signUpButton: function signUpButton() {
      this.registrationForm.reset();
      this.isSignUpActive = true;
      this.message = '';
    },
    signInButton: function signInButton() {
      this.form.email = '';
      this.form.password = '';
      this.isSignUpActive = false;
    },
    authenticate: function authenticate() {
      var _this = this;

      this.$Progress.start();

      if (!this.form.email) {
        this.loginError = 'Please enter an email address.';
        this.$Progress.fail();
      } else if (!this.validEmail(this.form.email)) {
        this.loginError = 'Invalid email address';
        this.$Progress.fail();
      } else if (!this.form.password) {
        this.loginError = 'Please enter your password.';
        this.$Progress.fail();
      } else if (this.form.password.length < 6) {
        this.loginError = 'Password must be at least 6 characters.';
        this.$Progress.fail();
      } else {
        this.$store.dispatch('login');
        Object(_helpers_auth__WEBPACK_IMPORTED_MODULE_0__["login"])(this.$data.form).then(function (res) {
          _this.$Progress.finish();

          _this.$store.commit("loginSuccess", res);

          if (_this.url) {
            _this.$router.push(_this.url);
          } else {
            _this.$router.push('/');
          }
        })["catch"](function (error) {
          _this.$Progress.fail();

          _this.loginError = error.message;

          _this.$store.commit("loginFailed", {
            error: error
          });
        });
      }
    },
    register: function register() {
      var _this2 = this;

      this.$Progress.start();
      this.registrationForm.post('/api/register').then(function () {
        _this2.message = 'Your account has been registered. Please, activate your account before login.';

        _this2.registrationForm.reset();

        _this2.form.email = '';
        _this2.form.password = '';
        _this2.isSignUpActive = false;
        Toast.fire({
          type: 'success',
          title: 'Your Account has been created!!!'
        });

        _this2.$Progress.finish();
      })["catch"](function (error) {
        _this2.$Progress.fail();
      });
    },
    validEmail: function validEmail(email) {
      var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      return re.test(email);
    }
  }
});

/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/frontend/auth/Login.vue?vue&type=style&index=0&id=dedb9278&lang=scss&scoped=true&":
/*!*****************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/sass-loader/dist/cjs.js??ref--7-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/frontend/auth/Login.vue?vue&type=style&index=0&id=dedb9278&lang=scss&scoped=true& ***!
  \*****************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "@charset \"UTF-8\";\nh1[data-v-dedb9278] {\n  font-weight: bold;\n  margin: 0;\n}\np[data-v-dedb9278] {\n  font-size: 14px;\n  font-weight: 100;\n  line-height: 20px;\n  letter-spacing: 0.5px;\n  margin: 20px 0 30px;\n}\nspan[data-v-dedb9278] {\n  font-size: 12px;\n}\na[data-v-dedb9278] {\n  color: #333;\n  font-size: 14px;\n  text-decoration: none;\n  margin: 15px 0;\n}\nbutton[data-v-dedb9278] {\n  border-radius: 25px;\n  border: 1px solid #232323;\n  background-color: #232323;\n  color: #ffffff;\n  font-size: 12px;\n  font-weight: bold;\n  padding: 12px 45px;\n  letter-spacing: 1px;\n  text-transform: uppercase;\n  transition: transform 80ms ease-in;\n}\nbutton[data-v-dedb9278]:active {\n  transform: scale(0.95);\n}\nbutton[data-v-dedb9278]:focus {\n  outline: none;\n}\nbutton.ghost[data-v-dedb9278] {\n  background-color: transparent;\n  border-color: #ffffff;\n}\nform[data-v-dedb9278] {\n  background-color: #ffffff;\n  display: flex;\n  align-items: center;\n  justify-content: center;\n  flex-direction: column;\n  padding: 0 20px;\n  height: 100%;\n  /*text-align: center;*/\n}\nlabel[data-v-dedb9278] {\n  font-weight: 900 !important;\n  font-size: 1em;\n  color: black;\n  margin-bottom: 0 !important;\n}\ninput[data-v-dedb9278], select[data-v-dedb9278] {\n  border: gray 1px solid;\n  outline: none;\n  border-radius: 0;\n}\n.main-box[data-v-dedb9278] {\n  background-color: #ffffff;\n  /*border-radius: 8px;*/\n  /*box-shadow: 0 15px 30px rgba(0, 0, 0, 0.30), 0 10px 10px rgba(0, 0, 0, 0.25);*/\n  position: relative;\n  overflow: hidden;\n  width: 100vw;\n  max-width: 100%;\n  min-height: 110vh;\n  /*margin: 0 1rem;*/\n  /*top: 10vh;*/\n}\n.title-size[data-v-dedb9278] {\n  color: #232323;\n  font-weight: bold;\n  font-size: 2em;\n}\n.form-box[data-v-dedb9278] {\n  position: absolute;\n  top: 0;\n  height: 80%;\n  transition: all 0.6s ease-in-out;\n}\n.login-box[data-v-dedb9278] {\n  left: 0;\n  width: 50%;\n  z-index: 2;\n  margin: auto;\n}\n.register-box[data-v-dedb9278] {\n  left: 0;\n  width: 50%;\n  opacity: 0;\n  z-index: 1;\n  margin: auto;\n}\n.main-box.right-panel-active .login-box[data-v-dedb9278] {\n  transform: translateX(100%);\n}\n.main-box.right-panel-active .register-box[data-v-dedb9278] {\n  transform: translateX(100%);\n  opacity: 1;\n  z-index: 5;\n  -webkit-animation: show-data-v-dedb9278 0.6s;\n          animation: show-data-v-dedb9278 0.6s;\n}\n@-webkit-keyframes show-data-v-dedb9278 {\n0%, 49.99% {\n    opacity: 0;\n    z-index: 1;\n}\n50%, 100% {\n    opacity: 1;\n    z-index: 5;\n}\n}\n@keyframes show-data-v-dedb9278 {\n0%, 49.99% {\n    opacity: 0;\n    z-index: 1;\n}\n50%, 100% {\n    opacity: 1;\n    z-index: 5;\n}\n}\n.in-registration-box[data-v-dedb9278] {\n  display: none;\n}\n.in-login-box-on[data-v-dedb9278] {\n  transition: 1s;\n  margin-top: -7rem !important;\n}\n.in-login-box-off[data-v-dedb9278] {\n  transition: 1s;\n  margin-top: 1rem;\n}\n.overlay-box[data-v-dedb9278] {\n  position: absolute;\n  top: 0;\n  left: 50%;\n  width: 50%;\n  height: 100%;\n  overflow: hidden;\n  transition: transform 0.6s ease-in-out;\n  z-index: 100;\n}\n.main-box.right-panel-active .overlay-box[data-v-dedb9278] {\n  transform: translateX(-100%);\n}\n.overlay[data-v-dedb9278] {\n  background: linear-gradient(to right, #232323, #232323);\n  background-repeat: no-repeat;\n  background-size: cover;\n  background-position: 0 0;\n  color: #ffffff;\n  position: relative;\n  left: -100%;\n  height: 100%;\n  width: 200%;\n  transform: translateX(0);\n  transition: transform 0.6s ease-in-out;\n}\n.main-box.right-panel-active .overlay[data-v-dedb9278] {\n  transform: translateX(50%);\n}\n.overlay-panel[data-v-dedb9278] {\n  position: absolute;\n  display: flex;\n  align-items: center;\n  justify-content: center;\n  flex-direction: column;\n  padding: 0 50px;\n  text-align: center;\n  top: -10px;\n  height: 100%;\n  width: 50%;\n  transform: translateX(0);\n  transition: transform 0.6s ease-in-out;\n}\n.overlay-left[data-v-dedb9278] {\n  transform: translateX(-20%);\n}\n.main-box.right-panel-active .overlay-left[data-v-dedb9278] {\n  transform: translateX(0);\n}\n.overlay-right[data-v-dedb9278] {\n  right: 0;\n  transform: translateX(0);\n}\n.main-box.right-panel-active .overlay-right[data-v-dedb9278] {\n  transform: translateX(20%);\n}\n.mobile[data-v-dedb9278] {\n  margin-top: 1rem;\n  display: none;\n}\n@media screen and (max-width: 768px) {\nform[data-v-dedb9278] {\n    padding: 0 5rem;\n}\n.in-registration-box[data-v-dedb9278] {\n    display: block;\n}\n.in-login-box-on[data-v-dedb9278] {\n    margin-top: 1rem !important;\n}\n.in-login-box-off[data-v-dedb9278] {\n    transition: 1s;\n    margin-top: 10rem;\n}\n.overlay-box[data-v-dedb9278] {\n    display: none;\n}\n.main-box[data-v-dedb9278] {\n    overflow: unset;\n}\n.form-box[data-v-dedb9278] {\n    position: unset;\n    height: auto;\n    margin-top: 1rem;\n}\n.login-box[data-v-dedb9278] {\n    width: 100%;\n    z-index: 2;\n}\n.register-box[data-v-dedb9278] {\n    width: 100%;\n    display: none;\n    z-index: 1;\n}\n.social-login[data-v-dedb9278] {\n    margin-top: 1rem;\n    margin-bottom: 1.5rem;\n}\n.main-box.right-panel-active .login-box[data-v-dedb9278] {\n    transform: translateY(100%);\n    display: none;\n}\n.main-box.right-panel-active .register-box[data-v-dedb9278] {\n    transform: translateY(0);\n    opacity: 1;\n    z-index: 5;\n    display: block;\n    -webkit-animation: show-data-v-dedb9278 0.7s;\n            animation: show-data-v-dedb9278 0.7s;\n}\n.overlay[data-v-dedb9278] {\n    display: none;\n}\n.mobile[data-v-dedb9278] {\n    display: block;\n}\n}\n@media screen and (max-width: 767px) {\nform[data-v-dedb9278] {\n    padding: 0 1.5rem;\n}\n}\n.checkbox-container[data-v-dedb9278] {\n  display: flex;\n  justify-content: center;\n  align-items: center;\n}\n.logged-in-checkbox[data-v-dedb9278] {\n  -webkit-appearance: none;\n  background-color: white;\n  border: 1px solid gray;\n  box-shadow: 0 1px 2px rgba(0, 0, 0, 0.05), inset 0px -15px 10px -12px rgba(0, 0, 0, 0.05);\n  padding: 9px;\n  margin-right: 5px;\n  display: inline-block;\n  position: relative;\n}\n.logged-in-checkbox[data-v-dedb9278]:active, .logged-in-checkbox[data-v-dedb9278]:checked:active {\n  box-shadow: 0 1px 2px rgba(0, 0, 0, 0.05), inset 0px 1px 3px rgba(0, 0, 0, 0.1);\n}\n.logged-in-checkbox[data-v-dedb9278]:checked {\n  background-color: #232323;\n  border: 1px solid #232323;\n  box-shadow: 0 1px 2px rgba(0, 0, 0, 0.05), inset 0px -15px 10px -12px rgba(0, 0, 0, 0.05), inset 15px 10px -12px rgba(255, 255, 255, 0.1);\n  color: #99a1a7;\n}\n.logged-in-checkbox[data-v-dedb9278]:checked:after {\n  content: \"\\2714\";\n  font-size: 14px;\n  position: absolute;\n  top: -1px;\n  left: 4px;\n  color: white;\n}", ""]);

// exports


/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/frontend/auth/Login.vue?vue&type=style&index=0&id=dedb9278&lang=scss&scoped=true&":
/*!*********************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/sass-loader/dist/cjs.js??ref--7-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/frontend/auth/Login.vue?vue&type=style&index=0&id=dedb9278&lang=scss&scoped=true& ***!
  \*********************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../../node_modules/css-loader!../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../node_modules/postcss-loader/src??ref--7-2!../../../../../node_modules/sass-loader/dist/cjs.js??ref--7-3!../../../../../node_modules/vue-loader/lib??vue-loader-options!./Login.vue?vue&type=style&index=0&id=dedb9278&lang=scss&scoped=true& */ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/frontend/auth/Login.vue?vue&type=style&index=0&id=dedb9278&lang=scss&scoped=true&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/frontend/auth/Login.vue?vue&type=template&id=dedb9278&scoped=true&":
/*!**********************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/frontend/auth/Login.vue?vue&type=template&id=dedb9278&scoped=true& ***!
  \**********************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    {
      staticClass: "main-box",
      class: { "right-panel-active": _vm.isSignUpActive },
      attrs: { id: "container" }
    },
    [
      _c(
        "div",
        { staticClass: "form-box register-box" },
        [
          _c(
            "form",
            {
              on: {
                submit: function($event) {
                  $event.preventDefault()
                  return _vm.register($event)
                }
              }
            },
            [
              _c("span", { staticClass: "title-size" }, [
                _vm._v("Create an account")
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "form-group row w-100" }, [
                _c(
                  "div",
                  { staticClass: "col-12" },
                  [
                    _c("label", { attrs: { for: "name" } }, [_vm._v("NAME* ")]),
                    _vm._v(" "),
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.registrationForm.name,
                          expression: "registrationForm.name"
                        }
                      ],
                      staticClass: "form-control",
                      class: {
                        "is-invalid": _vm.registrationForm.errors.has("name")
                      },
                      attrs: {
                        type: "text",
                        id: "name",
                        name: "name",
                        placeholder: "Enter your full name"
                      },
                      domProps: { value: _vm.registrationForm.name },
                      on: {
                        input: function($event) {
                          if ($event.target.composing) {
                            return
                          }
                          _vm.$set(
                            _vm.registrationForm,
                            "name",
                            $event.target.value
                          )
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("has-error", {
                      attrs: { form: _vm.registrationForm, field: "name" }
                    })
                  ],
                  1
                )
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "form-group row w-100" }, [
                _c(
                  "div",
                  { staticClass: "col-12" },
                  [
                    _c("label", { attrs: { for: "email" } }, [
                      _vm._v("EMAIL ADDRESS* ")
                    ]),
                    _vm._v(" "),
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.registrationForm.email,
                          expression: "registrationForm.email"
                        }
                      ],
                      staticClass: "form-control",
                      class: {
                        "is-invalid": _vm.registrationForm.errors.has("email")
                      },
                      attrs: {
                        type: "text",
                        id: "email",
                        name: "email",
                        placeholder: "Enter your email address"
                      },
                      domProps: { value: _vm.registrationForm.email },
                      on: {
                        input: function($event) {
                          if ($event.target.composing) {
                            return
                          }
                          _vm.$set(
                            _vm.registrationForm,
                            "email",
                            $event.target.value
                          )
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("has-error", {
                      attrs: { form: _vm.registrationForm, field: "email" }
                    })
                  ],
                  1
                )
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "form-group row w-100" }, [
                _c(
                  "div",
                  { staticClass: "col-12 col-md-6 pr-md-1" },
                  [
                    _c("label", { attrs: { for: "password" } }, [
                      _vm._v("PASSWORD* ")
                    ]),
                    _vm._v(" "),
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.registrationForm.password,
                          expression: "registrationForm.password"
                        }
                      ],
                      staticClass: "form-control",
                      class: {
                        "is-invalid": _vm.registrationForm.errors.has(
                          "password"
                        )
                      },
                      attrs: {
                        type: "password",
                        id: "password",
                        name: "password",
                        placeholder: "Enter a password"
                      },
                      domProps: { value: _vm.registrationForm.password },
                      on: {
                        input: function($event) {
                          if ($event.target.composing) {
                            return
                          }
                          _vm.$set(
                            _vm.registrationForm,
                            "password",
                            $event.target.value
                          )
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("has-error", {
                      attrs: { form: _vm.registrationForm, field: "password" }
                    })
                  ],
                  1
                ),
                _vm._v(" "),
                _c(
                  "div",
                  { staticClass: "col-12 col-md-6 pl-md-1 mt-3 mt-md-0" },
                  [
                    _c("label", { attrs: { for: "passwordConfirm" } }, [
                      _vm._v("CONFIRM PASSWORD* ")
                    ]),
                    _vm._v(" "),
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.registrationForm.password_confirmation,
                          expression: "registrationForm.password_confirmation"
                        }
                      ],
                      staticClass: "form-control",
                      class: {
                        "is-invalid": _vm.registrationForm.errors.has(
                          "password"
                        )
                      },
                      attrs: {
                        type: "password",
                        id: "passwordConfirm",
                        name: "passwordConfirm",
                        placeholder: "Enter the password again"
                      },
                      domProps: {
                        value: _vm.registrationForm.password_confirmation
                      },
                      on: {
                        input: function($event) {
                          if ($event.target.composing) {
                            return
                          }
                          _vm.$set(
                            _vm.registrationForm,
                            "password_confirmation",
                            $event.target.value
                          )
                        }
                      }
                    })
                  ]
                )
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "form-group row w-100" }, [
                _c(
                  "div",
                  { staticClass: "col-12 col-md-6 pr-md-1" },
                  [
                    _c("label", { attrs: { for: "address" } }, [
                      _vm._v("ADDRESS* ")
                    ]),
                    _vm._v(" "),
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.registrationForm.address,
                          expression: "registrationForm.address"
                        }
                      ],
                      staticClass: "form-control",
                      class: {
                        "is-invalid": _vm.registrationForm.errors.has("address")
                      },
                      attrs: {
                        type: "text",
                        id: "address",
                        name: "address",
                        placeholder: "Address"
                      },
                      domProps: { value: _vm.registrationForm.address },
                      on: {
                        input: function($event) {
                          if ($event.target.composing) {
                            return
                          }
                          _vm.$set(
                            _vm.registrationForm,
                            "address",
                            $event.target.value
                          )
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("has-error", {
                      attrs: { form: _vm.registrationForm, field: "address" }
                    })
                  ],
                  1
                ),
                _vm._v(" "),
                _c(
                  "div",
                  { staticClass: "col-12 col-md-6 pl-md-1 mt-3 mt-md-0" },
                  [
                    _c(
                      "label",
                      {
                        attrs: { for: "gender" },
                        model: {
                          value: _vm.registrationForm.gender,
                          callback: function($$v) {
                            _vm.$set(_vm.registrationForm, "gender", $$v)
                          },
                          expression: "registrationForm.gender"
                        }
                      },
                      [_vm._v("GENDER* ")]
                    ),
                    _vm._v(" "),
                    _c(
                      "select",
                      {
                        staticClass: "form-control",
                        class: {
                          "is-invalid": _vm.registrationForm.errors.has(
                            "gender"
                          )
                        },
                        attrs: { id: "gender", name: "gender" }
                      },
                      [
                        _c("option", [_vm._v("Male")]),
                        _vm._v(" "),
                        _c("option", [_vm._v("Female")]),
                        _vm._v(" "),
                        _c("option", [_vm._v("Other")])
                      ]
                    ),
                    _vm._v(" "),
                    _c("has-error", {
                      attrs: { form: _vm.registrationForm, field: "gender" }
                    })
                  ],
                  1
                )
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "form-group row w-100" }, [
                _c(
                  "div",
                  { staticClass: "col-12 col-md-6 pr-md-1" },
                  [
                    _c("label", { attrs: { for: "birthday" } }, [
                      _vm._v("DATE OF BIRTH* ")
                    ]),
                    _vm._v(" "),
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.registrationForm.birthday,
                          expression: "registrationForm.birthday"
                        }
                      ],
                      staticClass: "form-control",
                      class: {
                        "is-invalid": _vm.registrationForm.errors.has(
                          "birthday"
                        )
                      },
                      attrs: {
                        type: "date",
                        id: "birthday",
                        name: "birthday",
                        placeholder: "Date of Birth"
                      },
                      domProps: { value: _vm.registrationForm.birthday },
                      on: {
                        input: function($event) {
                          if ($event.target.composing) {
                            return
                          }
                          _vm.$set(
                            _vm.registrationForm,
                            "birthday",
                            $event.target.value
                          )
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("has-error", {
                      attrs: { form: _vm.registrationForm, field: "birthday" }
                    })
                  ],
                  1
                ),
                _vm._v(" "),
                _c(
                  "div",
                  { staticClass: "col-12 col-md-6 pl-md-1 mt-3 mt-md-0" },
                  [
                    _c("label", { attrs: { for: "phone" } }, [
                      _vm._v("PHONE NUMBER* ")
                    ]),
                    _vm._v(" "),
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.registrationForm.phone,
                          expression: "registrationForm.phone"
                        }
                      ],
                      staticClass: "form-control",
                      class: {
                        "is-invalid": _vm.registrationForm.errors.has("phone")
                      },
                      attrs: {
                        type: "text",
                        id: "phone",
                        name: "phone",
                        placeholder: "Phone Number"
                      },
                      domProps: { value: _vm.registrationForm.phone },
                      on: {
                        input: function($event) {
                          if ($event.target.composing) {
                            return
                          }
                          _vm.$set(
                            _vm.registrationForm,
                            "phone",
                            $event.target.value
                          )
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("has-error", {
                      attrs: { form: _vm.registrationForm, field: "phone" }
                    })
                  ],
                  1
                )
              ]),
              _vm._v(" "),
              _c("button", [_vm._v("Sign Up")]),
              _vm._v(" "),
              _c("div", { staticClass: "mobile" }, [
                _c("h6", [_vm._v("Already have an account?")]),
                _vm._v(" "),
                _c(
                  "button",
                  {
                    attrs: { type: "button" },
                    on: { click: _vm.signInButton }
                  },
                  [_vm._v("Sign In")]
                )
              ])
            ]
          ),
          _vm._v(" "),
          _c("social-login", {
            staticClass: "social-login mb-3 in-registration-box",
            attrs: { url: _vm.url }
          })
        ],
        1
      ),
      _vm._v(" "),
      _c(
        "div",
        { staticClass: "form-box login-box" },
        [
          _c(
            "form",
            {
              on: {
                submit: function($event) {
                  $event.preventDefault()
                  return _vm.authenticate($event)
                }
              }
            },
            [
              _vm.$route.query.account
                ? _c("div", { staticClass: "text-center text-success" }, [
                    _vm._v(
                      "Your account has been activated. Now you can proceed to login."
                    )
                  ])
                : _vm._e(),
              _vm._v(" "),
              _vm.$route.query.reset
                ? _c("div", { staticClass: "text-center text-success" }, [
                    _vm._v(
                      "Password reset is successful. Now you can proceed to login."
                    )
                  ])
                : _vm._e(),
              _vm._v(" "),
              _c("div", { staticClass: "text-center text-success" }, [
                _vm._v(
                  "\n                " + _vm._s(_vm.message) + "\n            "
                )
              ]),
              _vm._v(" "),
              _c("span", { staticClass: "title-size" }, [_vm._v("Sign in")]),
              _vm._v(" "),
              _c(
                "div",
                { staticClass: "form-group row justify-content-center w-100" },
                [
                  _c("input", {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.form.email,
                        expression: "form.email"
                      }
                    ],
                    staticClass: "form-control col-10 mx-md-5",
                    attrs: { type: "text", placeholder: "Email Address" },
                    domProps: { value: _vm.form.email },
                    on: {
                      input: function($event) {
                        if ($event.target.composing) {
                          return
                        }
                        _vm.$set(_vm.form, "email", $event.target.value)
                      }
                    }
                  })
                ]
              ),
              _vm._v(" "),
              _c(
                "div",
                { staticClass: "form-group justify-content-center row w-100" },
                [
                  _c("input", {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.form.password,
                        expression: "form.password"
                      }
                    ],
                    staticClass: "form-control col-10 mx-md-5",
                    attrs: { type: "password", placeholder: "Password" },
                    domProps: { value: _vm.form.password },
                    on: {
                      input: function($event) {
                        if ($event.target.composing) {
                          return
                        }
                        _vm.$set(_vm.form, "password", $event.target.value)
                      }
                    }
                  })
                ]
              ),
              _vm._v(" "),
              _c(
                "div",
                { staticClass: "d-block text-center invalid-feedback mb-1" },
                [_vm._v(_vm._s(_vm.loginError))]
              ),
              _vm._v(" "),
              _vm._m(0),
              _vm._v(" "),
              _c("button", [_vm._v("Sign In")]),
              _vm._v(" "),
              _c(
                "router-link",
                { attrs: { to: { name: "password-reset-form" } } },
                [
                  _vm._v(
                    "\n                Forgot your password?\n            "
                  )
                ]
              ),
              _vm._v(" "),
              _c("div", { staticClass: "mobile" }, [
                _c("h6", [_vm._v("Don't have a account?")]),
                _vm._v(" "),
                _c(
                  "button",
                  {
                    attrs: { type: "button" },
                    on: { click: _vm.signUpButton }
                  },
                  [_vm._v("Sign Up")]
                )
              ])
            ],
            1
          ),
          _vm._v(" "),
          _c("social-login", {
            staticClass: "social-login",
            class: {
              "in-login-box-on": !_vm.isSignUpActive,
              "in-login-box-off": _vm.isSignUpActive
            },
            attrs: { url: _vm.url }
          })
        ],
        1
      ),
      _vm._v(" "),
      _c("div", { staticClass: "overlay-box" }, [
        _c("div", { staticClass: "overlay" }, [
          _c("div", { staticClass: "overlay-panel overlay-left" }, [
            _c("h1", [_vm._v("Already have an account?")]),
            _vm._v(" "),
            _c("p", [
              _vm._v(
                "\n                    Login in to stay connected with us for new updates!\n                "
              )
            ]),
            _vm._v(" "),
            _c(
              "button",
              {
                staticClass: "ghost",
                attrs: { id: "signIn" },
                on: { click: _vm.signInButton }
              },
              [_vm._v("Sign In")]
            )
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "overlay-panel overlay-right" }, [
            _c("h1", [_vm._v("Not yet registered?")]),
            _vm._v(" "),
            _c("p", [
              _vm._v("You can speed up checkout process and track your orders!")
            ]),
            _vm._v(" "),
            _c(
              "button",
              {
                staticClass: "ghost",
                attrs: { id: "signUp" },
                on: { click: _vm.signUpButton }
              },
              [_vm._v("Sign Up")]
            )
          ])
        ])
      ])
    ]
  )
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "form-group row w-100" }, [
      _c("div", { staticClass: "col-12 text-center checkbox-container" }, [
        _c("input", {
          staticClass: "logged-in-checkbox",
          attrs: { type: "checkbox", id: "loggedIn" }
        }),
        _vm._v(" "),
        _c("label", { attrs: { for: "loggedIn" } }, [
          _vm._v("KEEP ME LOGGED IN")
        ])
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js":
/*!********************************************************************!*\
  !*** ./node_modules/vue-loader/lib/runtime/componentNormalizer.js ***!
  \********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return normalizeComponent; });
/* globals __VUE_SSR_CONTEXT__ */

// IMPORTANT: Do NOT use ES2015 features in this file (except for modules).
// This module is a runtime utility for cleaner component module output and will
// be included in the final webpack user bundle.

function normalizeComponent (
  scriptExports,
  render,
  staticRenderFns,
  functionalTemplate,
  injectStyles,
  scopeId,
  moduleIdentifier, /* server only */
  shadowMode /* vue-cli only */
) {
  // Vue.extend constructor export interop
  var options = typeof scriptExports === 'function'
    ? scriptExports.options
    : scriptExports

  // render functions
  if (render) {
    options.render = render
    options.staticRenderFns = staticRenderFns
    options._compiled = true
  }

  // functional template
  if (functionalTemplate) {
    options.functional = true
  }

  // scopedId
  if (scopeId) {
    options._scopeId = 'data-v-' + scopeId
  }

  var hook
  if (moduleIdentifier) { // server build
    hook = function (context) {
      // 2.3 injection
      context =
        context || // cached call
        (this.$vnode && this.$vnode.ssrContext) || // stateful
        (this.parent && this.parent.$vnode && this.parent.$vnode.ssrContext) // functional
      // 2.2 with runInNewContext: true
      if (!context && typeof __VUE_SSR_CONTEXT__ !== 'undefined') {
        context = __VUE_SSR_CONTEXT__
      }
      // inject component styles
      if (injectStyles) {
        injectStyles.call(this, context)
      }
      // register component module identifier for async chunk inferrence
      if (context && context._registeredComponents) {
        context._registeredComponents.add(moduleIdentifier)
      }
    }
    // used by ssr in case component is cached and beforeCreate
    // never gets called
    options._ssrRegister = hook
  } else if (injectStyles) {
    hook = shadowMode
      ? function () { injectStyles.call(this, this.$root.$options.shadowRoot) }
      : injectStyles
  }

  if (hook) {
    if (options.functional) {
      // for template-only hot-reload because in that case the render fn doesn't
      // go through the normalizer
      options._injectStyles = hook
      // register for functioal component in vue file
      var originalRender = options.render
      options.render = function renderWithStyleInjection (h, context) {
        hook.call(context)
        return originalRender(h, context)
      }
    } else {
      // inject component registration as beforeCreate hook
      var existing = options.beforeCreate
      options.beforeCreate = existing
        ? [].concat(existing, hook)
        : [hook]
    }
  }

  return {
    exports: scriptExports,
    options: options
  }
}


/***/ }),

/***/ "./resources/js/components/frontend/auth/Login.vue":
/*!*********************************************************!*\
  !*** ./resources/js/components/frontend/auth/Login.vue ***!
  \*********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Login_vue_vue_type_template_id_dedb9278_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Login.vue?vue&type=template&id=dedb9278&scoped=true& */ "./resources/js/components/frontend/auth/Login.vue?vue&type=template&id=dedb9278&scoped=true&");
/* harmony import */ var _Login_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Login.vue?vue&type=script&lang=js& */ "./resources/js/components/frontend/auth/Login.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _Login_vue_vue_type_style_index_0_id_dedb9278_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./Login.vue?vue&type=style&index=0&id=dedb9278&lang=scss&scoped=true& */ "./resources/js/components/frontend/auth/Login.vue?vue&type=style&index=0&id=dedb9278&lang=scss&scoped=true&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _Login_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Login_vue_vue_type_template_id_dedb9278_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Login_vue_vue_type_template_id_dedb9278_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "dedb9278",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/frontend/auth/Login.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/frontend/auth/Login.vue?vue&type=script&lang=js&":
/*!**********************************************************************************!*\
  !*** ./resources/js/components/frontend/auth/Login.vue?vue&type=script&lang=js& ***!
  \**********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Login_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./Login.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/frontend/auth/Login.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Login_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/frontend/auth/Login.vue?vue&type=style&index=0&id=dedb9278&lang=scss&scoped=true&":
/*!*******************************************************************************************************************!*\
  !*** ./resources/js/components/frontend/auth/Login.vue?vue&type=style&index=0&id=dedb9278&lang=scss&scoped=true& ***!
  \*******************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_Login_vue_vue_type_style_index_0_id_dedb9278_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/style-loader!../../../../../node_modules/css-loader!../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../node_modules/postcss-loader/src??ref--7-2!../../../../../node_modules/sass-loader/dist/cjs.js??ref--7-3!../../../../../node_modules/vue-loader/lib??vue-loader-options!./Login.vue?vue&type=style&index=0&id=dedb9278&lang=scss&scoped=true& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/frontend/auth/Login.vue?vue&type=style&index=0&id=dedb9278&lang=scss&scoped=true&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_Login_vue_vue_type_style_index_0_id_dedb9278_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_Login_vue_vue_type_style_index_0_id_dedb9278_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_Login_vue_vue_type_style_index_0_id_dedb9278_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_Login_vue_vue_type_style_index_0_id_dedb9278_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_Login_vue_vue_type_style_index_0_id_dedb9278_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./resources/js/components/frontend/auth/Login.vue?vue&type=template&id=dedb9278&scoped=true&":
/*!****************************************************************************************************!*\
  !*** ./resources/js/components/frontend/auth/Login.vue?vue&type=template&id=dedb9278&scoped=true& ***!
  \****************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Login_vue_vue_type_template_id_dedb9278_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./Login.vue?vue&type=template&id=dedb9278&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/frontend/auth/Login.vue?vue&type=template&id=dedb9278&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Login_vue_vue_type_template_id_dedb9278_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Login_vue_vue_type_template_id_dedb9278_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);