(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[74],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backend/AdminDashboard.vue?vue&type=script&lang=js&":
/*!*********************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/backend/AdminDashboard.vue?vue&type=script&lang=js& ***!
  \*********************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var element_ui_lib_theme_chalk_pagination_css__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! element-ui/lib/theme-chalk/pagination.css */ "./node_modules/element-ui/lib/theme-chalk/pagination.css");
/* harmony import */ var element_ui_lib_theme_chalk_pagination_css__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(element_ui_lib_theme_chalk_pagination_css__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var element_ui_lib_theme_chalk_table_css__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! element-ui/lib/theme-chalk/table.css */ "./node_modules/element-ui/lib/theme-chalk/table.css");
/* harmony import */ var element_ui_lib_theme_chalk_table_css__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(element_ui_lib_theme_chalk_table_css__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var element_ui_lib_locale_lang_en__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! element-ui/lib/locale/lang/en */ "./node_modules/element-ui/lib/locale/lang/en.js");
/* harmony import */ var element_ui_lib_locale_lang_en__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(element_ui_lib_locale_lang_en__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var element_ui_lib_locale__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! element-ui/lib/locale */ "./node_modules/element-ui/lib/locale/index.js");
/* harmony import */ var element_ui_lib_locale__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(element_ui_lib_locale__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var vue_data_tables__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! vue-data-tables */ "./node_modules/vue-data-tables/dist/data-tables.min.js");
/* harmony import */ var vue_data_tables__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(vue_data_tables__WEBPACK_IMPORTED_MODULE_4__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
// import _ from 'lodash';




element_ui_lib_locale__WEBPACK_IMPORTED_MODULE_3___default.a.use(element_ui_lib_locale_lang_en__WEBPACK_IMPORTED_MODULE_2___default.a);

Vue.use(vue_data_tables__WEBPACK_IMPORTED_MODULE_4__["DataTables"]);
/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'admin-dashboard',
  data: function data() {
    return {
      search: '',
      allNotifications: []
    };
  },
  watch: {
    '$route': {
      handler: function handler(to, from) {
        document.title = to.meta.title + ' | Admin | The NextTrends';
      },
      immediate: true
    }
  },
  created: function created() {
    var _this = this;

    axios.get('/api/notification/' + this.$store.getters.currentAdmin.id).then(function (response) {
      _this.allNotifications = response.data;
    })["catch"](function (error) {});
  },
  computed: {
    currentAdmin: function currentAdmin() {
      return this.$store.getters.currentAdmin;
    },
    unreadNotification: function unreadNotification() {
      return this.allNotifications.filter(function (notification) {
        return notification.read_at == null;
      });
    }
  },
  methods: {
    setEdit: function setEdit() {
      this.$store.dispatch('setEditMode', false);
    },
    logout: function logout() {
      this.$store.commit('logoutAdmin');
      this.$router.push({
        name: 'admin-login'
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backend/AdminDashboard.vue?vue&type=style&index=0&id=5525adea&scoped=true&lang=css&":
/*!****************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/backend/AdminDashboard.vue?vue&type=style&index=0&id=5525adea&scoped=true&lang=css& ***!
  \****************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n.navbar-badge[data-v-5525adea] {\n    top: 9px;\n    right: 20px;\n}\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backend/AdminDashboard.vue?vue&type=style&index=0&id=5525adea&scoped=true&lang=css&":
/*!********************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/backend/AdminDashboard.vue?vue&type=style&index=0&id=5525adea&scoped=true&lang=css& ***!
  \********************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../node_modules/css-loader??ref--6-1!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--6-2!../../../../node_modules/vue-loader/lib??vue-loader-options!./AdminDashboard.vue?vue&type=style&index=0&id=5525adea&scoped=true&lang=css& */ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backend/AdminDashboard.vue?vue&type=style&index=0&id=5525adea&scoped=true&lang=css&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backend/AdminDashboard.vue?vue&type=template&id=5525adea&scoped=true&":
/*!*************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/backend/AdminDashboard.vue?vue&type=template&id=5525adea&scoped=true& ***!
  \*************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "wrapper" }, [
    _vm._m(0),
    _vm._v(" "),
    _c(
      "aside",
      { staticClass: "main-sidebar sidebar-dark-primary elevation-4" },
      [
        _vm._m(1),
        _vm._v(" "),
        _c("div", { staticClass: "sidebar" }, [
          _c("div", { staticClass: "user-panel mt-3 pb-3 mb-3 d-flex" }, [
            _c("div", { staticClass: "info" }, [
              _c("h5", { staticClass: "text-danger" }, [
                _vm._v(_vm._s(_vm.currentAdmin.name))
              ])
            ])
          ]),
          _vm._v(" "),
          _c("nav", { staticClass: "mt-2" }, [
            _c(
              "ul",
              {
                staticClass: "nav nav-pills nav-sidebar flex-column",
                attrs: {
                  "data-widget": "treeview",
                  role: "menu",
                  "data-accordion": "false"
                }
              },
              [
                _c(
                  "li",
                  { staticClass: "nav-item" },
                  [
                    _c(
                      "router-link",
                      {
                        staticClass: "nav-link",
                        attrs: {
                          to: { name: "admin-dashboard" },
                          tag: "a",
                          "active-class": "active",
                          exact: ""
                        }
                      },
                      [
                        _c("i", {
                          staticClass: "nav-icon fas fa-tachometer-alt"
                        }),
                        _vm._v(" "),
                        _c("p", [
                          _vm._v(
                            "\n                                    Dashboard\n                                "
                          )
                        ])
                      ]
                    )
                  ],
                  1
                ),
                _vm._v(" "),
                _c("li", { staticClass: "nav-item has-treeview" }, [
                  _vm._m(2),
                  _vm._v(" "),
                  _c("ul", { staticClass: "nav nav-treeview ml-1" }, [
                    _c(
                      "li",
                      { staticClass: "nav-item" },
                      [
                        _c(
                          "router-link",
                          {
                            staticClass: "nav-link",
                            attrs: {
                              to: { name: "admin-report-sales" },
                              tag: "a",
                              "active-class": "active",
                              exact: ""
                            }
                          },
                          [
                            _c("i", {
                              staticClass: "nav-icon fas fa-file-alt"
                            }),
                            _vm._v(" "),
                            _c("p", [
                              _vm._v(
                                "\n                                            Sales Report\n                                        "
                              )
                            ])
                          ]
                        )
                      ],
                      1
                    )
                  ]),
                  _vm._v(" "),
                  _c("ul", { staticClass: "nav nav-treeview ml-1" }, [
                    _c(
                      "li",
                      { staticClass: "nav-item" },
                      [
                        _c(
                          "router-link",
                          {
                            staticClass: "nav-link",
                            attrs: {
                              to: { name: "admin-report-inventory-on-hand" },
                              tag: "a",
                              "active-class": "active",
                              exact: ""
                            }
                          },
                          [
                            _c("i", {
                              staticClass: "nav-icon fas fa-file-alt"
                            }),
                            _vm._v(" "),
                            _c("p", [
                              _vm._v(
                                "\n                                            Inventory On Hand\n                                        "
                              )
                            ])
                          ]
                        )
                      ],
                      1
                    )
                  ]),
                  _vm._v(" "),
                  _c("ul", { staticClass: "nav nav-treeview ml-1" }, [
                    _c(
                      "li",
                      { staticClass: "nav-item" },
                      [
                        _c(
                          "router-link",
                          {
                            staticClass: "nav-link",
                            attrs: {
                              to: { name: "admin-report-low-stock" },
                              tag: "a",
                              "active-class": "active",
                              exact: ""
                            }
                          },
                          [
                            _c("i", {
                              staticClass: "nav-icon fas fa-file-alt"
                            }),
                            _vm._v(" "),
                            _c("p", [
                              _vm._v(
                                "\n                                            Low Stock\n                                        "
                              )
                            ])
                          ]
                        )
                      ],
                      1
                    )
                  ]),
                  _vm._v(" "),
                  _c("ul", { staticClass: "nav nav-treeview ml-1" }, [
                    _c(
                      "li",
                      { staticClass: "nav-item" },
                      [
                        _c(
                          "router-link",
                          {
                            staticClass: "nav-link",
                            attrs: {
                              to: { name: "admin-report-slow-moving-stock" },
                              tag: "a",
                              "active-class": "active",
                              exact: ""
                            }
                          },
                          [
                            _c("i", {
                              staticClass: "nav-icon fas fa-file-alt"
                            }),
                            _vm._v(" "),
                            _c("p", [
                              _vm._v(
                                "\n                                            Slow Moving Stock\n                                        "
                              )
                            ])
                          ]
                        )
                      ],
                      1
                    )
                  ])
                ]),
                _vm._v(" "),
                _c(
                  "li",
                  { staticClass: "nav-item" },
                  [
                    _c(
                      "router-link",
                      {
                        staticClass: "nav-link",
                        attrs: {
                          to: { name: "users" },
                          tag: "a",
                          "active-class": "active",
                          exact: ""
                        }
                      },
                      [
                        _c("i", { staticClass: "nav-icon fas fa-users" }),
                        _vm._v(" "),
                        _c("p", [
                          _vm._v(
                            "\n                                    Users\n                                "
                          )
                        ])
                      ]
                    )
                  ],
                  1
                ),
                _vm._v(" "),
                _c("li", { staticClass: "nav-item has-treeview" }, [
                  _vm._m(3),
                  _vm._v(" "),
                  _c("ul", { staticClass: "nav nav-treeview ml-1" }, [
                    _c(
                      "li",
                      { staticClass: "nav-item" },
                      [
                        _c(
                          "router-link",
                          {
                            staticClass: "nav-link",
                            attrs: {
                              to: { name: "category" },
                              tag: "a",
                              "active-class": "active",
                              exact: ""
                            }
                          },
                          [
                            _c("i", { staticClass: "nav-icon fas fa-sitemap" }),
                            _vm._v(
                              "Category\n                                    "
                            )
                          ]
                        )
                      ],
                      1
                    ),
                    _vm._v(" "),
                    _c(
                      "li",
                      { staticClass: "nav-item" },
                      [
                        _c(
                          "router-link",
                          {
                            staticClass: "nav-link",
                            attrs: {
                              to: { name: "navigation-image" },
                              tag: "a",
                              "active-class": "active",
                              exact: ""
                            }
                          },
                          [
                            _c("i", { staticClass: "nav-icon fas fa-images" }),
                            _vm._v(
                              "Navigation Image\n                                    "
                            )
                          ]
                        )
                      ],
                      1
                    )
                  ])
                ]),
                _vm._v(" "),
                _c("li", { staticClass: "nav-item has-treeview" }, [
                  _vm._m(4),
                  _vm._v(" "),
                  _c("ul", { staticClass: "nav nav-treeview ml-1" }, [
                    _c(
                      "li",
                      { staticClass: "nav-item", on: { click: _vm.setEdit } },
                      [
                        _c(
                          "router-link",
                          {
                            staticClass: "nav-link",
                            attrs: {
                              to: { name: "add-product" },
                              tag: "a",
                              "active-class": "active",
                              exact: ""
                            }
                          },
                          [
                            _c("i", {
                              staticClass: "nav-icon fas fa-cart-plus"
                            }),
                            _vm._v(" "),
                            _c("p", [
                              _vm._v(
                                "\n                                            Add Product\n                                        "
                              )
                            ])
                          ]
                        )
                      ],
                      1
                    )
                  ]),
                  _vm._v(" "),
                  _c("ul", { staticClass: "nav nav-treeview ml-1" }, [
                    _c(
                      "li",
                      { staticClass: "nav-item" },
                      [
                        _c(
                          "router-link",
                          {
                            staticClass: "nav-link",
                            attrs: {
                              to: { name: "view-product" },
                              tag: "a",
                              "active-class": "active",
                              exact: ""
                            }
                          },
                          [
                            _c("i", { staticClass: "nav-icon fas fa-eye" }),
                            _vm._v(" "),
                            _c("p", [
                              _vm._v(
                                "\n                                            View Product\n                                        "
                              )
                            ])
                          ]
                        )
                      ],
                      1
                    )
                  ]),
                  _vm._v(" "),
                  _c("ul", { staticClass: "nav nav-treeview ml-1" }, [
                    _c(
                      "li",
                      { staticClass: "nav-item" },
                      [
                        _c(
                          "router-link",
                          {
                            staticClass: "nav-link",
                            attrs: {
                              to: { name: "category" },
                              tag: "a",
                              "active-class": "active",
                              exact: ""
                            }
                          },
                          [
                            _c("i", { staticClass: "nav-icon fas fa-sitemap" }),
                            _vm._v(
                              "Category\n                                    "
                            )
                          ]
                        )
                      ],
                      1
                    )
                  ]),
                  _vm._v(" "),
                  _c("ul", { staticClass: "nav nav-treeview ml-1" }, [
                    _c(
                      "li",
                      { staticClass: "nav-item" },
                      [
                        _c(
                          "router-link",
                          {
                            staticClass: "nav-link",
                            attrs: {
                              to: { name: "store" },
                              tag: "a",
                              "active-class": "active",
                              exact: ""
                            }
                          },
                          [
                            _c("i", {
                              staticClass: "nav-icon fas fa-store-alt"
                            }),
                            _vm._v(
                              "Store\n                                    "
                            )
                          ]
                        )
                      ],
                      1
                    )
                  ]),
                  _vm._v(" "),
                  _c("ul", { staticClass: "nav nav-treeview ml-1" }, [
                    _c(
                      "li",
                      { staticClass: "nav-item" },
                      [
                        _c(
                          "router-link",
                          {
                            staticClass: "nav-link",
                            attrs: {
                              to: { name: "brand" },
                              tag: "a",
                              "active-class": "active",
                              exact: ""
                            }
                          },
                          [
                            _c("i", {
                              staticClass: "nav-icon fab fa-bootstrap"
                            }),
                            _vm._v(
                              "Brands\n                                    "
                            )
                          ]
                        )
                      ],
                      1
                    )
                  ]),
                  _vm._v(" "),
                  _c("ul", { staticClass: "nav nav-treeview ml-1" }, [
                    _c(
                      "li",
                      { staticClass: "nav-item" },
                      [
                        _c(
                          "router-link",
                          {
                            staticClass: "nav-link",
                            attrs: {
                              to: { name: "fabric" },
                              tag: "a",
                              "active-class": "active",
                              exact: ""
                            }
                          },
                          [
                            _c("i", {
                              staticClass: "nav-icon fab fa-foursquare"
                            }),
                            _vm._v(
                              "Fabrics\n                                    "
                            )
                          ]
                        )
                      ],
                      1
                    )
                  ]),
                  _vm._v(" "),
                  _c("ul", { staticClass: "nav nav-treeview ml-1" }, [
                    _c(
                      "li",
                      { staticClass: "nav-item" },
                      [
                        _c(
                          "router-link",
                          {
                            staticClass: "nav-link",
                            attrs: {
                              to: { name: "occasion" },
                              tag: "a",
                              "active-class": "active",
                              exact: ""
                            }
                          },
                          [
                            _c("i", {
                              staticClass: "nav-icon fas fa-glass-cheers"
                            }),
                            _vm._v(
                              "Occasions\n                                    "
                            )
                          ]
                        )
                      ],
                      1
                    )
                  ]),
                  _vm._v(" "),
                  _c("ul", { staticClass: "nav nav-treeview ml-1" }, [
                    _c(
                      "li",
                      { staticClass: "nav-item" },
                      [
                        _c(
                          "router-link",
                          {
                            staticClass: "nav-link",
                            attrs: {
                              to: { name: "color" },
                              tag: "a",
                              "active-class": "active",
                              exact: ""
                            }
                          },
                          [
                            _c("i", { staticClass: "nav-icon fas fa-palette" }),
                            _vm._v(
                              "Colors\n                                    "
                            )
                          ]
                        )
                      ],
                      1
                    )
                  ]),
                  _vm._v(" "),
                  _c("ul", { staticClass: "nav nav-treeview ml-1" }, [
                    _c(
                      "li",
                      { staticClass: "nav-item" },
                      [
                        _c(
                          "router-link",
                          {
                            staticClass: "nav-link",
                            attrs: {
                              to: { name: "size" },
                              tag: "a",
                              "active-class": "active",
                              exact: ""
                            }
                          },
                          [
                            _c("i", {
                              staticClass: "nav-icon fas fa-compress-arrows-alt"
                            }),
                            _vm._v(
                              "Sizes\n                                    "
                            )
                          ]
                        )
                      ],
                      1
                    )
                  ])
                ]),
                _vm._v(" "),
                _c("li", { staticClass: "nav-item has-treeview" }, [
                  _vm._m(5),
                  _vm._v(" "),
                  _c("ul", { staticClass: "nav nav-treeview ml-1" }, [
                    _c(
                      "li",
                      { staticClass: "nav-item", on: { click: _vm.setEdit } },
                      [
                        _c(
                          "router-link",
                          {
                            staticClass: "nav-link",
                            attrs: {
                              to: { name: "admin-add-sales" },
                              tag: "a",
                              "active-class": "active",
                              exact: ""
                            }
                          },
                          [
                            _c("i", { staticClass: "nav-icon fas fa-tag" }),
                            _vm._v(" "),
                            _c("p", [
                              _vm._v(
                                "\n                                            Add Sales\n                                        "
                              )
                            ])
                          ]
                        )
                      ],
                      1
                    )
                  ]),
                  _vm._v(" "),
                  _c("ul", { staticClass: "nav nav-treeview ml-1" }, [
                    _c(
                      "li",
                      { staticClass: "nav-item", on: { click: _vm.setEdit } },
                      [
                        _c(
                          "router-link",
                          {
                            staticClass: "nav-link",
                            attrs: {
                              to: { name: "admin-sales" },
                              tag: "a",
                              "active-class": "active",
                              exact: ""
                            }
                          },
                          [
                            _c("i", { staticClass: "nav-icon fas fa-eye" }),
                            _vm._v(" "),
                            _c("p", [
                              _vm._v(
                                "\n                                            View Sales\n                                        "
                              )
                            ])
                          ]
                        )
                      ],
                      1
                    )
                  ])
                ]),
                _vm._v(" "),
                _c(
                  "li",
                  { staticClass: "nav-item" },
                  [
                    _c(
                      "router-link",
                      {
                        staticClass: "nav-link",
                        attrs: {
                          to: { name: "admin-order" },
                          tag: "a",
                          "active-class": "active",
                          exact: ""
                        }
                      },
                      [
                        _c("i", { staticClass: "nav-icon fas fa-dolly" }),
                        _vm._v(" "),
                        _c("p", [
                          _vm._v(
                            "\n                                    Order Management\n                                "
                          )
                        ]),
                        _vm._v(" "),
                        _c(
                          "span",
                          { staticClass: "badge badge-warning navbar-badge" },
                          [
                            _vm._v(
                              "\n                                    " +
                                _vm._s(_vm.unreadNotification.length) +
                                "\n                                "
                            )
                          ]
                        )
                      ]
                    )
                  ],
                  1
                ),
                _vm._v(" "),
                _c(
                  "li",
                  { staticClass: "nav-item" },
                  [
                    _c(
                      "router-link",
                      {
                        staticClass: "nav-link",
                        attrs: {
                          to: { name: "admin-invoice" },
                          tag: "a",
                          "active-class": "active",
                          exact: ""
                        }
                      },
                      [
                        _c("i", {
                          staticClass: "nav-icon fas fa-file-invoice-dollar"
                        }),
                        _vm._v(" "),
                        _c("p", [
                          _vm._v(
                            "\n                                    Invoice Management\n                                "
                          )
                        ])
                      ]
                    )
                  ],
                  1
                ),
                _vm._v(" "),
                _c(
                  "li",
                  { staticClass: "nav-item" },
                  [
                    _c(
                      "router-link",
                      {
                        staticClass: "nav-link",
                        attrs: {
                          to: { name: "admin-enquiry" },
                          tag: "a",
                          "active-class": "active",
                          exact: ""
                        }
                      },
                      [
                        _c("i", {
                          staticClass: "nav-icon far fa-comment-dots"
                        }),
                        _vm._v(" "),
                        _c("p", [
                          _vm._v(
                            "\n                                    Enquiry\n                                "
                          )
                        ])
                      ]
                    )
                  ],
                  1
                ),
                _vm._v(" "),
                _c(
                  "li",
                  { staticClass: "nav-item" },
                  [
                    _c(
                      "router-link",
                      {
                        staticClass: "nav-link",
                        attrs: {
                          to: { name: "admin-voucher" },
                          tag: "a",
                          "active-class": "active",
                          exact: ""
                        }
                      },
                      [
                        _c("i", { staticClass: "nav-icon fas fa-dollar-sign" }),
                        _vm._v(" "),
                        _c("p", [
                          _vm._v(
                            "\n                                    Voucher Management\n                                "
                          )
                        ])
                      ]
                    )
                  ],
                  1
                ),
                _vm._v(" "),
                _c("li", { staticClass: "nav-item" }, [
                  _c(
                    "a",
                    { staticClass: "nav-link", on: { click: _vm.logout } },
                    [
                      _c("i", { staticClass: "nav-icon fas fa-power-off red" }),
                      _vm._v(" "),
                      _c("p", { staticStyle: { color: "#C2C7D0" } }, [
                        _vm._v(
                          "\n                                    Logout\n                                "
                        )
                      ])
                    ]
                  )
                ])
              ]
            )
          ])
        ])
      ]
    ),
    _vm._v(" "),
    _c("div", { staticClass: "content-wrapper" }, [
      _c("div", { staticClass: "content" }, [
        _c(
          "div",
          { staticClass: "container-fluid" },
          [_c("router-view"), _vm._v(" "), _c("vue-progress-bar")],
          1
        )
      ])
    ]),
    _vm._v(" "),
    _vm._m(6)
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "nav",
      {
        staticClass:
          "main-header navbar navbar-expand navbar-white navbar-light border-bottom"
      },
      [
        _c("ul", { staticClass: "navbar-nav" }, [
          _c("li", { staticClass: "nav-item" }, [
            _c(
              "a",
              {
                staticClass: "nav-link",
                attrs: { "data-widget": "pushmenu", href: "#" }
              },
              [_c("i", { staticClass: "fas fa-bars" })]
            )
          ])
        ])
      ]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "a",
      { staticClass: "brand-link", attrs: { href: "index3.html" } },
      [
        _c("span", { staticClass: "brand-text font-weight-light" }, [
          _vm._v("TheNexTrend")
        ])
      ]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("a", { staticClass: "nav-link", attrs: { href: "#" } }, [
      _c("i", { staticClass: "nav-icon fas fa-book" }),
      _vm._v(" "),
      _c("p", [
        _vm._v(
          "\n                                    Reports\n                                    "
        ),
        _c("i", { staticClass: "right fas fa-angle-left" })
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("a", { staticClass: "nav-link", attrs: { href: "#" } }, [
      _c("i", { staticClass: "nav-icon fas fa-sitemap" }),
      _vm._v(" "),
      _c("p", [
        _vm._v(
          "\n                                    Category Management\n                                    "
        ),
        _c("i", { staticClass: "right fas fa-angle-left" })
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("a", { staticClass: "nav-link", attrs: { href: "#" } }, [
      _c("i", { staticClass: "nav-icon fab fa-product-hunt" }),
      _vm._v(" "),
      _c("p", [
        _vm._v(
          "\n                                    Products Management\n                                    "
        ),
        _c("i", { staticClass: "right fas fa-angle-left" })
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("a", { staticClass: "nav-link", attrs: { href: "#" } }, [
      _c("i", { staticClass: "nav-icon fas fa-percent" }),
      _vm._v(" "),
      _c("p", [
        _vm._v(
          "\n                                    Sale/Offer\n                                    "
        ),
        _c("i", { staticClass: "right fas fa-angle-left" })
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("footer", { staticClass: "main-footer" }, [
      _c("strong", [
        _vm._v("Copyright © 2014-2019 "),
        _c("a", { attrs: { href: "http://yashrisoft.com" } }, [
          _vm._v("Yashri Soft Pvt.ltd")
        ]),
        _vm._v(".")
      ]),
      _vm._v(" All\n            rights\n            reserved.\n        ")
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/components/backend/AdminDashboard.vue":
/*!************************************************************!*\
  !*** ./resources/js/components/backend/AdminDashboard.vue ***!
  \************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _AdminDashboard_vue_vue_type_template_id_5525adea_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./AdminDashboard.vue?vue&type=template&id=5525adea&scoped=true& */ "./resources/js/components/backend/AdminDashboard.vue?vue&type=template&id=5525adea&scoped=true&");
/* harmony import */ var _AdminDashboard_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./AdminDashboard.vue?vue&type=script&lang=js& */ "./resources/js/components/backend/AdminDashboard.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _AdminDashboard_vue_vue_type_style_index_0_id_5525adea_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./AdminDashboard.vue?vue&type=style&index=0&id=5525adea&scoped=true&lang=css& */ "./resources/js/components/backend/AdminDashboard.vue?vue&type=style&index=0&id=5525adea&scoped=true&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _AdminDashboard_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _AdminDashboard_vue_vue_type_template_id_5525adea_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _AdminDashboard_vue_vue_type_template_id_5525adea_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "5525adea",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/backend/AdminDashboard.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/backend/AdminDashboard.vue?vue&type=script&lang=js&":
/*!*************************************************************************************!*\
  !*** ./resources/js/components/backend/AdminDashboard.vue?vue&type=script&lang=js& ***!
  \*************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_AdminDashboard_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./AdminDashboard.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backend/AdminDashboard.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_AdminDashboard_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/backend/AdminDashboard.vue?vue&type=style&index=0&id=5525adea&scoped=true&lang=css&":
/*!*********************************************************************************************************************!*\
  !*** ./resources/js/components/backend/AdminDashboard.vue?vue&type=style&index=0&id=5525adea&scoped=true&lang=css& ***!
  \*********************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_AdminDashboard_vue_vue_type_style_index_0_id_5525adea_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/style-loader!../../../../node_modules/css-loader??ref--6-1!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--6-2!../../../../node_modules/vue-loader/lib??vue-loader-options!./AdminDashboard.vue?vue&type=style&index=0&id=5525adea&scoped=true&lang=css& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backend/AdminDashboard.vue?vue&type=style&index=0&id=5525adea&scoped=true&lang=css&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_AdminDashboard_vue_vue_type_style_index_0_id_5525adea_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_AdminDashboard_vue_vue_type_style_index_0_id_5525adea_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_AdminDashboard_vue_vue_type_style_index_0_id_5525adea_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_AdminDashboard_vue_vue_type_style_index_0_id_5525adea_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_AdminDashboard_vue_vue_type_style_index_0_id_5525adea_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./resources/js/components/backend/AdminDashboard.vue?vue&type=template&id=5525adea&scoped=true&":
/*!*******************************************************************************************************!*\
  !*** ./resources/js/components/backend/AdminDashboard.vue?vue&type=template&id=5525adea&scoped=true& ***!
  \*******************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_AdminDashboard_vue_vue_type_template_id_5525adea_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./AdminDashboard.vue?vue&type=template&id=5525adea&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backend/AdminDashboard.vue?vue&type=template&id=5525adea&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_AdminDashboard_vue_vue_type_template_id_5525adea_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_AdminDashboard_vue_vue_type_template_id_5525adea_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);