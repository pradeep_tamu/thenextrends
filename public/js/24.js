(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[24],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/frontend/BrandDirectory.vue?vue&type=script&lang=js&":
/*!**********************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/frontend/BrandDirectory.vue?vue&type=script&lang=js& ***!
  \**********************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  name: "BrandDirectory",
  data: function data() {
    return {
      isMen: false,
      isWomen: true,
      isClothing: false,
      isShoes: false,
      isAccessories: false,
      isSports: false,
      loader: true,
      brands: []
    };
  },
  created: function created() {
    this.getBrands();
  },
  watch: {
    $route: function $route() {
      this.getBrands();
    }
  },
  methods: {
    getBrands: function getBrands() {
      var _this = this;

      this.loader = true;
      this.checkGender();
      this.checkCategory();
      var gender = this.$route.query.gender;

      if (this.$route.query.category) {
        var category = this.$route.query.category;
        axios.get('/api/brandDirectory/' + gender + '/' + category).then(function (data) {
          _this.brands = data.data;
          _this.loader = false;
        });
      } else {
        axios.get('/api/brandDirectory/' + gender).then(function (data) {
          _this.brands = data.data;
          _this.loader = false;
        });
      }
    },
    checkGender: function checkGender() {
      if (this.$route.query.gender == 'Men') {
        this.isMen = true;
        this.isWomen = false;
      } else {
        this.isMen = false;
        this.isWomen = true;
      }
    },
    checkCategory: function checkCategory() {
      var category = this.$route.query.category;

      switch (category) {
        case 'Clothing':
          this.isClothing = true;
          this.isAccessories = false;
          this.isShoes = false;
          this.isSports = false;
          break;

        case 'Shoes':
          this.isClothing = false;
          this.isAccessories = false;
          this.isShoes = true;
          this.isSports = false;
          break;

        case 'Accessories':
          this.isClothing = false;
          this.isAccessories = true;
          this.isShoes = false;
          this.isSports = false;
          break;

        case 'Sports':
          this.isClothing = false;
          this.isAccessories = false;
          this.isShoes = false;
          this.isSports = true;
          break;

        default:
          this.isClothing = false;
          this.isAccessories = false;
          this.isShoes = false;
          this.isSports = false;
          break;
      }
    },
    goFilter: function goFilter(brand) {
      var gender = this.isMen ? 'Men' : 'Women';

      if (this.$route.query.category) {
        var category = this.$route.query.category;
        this.$router.push({
          name: 'filter',
          params: {
            gender: gender,
            category: category
          },
          query: {
            brand: brand
          }
        });
      } else {
        this.$router.push({
          name: 'filter',
          params: {
            gender: gender
          },
          query: {
            brand: brand
          }
        });
      }
    }
  }
});

/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/frontend/BrandDirectory.vue?vue&type=style&index=0&id=3e8e9b30&scoped=true&lang=scss&":
/*!*********************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/sass-loader/dist/cjs.js??ref--7-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/frontend/BrandDirectory.vue?vue&type=style&index=0&id=3e8e9b30&scoped=true&lang=scss& ***!
  \*********************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".directory-title[data-v-3e8e9b30] {\n  font-family: \"Roboto\", sans-serif;\n  color: #4d4d4d;\n}\n.brand-container[data-v-3e8e9b30] {\n  margin-bottom: 3rem;\n  margin-top: 3rem;\n}\n.show_active[data-v-3e8e9b30] {\n  border-bottom: 3px solid #42abc8;\n}\n.sub-brand-title a[data-v-3e8e9b30] {\n  color: grey;\n  margin-right: 1rem;\n}\n.brand-lists-title[data-v-3e8e9b30] {\n  display: flex;\n  flex-wrap: wrap;\n}\n.brand-lists-title .brand-lists[data-v-3e8e9b30] {\n  width: 25%;\n  margin-bottom: 1rem;\n}\n.brand-lists-title .brand-lists span[data-v-3e8e9b30] {\n  text-transform: capitalize;\n}\n.shop-women-men[data-v-3e8e9b30] {\n  cursor: pointer;\n}\n.directory-color[data-v-3e8e9b30] {\n  font-family: \"Roboto\", sans-serif;\n  color: #4d4d4d !important;\n  font-size: 1rem;\n}\n.directory-color[data-v-3e8e9b30]:hover {\n  text-decoration: none;\n}", ""]);

// exports


/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/frontend/BrandDirectory.vue?vue&type=style&index=0&id=3e8e9b30&scoped=true&lang=scss&":
/*!*************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/sass-loader/dist/cjs.js??ref--7-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/frontend/BrandDirectory.vue?vue&type=style&index=0&id=3e8e9b30&scoped=true&lang=scss& ***!
  \*************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../node_modules/css-loader!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--7-2!../../../../node_modules/sass-loader/dist/cjs.js??ref--7-3!../../../../node_modules/vue-loader/lib??vue-loader-options!./BrandDirectory.vue?vue&type=style&index=0&id=3e8e9b30&scoped=true&lang=scss& */ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/frontend/BrandDirectory.vue?vue&type=style&index=0&id=3e8e9b30&scoped=true&lang=scss&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/frontend/BrandDirectory.vue?vue&type=template&id=3e8e9b30&scoped=true&":
/*!**************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/frontend/BrandDirectory.vue?vue&type=template&id=3e8e9b30&scoped=true& ***!
  \**************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "container brand-container" }, [
    _c("h3", { staticClass: "mb-4 directory-title" }, [
      _vm._v("BRAND DIRECTORY")
    ]),
    _vm._v(" "),
    _c("div", { staticClass: "brand-title-list d-flex mb-4" }, [
      _c(
        "div",
        { staticClass: "brand-title mr-5" },
        [
          _c(
            "router-link",
            {
              staticClass: "directory-color mr-3",
              class: { show_active: _vm.isWomen },
              attrs: {
                to: { name: "brand-directory", query: { gender: "Women" } }
              }
            },
            [_vm._v("\n                WOMEN\n            ")]
          ),
          _vm._v(" "),
          _c(
            "router-link",
            {
              staticClass: "directory-color",
              class: { show_active: _vm.isMen },
              attrs: {
                to: { name: "brand-directory", query: { gender: "Men" } }
              }
            },
            [_vm._v("\n                MEN\n            ")]
          )
        ],
        1
      ),
      _vm._v(" "),
      _c(
        "div",
        {
          staticClass: "sub-brand-title d-flex ",
          staticStyle: { "justify-content": "space-evenly" }
        },
        [
          _c(
            "div",
            {
              directives: [
                {
                  name: "show",
                  rawName: "v-show",
                  value: _vm.isMen,
                  expression: "isMen"
                }
              ]
            },
            [
              _c(
                "router-link",
                {
                  staticClass: "directory-color",
                  class: { show_active: _vm.isClothing },
                  attrs: {
                    to: {
                      name: "brand-directory",
                      query: { gender: "Men", category: "Clothing" }
                    }
                  }
                },
                [
                  _vm._v(
                    "\n                    CLOTHING BRANDS\n                "
                  )
                ]
              ),
              _vm._v(" "),
              _c(
                "router-link",
                {
                  staticClass: "directory-color",
                  class: { show_active: _vm.isShoes },
                  attrs: {
                    to: {
                      name: "brand-directory",
                      query: { gender: "Men", category: "Shoes" }
                    }
                  }
                },
                [_vm._v("\n                    SHOES BRANDS\n                ")]
              ),
              _vm._v(" "),
              _c(
                "router-link",
                {
                  staticClass: "directory-color",
                  class: { show_active: _vm.isAccessories },
                  attrs: {
                    to: {
                      name: "brand-directory",
                      query: { gender: "Men", category: "Accessories" }
                    }
                  }
                },
                [
                  _vm._v(
                    "\n                    ACCESSORIES BRANDS\n                "
                  )
                ]
              ),
              _vm._v(" "),
              _c(
                "router-link",
                {
                  staticClass: "directory-color",
                  class: { show_active: _vm.isSports },
                  attrs: {
                    to: {
                      name: "brand-directory",
                      query: { gender: "Men", category: "Sports" }
                    }
                  }
                },
                [
                  _vm._v(
                    "\n                    SPORTS BRANDS\n                "
                  )
                ]
              )
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "div",
            {
              directives: [
                {
                  name: "show",
                  rawName: "v-show",
                  value: _vm.isWomen,
                  expression: "isWomen"
                }
              ]
            },
            [
              _c(
                "router-link",
                {
                  staticClass: "directory-color",
                  class: { show_active: _vm.isClothing },
                  attrs: {
                    to: {
                      name: "brand-directory",
                      query: { gender: "Women", category: "Clothing" }
                    }
                  }
                },
                [
                  _vm._v(
                    "\n                    CLOTHING BRANDS\n                "
                  )
                ]
              ),
              _vm._v(" "),
              _c(
                "router-link",
                {
                  staticClass: "directory-color",
                  class: { show_active: _vm.isShoes },
                  attrs: {
                    to: {
                      name: "brand-directory",
                      query: { gender: "Women", category: "Shoes" }
                    }
                  }
                },
                [_vm._v("\n                    SHOES BRANDS\n                ")]
              ),
              _vm._v(" "),
              _c(
                "router-link",
                {
                  staticClass: "directory-color",
                  class: { show_active: _vm.isAccessories },
                  attrs: {
                    to: {
                      name: "brand-directory",
                      query: { gender: "Women", category: "Accessories" }
                    }
                  }
                },
                [
                  _vm._v(
                    "\n                    ACCESSORIES BRANDS\n                "
                  )
                ]
              ),
              _vm._v(" "),
              _c(
                "router-link",
                {
                  staticClass: "directory-color",
                  class: { show_active: _vm.isSports },
                  attrs: {
                    to: {
                      name: "brand-directory",
                      query: { gender: "Women", category: "Sports" }
                    }
                  }
                },
                [
                  _vm._v(
                    "\n                    SPORTS BRANDS\n                "
                  )
                ]
              )
            ],
            1
          )
        ]
      )
    ]),
    _vm._v(" "),
    _c("hr"),
    _vm._v(" "),
    _c(
      "nav",
      {
        staticClass: "p-0 my-1 d-flex justify-content-center",
        attrs: { "aria-label": "breadcrumb" }
      },
      [
        _c("strong", [
          _c(
            "ol",
            {
              staticClass: "breadcrumb p-0",
              staticStyle: { background: "none", "margin-bottom": "0" }
            },
            [
              _c("li", { staticClass: "breadcrumb-item" }, [
                _c("span", [_vm._v(_vm._s(_vm.$route.query.gender))])
              ]),
              _vm._v(" "),
              _vm.$route.query.category
                ? _c("li", { staticClass: "breadcrumb-item active" }, [
                    _c("span", [_vm._v(_vm._s(_vm.$route.query.category))])
                  ])
                : _vm._e()
            ]
          )
        ])
      ]
    ),
    _vm._v(" "),
    _c("hr"),
    _vm._v(" "),
    _c("div", [
      _vm.loader
        ? _c("div", [_vm._m(0)])
        : _c(
            "div",
            { staticClass: "brand-lists-title" },
            [
              _vm._l(_vm.brands, function(brand) {
                return [
                  _c("div", { staticClass: "brand-lists" }, [
                    _c(
                      "span",
                      {
                        on: {
                          click: function($event) {
                            return _vm.goFilter(brand.name)
                          }
                        }
                      },
                      [_vm._v(_vm._s(brand.name))]
                    )
                  ])
                ]
              })
            ],
            2
          )
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticStyle: { "margin-left": "50%" } }, [
      _c("div", { staticClass: "spinner-border", attrs: { role: "status" } }, [
        _c("span", { staticClass: "sr-only" }, [_vm._v("Loading...")])
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js":
/*!********************************************************************!*\
  !*** ./node_modules/vue-loader/lib/runtime/componentNormalizer.js ***!
  \********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return normalizeComponent; });
/* globals __VUE_SSR_CONTEXT__ */

// IMPORTANT: Do NOT use ES2015 features in this file (except for modules).
// This module is a runtime utility for cleaner component module output and will
// be included in the final webpack user bundle.

function normalizeComponent (
  scriptExports,
  render,
  staticRenderFns,
  functionalTemplate,
  injectStyles,
  scopeId,
  moduleIdentifier, /* server only */
  shadowMode /* vue-cli only */
) {
  // Vue.extend constructor export interop
  var options = typeof scriptExports === 'function'
    ? scriptExports.options
    : scriptExports

  // render functions
  if (render) {
    options.render = render
    options.staticRenderFns = staticRenderFns
    options._compiled = true
  }

  // functional template
  if (functionalTemplate) {
    options.functional = true
  }

  // scopedId
  if (scopeId) {
    options._scopeId = 'data-v-' + scopeId
  }

  var hook
  if (moduleIdentifier) { // server build
    hook = function (context) {
      // 2.3 injection
      context =
        context || // cached call
        (this.$vnode && this.$vnode.ssrContext) || // stateful
        (this.parent && this.parent.$vnode && this.parent.$vnode.ssrContext) // functional
      // 2.2 with runInNewContext: true
      if (!context && typeof __VUE_SSR_CONTEXT__ !== 'undefined') {
        context = __VUE_SSR_CONTEXT__
      }
      // inject component styles
      if (injectStyles) {
        injectStyles.call(this, context)
      }
      // register component module identifier for async chunk inferrence
      if (context && context._registeredComponents) {
        context._registeredComponents.add(moduleIdentifier)
      }
    }
    // used by ssr in case component is cached and beforeCreate
    // never gets called
    options._ssrRegister = hook
  } else if (injectStyles) {
    hook = shadowMode
      ? function () { injectStyles.call(this, this.$root.$options.shadowRoot) }
      : injectStyles
  }

  if (hook) {
    if (options.functional) {
      // for template-only hot-reload because in that case the render fn doesn't
      // go through the normalizer
      options._injectStyles = hook
      // register for functioal component in vue file
      var originalRender = options.render
      options.render = function renderWithStyleInjection (h, context) {
        hook.call(context)
        return originalRender(h, context)
      }
    } else {
      // inject component registration as beforeCreate hook
      var existing = options.beforeCreate
      options.beforeCreate = existing
        ? [].concat(existing, hook)
        : [hook]
    }
  }

  return {
    exports: scriptExports,
    options: options
  }
}


/***/ }),

/***/ "./resources/js/components/frontend/BrandDirectory.vue":
/*!*************************************************************!*\
  !*** ./resources/js/components/frontend/BrandDirectory.vue ***!
  \*************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _BrandDirectory_vue_vue_type_template_id_3e8e9b30_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./BrandDirectory.vue?vue&type=template&id=3e8e9b30&scoped=true& */ "./resources/js/components/frontend/BrandDirectory.vue?vue&type=template&id=3e8e9b30&scoped=true&");
/* harmony import */ var _BrandDirectory_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./BrandDirectory.vue?vue&type=script&lang=js& */ "./resources/js/components/frontend/BrandDirectory.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _BrandDirectory_vue_vue_type_style_index_0_id_3e8e9b30_scoped_true_lang_scss___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./BrandDirectory.vue?vue&type=style&index=0&id=3e8e9b30&scoped=true&lang=scss& */ "./resources/js/components/frontend/BrandDirectory.vue?vue&type=style&index=0&id=3e8e9b30&scoped=true&lang=scss&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _BrandDirectory_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _BrandDirectory_vue_vue_type_template_id_3e8e9b30_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _BrandDirectory_vue_vue_type_template_id_3e8e9b30_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "3e8e9b30",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/frontend/BrandDirectory.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/frontend/BrandDirectory.vue?vue&type=script&lang=js&":
/*!**************************************************************************************!*\
  !*** ./resources/js/components/frontend/BrandDirectory.vue?vue&type=script&lang=js& ***!
  \**************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_BrandDirectory_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./BrandDirectory.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/frontend/BrandDirectory.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_BrandDirectory_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/frontend/BrandDirectory.vue?vue&type=style&index=0&id=3e8e9b30&scoped=true&lang=scss&":
/*!***********************************************************************************************************************!*\
  !*** ./resources/js/components/frontend/BrandDirectory.vue?vue&type=style&index=0&id=3e8e9b30&scoped=true&lang=scss& ***!
  \***********************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_BrandDirectory_vue_vue_type_style_index_0_id_3e8e9b30_scoped_true_lang_scss___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/style-loader!../../../../node_modules/css-loader!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--7-2!../../../../node_modules/sass-loader/dist/cjs.js??ref--7-3!../../../../node_modules/vue-loader/lib??vue-loader-options!./BrandDirectory.vue?vue&type=style&index=0&id=3e8e9b30&scoped=true&lang=scss& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/frontend/BrandDirectory.vue?vue&type=style&index=0&id=3e8e9b30&scoped=true&lang=scss&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_BrandDirectory_vue_vue_type_style_index_0_id_3e8e9b30_scoped_true_lang_scss___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_BrandDirectory_vue_vue_type_style_index_0_id_3e8e9b30_scoped_true_lang_scss___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_BrandDirectory_vue_vue_type_style_index_0_id_3e8e9b30_scoped_true_lang_scss___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_BrandDirectory_vue_vue_type_style_index_0_id_3e8e9b30_scoped_true_lang_scss___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_BrandDirectory_vue_vue_type_style_index_0_id_3e8e9b30_scoped_true_lang_scss___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./resources/js/components/frontend/BrandDirectory.vue?vue&type=template&id=3e8e9b30&scoped=true&":
/*!********************************************************************************************************!*\
  !*** ./resources/js/components/frontend/BrandDirectory.vue?vue&type=template&id=3e8e9b30&scoped=true& ***!
  \********************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_BrandDirectory_vue_vue_type_template_id_3e8e9b30_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./BrandDirectory.vue?vue&type=template&id=3e8e9b30&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/frontend/BrandDirectory.vue?vue&type=template&id=3e8e9b30&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_BrandDirectory_vue_vue_type_template_id_3e8e9b30_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_BrandDirectory_vue_vue_type_template_id_3e8e9b30_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);