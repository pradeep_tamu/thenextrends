<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// for admin login
Route::group(['prefix' => 'admin'], function () {
    Route::post('/login', 'Auth\AdminAuthController@login');
    Route::middleware('auth:api')->group(function () {
        Route::post('/logout', 'Auth\AdminAuthController@logout');
    });
});

Route::get('/users', 'Auth\AdminAuthController@users');

// for users login
Route::post('/register', 'Auth\AuthController@register');
Route::post('/login', 'Auth\AuthController@login');
Route::post('/updateInfo', 'Auth\AuthController@update');
Route::middleware('auth:api')->group(function () {
Route::post('/logout', 'Auth\AuthController@logout');
});

// account activation
Route::any('/register/activate/{token}', 'Auth\AuthController@accountActivation');

//category
Route::post('category', 'backend\CategoryController@store');
Route::get('parent', 'backend\CategoryController@getParent');
Route::get('option', 'backend\CategoryController@getCategoryOptions');
Route::delete('category_delete/{id}', 'backend\CategoryController@destroy');

//navigation
Route::get('navigation', 'frontend\HomeController@getNavigation');

//Brand
Route::post('brand', 'backend\BrandController@store');
Route::get('getAllBrands', 'backend\BrandController@getAllBrands');
Route::get('getBrands/{gender}/{type}', 'backend\BrandController@getBrand');
Route::delete('delete_brand/{id}', 'backend\BrandController@destroy');

//Fabric
Route::post('fabric', 'backend\FabricController@store');
Route::get('getfabrics', 'backend\FabricController@getFabric');
Route::delete('delete_fabric/{id}', 'backend\FabricController@destroy');

//Occasion
Route::post('occasion', 'backend\OccassionController@store');
Route::get('getoccasions', 'backend\OccassionController@getOccasion');
Route::delete('delete_occasion/{id}', 'backend\OccassionController@destroy');

//color
Route::post('color', 'backend\ColorController@store');
Route::get('getcolor', 'backend\ColorController@getColor');
Route::delete('delete_color/{id}', 'backend\ColorController@destroy');

//size
Route::post('size', 'backend\ColorController@create');
Route::get('getsizes', 'backend\ColorController@getSize');
Route::delete('delete_size/{id}', 'backend\ColorController@delete');

//for store
Route::apiResources(['store'=>'backend\StoreController']);
Route::get('getAllStores','backend\StoreController@getAllStores');

//product
Route::post('product', 'backend\ProductController@store');
Route::get('productInfo/{id}', 'backend\ProductController@editInfo');
Route::get('color_sizeInfo/{id}', 'backend\ProductController@colorSize');
Route::get('getProduct', 'backend\ProductController@getProduct');
Route::get('getProductFull/{id}', 'backend\ProductController@getProductFull');
Route::get('getColorSize/{id}', 'backend\ProductController@getColorSize');
Route::get('getImage/{id}', 'backend\ProductController@getImages');
Route::get('getEditImages/{id}', 'backend\ProductController@editImages');
Route::delete('delete_product/{id}', 'backend\ProductController@destroy');
Route::put('update_product/{id}', 'backend\ProductController@updateProduct');

//for frontend
Route::get('getProductFront/{gender}/{category?}/{subCategory?}/{sales?}/{search?}/{brand?}', 'frontend\ProductController@getProduct');
Route::get('getProductColor/{id}', 'frontend\ProductController@getProductColor');

// all APIs for single product page here
Route::get('singleProduct/{id}', 'frontend\ProductController@singleProduct');

Route::get('getCategoryData', 'frontend\HomeController@getCategoryData');
Route::get('getTrending', 'frontend\ProductController@getTrending');

//order
Route::post('checkItem', 'frontend\OrderController@checkItem');
Route::post('confirmOrder', 'frontend\OrderController@store');
Route::get('myOrders/{id}', 'frontend\OrderController@myOrders');

//backend order management
Route::get('getOrder', 'frontend\OrderController@getOrderList');
Route::get('getOrderDetail/{id}', 'frontend\OrderController@getOrderDetail');
Route::put('changeStatus/{id}/{value}','frontend\OrderController@changeStatus');
Route::post('orderView/{id}','frontend\OrderController@orderView');

//invoice
Route::post('createInvoice/{id}/{total}', 'backend\InvoiceController@createInvoice');
Route::get('getInvoice', 'backend\InvoiceController@getInvoice');
Route::get('getItemDetails/{id}', 'backend\InvoiceController@getItemDetails');
Route::get('complete-order/{id}/{order_id}', 'backend\InvoiceController@orderCompleted');

//filter
Route::get('getFilterProduct/{gender}/{category?}/{subCategory?}/{brand?}', 'frontend\ProductController@getFilterProduct');
Route::get('afterFilterProducts/{gender}/{category?}/{subCategory?}/{filter?}/{sales?}/{search?}/{brand?}/{similar?}', 'frontend\ProductController@afterFilterProducts');

//notification
Route::get('notification/{id}', 'Auth\AdminAuthController@notification');
Route::post('viewNotification/{id}', 'Auth\AdminAuthController@viewNotification');

//subscription
Route::post('newsletter','frontend\NewsletterController@store');

//brand directory frontend
Route::get('brandDirectory/{gender}/{category?}', 'frontend\BrandController@brandDirectory');
Route::get('brandFilter/{gender}', 'frontend\BrandController@brandFilter');

//Navigation Image Banner
Route::post('navigation-image','backend\CategoryController@storeImage');
Route::get('banner-image','backend\CategoryController@bannerList');
Route::get('navigation-banner-image/{gender}/{category}','frontend\HomeController@getNavigationBanner');
Route::delete('delete_banner/{id}','backend\CategoryController@deleteBanner');

//featured brand
Route::post('featureAdd','backend\BrandController@featuredAdd');
Route::get('featuredBrand/{gender}','frontend\BrandController@featuredBrand');

//sales offer
Route::get('getStores', 'backend\SalesOfferController@getStores');
Route::get('getStoreProducts/{id}', 'backend\SalesOfferController@getStoreProducts');
Route::get('getSales', 'backend\SalesOfferController@getSales');
Route::post('setSales', 'backend\SalesOfferController@store'); // for add and updating sales rate
Route::post('deleteSales/{id}', 'backend\SalesOfferController@delete');
//Route::post('salesImage', 'backend\SalesOfferController@setImage');

//socialite
Route::post('socialLogin/{provider}', 'Auth\SocialLoginController@SocialSignUp');

//payment gateway
Route::post('khalti', 'frontend\NewsletterController@payment');
Route::post('esewa', 'frontend\NewsletterController@paymentEsewa');

//password reset
Route::post('/password_reset/email_verify', 'Auth\PasswordResetController@create');
Route::post('/password_reset', 'Auth\PasswordResetController@reset');

// admin dashboard data
Route::get('/data', 'backend\DashboardController@data');

//Enquiry
Route::post('enquiry', 'frontend\HomeController@sendEnquiry');
Route::get('getEnquiry', 'frontend\HomeController@getEnquiry');
Route::get('getMessage/{id}', 'frontend\HomeController@getMessage');

// WishList
Route::get('getWishListProduct/{ids}', 'frontend\ProductController@getWishListProduct');

// Charts
Route::get('salesReport/{sort}/{start?}/{end?}', 'backend\DashboardController@salesReport');
Route::get('salesReportBySize/{sort}/{start?}/{end?}', 'backend\ReportsController@salesReportBySize');

// Reports
Route::get('getReportFilter', 'backend\ReportsController@getReportFilter');
Route::get('detailReport/{filter}/{start}/{end}/{trending?}', 'backend\ReportsController@detailReport');
Route::get('inventoryOnHand', 'backend\ReportsController@inventoryOnHand');
Route::get('lowStock', 'backend\ReportsController@lowStock');
Route::get('slowMovingStock', 'backend\ReportsController@slowMovingStock');

//voucher
Route::post('voucher', 'backend\VoucherController@store');
Route::get('getvoucher', 'backend\VoucherController@getVoucher');
Route::delete('delete_voucher/{id}', 'backend\VoucherController@destroy');

//front voucher
Route::post('voucher-code/{voucher}', 'backend\VoucherController@pickVoucher');
Route::put('voucher-update/{voucher}', 'backend\VoucherController@updateVoucher');