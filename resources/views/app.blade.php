<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title></title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Fonts -->

    <!-- Favicon -->
    <link rel="icon" href="{{ asset('image/favicon.png') }}">
    <meta property="og:image" content="https://thenextrends.com/image/Denim.jpg" />

    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css"
          integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">

    <!-- Styles -->
    <style>
        html, body {
            margin: 0;
        }
    </style>
</head>
<body>
<div id="app">
    <router-view></router-view>
    <vue-progress-bar></vue-progress-bar>
</div>

<script src="{{asset('js/app.js')}}"></script>

</body>
</html>
