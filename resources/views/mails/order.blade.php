@component('mail::message')
<div class="order-mail">
<h4>Hello,</h4>
<h4>Please Check the product Details Below:</h4>
<div class="order-details">
<div class="product-details"><span>Customer Name: </span><strong>{{$orders->orders->name}}</strong></div>
<div class="product-details"><span>Customer Address: </span><strong>{{$orders->orders->address}}</strong>
</div>
<div class="product-details">
<span>Customer Mobile Number: </span><strong>{{$orders->orders->mobile}}</strong></div>
<div class="product-details"><span>Products Name: </span><strong>{{$orders->products->name}} <a
href="https://thenextrends.com/{{$orders->products->slug}}-{{$orders->products->id}}"
target="_blank">Product Link</a></strong></div>
<div class="product-details">
<span>Products Price: </span>
<strong>
@if($orders->products->sales)
{{$orders->products->price - $orders->products->price * $orders->products->sales->rate}}
@else
{{$orders->products->price}}
@endif
</strong>
</div>
<div class="product-details"><span>Color: </span><strong>{{$orders->color}}</strong></div>
<div class="product-details"><span>Size: </span><strong>{{$orders->size}}</strong></div>
<div class="product-details"><span>Quantity: </span><strong>{{$orders->quantity}}</strong></div>
<div class="product-details">
<span>Voucher Amount: </span><strong>{{$orders->orders->voucher_amount}}</strong></div>
<div class="product-details"><span>Shipping Charge: </span><strong>{{$orders->orders->shipping}}</strong>
</div>
<div class="product-details">
<span>Total Price:</span>
<strong>
{{$orders->orders->total}}
</strong>
</div>
<br>
</div>
<h4>Thank You,</h4>
<h4>Visit Again!!</h4>
<h4>Regards,</h4>
<h2>{{ config('app.name') }}</h2>
</div>
@endcomponent


