@component('mail::message')
<style>
td{
margin: 1px 5px !important;
}
</style>
<h4>Hello,</h4>
<h4>Please re-stock the following product</h4>

@component('mail::table')
| ID&emsp; | &nbsp;Product Details&nbsp; | &nbsp;Minimum Stock&nbsp; | &nbsp;Stock On Hand&nbsp; | &nbsp;Stock Low By&nbsp; |
| :------- | :-------------------------- | :-----------------------: | :-----------------------: | :----------------------: |
@foreach($items as $item)
| {{ $loop->index+1 }} | Name: <strong>{{ $item->name }}</strong> <br>Product ID: <strong>{{ $item->product_id }}</strong><br>Gender: <strong> {{ $item->gender }}</strong><br>Category: <strong>{{ $item->category }}</strong><br>Sub-Category: <strong>{{ $item->sub }}</strong><br>Color: <strong>{{ $item->color }}</strong><br>Size: <strong>{{ $item->size }}</strong> | {{ $item->minimum }} | {{ $item->stock }} | {{ $item->low }} |
@endforeach
@endcomponent

<h4>Thank You,</h4>

{{ config('app.name') }}
@endcomponent
