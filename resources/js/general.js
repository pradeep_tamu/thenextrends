//this guard for router checking correct authentication
export function initialize(store, router) {
    // for denying access to login page after logged in
    router.beforeEach((to, from, next) => {
        const requiresAdminAuth = to.matched.some(record => record.meta.requiresAdminAuth);
        const requiresAuth = to.matched.some(record => record.meta.requiresAuth);
        const noLogin = to.matched.some(record => record.meta.noLogin);
        const currentAdmin = store.state.currentAdmin;
        const currentUser = store.state.currentUser;

        if(requiresAdminAuth){
            store.dispatch('timeoutAdmin')
                .then()
                .catch(() => {
                    store.commit('logoutAdmin')
                    next({name: 'admin-login'})
                });
        } else {
            store.dispatch('timeout')
                .then()
                .catch(() => {
                    store.commit('logout')
                    next()
                });
        }

        //redirection for admin
        if(requiresAdminAuth && !currentAdmin) {
            next('/admin/login');
        } else if(to.path == '/admin/login' && currentAdmin) {
            next('/admin/dashboard');
        } else {
            next();
        }

        //redirection for user
        if(requiresAuth && !currentUser) {
            next('/login');
        } else if(to.path == '/login' && currentUser) {
            next('/');
        } else if(to.path == '/register' && currentUser) {
            next('/');
        } else {
            next();
        }

        if(noLogin && currentUser){
            next('/')
        } else{
            next();
        }

    });

    if(store.getters.currentAdmin){
        setAuthorization(store.getters.currentAdmin.token);
    }
}

export function setAuthorization(token) {
    axios.defaults.headers.common["Authorization"] = `Bearer ${token}`
}
