<template>
    <div>
        <!-- Modal -->
        <div class="modal fade" id="editSales" tabindex="-1" role="dialog" aria-labelledby="modal-label"
             aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="modal-label">Edit Sales</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form @submit.prevent="editSales">
                        <div class="modal-body">
                            <div class="form-group">
                                <input class="form-control" v-model="form.rate" name="rate" id="rate"
                                       placeholder="Rate"
                                       :class="{ 'is-invalid': form.errors.has('rate')}"
                                       required>
                                <small id="rateHelp" class="form-text text-muted">
                                    For Example: If rate is 12.5% then 12.5
                                </small>
                                <has-error :form="form" field="rate"></has-error>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-outline-primary">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- end of modal -->

        <!-- loader section -->
        <div v-if="loading" style="height: 100vh">
            <div class="mt-5" style="margin-left:45%">
                <div class="spinner-border" role="status">
                    <span class="sr-only">Loading...</span>
                </div>
            </div>
        </div>
        <!-- end of loader section -->

        <!-- data table -->
        <div v-else class="pt-3 pb-5">
            <h4>Products</h4>
            <div class="mt-3 mb-2">
                <el-row>
                    <el-col :span="8">
                        <el-input v-model="filters[0].value" placeholder="Search"></el-input>
                    </el-col>
                </el-row>
            </div>
            <data-tables :data="data"
                         :filters="filters"
                         :pagination-props="{ background: true, pageSizes: [10, 20, 50] }">

                <el-table-column prop="id" label="ID" sortable="custom" min-width="35%">
                </el-table-column>

                <el-table-column prop="product.name" label="Product Name" sortable="custom">
                </el-table-column>

                <el-table-column prop="product.price" label="Price" sortable="custom">
                </el-table-column>

                <el-table-column prop="rate" label="Rate" sortable="custom">
                    <template slot-scope="scope">
                        {{ scope.row.rate * 100 }}%
                    </template>
                </el-table-column>

                <el-table-column prop="store.store_name" label="Store" sortable="custom">
                </el-table-column>

                <el-table-column label="Action">
                    <template slot-scope="scope">
                        <button type="button" class="btn btn-outline-dark btn-sm"
                                @click="setProduct(scope.row.product_id, scope.row.rate)"
                                data-toggle="modal" data-target="#editSales">
                            <i class="fas fa-edit"></i>
                        </button>
                        <button type="button" class="btn btn-outline-danger btn-sm"
                                @click="deleteSales(scope.row.id)">
                            <i class="fa fa-trash"></i>
                        </button>
                    </template>
                </el-table-column>

            </data-tables>
        </div>
        <!-- data table -->

    </div>
</template>
<script>
    export default {
        name: "sales",
        data() {
            return {
                loading: true,
                form: new Form({
                    product_id: '',
                    rate: ''
                }),
                data: [],
                filters: [
                    {
                        prop: ['id', 'product.name', 'product.price', 'rate', 'store.store_name'],
                        value: ''
                    }
                ]
            }
        },
        created() {
            this.getSales();
        },
        methods: {
            getSales(){
                this.loading = true;
                axios.get('/api/getSales')
                    .then((response) => {
                        this.data = response.data;
                        this.loading = false;
                    })
            },

            setProduct(id, rate){
                this.form.reset();
                this.form.product_id = id;
                this.form.rate = rate * 100;
            },

            editSales() {
                this.$Progress.start();
                this.form.post('/api/setSales')
                    .then((response) => {
                        this.form.reset();
                        $('#editSales').modal('hide');
                        Toast.fire({
                            type: 'success',
                            title: 'Sales Edited!!!'
                        });
                        this.$Progress.finish();
                        this.$loading = true;
                        this.getSales();
                    })
                    .catch((error) => {
                        this.$Progress.fail();
                    })
            },

            deleteSales(id) {
                swal.fire({
                    title: 'Are you sure?',
                    text: "You won't be able to revert this!",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!'
                }).then((result) => {
                    // Send request to the server
                    if (result.value) {
                        this.$Progress.start();
                        axios.post('/api/deleteSales/' + id).then(() => {
                            swal.fire(
                                'Deleted!',
                                'Sales has been deleted.',
                                'success'
                            );
                            this.$Progress.finish();
                            this.getSales();

                        }).catch(() => {
                            swal("Failed!", "There was something wrong.", "warning");
                            this.$Progress.fail();
                        });
                    }
                })
            }
        }
    }
</script>

<style scoped>

</style>