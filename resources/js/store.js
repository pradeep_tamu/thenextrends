import {getAdmin} from "./components/backend/helpers/auth";

import {getUser} from "./components/frontend/helpers/auth";

const admin = getAdmin();
const user = getUser();

let cart = window.localStorage.getItem('cart');
let wish = window.localStorage.getItem('wish');
let total = window.localStorage.getItem('total');

export default {
    state: {
        currentUser: user,
        currentAdmin: admin,
        isLoggedIn: !!user,//!! cast user into boolean if no object
        isAdminLoggedIn: !!admin,
        loading: false,
        auth_error: null,
        userType: '',
        componentName: '',
        editModeProduct: false,
        carts: cart ? JSON.parse(cart) : [],
        wishLists: wish ? JSON.parse(wish) : [],
        total: total ? JSON.parse(total) : 0,
        qty: 0, //

    },

    getters: {
        isLoading(state) {
            return state.loading;
        },
        isLoggedIn(state) {
            return state.isLoggedIn;
        },
        isAdminLoggedIn(state) {
            return state.isAdminLoggedIn;
        },
        currentUser(state) {
            return state.currentUser;
        },
        currentAdmin(state) {
            return state.currentAdmin;
        },
        authError(state) {
            return state.auth_error;
        },
        getComponentName(state) {
            return state.componentName;
        },
        isEdit(state) {
            return state.editModeProduct;
        },
        getCart(state) {
            return state.carts;
        },
        getTotal(state) {
            return state.total;
        },
        getQty(state) { //
            return state.qty; //
        }, //

        getWish(state) {
            return state.wishLists;
        }
    }

    ,

    mutations: {
        login(state) {
            state.loading = true;
            state.auth_error = null;
        },
        loginSuccess(state, payload) {
            state.auth_error = null;
            state.loading = false;
            let now = new Date().getTime();
            if (payload.user.type === "admin") {
                state.isAdminLoggedIn = true;
                state.currentAdmin = Object.assign({}, payload.user, {token: payload.access_token});
                localStorage.setItem("admin", JSON.stringify(state.currentAdmin));
                localStorage.setItem("adminLoginTime", now);
            } else {
                state.isLoggedIn = true;
                state.currentUser = Object.assign({}, payload.user, {token: payload.access_token});
                localStorage.setItem("user", JSON.stringify(state.currentUser));
                localStorage.setItem("loginTime", now);
            }

        },
        loginFailed(state, payload) {
            state.loading = false;
            state.auth_error = payload.error;
        },
        logoutAdmin(state) {
            localStorage.removeItem("admin");
            localStorage.removeItem("adminLoginTime");
            state.isAdminLoggedIn = false;
            state.currentAdmin = null;
        },
        logout(state) {
            localStorage.removeItem("user");
            localStorage.removeItem("loginTime");
            state.isLoggedIn = false;
            state.currentUser = null;
        },
        setComponentName(state, payload) {
            state.componentName = payload;
        },
        setEditMode(state, payload) {
            state.editModeProduct = payload;
        },
        updateInfo(state, payload) {
            state.currentUser = Object.assign({}, payload.user);
            localStorage.setItem("user", JSON.stringify(state.currentUser));
        },

        addToCart(state, payload) {
            let data = {};
            Object.assign(data, payload);
            let found = state.carts.find(product => product.colorsize_id === data.colorsize_id);

            if (found) {
                found.quantity++;
                found.total_quantity--; //
                found.subTotal = found.quantity * found.price;
            } else {
                data.total_quantity--; //
                state.carts.push(data);
            }
            state.total = 0;
            for (let i = 0; i < state.carts.length; i++) {
                state.total += state.carts[i].subTotal;
            }

            this.commit('saveData');
            this.commit('saveTotal');
        },

        addToWish(state, payload) {
            state.wishLists.push(payload);
            this.commit('saveWish');
        },

        removeWish(state, payload) {
            let found = state.wishLists.find(product => product.productId == payload.productId);

            let item = state.wishLists.indexOf(found);
            state.wishLists.splice(item, 1);
            this.commit('saveWish');
        },

        //total for cart
        saveTotal(state) {
            window.localStorage.setItem('total', JSON.stringify(state.total));
        },

        //cart data
        saveData(state) {
            window.localStorage.setItem('cart', JSON.stringify(state.carts));
        },

        //save wish list to local storage
        saveWish(state) {
            window.localStorage.setItem('wish', JSON.stringify(state.wishLists));
        },

        removeFromCart(state, payload) {

            let found = state.carts.find(product => product.colorsize_id == payload.colorsize_id);
            state.total = state.total - found.subTotal;
            this.commit('saveTotal');
            let item = state.carts.indexOf(found);
            state.carts.splice(item, 1);
            this.commit('saveData');
        },

        incQuantity(state, payload) {
            let found = state.carts.find(product => product.colorsize_id == payload.colorsize_id);
            found.quantity++;
            found.total_quantity--;
            found.subTotal = Number(found.quantity * found.price);
            state.total += found.price;
            this.commit('saveData');
            this.commit('saveTotal');

        },

        decQuantity(state, payload) {
            let found = state.carts.find(product => product.colorsize_id == payload.colorsize_id);
            found.quantity--;
            found.total_quantity++;
            found.subTotal = Number(found.quantity * found.price);
            state.total -= found.price;
            this.commit('saveData');
            this.commit('saveTotal');
        },

        removeData(state) {
            localStorage.removeItem("cart");
            localStorage.removeItem("total");
            state.carts = [];
            state.total = '';
            this.commit('saveData');
            this.commit('saveTotal');

        },

        getQuantity(state, id) {
            let found = state.carts.find(product => product.colorsize_id == id.colorsize_id);
            if (found) {
                state.qty = found.total_quantity;
            } else {
                state.qty = null;
            }
        }
    },
    actions: {
        login(context) {
            context.commit("login");
        },
        setComponentName(state, payload) {
            state.commit('setComponentName', payload);
        },
        setEditMode(state, payload) {
            state.commit('setEditMode', payload);
        },
        timeout() {
            return new Promise((resolve, reject) => {
                let now = new Date().getTime();
                let loginTime = localStorage.getItem("loginTime");
                if (loginTime === null) {
                    localStorage.setItem("loginTime", now);
                    resolve()
                } else {
                    if (now - loginTime > 7 * 24 * 60 * 60 * 1000) {
                        reject()
                    }
                }
            });
        },
        timeoutAdmin() {
            return new Promise((resolve, reject) => {
                let now = new Date().getTime();
                let loginTime = localStorage.getItem("adminLoginTime");
                if (loginTime === null) {
                    localStorage.setItem("adminLoginTime", now);
                    resolve();
                } else {
                    if (now - loginTime > 7 * 24 * 60 * 60 * 1000) {
                        reject();
                    }
                }
            });
        },
    }
};
