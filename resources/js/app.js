require('./bootstrap');

window.Vue = require('vue');

import VueRouter from 'vue-router';
import Vuex from 'vuex';
import {Form, HasError, AlertError} from 'vform';
import VueCarousel from 'vue-carousel';

Vue.use(VueCarousel);
Vue.use(VueRouter);
window.Fire = new Vue();
//form validation
window.Form = Form;
Vue.component(HasError.name, HasError)
Vue.component(AlertError.name, AlertError)

import {routes} from "./routes.js"; //Route information for vue router
const router = new VueRouter({
    history: true,
    mode: 'history',
    routes,
});

// for social login
import VueAxios from 'vue-axios'
import axios from 'axios';
Vue.use(VueAxios, axios)

// Vue.component('social-login', require('./components/frontend/auth/SocialLogin').default);

//for pagination
Vue.component('pagination', () => import('laravel-vue-pagination'));

//filters
Vue.filter('upText',(text)=>{
    return text.charAt(0).toUpperCase() + text.slice(1)
});
Vue.filter('lowerCase',(text)=>{
    return text.toLowerCase();
});
Vue.filter('round',(num)=>{
    return parseFloat(num).toFixed(0);
});
Vue.filter('price',(num)=>{
    return parseFloat(num).toLocaleString();
});
Vue.filter('boolValue', (text) => {
    if (text === 1) {
        return true;
    } else {
        return false;
    }
});

//vue gtag or analytics
import VueGtag from "vue-gtag";

Vue.use(VueGtag, {
        config: { id: "UA-155560712-1" }},
    router
);

// for data table

Vue.component('el-pagination', () => import('element-ui/lib/pagination'));
Vue.component('el-table-column', () => import('element-ui/lib/table-column'));
Vue.component('el-table', () => import('element-ui/lib/table'));

//sweetalert
import swal from 'sweetalert2'
window.swal = swal;
const toast = swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 3000
});
window.Toast = toast;

// vuex
import StoreData from "./store";
const store = new Vuex.Store(StoreData);

//for progressbar
import Progressbar from "./progressbar.js";
import VueProgressBar from 'vue-progressbar';
Vue.use(VueProgressBar, Progressbar);

// interceptors
import {initialize} from "./general";

initialize(store, router);

const app = new Vue({
    el: '#app',
    router,
    store
});
