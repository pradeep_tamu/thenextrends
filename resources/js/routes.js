
//frontend components
const NotFound = () => import('./components/NotFound');
const Home = () => import('./components/frontend/Home');
const SingleProduct = () => import('./components/frontend/SingleProduct');
const Layout = () => import('./components/frontend/layout/Layout');
const Filter = () => import('./components/frontend/Filter');
const Login = () => import('./components/frontend/auth/Login');
const Cart = () => import('./components/frontend/Cart');
const Wishlist = () => import('./components/frontend/Wishlist');
const Checkout = () => import('./components/frontend/Checkout');
const UserDashboard = () => import('./components/frontend/user/Dashboard');
const UserProfile = () => import('./components/frontend/user/Profile');
const UserOrder = () => import('./components/frontend/user/Order');
const UserVoucher = () => import('./components/frontend/user/Voucher');
const UserOrderDetails = () => import('./components/frontend/user/UserOrderDetails');
const BrandDirectory = () => import('./components/frontend/BrandDirectory');
const ConfirmAccount = () => import("./components/frontend/auth/ConfirmAccount");
const PasswordResetForm = () => import("./components/frontend/auth/PasswordResetForm");
const ResetPassword = () => import("./components/frontend/auth/ResetPassword");
const SizeGuide = () => import('./components/frontend/SizeGuide');
const Delivery = () => import('./components/frontend/Delivery');
const Privacy = () => import('./components/frontend/Privacy');
const Returns = () => import('./components/frontend/Returns');
const AboutUs = () => import('./components/frontend/AboutUs');
const Contact = () => import('./components/frontend/Contact');
const Careers = () => import('./components/frontend/Careers');


// backend
const AdminDashboard = () => import('./components/backend/AdminDashboard');
const Dashboard = () => import('./components/backend/Dashboard');
const AdminLogin = () => import('./components/backend/Login');
const Category = () => import('./components/backend/Category');
const Brand = () => import('./components/backend/Brand');
const Fabric = () => import('./components/backend/Fabric');
const Occasion = () => import('./components/backend/Occasion');
const AddProduct = () => import('./components/backend/product/AddProduct');
const Color = () => import('./components/backend/Color');
const Size = () => import('./components/backend/Size');
const Store = () => import('./components/backend/Store');
const ViewProduct = () => import('./components/backend/product/ViewProduct');
const Users = () => import("./components/backend/User");
const Order = () => import('./components/backend/order/Order');
const OrderDetails = () => import('./components/backend/order/OrderDetails');
const Invoice = () => import("./components/backend/invoice/Invoice");
const InvoiceDetails = () => import("./components/backend/invoice/InvoiceDetails");
const Navigation = () => import("./components/backend/helpers/Navigation");
const Sales = () => import("./components/backend/sales/Sales");
const AddSales = () => import("./components/backend/sales/AddSales");
const Enquiry = () => import("./components/backend/Enquiry");
const Voucher = () => import("./components/backend/Voucher");
const Charts = () => import("./components/backend/reports/Charts");
const SalesReport = () => import("./components/backend/reports/SalesReport");
const InventoryOnHand = () => import("./components/backend/reports/InventoryOnHand");
const LowStock = () => import("./components/backend/reports/LowStock");
const SlowMovingStock = () => import("./components/backend/reports/SlowMovingStock");
// const SalesPhoto = () => import(./components/backend/sales/SalesPhoto");

export const routes = [

    // admin routes
    {
        path: '/admin/login',
        name: 'admin-login',
        component: AdminLogin,
        meta: {
            title: 'Login'
        }
    },
    {
        path: '/admin',
        component: AdminDashboard,
        redirect: {name: 'admin-dashboard'},
        meta: {
            requiresAdminAuth: true
        },
        children: [
            {
                path: 'dashboard',
                name: 'admin-dashboard',
                component: Dashboard,
                meta: {
                    title: 'Dashboard'
                }
            },
            {
                path: 'category',
                name: 'category',
                component: Category,
                meta: {
                    title: 'Category'
                }
            },
            {
                path: 'navigation-image',
                name: 'navigation-image',
                component: Navigation,
                meta: {
                    title: 'Navigation'
                }
            },
            {
                path: 'brand',
                name: 'brand',
                component: Brand,
                meta: {
                    title: 'Brand'
                }
            },
            {
                path: 'fabric',
                name: 'fabric',
                component: Fabric,
                meta: {
                    title: 'Fabric'
                }
            },
            {
                path: 'occasion',
                name: 'occasion',
                component: Occasion,
                meta: {
                    title: 'Occasion'
                }
            },
            {
                path: 'addProduct/:id?',
                name: 'add-product',
                component: AddProduct,
                meta: {
                    title: 'Add Product'
                }
            },

            {
                path: 'view-product',
                name: 'view-product',
                component: ViewProduct,
                meta: {
                    title: 'View Product'
                }
            },
            {
                path: 'color',
                name: 'color',
                component: Color,
                meta: {
                    title: 'Color'
                }
            },
            {
                path: 'store',
                name: 'store',
                component: Store,
                meta: {
                    title: 'Store'
                }
            },
            {
                path: 'size',
                name: 'size',
                component: Size,
                meta: {
                    title: 'Size'
                }
            },
            {
                path: 'users',
                name: 'users',
                component: Users,
                meta: {
                    title: 'Users'
                }
            },
            {
                path: 'order',
                name: 'admin-order',
                component: Order,
                meta: {
                    title: 'Order'
                }
            },
            {
                path: 'order-details/:id?',
                name: 'admin-order-details',
                component: OrderDetails,
                meta: {
                    title: 'Order Details'
                }
            },
            {
                path: 'invoice',
                name: 'admin-invoice',
                component: Invoice,
                meta: {
                    title: 'Invoice'
                }
            },
            {
                path: 'invoice-details/:id?',
                name: 'admin-invoice-details',
                component: InvoiceDetails,
                meta: {
                    title: 'Invoice Details'
                }
            },
            {
                path: 'sales',
                name: 'admin-sales',
                component: Sales,
                meta: {
                    title: 'Sale/Offer'
                }
            },
            {
                path: 'add-sales',
                name: 'admin-add-sales',
                component: AddSales
                ,
                meta: {
                    title: 'Add Sale/Offer'
                }
            },
            {
                path: 'enquiry',
                name: 'admin-enquiry',
                component: Enquiry,
                meta: {
                    title: 'Enquiry'
                }
            },
            {
                path: 'voucher',
                name: 'admin-voucher',
                component: Voucher,
                meta: {
                    title: 'Voucher'
                }
            },
            {
                path: 'reports/charts',
                name: 'admin-reports-charts',
                component: Charts,
                meta: {
                    title: 'Charts'
                }
            },
            {
                path: 'report/sales',
                name: 'admin-report-sales',
                component: SalesReport,
                meta: {
                    title: 'Sales Report'
                }
            },
            {
                path: 'report/inventory-on-hand',
                name: 'admin-report-inventory-on-hand',
                component: InventoryOnHand,
                meta: {
                    title: 'Inventory On Hand'
                }
            },
            {
                path: 'report/low-stock',
                name: 'admin-report-low-stock',
                component: LowStock,
                meta: {
                    title: 'Low Stock'
                }
            },
            {
                path: 'report/slow-moving-stock',
                name: 'admin-report-slow-moving-stock',
                component: SlowMovingStock,
                meta: {
                    title: 'Slow Moving Stock'
                }
            },
            {
                path: '*',
                component: NotFound,
                meta: {
                    title: 'Page Not Found'
                }
            },
        ]
    },


// website routes
    {
        path: '/auth/:provider/callback',
        component: {
            template: '<div class="auth-component"></div>'
        }
    },
    {
        path: '/',
        name: 'Layout',
        redirect: {name: 'home'},
        component: Layout,
        children: [
            {
                path: 'Home',
                name: 'home',
                component: Home,
                meta: {
                    title: 'Home'
                }
            },
            {
                path: '/login',
                name: 'login',
                component: Login,
                meta: {
                    title: 'Login'
                }
            },
            {
                path: '/confirm/:token',
                name: 'confirm',
                component: ConfirmAccount,
                meta: {
                    noLogin: true,
                    title: 'Confirmation'
                },
            },
            {
                path: '/password-reset-form',
                name: 'password-reset-form',
                component: PasswordResetForm,
                meta: {
                    noLogin: true,
                    title: 'Password-Reset'
                },
            },
            {
                path: '/password-reset/:token',
                name: 'password-reset',
                component: ResetPassword,
                meta: {
                    noLogin: true,
                    title: 'Password'
                },
            },
            {
                path: 'cart',
                name: 'cart',
                component: Cart,
                meta: {
                    title: 'Cart'
                }
            },
            {
                path: 'wish-list',
                name: 'Wishlist',
                component: Wishlist,
                meta: {
                    title: 'WishList'
                }
            },
            {
                path: 'checkout',
                name: 'checkout',
                component: Checkout,
                meta: {
                    // requiresAuth: true,
                    title: 'Checkout'
                },
            },
            {
                path: 'brand-directory',
                name: 'brand-directory',
                component: BrandDirectory,
                meta: {
                    title: 'Brands'
                }
            },
            // user dashboard routes
            {
                path: 'dashboard',
                name: 'dashboard',
                redirect: {name: 'userProfile'},
                component: UserDashboard,
                meta: {
                    requiresAuth: true
                },
                children: [
                    {
                        path: '/profile',
                        name: 'userProfile',
                        component: UserProfile,
                        meta: {
                            requiresAuth: true,
                            title: 'My Profile'
                        }
                    },
                    {
                        path: '/order',
                        name: 'userOrder',
                        component: UserOrder,
                        meta: {
                            requiresAuth: true,
                            title: 'My Order'
                        }
                    },
                    {
                        path: '/voucher',
                        name: 'uservoucher',
                        component: UserVoucher,
                        meta: {
                            requiresAuth: true,
                            title: 'Voucher'
                        }
                    },
                    {
                        path: '/orderDetails/:id',
                        name: 'orderDetails',
                        component: UserOrderDetails,
                        meta: {
                            requiresAuth: true,
                            title: 'Order Details'
                        }
                    },
                ]
            },
            {
                path: 'about',
                name: 'AboutUs',
                component: AboutUs,
                meta: {
                    title: 'About Us'
                }
            },
            {
                path: 'size-guide',
                name: 'SizeGuide',
                component: SizeGuide,
                props:true,
                meta: {
                    title: 'SizeGuide'
                }
            },
            {
                path: 'privacy',
                name: 'privacy',
                component: Privacy,
                props:true,
                meta: {
                    title: 'Privacy'
                }
            },
            {
                path: 'delivery',
                name: 'Delivery',
                component: Delivery,
                meta: {
                    title: 'Delivery'
                }
            },
            {
                path: 'careers',
                name: 'Careers',
                component: Careers,
                meta: {
                    title: 'Careers'
                }
            },
            {
                path: 'contact',
                name: 'Contact',
                component: Contact,
                meta: {
                    title: 'Contact'
                }
            },
            {
                path: 'returns',
                name: 'Returns',
                component: Returns,
                meta: {
                    title: 'Returns'
                }
            },
            {
                path: ':slug-:id',
                name: 'singleProduct',
                component: SingleProduct,
                meta: {
                    title: 'Product'
                }
            },
            {
                path: '*!!',
                component: NotFound,
                meta: {
                    title: 'Page Not Found'
                }
            },
            {
                path: ':gender/:category?/:subCategory?',
                name: 'filter',
                component: Filter,
                props:true,
                meta: {
                    title: 'Shop'
                }
            },

        ]
    },
    {
        path: '*',
        component: NotFound,
        meta: {
            title: 'Page Not Found'
        }
    },
];


