<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Color extends Model
{
    protected $table = "colors";
    protected $fillable = ['name', 'value'];
    protected $hidden = array('created_at', 'updated_at');


}
