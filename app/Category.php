<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = "categories";

    protected $fillable = ['label', 'parent_id'];
    protected $hidden = array('created_at', 'updated_at');

    public function product(){
        return $this->hasMany(Product::class);
    }

    //each category have one parent
    public function grandparent() { // used in navigation
        return $this->belongsTo(static::class, 'parent_id');
    }
    /* if the product is blue shirt of Shirts > Clothing > Men then calling parent function we get,
            category: Shirt
               parent: Clothing
                grandparent: Men
    */
    public function parent(){ // used in single product breadcrumb
        return $this->grandparent()->with('grandparent');
    }


    // grandchildren with products
    public function grandchildren() {
        return $this->hasMany(static::class, 'parent_id')->with('product');
    }
    /* calling children function we get,
           category: Men
              children: Clothing
               grandchildren: Shirts
                 product: blue shirt, full shirt, ......
   */
    public function children() {
        return $this->grandchildren()->with('grandchildren');
    }


    // grandchildren without products
    public function grandchilds() { // used in showing add category option
        return $this->hasMany(static::class, 'parent_id');
    }
    /* calling childs function we get,
               category: Men
                  children: Clothing
                   grandchildren: Shirts
       */
    public function childs() { // used in display all category
        return $this->grandchilds()->with('grandchilds');
    }

//    //add category, show category and add product
//     public function parents()
//     {
//         return $this->belongsTo(static::class, 'parent_id');
//     }
//
//
////     public function childrens() {
////         return $this->hasMany(self::class, 'parent_id');
////     }
//
//
//     public function grandChildrens()
//     {
//         return $this->childrens()->with('grandChildrens');
//
//     }


     //homepage
    public function products()
    {
        return $this->hasMany('App\Product')->latest()->limit('8')->with('brand')->with('sales');
    }

    public function grandChild(){
        return $this->child()->with('products');
    }
    /* calling child function we get,
               category: Men
                  children: Clothing
                   grandchildren: Shirts
                     product: blue shirt, full shirt, upto 8 data at most
       */

    //for homepage product acc to parent
    public function child(){ // used in navigation
        return $this->hasMany(self::class, 'parent_id')->with('grandChild');
    }


}
