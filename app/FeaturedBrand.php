<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FeaturedBrand extends Model
{
    protected $table = "featured_brands";

    protected $fillable = ['brand_id','image'];


}
