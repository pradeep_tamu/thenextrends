<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    protected $table = "brands";
    protected $fillable = ['name'];
    protected $hidden = array('created_at', 'updated_at');

    public function product(){
        return $this->hasMany(Product::class);
    }
}
