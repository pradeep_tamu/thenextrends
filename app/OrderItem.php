<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderItem extends Model
{
    protected $table = "orderitems";
    protected $fillable = ['order_id', 'product_id', 'quantity', 'color', 'size', 'price', 'image', 'product_sku', 'colorsize_id'];

    public function stores()
    {
        return $this->hasManyThrough('App\Store',
            'App\Product',
            'id',
            'id',
            'product_id',
            'store_id')
            ->select( 'owner_name','store_name','email','address','phone');
    }

    public function products(){
        return $this->belongsTo('App\Product', 'product_id')
            ->select('id', 'name', 'price', 'slug', 'thumbnail');
    }
    public function orders(){
//<<<<<<< HEAD
        return $this->belongsTo('App\Order','order_id')->select('id','name','address','mobile','status','created_at','voucher_amount','guestemail','shipping','total');
//=======
//        return $this->belongsTo('App\Order','order_id')->select('id','name','address','mobile','status','shipping','created_at','voucher_amount');
//>>>>>>> d4b7414eaacc6105d75ad76f38282d84283805a5
    }

    public function users()
    {
        return $this->hasManyThrough('App\User',
            'App\Order',
            'id',
            'id',
            'order_id',
            'customer_id')
            ->select( 'users.name','email');
    }


}
