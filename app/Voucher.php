<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Voucher extends Model
{
    protected $table = "vouchers";
    protected $fillable = ['voucher', 'amount'];
    protected $hidden = array('created_at', 'updated_at');
}
