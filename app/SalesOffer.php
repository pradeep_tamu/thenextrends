<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SalesOffer extends Model
{
    protected $table = 'sales_offers';

    protected $fillable = [
        'product_id',
        'rate'
    ];

    protected $hidden = [
        'created_at',
        'updated_at'
    ];

    public function product(){
        return $this->belongsTo(Product::class);
    }

    public function store()
    {
        return $this->hasOneThrough('App\Store',
            'App\Product',
            'id',
            'id',
            'product_id',
            'store_id')
            ->select('store_name');
    }
}
