<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ColorSize extends Model
{
    protected $table = "colorsizes";
    protected $fillable = ['product_id', 'color_id', 'size_id', 'quantity', 'sku', 'minimum_stock'];
    protected $hidden = array('created_at', 'updated_at');

}
