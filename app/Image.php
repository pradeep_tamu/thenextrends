<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    protected $table = "images";
    protected $fillable = ['images', 'product_id','color_id'];
    protected $hidden = array('created_at', 'updated_at');

}
