<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BrandGender extends Model
{
    protected $table = "brand_genders";

    protected $fillable = ['brand_id', 'gender'];

}
