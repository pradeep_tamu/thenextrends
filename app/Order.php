<?php

namespace App;

use App\Notifications\OrderNotification;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Notification;

class Order extends Model
{
    protected $table = "orders";
    protected $fillable = ['customer_id', 'name', 'address', 'mobile', 'total', 'region', 'shipping','voucher_amount', 'status','guestemail'];

    public function orderItems()
    {
        return $this->hasMany('App\OrderItem');
    }

//    public static function boot()
//    {
//        parent::boot(); // TODO: Change the autogenerated stub
//
//        static::created(function ($model){
//            $admins = User::all()->where('type','admin');
//
//            Notification::send($admins, new OrderNotification($model));
//        });
//    }
}
