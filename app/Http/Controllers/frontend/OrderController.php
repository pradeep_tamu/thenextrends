<?php

namespace App\Http\Controllers\frontend;

use App\ColorSize;
use App\Notifications\OrderNotification;
use App\Order;
use App\OrderItem;
use App\Store;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Notification;

class OrderController extends Controller
{

    public function checkItem(Request $request){
        foreach ($request->get('product_detail') as $detail) {
            $id = $detail['colorsize_id'];
            $item = ColorSize::where('id', $id)->select('id', 'quantity')->first();
            if ($item->quantity <= 0) {
                return response()->json($item, 400);
            }
        }
    }

    //when order is placed
    public function store(Request $request)
    {
        foreach ($request->get('product_detail') as $detail) {
            $id = $detail['colorsize_id'];
            $item = ColorSize::where('id', $id)->select('id', 'quantity')->first();
            if ($item->quantity <= 0) {
                return response()->json($item, 400);
            }
        }

        $request->validate([
            'name' => 'required|string',
            'region' => 'required|string',
            'address' => 'required|string',
            'mobile' => 'required|string',
        ]);

        $order_detail = new Order([
            'customer_id' => $request['customer_id'],
            'name' => $request['name'],
            'region' => $request['region'],
            'address' => $request['address'],
            'mobile' => $request['mobile'],
            'shipping' => $request['shipping'],
            'total' => $request['total'],
            'voucher_amount' => $request['voucher_amount'],
            'guestemail' => $request['guestemail'],
        ]);
        $order_detail->save();

        foreach ($request->get('product_detail') as $key => $detail) {
            $product_detail = new OrderItem([
                'order_id' => $order_detail->id,
                'product_id' => $detail['productId'],
                //                'product_sku' => $detail['productSKU'],
                'colorsize_id' => $detail['colorsize_id'],
                'color' => $detail['color'],
                'size' => $detail['size'],
                'quantity' => $detail['quantity'],
                'image' => $detail['image'],
            ]);
            $product_detail->save();

            $qty = ColorSize::findOrFail($detail['colorsize_id']);

            $inital_quantity = ColorSize::where('id', $detail['colorsize_id'])->pluck('quantity');

            $qty->quantity = $inital_quantity[0] - $detail['quantity'];
            $qty->save();
        }

        $admins = User::all()->where('type', 'admin');

        if ($request['guestemail']) {
            $details = OrderItem::with('products')
                ->with('stores')
                ->with('orders')
                ->select('order_id', 'product_id', 'quantity', 'color', 'size', 'image')
                ->where('order_id', $order_detail->id)
                ->get();
        } else {
            $details = OrderItem::with('users')
                ->with('products')
                ->with('stores')
                ->with('orders')
                ->select('order_id', 'product_id', 'quantity', 'color', 'size', 'image')
                ->where('order_id', $order_detail->id)
                ->get();
        }

        foreach ($details as $detail) {
            Notification::send($admins, new OrderNotification($detail));
        }
    }

    public function myOrders($id)
    {
        $orders = Order::where('customer_id', '=', $id)->get();

        return response()->json($orders);
    }

    //backend order list
    public function getOrderList()
    {
        $order_details = Order::select('id', 'name', 'address', 'mobile', 'status', 'created_at', 'total')->latest()->get();
        return response()->json($order_details);
    }

    //backend order details
    public function getOrderDetail($id)
    {
        $order = Order::findOrFail($id);

        $user = User::where('id', $order->customer_id)->first();

        $orderDetails = OrderItem::with('products')
            ->with('stores')
            ->select('order_id', 'product_id', 'quantity', 'color', 'size', 'image')
            ->where('order_id', $id)
            ->get();

        return response()->json([
            'order' => $order,
            'user' => $user,
            'orderDetails' => $orderDetails
        ]);
    }

    //change status
    public function changeStatus($id, $value)
    {
        $order = Order::findOrFail($id);
        $order->status = $value;
        $order->save();
        return response()->json(['Success']);
    }

    //backend order notification
    public function orderView($id)
    {
        DB::table('notifications')->where('data', $id)->update(['read_at' => now()]);
    }
}