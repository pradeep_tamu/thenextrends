<?php

namespace App\Http\Controllers\Frontend;

use App\BrandGender;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class BrandController extends Controller
{
    public function brandDirectory($gender, $category = null)
    {

        if(is_null($category)){
            $brands = DB::table('brands')
                ->join('brand_genders', 'brand_genders.brand_id', '=', 'brands.id')
                ->select('brands.id as id', 'brands.name as name')
                ->where('brand_genders.gender', $gender)
                ->distinct()
                ->get();
        } else{
            $brands = DB::table('brands')
                ->join('brand_genders', 'brand_genders.brand_id', '=', 'brands.id')
                ->join('brand_types', 'brand_types.brand_id', '=', 'brands.id')
                ->select('brands.id as id', 'brands.name as name', 'brand_types.type as type')
                ->where('brand_genders.gender', $gender)
                ->where('brand_types.type', $category)
                ->distinct()
                ->get();
        }

        return response()->json($brands);
    }

    public function featuredBrand($gender)
    {
        $genderInfo = BrandGender::where('gender', $gender)->select('brand_id')->get();

        $brandLists = [];
        foreach ($genderInfo as $info) {
            $lists = DB::table('featured_brands')
                ->join('brands', 'brands.id', '=', 'featured_brands.brand_id')
                ->where('brands.id', $info->brand_id)
                ->select( 'featured_brands.image', 'brands.name')
                ->get();
                foreach ($lists as $list) {
                    array_push($brandLists, $list);
                }
            if(count($brandLists) == 8){
                break;
            }
        }

        return response()->json($brandLists);
    }
}
