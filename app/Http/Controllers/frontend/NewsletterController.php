<?php

namespace App\Http\Controllers\frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Spatie\Newsletter\NewsletterFacade as Newsletter;

class NewsletterController extends Controller
{
    public function store(Request $request)
    {

        if (!Newsletter::isSubscribed($request->email)) {
            Newsletter::subscribe($request->email);
            return 'success';
        }
        return 'failure';
    }

    //khalti for cors
    public function payment(Request $request)
    {

        $args = http_build_query(array(
            'token' => $request->token,
            'amount' => $request->amount
        ));

        $url = "https://khalti.com/api/v2/payment/verify/";

        # Make the call using API.
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $args);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        $headers = ['Authorization: Key live_secret_key_6565183437d148f9bc038775904429e2'];
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

// Response
        $response = curl_exec($ch);
        $status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

//        dd($response, $status_code);
//        return ($response, $status_code);

    }

    public function paymentEsewa(Request $request)
    {
        $url = "https://uat.esewa.com.np/epay/transrec";
        $data = [
            'amt' => $request->amt,
            'rid' => $request->refid,
            'pid' => $request->oid,
            'scd' => 'epay_payment'
        ];

        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($curl);
        curl_close($curl);

        return strtoupper(trim(strip_tags($response)));
    }
}

