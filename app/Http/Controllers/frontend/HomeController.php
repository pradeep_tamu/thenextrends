<?php

namespace App\Http\Controllers\frontend;

use App\Banner;
use App\Brand;
use App\Category;
use App\Enquiry;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Notification;

class HomeController extends Controller
{
//    public function getNavigation(){
//
//    }

    public function getCategoryData()
    {

        $required = ['Clothing', 'Shoes', 'Accessories', 'Sports'];

        $data = [];
        foreach ($required as $name) {

            $category = Category::with('grandparent')
                ->with('child')
                ->where('label', '=', $name)
                ->get();

            $brand = DB::table('brands')
                ->join('brand_types', 'brand_types.brand_id', '=', 'brands.id')
                ->select('brands.id as id', 'brands.name as name', 'brand_types.type as type')
                ->where('brand_types.type', $name)
                ->distinct()
                ->get();

            // creating data structure to separate men and women
            foreach ($category as $cat) {
                if (strtolower($cat->grandparent->label) == 'men') {
                    $data[$name]['men'] = $cat->child;
                } else if (strtolower($cat->grandparent->label) == 'women') {
                    $data[$name]['women'] = $cat->child;
                }
            }
            $data[$name]['brand'] = $brand;
        }

        return response()->json($data);
    }

    //frontend banner image
    public function getNavigationBanner($gender, $category)
    {
        $banner = Banner::where('gender', $gender)
            ->where('category', $category)
            ->select('images')
            ->first();
        return response()->json($banner);
    }

    //Enquiry Section Front Page
    public function sendEnquiry(Request $request)
    {
        $request->validate([
            'first_name' => 'required|string',
            'last_name' => 'required|string',
            'email' => 'required|email',
            'message' => 'required|string',
        ]);

        $enquiry = new Enquiry([
            'first_name' => $request->get('first_name'),
            'last_name' => $request->get('last_name'),
            'email' => $request->get('email'),
            'message' => $request->get('message'),
        ]);

        $enquiry->save();

        $name = $enquiry->first_name . ' ' . $enquiry->last_name;
        Mail::send('mails.email',
            array(
                'name' => $name,
                'email' => $request->get('email'),
                'user_message' => $request->get('message')
            ), function ($message) {
                $message->from('info@thenextrends.com');
                $message->to('info@thenextrends.com', 'TheNextTrend')->subject('Enquiry Received');
            });
        return response()->json('success');
    }

    //Enquiry Section backend
    public function getEnquiry()
    {
        $enquiry = Enquiry::all();

        return response()->json($enquiry);
    }

    //Enquiry Section backend
    public function getMessage($id)
    {

        $enquiry = Enquiry::where('id', $id)->select('message')->first();

        return response()->json($enquiry);
    }

}
