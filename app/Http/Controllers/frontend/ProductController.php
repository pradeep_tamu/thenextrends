<?php

namespace App\Http\Controllers\frontend;


use App\Category;
use App\Image;
use App\Product;

//use Illuminate\Database\Eloquent\Collection;
//use Illuminate\Http\Request;
//use App\ColorSize;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class ProductController extends Controller
{

    // for single product page
    public function singleProduct($id)
    {
        // getting product details and its brand, fabric, occasion, store
        $details = Product::with('brand:id,name')
            ->with('store:id,store_name')
            ->with('fabric:id,name')
            ->with('occasion:id,name')
            ->with('sales')
            ->where('id', '=', $id)
            ->first();

        // getting category (entire three level) of current product
        $category = Category::with('parent')
            ->where('id', $details->category_id)
            ->first();

        // getting all the color available for current product
        $colors = DB::table('colorsizes')
            ->leftjoin('colors', 'colors.id', '=', 'colorsizes.color_id')
            ->select('colors.id', 'colors.name', 'colors.value')
            ->where('colorsizes.product_id', $id)
            ->distinct()
            ->get('colorsizes.color_id');

        // getting images according to the color available for current product
        $images = Image::where('product_id', $id)
            ->select('images.images', 'images.color_id')
            ->get();

        // getting all the sizes according to color available for the current product
        $sizes = DB::table('colorsizes')
            ->join('sizes', 'sizes.id', 'colorsizes.size_id')
            ->select('sizes.name',
                'colorsizes.sku as sku',
                'colorsizes.quantity',
                'colorsizes.color_id as id',
                'colorsizes.id as colorsize_id')
            ->where('colorsizes.product_id', '=', $id)
            ->get('colorsizes.size_id');

        /* creating a data structure (associative array) for color, its image, sizes and quantity
         for example :
         color_sizes = [
                        'id' => '1',
                        'name' => 'black',
                        'value' => '#ffffff' ,
                        'images' => 'data,data,data',
                        'sizes' => [
                                    'name' => 'M',
                                    'quantity' => '2'
                                   ]
                      ]
        */

        $color_sizes = [];
        $i = 0;
        foreach ($colors as $color) {
            // first saving color information
            $color_sizes[$i] = [
                "id" => $color->id,
                "name" => $color->name,
                "value" => $color->value
            ];

            // saving images according to color
            foreach ($images as $image) {
                if ($color->id == $image->color_id) {
                    $color_sizes[$i]['images'] = $image->images;
                }
            }

            // saving sizes according to color
            $j = 0;
            foreach ($sizes as $size) {
                if ($color->id == $size->id) {
                    $color_sizes[$i]['sizes'][$j] = [
                        'colorsize_id' => $size->colorsize_id,
                        'name' => $size->name,
                        'quantity' => $size->quantity,
                        'sku' => $size->sku
                    ];
                    $j++;
                }
            }
            $i++;
        }

        $similar_products = Product::with('brand')
            ->where([['category_id', '=', $category->id], ['id', '!=', $id]])
            ->select('id', 'name', 'price', 'thumbnail', 'brand_id', 'slug')
            ->inRandomOrder()
            ->limit(6)
            ->get();

        $parent = $category->parent->label;
        $grandParent = $category->parent->grandparent->label;

        $parent_products = DB::table('categories as t1')
            ->leftjoin('categories as t2', 't2.parent_id', '=', 't1.id')
            ->leftjoin('categories as t3', 't3.parent_id', '=', 't2.id')
            ->join('products', 'products.category_id', '=', 't3.id')
            ->leftjoin('brands', 'brands.id', '=', 'products.brand_id')
            ->select('products.id',
                'products.name',
                'products.price',
                'brands.name as brand',
                'products.thumbnail', 'products.slug',
                'brands.id as brandId')
            ->whereRaw("t2.label='$parent' && t1.label='$grandParent' && t3.label!='$category->label'")
            ->inRandomOrder()
            ->limit(6)
            ->get();

        return response()->json([
            "details" => $details,
            "category" => $category,
            "color_sizes" => $color_sizes,
            "similar_products" => $similar_products,
            "parent_products" => $parent_products,
        ]);
    }

    //trending products for homepage
    public function getTrending()
    {
        $men = Category::with('child')->where('label', 'men')->get();
        $women = Category::with('child')->where('label', 'women')->get();
        return [$men, $women];
    }

    //filter controller section starts here
    public function getFilterProduct($gender, $category, $subCategory, $brand = null)
    {

        if ($subCategory == 'undefined' && $category == 'undefined') {
            $sql = null;

            // occasion
            if (!is_null($brand)) {
                $sql = 'SELECT distinct occassions.name as occasions,occassions.id as id
                        FROM `categories` as t1
                        left join categories as t2 on t2.parent_id=t1.id
                        left join categories as t3 on t3.parent_id=t2.id
                        inner join products on products.category_id=t3.id
                        inner join occassions on occassions.id=products.occasion_id
                        inner join brands on brands.id=products.brand_id
                        where brands.name="' . $brand . '"';
            } else {
                $sql = 'SELECT distinct occassions.name as occasions,occassions.id as id
                        FROM `categories` as t1
                        left join categories as t2 on t2.parent_id=t1.id
                        left join categories as t3 on t3.parent_id=t2.id
                        inner join products on products.category_id=t3.id
                        inner join occassions on occassions.id=products.occasion_id';
            }
            if ($gender != 'All') {
                $sql = $sql . ' where t1.label="' . $gender . '"';
            }
            $all = DB::select($sql);
            $occasions = $all;
            // end of occasion

            // fabrics
            $sql = 'SELECT distinct fabrics.name as fabrics,fabrics.id as id
                    FROM `categories` as t1
                    left join categories as t2 on t2.parent_id=t1.id
                    left join categories as t3 on t3.parent_id=t2.id
                    inner join products on products.category_id=t3.id
                    inner join fabrics on fabrics.id=products.fabric_id';
            if (!is_null($brand)) {
                $sql = $sql .
                    ' inner join brands on brands.id=products.brand_id where brands.name="' . $brand . '"';
            }
            if ($gender != 'All') {
                $sql = $sql . ' where t1.label="' . $gender . '"';
            }
            $all = DB::select($sql);
            $fabrics = $all;
            // end of fabrics

            // brands
            $sql = 'SELECT distinct brands.name as brands, brands.id as id
                    FROM `categories` as t1
                    left join categories as t2 on t2.parent_id=t1.id
                    left join categories as t3 on t3.parent_id=t2.id
                    inner join products on products.category_id=t3.id
                    inner join brands on brands.id=products.brand_id';
            if ($gender != 'All') {
                $sql = $sql . ' where t1.label="' . $gender . '"';
            }
            $all = DB::select($sql);
            $brands = $all;
            // end of brands

            $price = 450;//test

            // colors
            $sql = 'SELECT distinct colors.name as colors,colors.id as id
                    FROM `categories` as t1
                    left join categories as t2 on t2.parent_id=t1.id
                    left join categories as t3 on t3.parent_id=t2.id
                    inner join products on products.category_id=t3.id
                    inner join colorsizes on colorsizes.product_id=products.id
                    inner join colors on colors.id=colorsizes.color_id';
            if (!is_null($brand)) {
                $sql = $sql .
                    ' inner join brands on brands.id=products.brand_id where brands.name="' . $brand . '"';
            }
            if ($gender != 'All') {
                $sql = $sql . ' where t1.label="' . $gender . '"';
            }
            $all = DB::select($sql);
            $colors = $all;
            // end of colors

            // sizes
            $sql = 'SELECT distinct sizes.name as sizes,sizes.id as id 
                    FROM `categories` as t1
                    left join categories as t2 on t2.parent_id=t1.id
                    left join categories as t3 on t3.parent_id=t2.id
                    inner join products on products.category_id=t3.id
                    inner join colorsizes on colorsizes.product_id=products.id
                    inner join sizes on sizes.id=colorsizes.size_id';
            if (!is_null($brand)) {
                $sql = $sql .
                    ' inner join brands on brands.id=products.brand_id where brands.name="' . $brand . '"';
            }
            if ($gender != 'All') {
                $sql = $sql . ' where t1.label="' . $gender . '"';
            }
            $sql = $sql . ' order by sizes.id';
            $all = DB::select($sql);
            $sizes = $all;
            // end of sizes

            // category
            if (!is_null($brand)) {
                $sql = 'SELECT distinct brands.name as home 
                        FROM `categories` as t1
                        left join categories as t2 on t2.parent_id=t1.id
                        left join categories as t3 on t3.parent_id=t2.id
                        inner join products on products.category_id=t3.id
                        inner join brands on brands.id=products.brand_id where brands.name="' . $brand . '"';
            } else {
                $sql = 'SELECT distinct t2.label as home 
                        FROM `categories` as t1
                        left join categories as t2 on t2.parent_id=t1.id
                        left join categories as t3 on t3.parent_id=t2.id
                        inner join products on products.category_id=t3.id';
            }
            if ($gender != 'All') {
                $sql = $sql . ' where t1.label="' . $gender . '"';
            }
            $all = DB::select($sql);
            $category = $all;
            // end of category

            // sub-category
            $subcategory = null;
            if (!is_null($brand)) {
                $sql = 'SELECT distinct t3.label as subcategory
                        FROM `categories` as t1
                        left join categories as t2 on t2.parent_id=t1.id
                        left join categories as t3 on t3.parent_id=t2.id
                        inner join products on products.category_id=t3.id
                        inner join brands on brands.id=products.brand_id
                        where brands.name="' . $brand . '"';
                if ($gender != 'All') {
                    $sql = $sql . ' where t1.label="' . $gender . '"';
                }
                $all = DB::select($sql);
                $subcategory = $all;
            }
            // end of sub-category
        } else if ($category == $subCategory || $subCategory == "All") { //category == label
            $sql = null;

            // occasion
            if (!is_null($brand)) {
                $sql = 'SELECT distinct occassions.name as occasions,occassions.id as id
                        FROM `categories` as t1
                        left join categories as t2 on t2.parent_id=t1.id
                        left join categories as t3 on t3.parent_id=t2.id
                        inner join products on products.category_id=t3.id
                        inner join occassions on occassions.id=products.occasion_id
                        inner join brands on brands.id=products.brand_id
                        where t2.label="' . $category . '"&& brands.name="' . $brand . '"';
            } else {
                $sql = 'SELECT distinct occassions.name as occasions,occassions.id as id
                        FROM `categories` as t1
                        left join categories as t2 on t2.parent_id=t1.id
                        left join categories as t3 on t3.parent_id=t2.id
                        inner join products on products.category_id=t3.id
                        inner join occassions on occassions.id=products.occasion_id
                        where t2.label="' . $category . '"';
            }
            if ($gender != 'All') {
                $sql = $sql . ' && t1.label="' . $gender . '"';
            }
            $all = DB::select($sql);
            $occasions = $all;
            // end of occasion

            // fabrics
            $sql = 'SELECT distinct fabrics.name as fabrics,fabrics.id as id
                    FROM `categories` as t1
                    left join categories as t2 on t2.parent_id=t1.id
                    left join categories as t3 on t3.parent_id=t2.id
                    inner join products on products.category_id=t3.id
                    inner join fabrics on fabrics.id=products.fabric_id';
            if (!is_null($brand)) {
                $sql = $sql .
                    ' inner join brands on brands.id=products.brand_id where t2.label="' . $category . '"&& brands.name="' . $brand . '"';
            } else {
                $sql = $sql . ' where t2.label="' . $category . '"';
            }
            if ($gender != 'All') {
                $sql = $sql . ' && t1.label="' . $gender . '"';
            }
            $all = DB::select($sql);
            $fabrics = $all;
            // end of fabrics

            // brands
            $sql = 'SELECT distinct brands.name as brands,brands.id as id
                    FROM `categories` as t1
                    left join categories as t2 on t2.parent_id=t1.id
                    left join categories as t3 on t3.parent_id=t2.id
                    inner join products on products.category_id=t3.id
                    inner join brands on brands.id=products.brand_id';
            if (!is_null($brand)) {
                $sql = $sql .
                    ' where t2.label="' . $category . '"&& brands.name="' . $brand . '"';
            } else {
                $sql = $sql . ' where t2.label="' . $category . '"';
            }
            if ($gender != 'All') {
                $sql = $sql . ' && t1.label="' . $gender . '"';
            }
            $all = DB::select($sql);
            $brands = $all;
            // end of brands

            $price = 450;//test

            // colors
            $sql = 'SELECT distinct colors.name as colors,colors.id as id
                    FROM `categories` as t1
                    left join categories as t2 on t2.parent_id=t1.id
                    left join categories as t3 on t3.parent_id=t2.id
                    inner join products on products.category_id=t3.id
                    inner join colorsizes on colorsizes.product_id=products.id
                    inner join colors on colors.id=colorsizes.color_id';
            if (!is_null($brand)) {
                $sql = $sql .
                    ' inner join brands on brands.id=products.brand_id where t2.label="' . $category . '"&& brands.name="' . $brand . '"';
            } else {
                $sql = $sql . ' where t2.label="' . $category . '"';
            }
            if ($gender != 'All') {
                $sql = $sql . ' && t1.label="' . $gender . '"';
            }
            $all = DB::select($sql);
            $colors = $all;
            // end of colors

            // sizes
            $sql = 'SELECT distinct sizes.name as sizes,sizes.id as id 
                    FROM `categories` as t1
                    left join categories as t2 on t2.parent_id=t1.id
                    left join categories as t3 on t3.parent_id=t2.id
                    inner join products on products.category_id=t3.id
                    inner join colorsizes on colorsizes.product_id=products.id
                    inner join sizes on sizes.id=colorsizes.size_id';
            if (!is_null($brand)) {
                $sql = $sql .
                    ' inner join brands on brands.id=products.brand_id where t2.label="' . $category . '"&& brands.name="' . $brand . '"';
            } else {
                $sql = $sql . ' where  t2.label="' . $category . '"';
            }
            if ($gender != 'All') {
                $sql = $sql . ' && t1.label="' . $gender . '"';
            }
            $sql = $sql . ' order by sizes.id';
            $all = DB::select($sql);
            $sizes = $all;
            // end of sizes

            // category
            if (!is_null($brand)) {
                $sql = 'SELECT distinct brands.name as home 
                        FROM `categories` as t1
                        left join categories as t2 on t2.parent_id=t1.id
                        left join categories as t3 on t3.parent_id=t2.id
                        inner join products on products.category_id=t3.id
                        inner join brands on brands.id=products.brand_id where t2.label="' . $category . '"&& brands.name="' . $brand . '"';
            } else {
                $sql = 'SELECT distinct t2.label as home 
                        FROM `categories` as t1
                        left join categories as t2 on t2.parent_id=t1.id
                        left join categories as t3 on t3.parent_id=t2.id
                        inner join products on products.category_id=t3.id
                        where t2.label="' . $category . '"';
            }
            if ($gender != 'All') {
                $sql = $sql . ' && t1.label="' . $gender . '"';
            }
            $all = DB::select($sql);
            $category = $all;
            // end of category

            // sub-category
            $subcategory = null;
            if (!is_null($brand)) {
                $sql = 'SELECT distinct t3.label as subcategory
                        FROM `categories` as t1
                        left join categories as t2 on t2.parent_id=t1.id
                        left join categories as t3 on t3.parent_id=t2.id
                        inner join products on products.category_id=t3.id
                        inner join brands on brands.id=products.brand_id
                        where brands.name="' . $brand . '"';
                if ($gender != 'All') {
                    $sql = $sql . ' && t1.label="' . $gender . '"';
                }
                $all = DB::select($sql);
                $subcategory = $all;
            }
            // end of sub-category
        } else {  // if category != label
            $sql = null;

            // occasion
            if (!is_null($brand)) {
                $sql = 'SELECT distinct occassions.name as occasions,occassions.id as id
                        FROM `categories` as t1
                        left join categories as t2 on t2.parent_id=t1.id
                        left join categories as t3 on t3.parent_id=t2.id
                        inner join products on products.category_id=t3.id
                        inner join occassions on occassions.id=products.occasion_id
                        inner join brands on brands.id=products.brand_id
                        where t2.label="' . $category . '"&& brands.name="' . $brand . '"';
            } else {

                $sql = 'SELECT distinct occassions.name as occasions,occassions.id as id
                               FROM `categories` as t1
            left join categories as t2 on t2.parent_id=t1.id
            left join categories as t3 on t3.parent_id=t2.id
            inner join products on products.category_id=t3.id
            inner join occassions on occassions.id=products.occasion_id
            where t2.label="' . $category . '" && t3.label="' . $subCategory . '"';
            }
            if ($gender != 'All') {
                $sql = $sql . ' && t1.label="' . $gender . '"';
            }
            $all = DB::select($sql);
            $occasions = $all;
            // end of occasion

            // fabrics
            $sql = 'SELECT distinct fabrics.name as fabrics,fabrics.id as id
                    FROM `categories` as t1
                    left join categories as t2 on t2.parent_id=t1.id
                    left join categories as t3 on t3.parent_id=t2.id
                    inner join products on products.category_id=t3.id
                    inner join fabrics on fabrics.id=products.fabric_id';
            if (!is_null($brand)) {
                $sql = $sql .
                    ' inner join brands on brands.id=products.brand_id where t2.label="' . $category . '"&& brands.name="' . $brand . '"';
            } else {
                $sql = $sql . ' where t2.label="' . $category . '" && t3.label="' . $subCategory . '"';
            }
            if ($gender != 'All') {
                $sql = $sql . ' && t1.label="' . $gender . '"';
            }
            $all = DB::select($sql);
            $fabrics = $all;
            // end of fabrics

            // brands
            $sql = 'SELECT distinct brands.name as brands,brands.id as id
                    FROM `categories` as t1
                    left join categories as t2 on t2.parent_id=t1.id
                    left join categories as t3 on t3.parent_id=t2.id
                    inner join products on products.category_id=t3.id
                    inner join brands on brands.id=products.brand_id';
            if (!is_null($brand)) {
                $sql = $sql .
                    'where t2.label="' . $category . '"&& brands.name="' . $brand . '"';
            } else {
                $sql = $sql . ' where t2.label="' . $category . '" && t3.label="' . $subCategory . '"';
            }
            if ($gender != 'All') {
                $sql = $sql . ' && t1.label="' . $gender . '"';
            }
            $all = DB::select($sql);
            $brands = $all;
            // end of brands

            $price = 450;//test

            // colors
            $sql = 'SELECT distinct colors.name as colors,colors.id as id
                    FROM `categories` as t1
                    left join categories as t2 on t2.parent_id=t1.id
                    left join categories as t3 on t3.parent_id=t2.id
                    inner join products on products.category_id=t3.id
                    inner join colorsizes on colorsizes.product_id=products.id
                    inner join colors on colors.id=colorsizes.color_id';
            if (!is_null($brand)) {
                $sql = $sql .
                    ' inner join brands on brands.id=products.brand_id where t2.label="' . $category . '"&& brands.name="' . $brand . '"';
            } else {
                $sql = $sql . ' where t2.label="' . $category . '" && t3.label="' . $subCategory . '"';
            }
            if ($gender != 'All') {
                $sql = $sql . ' && t1.label="' . $gender . '"';
            }
            $all = DB::select($sql);
            $colors = $all;
            // end of colors

            // sizes
            $sql = 'SELECT distinct sizes.name as sizes,sizes.id as id 
                    FROM `categories` as t1
                    left join categories as t2 on t2.parent_id=t1.id
                    left join categories as t3 on t3.parent_id=t2.id
                    inner join products on products.category_id=t3.id
                    inner join colorsizes on colorsizes.product_id=products.id
                    inner join sizes on sizes.id=colorsizes.size_id';
            if (!is_null($brand)) {
                $sql = $sql .
                    ' inner join brands on brands.id=products.brand_id where t2.label="' . $category . '"&& brands.name="' . $brand . '"';
            } else {
                $sql = $sql . ' where t2.label="' . $category . '" && t3.label="' . $subCategory . '"';
            }
            if ($gender != 'All') {
                $sql = $sql . ' && t1.label="' . $gender . '"';
            }
            $sql = $sql . ' order by sizes.id';
            $all = DB::select($sql);
            $sizes = $all;
            // end of sizes

            // category
            if (!is_null($brand)) {
                $sql = 'SELECT distinct brands.name as home 
                        FROM `categories` as t1
                        left join categories as t2 on t2.parent_id=t1.id
                        left join categories as t3 on t3.parent_id=t2.id
                        inner join products on products.category_id=t3.id
                        inner join brands on brands.id=products.brand_id where t2.label="' . $category . '"&& brands.name="' . $brand . '"';
            } else {
                $sql = 'SELECT distinct t2.label as home 
                        FROM `categories` as t1
                        left join categories as t2 on t2.parent_id=t1.id
                        left join categories as t3 on t3.parent_id=t2.id
                        inner join products on products.category_id=t3.id
                        where t2.label="' . $category . '"';
            }
            if ($gender != 'All') {
                $sql = $sql . ' && t1.label="' . $gender . '"';
            }
            $all = DB::select($sql);
            $category = $all;
            // end of category

            // sub-category
            $subcategory = null;
            if (!is_null($brand)) {
                $sql = 'SELECT distinct t3.label as subcategory
                        FROM `categories` as t1
                        left join categories as t2 on t2.parent_id=t1.id
                        left join categories as t3 on t3.parent_id=t2.id
                        inner join products on products.category_id=t3.id
                        inner join brands on brands.id=products.brand_id
                        where brands.name="' . $brand . '"';
                if ($gender != 'All') {
                    $sql = $sql . ' && t1.label="' . $gender . '"';
                }
                $all = DB::select($sql);
                $subcategory = $all;
            }
            // end of sub-category
        }

        return response()->json([$sizes, $colors, $occasions, $fabrics, $price, $brands, $category, $subcategory]);

    }

    //all products list for filter page
    public function getProduct($gender, $category = null, $subCategory = null, $sales = null, $search = null, $brand = null)
    {
        //brand condition
        $product = null;

        if($sales == 'undefined'){
            $sales = null;
        }
        if($brand == 'undefined'){
            $brand = null;
        }
        if($search == 'undefined'){
            $search = null;
        }

        // if only gender
        if (($subCategory == 'undefined' && $category == 'undefined') || (is_null($subCategory) && is_null($category))) {
            if (is_null($brand)) {
                $product = DB::table('categories as t1')
                    ->leftjoin('categories as t2', 't2.parent_id', '=', 't1.id')
                    ->leftjoin('categories as t3', 't3.parent_id', '=', 't2.id')
                    ->join('products', 'products.category_id', '=', 't3.id')
                    ->leftjoin('brands', 'brands.id', '=', 'products.brand_id')
                    ->leftJoin('sales_offers', 'sales_offers.product_id', '=', 'products.id')// added salesoffer
                    ->select('t3.label', 'products.id', 'products.name', 'products.price', 'brands.name as brand', 'products.thumbnail', 'products.slug', 'brands.id as brandId', 'sales_offers.rate as rate');
            } else {
                $product = DB::table('categories as t1')
                    ->leftjoin('categories as t2', 't2.parent_id', '=', 't1.id')
                    ->leftjoin('categories as t3', 't3.parent_id', '=', 't2.id')
                    ->join('products', 'products.category_id', '=', 't3.id')
                    ->leftjoin('brands', 'brands.id', '=', 'products.brand_id')
                    ->leftjoin('sales_offers', 'sales_offers.product_id', '=', 'products.id')// added salesoffer
                    ->select('t3.label', 'products.id', 'products.name', 'products.price', 'brands.name as brand', 'products.thumbnail', 'products.slug', 'brands.id as brandId', 'sales_offers.rate as rate')
                    ->whereRaw("brands.name='$brand'");
            }
        } // if gender and main-category ( Clothing / Sports / Shoes / ..... ) is requested
        else if ($category == $subCategory || $subCategory == "All") {
            if (is_null($brand)) {
                $product = DB::table('categories as t1')
                    ->leftjoin('categories as t2', 't2.parent_id', '=', 't1.id')
                    ->leftjoin('categories as t3', 't3.parent_id', '=', 't2.id')
                    ->join('products', 'products.category_id', '=', 't3.id')
                    ->leftjoin('brands', 'brands.id', '=', 'products.brand_id')
                    ->leftJoin('sales_offers', 'sales_offers.product_id', '=', 'products.id')// added salesoffer
                    ->select('t3.label', 'products.id', 'products.name', 'products.price', 'brands.name as brand', 'products.thumbnail', 'products.slug', 'brands.id as brandId', 'sales_offers.rate as rate')
                    ->whereRaw("t2.label='$category' ");
            } else {
                $product = DB::table('categories as t1')
                    ->leftjoin('categories as t2', 't2.parent_id', '=', 't1.id')
                    ->leftjoin('categories as t3', 't3.parent_id', '=', 't2.id')
                    ->join('products', 'products.category_id', '=', 't3.id')
                    ->leftjoin('brands', 'brands.id', '=', 'products.brand_id')
                    ->leftjoin('sales_offers', 'sales_offers.product_id', '=', 'products.id')// added salesoffer
                    ->select('t3.label', 'products.id', 'products.name', 'products.price', 'brands.name as brand', 'products.thumbnail', 'products.slug', 'brands.id as brandId', 'sales_offers.rate as rate')
                    ->whereRaw("t2.label='$category' && brands.name='$brand'");
            }
        } // if gender, main-category and sub-category ( Shirts / Pants / ..... ) is requested
        else {
            if (is_null($brand)) {
                $product = DB::table('categories as t1')
                    ->leftjoin('categories as t2', 't2.parent_id', '=', 't1.id')
                    ->leftjoin('categories as t3', 't3.parent_id', '=', 't2.id')
                    ->join('products', 'products.category_id', '=', 't3.id')
                    ->leftjoin('brands', 'brands.id', '=', 'products.brand_id')
                    ->leftJoin('sales_offers', 'sales_offers.product_id', '=', 'products.id')// added salesoffer
                    ->select('t3.label', 'products.id', 'products.name', 'products.price', 'brands.name as brand', 'products.thumbnail', 'products.slug', 'brands.id as brandId', 'sales_offers.rate as rate')
                    ->whereRaw("t2.label='$category' && t3.label='$subCategory'");
            } else {
                $product = DB::table('categories as t1')
                    ->leftjoin('categories as t2', 't2.parent_id', '=', 't1.id')
                    ->leftjoin('categories as t3', 't3.parent_id', '=', 't2.id')
                    ->join('products', 'products.category_id', '=', 't3.id')
                    ->leftjoin('brands', 'brands.id', '=', 'products.brand_id')
                    ->leftjoin('sales_offers', 'sales_offers.product_id', '=', 'products.id')// added salesoffer
                    ->select('t3.label', 'products.id', 'products.name', 'products.price', 'brands.name as brand', 'products.thumbnail', 'products.slug', 'brands.id as brandId', 'sales_offers.rate as rate')
                    ->whereRaw("t2.label='$category' && brands.name='$brand'");
            }
        }

        if ($gender != 'All') {
            $product = $product->whereRaw("t1.label='$gender'");
        }

        $product = $product->inRandomOrder();

        if ($sales != 'undefined' && $sales != null) {
            if ($sales == 'yes') {
                $product->where('rate', '!=', null);
            } else {
                $product->where('rate', '<', $sales / 100);
            }
        }

        if ($search != 'undefined' && $search != null) {
            $product->where('products.name', 'Like', '%' . $search . '%');
        }

        $result = $product->distinct()->paginate(40);

        return response()->json($result);

    }

    // after filters are sent
    public function afterFilterProducts($gender, $category, $subCategory, $filter, $sales = null, $search = null, $brand = null, $similar = null)
    {
        $product = null;

        if($sales == 'undefined'){
            $sales = null;
        }
        if($brand == 'undefined'){
            $brand = null;
        }
        if($search == 'undefined'){
            $search = null;
        }

        // if only gender
        if (($subCategory == 'undefined' && $category == 'undefined') || (is_null($subCategory) && is_null($category))) {
            if (is_null($brand)) {
                $product = DB::table('categories as t1')
                    ->leftjoin('categories as t2', 't2.parent_id', '=', 't1.id')
                    ->leftjoin('categories as t3', 't3.parent_id', '=', 't2.id')
                    ->join('products', 'products.category_id', '=', 't3.id')
                    ->leftjoin('brands', 'brands.id', '=', 'products.brand_id')
                    ->leftJoin('sales_offers', 'sales_offers.product_id', '=', 'products.id')// added salesoffer
                    ->select('t3.label', 'products.id', 'products.name', 'products.price', 'brands.name as brand', 'products.thumbnail', 'products.slug', 'brands.id as brandId', 'sales_offers.rate as rate');
            } else {
                $product = DB::table('categories as t1')
                    ->leftjoin('categories as t2', 't2.parent_id', '=', 't1.id')
                    ->leftjoin('categories as t3', 't3.parent_id', '=', 't2.id')
                    ->join('products', 'products.category_id', '=', 't3.id')
                    ->leftjoin('brands', 'brands.id', '=', 'products.brand_id')
                    ->leftjoin('sales_offers', 'sales_offers.product_id', '=', 'products.id')// added salesoffer
                    ->select('t3.label', 'products.id', 'products.name', 'products.price', 'brands.name as brand', 'products.thumbnail', 'products.slug', 'brands.id as brandId', 'sales_offers.rate as rate')
                    ->whereRaw("brands.name='$brand'");
            }
        } // if gender and main-category ( Clothing / Sports / Shoes / ..... ) is requested
        else if ($category == $subCategory || $subCategory == 'All') {
            if (is_null($brand)) {
                $product = DB::table('categories as t1')
                    ->leftjoin('categories as t2', 't2.parent_id', '=', 't1.id')
                    ->leftjoin('categories as t3', 't3.parent_id', '=', 't2.id')
                    ->join('products', 'products.category_id', '=', 't3.id')
                    ->leftjoin('brands', 'brands.id', '=', 'products.brand_id')
                    ->leftJoin('sales_offers', 'sales_offers.product_id', '=', 'products.id')// added salesoffer
                    ->select('t3.label', 'products.id', 'products.name', 'products.price', 'brands.name as brand', 'products.thumbnail', 'products.slug', 'brands.id as brandId', 'sales_offers.rate as rate')
                    ->whereRaw("t2.label='$subCategory' ");
            } else {
                $product = DB::table('categories as t1')
                    ->leftjoin('categories as t2', 't2.parent_id', '=', 't1.id')
                    ->leftjoin('categories as t3', 't3.parent_id', '=', 't2.id')
                    ->join('products', 'products.category_id', '=', 't3.id')
                    ->leftjoin('brands', 'brands.id', '=', 'products.brand_id')
                    ->leftjoin('sales_offers', 'sales_offers.product_id', '=', 'products.id')// added salesoffer
                    ->select('t3.label', 'products.id', 'products.name', 'products.price', 'brands.name as brand', 'products.thumbnail', 'products.slug', 'brands.id as brandId', 'sales_offers.rate as rate')
                    ->whereRaw("t2.label='$category' && brands.name='$brand'");
            }
        } // if gender, main-category and sub-category ( Shirts / Pants / ..... ) is requested
        else {
            if (is_null($brand)) {
                $product = DB::table('categories as t1')
                    ->leftjoin('categories as t2', 't2.parent_id', '=', 't1.id')
                    ->leftjoin('categories as t3', 't3.parent_id', '=', 't2.id')
                    ->join('products', 'products.category_id', '=', 't3.id')
                    ->leftjoin('brands', 'brands.id', '=', 'products.brand_id')
                    ->leftJoin('sales_offers', 'sales_offers.product_id', '=', 'products.id')// added salesoffer
                    ->select('t3.label', 'products.id', 'products.name', 'products.price', 'brands.name as brand', 'products.thumbnail', 'products.slug', 'brands.id as brandId', 'sales_offers.rate as rate')
                    ->whereRaw("t2.label='$category' && t3.label='$subCategory' ");
            } else {
                $product = DB::table('categories as t1')
                    ->leftjoin('categories as t2', 't2.parent_id', '=', 't1.id')
                    ->leftjoin('categories as t3', 't3.parent_id', '=', 't2.id')
                    ->join('products', 'products.category_id', '=', 't3.id')
                    ->leftjoin('brands', 'brands.id', '=', 'products.brand_id')
                    ->leftjoin('sales_offers', 'sales_offers.product_id', '=', 'products.id')// added salesoffer
                    ->select('t3.label', 'products.id', 'products.name', 'products.price', 'brands.name as brand', 'products.thumbnail', 'products.slug', 'brands.id as brandId', 'sales_offers.rate as rate')
                    ->whereRaw("t2.label='$category' && brands.name='$brand'");
            }
        }

        if ($gender != 'All') {
            $product = $product->whereRaw("t1.label='$gender'");
        }

        foreach (json_decode($filter) as $column => $value) {
            switch ($column) {
                case 'price':
                    if ($value->min > 0 && $value->max > 0) {
                        $product->whereBetween(
                            DB::raw("IF(rate!=null, price-price*rate, price)"),
                            array($value->min, $value->max));
                    }
                    break;//end of price

                case 'sizes':
                    if (!in_array('all', $value) && !empty($value)) {
                        $product->join('colorsizes as t4', 't4.product_id', '=', 'products.id'); // give different name to table colorsizes to differentiate with color filter
                        $product->whereIn("t4.size_id", $value);
                    }
                    break;//end of sizes

                case 'brands':
                    if (!empty($value)) {
                        $product->whereIn("products.brand_id", $value);
                    }
                    break;

                case 'colors':
                    if (!in_array('all', $value) && !empty($value)) {
                        $product->join('colorsizes', 'colorsizes.product_id', '=', 'products.id');
                        $product->whereIn("colorsizes.color_id", $value)->distinct(); // added distinct to stop repeating of same color
                    }
                    break;

                case 'fabrics':
                    if (!in_array('all', $value) && !empty($value)) {
                        $product->whereIn("products.fabric_id", $value);
                    }
                    break;

                case 'occasions':
                    if (!in_array('all', $value) && !empty($value)) {
                        $product->whereIn("products.occasion_id", $value);
                    }
                    break;

                case 'subcategory':
                    if (!in_array('all', $value) && !empty($value)) {
                        $product->whereIn('t3.label', $value);
                    }
                    break;

                case 'sort':
                    switch ($value) {
                        case 'high':
                            $product->orderBy(
                                DB::raw("IF(rate, price-price*rate, price)"),
                                'desc')->get();
                            break;
                        case 'low':
                            $product->orderBy(
                                DB::raw("IF(rate, price-price*rate, price)"),
                                'asc')->get();
                            break;
                        case 'latest':
                            $product->latest('products.created_at');
                            break;
                        case 'oldest':
                            $product->oldest('products.created_at');
                            break;
                    }
                    break;
            }//end of outer switch
        }//end of foreach json decode

        if (!array_key_exists('sort', json_decode($filter))) {
            $product = $product->inRandomOrder();
        }

        $words = explode(' ', $similar);


        if ($sales != 'undefined' && $sales != null) {
            if ($sales == 'yes') {
                $product->where('rate', '!=', null);
            } else {
                $product->where('rate', '<', $sales / 100);
            }
        }

        if ($search != 'undefined' && $search != null) {
            $product->where('products.name', 'Like', '%' . $search . '%');
        }

        if(!is_null($similar)){ // if similar items are requested

            // getting all the products to filter
            $products = $product->get();

            // first getting all the similar items according to the name
            $similarProducts = $products->filter(function ($product) use ($words) {
                $arr = [];
                foreach ($words as $word){
                    $temp = Str::contains($product->name, $word);
                    if($temp){
                        array_push($arr, $temp);
                    }
                }
                return $arr;
            });

            // getting items that does not match the query
            $notSimilarProducts = $products->reject(function ($product) use ($words) {
                $arr = [];
                foreach ($words as $word){
                    $temp = Str::contains($product->name, $word);
                    if($temp){
                        array_push($arr, $temp);
                    }
                }
                return $arr;
            });

            // setting the product from which similar was requested as first item in list
            $firstProduct = $similarProducts->filter(function ($product) use ($similar) {
                return Str::contains($product->name, $similar);
            });

            // getting similar items other than above $firstProduct
            $otherSimilarProducts = $similarProducts->reject(function ($product) use ($similar) {
                return Str::contains($product->name, $similar);
            });

            // arranging list of similar items
            $allSimilarProducts = $otherSimilarProducts->prepend($firstProduct->first());

            // adding non similar items to end of similar items
            $allProducts = $allSimilarProducts->merge($notSimilarProducts);

            // limiting the items
            $result = $allProducts->take(40);
            return response()->json($result);
        }

        $result = $product->distinct()->paginate(40);
        return response()->json($result);

    }//end of function afterFilterProducts

    public function getWishListProduct($ids){
        $productIds = explode(',', $ids);

        $products = [];

        foreach ($productIds as $id){
            $item = Product::where('id', $id)
                ->with('brand:id,name')
                ->with('sales')
                ->select('id', 'brand_id', 'name', 'price', 'slug', 'thumbnail')
                ->first();

            $colors = DB::table('colorsizes')
                ->leftjoin('colors', 'colors.id', '=', 'colorsizes.color_id')
                ->select('colors.id', 'colors.name', 'colors.value')
                ->where('colorsizes.product_id', $id)
                ->distinct()
                ->get('colorsizes.color_id');

            $images = Image::where('product_id', $id)
                ->select('images.images', 'images.color_id')
                ->get();

            $sizes = DB::table('colorsizes')
                ->join('sizes', 'sizes.id', 'colorsizes.size_id')
                ->select('sizes.name', 'colorsizes.quantity', 'colorsizes.sku as sku', 'colorsizes.color_id as id', 'colorsizes.id as colorsize_id')
                ->where('colorsizes.product_id', '=', $id)
                ->get('colorsizes.size_id');

            $color_sizes = [];
            $i = 0;
            foreach ($colors as $color) {
                // first saving color information
                $color_sizes[$i] = [
                    "id" => $color->id,
                    "name" => $color->name,
                    "value" => $color->value
                ];

                // saving images according to color
                foreach ($images as $image) {
                    if ($color->id == $image->color_id) {
                        $color_sizes[$i]['images'] = $image->images;
                    }
                }

                // saving sizes according to color
                $j = 0;
                foreach ($sizes as $size) {
                    if ($color->id == $size->id) {
                        $color_sizes[$i]['sizes'][$j] = [
                            'colorsize_id' => $size->colorsize_id,
                            'name' => $size->name,
                            'quantity' => $size->quantity,
                            'sku' => $size->sku
                        ];
                        $j++;
                    }
                }
                $i++;
            }

            $item["color_sizes"] = $color_sizes;

            array_push($products, $item);
        }

        return response()->json($products);
    }
}
