<?php

namespace App\Http\Controllers\backend;

use App\Category;
use App\Color;
use App\Notifications\LowStock;
use App\Order;
use App\OrderItem;
use App\SalesOffer;
use App\Size;
use App\Store;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Notification;

class ReportsController extends Controller
{
    public function salesReportBySize($sort, $start = null, $end = null)
    {

        if ($start == 'null') {
            $start = null;
            $end = null;
        }

        $start_date = $start;
        if (is_null($end)) {
            $end_date = Carbon::now();
        } else {
            $end_date = $end;
        }

        $reports = null;

        switch ($sort) {
            case 'Monthly':
                if (is_null($start_date)) {
                    $start_date = Carbon::now()->subYear();
                }
                $reports = OrderItem::select(DB::raw('sum(total) as sales'), 'size', DB::raw('DATE_FORMAT(created_at, "%M-%Y") as date'))
                    ->whereDate('created_at', '<=', $end_date)
                    ->whereDate('created_at', '>=', $start_date)
                    ->groupBy('size', 'date')
                    ->orderBy('created_at')
                    ->get();

                $reports->each(function ($item, $key) {
                    $item->date = date('M-Y', strtotime($item->date));
                });
                break;
            case 'Yearly':
                if (is_null($start_date)) {
                    $start_date = Carbon::now()->subYears(5);
                }
                $reports = OrderItem::select(DB::raw('sum(total) as sales'), 'size', DB::raw('YEAR(created_at) as date'))
                    ->whereDate('created_at', '<=', $end_date)
                    ->whereDate('created_at', '>=', $start_date)
                    ->groupBy('size', 'date')
                    ->orderBy('created_at')
                    ->get();
                break;
            default:
                if (is_null($start_date)) {
                    $start_date = Carbon::now()->subDays(7);
                }
                $reports = OrderItem::select(DB::raw('sum(total) as sales'), 'size', DB::raw('DATE(created_at) as date'))
                    ->whereDate('created_at', '<=', $end_date)
                    ->whereDate('created_at', '>=', $start_date)
                    ->groupBy('size', 'date')
                    ->orderBy('created_at')
                    ->get();

                $reports->each(function ($item, $key) {
                    $item->date = date('d-M-Y', strtotime($item->date));
                });
                break;
        }

        $labels = [];
        $sizes = [];
        $sales = [];
        foreach ($reports as $report) {
            if (!in_array($report->date, $labels)) {
                array_push($labels, $report->date);
            }
            if (!in_array($report->size, $sizes)) {
                array_push($sizes, $report->size);
                $sales[$report->size] = [];
            }
        }

        $data = [];

        foreach ($labels as $label) {
            $data[$label] = [];
            foreach ($reports as $report) {
                if ($label == $report->date) {
                    $data[$label][$report->size] = $report->sales;
                }
            }
        }

        foreach ($data as $key => $d) {
            foreach ($sizes as $size) {
                if (!array_key_exists($size, $d)) {
                    $data[$key][$size] = 0;
                }
            }
        }

        foreach ($data as $item) {
            foreach ($sizes as $size) {
                array_push($sales[$size], $item[$size]);
            }
        }

        return response()->json([
            'sales' => $sales,
            'labels' => $labels
        ]);
    }

    public function getReportFilter()
    {

        $stores = Store::select('id', 'store_name')->get();
        $colors = Color::select('id', 'name')->get();
        $sizes = Size::all();
        $subCategories = Category::with('childs')->where('parent_id', null)->get();
        $region = Order::select('region')->distinct()->get();

        return response()->json([
            'stores' => $stores,
            'colors' => $colors,
            'sizes' => $sizes,
            'subCategories' => $subCategories,
            'region' => $region
        ]);

    }

    public function detailReport($filter, $start, $end, $trending = null)
    {
        $start_date = $start;
        $end_date = $end;

        $fields = [
            'products.id as product_id',
            DB::raw('MAX(orderitems.id) as id'),
            DB::raw('MAX(products.name) as product'),
            DB::raw('SUM(orderitems.quantity) as quantity'),
            DB::raw('MAX(products.price) as sp'),
            DB::raw('MAX(products.purchase_price) as cp'),
            DB::raw('MAX(sales_offers.rate) as discount'),
        ];

        $isTrending = false;

        $group = [
            'product_id'
        ];

        $filters = json_decode($filter);

        foreach ($filters as $key => $filter) {
            if ($filter == 'undefined' || $filter == 'null') {
                $filters->$key = '';
            }
        }

        if (count($filters->store) != 0) {
            array_push($fields, DB::raw('MAX(stores.store_name) as store'));
        }

        if (count($filters->color) != 0) {
            array_push($fields, DB::raw('MAX(orderitems.color) as color'));
            array_push($group, 'color');
        }

        if (count($filters->size) != 0) {
            array_push($fields, DB::raw('MAX(orderitems.size) as size'));
            array_push($group, 'size');
        }

        if (count($filters->gender) != 0) {
            array_push($fields, DB::raw('MAX(t1.label) as gender'));
        }

        if (count($filters->category) != 0) {
            array_push($fields, DB::raw('MAX(t2.label) as category'));
        }

        if (count($filters->subCategory) != 0) {
            array_push($fields, DB::raw('MAX(t3.label) as sub'));
        }

        if (count($filters->region) != 0) {
            array_push($fields, DB::raw('MAX(orders.region) as region'));
        }

        $items = DB::table('categories as t1')
            ->leftjoin('categories as t2', 't2.parent_id', '=', 't1.id')
            ->leftjoin('categories as t3', 't3.parent_id', '=', 't2.id')
            ->join('products', 'products.category_id', '=', 't3.id')
            ->join('stores', 'stores.id', '=', 'products.store_id')
            ->join('orderitems', 'orderitems.product_id', '=', 'products.id')
            ->leftJoin('sales_offers', 'sales_offers.product_id', '=', 'products.id')
            ->leftJoin('orders', 'orders.id', '=', 'orderitems.order_id')
            ->select($fields)
            ->whereDate('orderitems.created_at', '<=', $end_date)
            ->whereDate('orderitems.created_at', '>=', $start_date)
            ->groupBy($group);

        switch ($trending) {
            case 'Top':
                $items->orderBy('quantity', 'desc');
                $isTrending = true;
                break;
            case 'Worst':
                $items->orderBy('quantity', 'asc');
                $isTrending = true;
                break;
            case '':
                $items->orderBy('product_id');
                break;
            default:
                $items->orderBy('product_id');
                break;
        }

        foreach ($filters as $filter => $value) {
            switch ($filter) {
                case 'store':
                    if (!in_array('all', $value) && !empty($value)) {
                        $items->whereIn('stores.store_name', $value);
                    }
                    break;
                case 'color':
                    if (!in_array('all', $value) && !empty($value)) {
                        $items->whereIn('orderitems.color', $value);
                    }
                    break;
                case 'size':
                    if (!in_array('all', $value) && !empty($value)) {
                        $items->whereIn('orderitems.size', $value);
                    }
                    break;
                case 'gender':
                    if (!in_array('all', $value) && !empty($value)) {
                        $items->whereIn('t1.label', $value);
                    }
                    break;
                case 'category':
                    if (!in_array('all', $value) && !empty($value)) {
                        $items->whereIn('t2.label', $value);
                    }
                    break;
                case 'subCategory':
                    if (!in_array('all', $value) && !empty($value)) {
                        $items->whereIn('t3.label', $value);
                    }
                    break;
                case 'region':
                    if (!in_array('all', $value) && !empty($value)) {
                        $items->whereIn('orders.region', $value);
                    }
                    break;
            }
        }

        $items = $items->get();

        if ($isTrending) {
            $items = $items->take(10);
        }

        foreach ($items as $item) {
            $item->gross_sales = ($item->sp * $item->quantity);
            $grossCost = ($item->cp * $item->quantity);
            if ($item->discount != null) {
                $item->discount = round(($item->discount * $item->sp), 2);
            } else {
                $item->discount = 0;
            }
            $item->net_sales = round($item->gross_sales - $item->discount, 2);
            $item->gross_profit = round($item->net_sales - $grossCost, 2);
            $item->profit = round(($item->gross_profit / $item->net_sales) * 100, 2);
        }

        $start_date = date('d F, Y', strtotime($start_date));
        $end_date = date('d F, Y', strtotime($end_date));

        return response()->json([
            'items' => $items,
            'start' => $start_date,
            'end' => $end_date
        ]);

    }

    public function inventoryOnHand()
    {
        $items = DB::table('products')
            ->join('colorsizes', 'colorsizes.product_id', '=', 'products.id')
            ->join('categories as sub', 'sub.id', '=', 'products.category_id')
            ->join('categories as cat', 'cat.id', '=', 'sub.parent_id')
            ->join('categories as gen', 'gen.id', '=', 'cat.parent_id')
            ->select('products.id as id',
                DB::raw('MAX(products.name) as name'),
                DB::raw('SUM(colorsizes.quantity) as quantity'),
                DB::raw('MAX(gen.label) as gender'),
                DB::raw('MAX(cat.label) as category'),
                DB::raw('MAX(sub.label) as sub'),
                DB::raw('MAX(products.purchase_price) as cp'))
            ->groupBy('id')
            ->get();

        foreach ($items as $item) {
            $item->value = $item->cp * $item->quantity;
        }

        return response()->json($items);
    }

    public function lowStock()
    {

        $items = DB::table('products')
            ->join('colorsizes', 'colorsizes.product_id', '=', 'products.id')
            ->join('categories as sub', 'sub.id', '=', 'products.category_id')
            ->join('categories as cat', 'cat.id', '=', 'sub.parent_id')
            ->join('categories as gen', 'gen.id', '=', 'cat.parent_id')
            ->join('colors', 'colors.id', '=', 'colorsizes.color_id')
            ->join('sizes', 'sizes.id', '=', 'colorsizes.size_id')
            ->select('colorsizes.id as id',
                'products.id as product_id',
                'products.name as name',
                'colorsizes.quantity as stock',
                'colorsizes.minimum_stock as minimum',
                'gen.label as gender',
                'cat.label as category',
                'sub.label as sub',
                'colors.name as color',
                'sizes.name as size')
            ->orderBy('id')
            ->get();

        $low = [];

        foreach ($items as $key => $item){
            if ($item->stock < $item->minimum) {
                $item->low = $item->minimum - $item->stock;
                $item->remarks = 'Required';
                array_push($low, $item);
            } else {
                $item->low = 0;
                $item->remarks = 'Not Required';
            }
        }

        Notification::route('mail', 'info@thenextrends.com')
            ->notify(new LowStock($low));

        return response()->json($items);

    }

    public function slowMovingStock()
    {
        $end = Carbon::now();
        $start = Carbon::now()->subDays(30);

        $items = DB::table('products')
            ->join('colorsizes', 'colorsizes.id', '=', 'products.id')
            ->join('categories as sub', 'sub.id', '=', 'products.category_id')
            ->join('categories as cat', 'cat.id', '=', 'sub.parent_id')
            ->join('categories as gen', 'gen.id', '=', 'cat.parent_id')
            ->join('colors', 'colors.id', '=', 'colorsizes.color_id')
            ->join('sizes', 'sizes.id', '=', 'colorsizes.size_id')
            ->leftJoin('orderitems', 'orderitems.colorsize_id', '=', 'colorsizes.id')
            ->select('colorsizes.id as id',
                'products.id as product_id',
                'products.name as name',
                'products.purchase_price as cp',
                'colorsizes.quantity as stock',
                'orderitems.quantity as qty_sold',
                'orderitems.created_at as date',
                'cat.label as category',
                'sub.label as sub',
                'colors.name as color',
                'sizes.name as size')
            ->orderBy('id')
            ->get();

        $items->each(function ($item) use($end) {
            $item->today = date('d/m/Y', strtotime($end));
            if(is_null($item->date)){
                $item->date = 'None';
            } else {
                $item->date = date('d/m/Y', strtotime($item->date));
            }
            $item->value = $item->cp * $item->stock;
            if (is_null($item->qty_sold)) {
                $item->slow = 'TRUE';
                $item->qty_sold = 0;
                SalesOffer::updateOrCreate(
                    ['product_id' => $item->product_id],
                    ['rate' => 0.20]
                );
            } else {
                $item->slow = 'FALSE';
            }
        });

        $start = date('d F, Y', strtotime($start));
        $end = date('d F, Y', strtotime($end));

        return response()->json([
            'items' => $items,
            'start' => $start,
            'end' => $end
        ]);

    }
}
