<?php

namespace App\Http\Controllers\backend;

use App\Fabric;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FabricController extends Controller
{
    public function store(Request $request){
        $request->validate([
            'name' => 'required|string',
        ]);
        $fabric = new Fabric([
            'name' => $request->get('name')
        ]);

        $fabric->save();

        return response()->json('success');
    }

    public function getFabric(){
        $fabric = Fabric::get();
        return response()->json($fabric);
    }

    public function destroy($id){
        $fabric = Fabric::findOrFail($id);
        $fabric->delete();
        return ['message' => 'Fabric Deleted'];
    }
}
