<?php

namespace App\Http\Controllers\backend;

use App\Brand;
use App\BrandGender;
use App\BrandType;
use App\FeaturedBrand;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class BrandController extends Controller
{
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|string',
        ]);
        $brand = new Brand([
            'name' => $request->get('name'),
        ]);

        $brand->save();

        foreach ($request->get('gender') as $key => $field) {
            $gender = new BrandGender([
                'brand_id' => $brand->id,
                'gender' => $field
            ]);
            $gender->save();
        }

        foreach ($request->get('type') as $key => $field) {
            $type = new BrandType([
                'brand_id' => $brand->id,
                'type' => $field
            ]);
            $type->save();
        }

        return response()->json('success');
    }

    public function getAllBrands(){
        $brands = Brand::all();
        return response()->json($brands);
    }

    // for adding product
    public function getBrand($gender, $type)
    {
        $brands = DB::table('brands')
            ->join('brand_types', 'brand_types.brand_id', '=', 'brands.id')
            ->join('brand_genders', 'brand_genders.brand_id', '=', 'brands.id')
            ->where('brand_genders.gender', $gender)
            ->where('brand_types.type', $type)
            ->select('brands.id', 'brands.name', 'brand_types.type')
            ->get();
        return response()->json($brands);
    }

    public function destroy($id)
    {
        $brand = Brand::findOrFail($id);
        $brand->delete();
        return ['message' => 'Brand Deleted'];
    }

    public function featuredAdd(Request $request)
    {
        $imageName = time().'.'.$request->image->getClientOriginalExtension();
        \Image::make($request->image)->fit(300, 300)->save(public_path('image/featuredBrand/') . $imageName);

        FeaturedBrand::updateOrCreate(
            ['brand_id' => $request->get('brand_id')],
            ['image' => $imageName]
        );

    }
}
