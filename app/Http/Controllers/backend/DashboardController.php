<?php

namespace App\Http\Controllers\backend;

use App\Order;
use App\OrderItem;
use App\User;
use Carbon\Carbon;
use function foo\func;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationData;

class DashboardController extends Controller
{
    public function data(){

        $user = User::where('type', 'user')->get();
        $order = Order::select('total')->get();
        $order_item = OrderItem::all();

        $user_number = count($user);
        $order_number = count($order);

        $quantity = 0;
        foreach ($order_item as $item){
            $quantity += $item->quantity;
        }

        $sales = 0;
        foreach ($order as $sale){
            $sales += $sale->total;
        }

        return response()->json([
            'user' => $user_number,
            'order' => $order_number,
            'quantity' => $quantity,
            'sales' => round($sales, 2),
            ]);
    }

    public function salesReport($sort, $start=null, $end=null){

        if($start == 'null'){
            $start = null;
            $end = null;
        }

        $start_date = $start;
        if(is_null($end)){
            $end_date = Carbon::now();
        } else {
            $end_date = $end;
        }

        switch ($sort) {
            case 'Monthly':
                if(is_null($start_date)){
                    $start_date = Carbon::now()->subYear();
                }
                $orders = Order::select(DB::raw('sum(total) as total'), DB::raw('DATE_FORMAT(created_at, "%M-%Y") as date'))
                    ->whereDate('created_at', '<=', $end_date)
                    ->whereDate('created_at', '>=', $start_date)
                    ->groupBy('date')
                    ->get();
                $orders->each(function ($item, $key){
                    $item->total = round($item->total, 2);
                    $item->date = date('M-Y', strtotime($item->date));
                });
                break;
            case 'Yearly':
                if(is_null($start_date)) {
                    $start_date = Carbon::now()->subYears(5);
                }
                $orders = Order::select(DB::raw('sum(total) as total'), DB::raw('YEAR(created_at) as date'))
                    ->whereDate('created_at', '<=', $end_date)
                    ->whereDate('created_at', '>=', $start_date)
                    ->groupBy('date')
                    ->get();
                $orders->each(function ($item, $key){
                    $item->total = round($item->total, 2);
                });
                break;
            default:
                if(is_null($start_date)) {
                    $start_date = Carbon::now()->subDays(7);
                }
                $orders = Order::select(DB::raw('sum(total) as total'), DB::raw('DATE(created_at) as date'))
                    ->whereDate('created_at', '<=', $end_date)
                    ->whereDate('created_at', '>=', $start_date)
                    ->groupBy('date')
                    ->get();
                $orders->each(function ($item, $key){
                    $item->total = round($item->total, 2);
                    $item->date = date('d-M-Y', strtotime($item->date));
                });
                break;
        }

        $orders->sortBy('date');

        $data = [];
        $label = [];
        foreach ($orders as $sale){
            array_push($data, $sale->total);
            array_push($label, $sale->date);
        }

        return response()->json([
            'data' => $data,
            'label' => $label
        ]);
    }
}
