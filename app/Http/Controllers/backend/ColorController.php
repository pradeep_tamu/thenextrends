<?php

namespace App\Http\Controllers\backend;

use App\Color;
use App\Size;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ColorController extends Controller
{
    //Color CRUD
    public function store(Request $request){
        $request->validate([
            'name' => 'required|string',
            'value' => 'required|string',
        ]);

        $color = new Color([
            'name' => $request->get('name'),
            'value' => $request->get('value'),
        ]);

        $color->save();

        return response()->json('success');
    }

    public function getColor(){
        $color = Color::get();
        return response()->json($color);
    }

    public function destroy($id){
        $color = Color::findOrFail($id);
        $color->delete();
        return ['message' => 'Color Deleted'];
    }


    //Size CRUD
    public function create(Request $request){
        $request->validate([
            'name' => 'required|string',
        ]);
        $size = new Size([
            'name' => $request->get('name')
        ]);

        $size->save();

        return response()->json('success');
    }

    public function getSize(){
        $size = Size::get();
        return response()->json($size);
    }

    public function delete($id){
        $size = Size::findOrFail($id);
        $size->delete();
        return ['message' => 'Size Deleted'];
    }
}
