<?php

namespace App\Http\Controllers\backend;

use App\Banner;
use App\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;


class CategoryController extends Controller
{

    public function store(Request $request)
    {
        $request->validate([
            'label' => 'required|string',
        ]);
        $category = new Category([
            'label' => $request->get('label'),
            'parent_id' => $request->get('parent_id')
        ]);

        $category->save();

        return response()->json('success');
    }
    public function getParent()
    {
        $parent = Category::with('childs')->where('parent_id',null)->get();
        return response()->json($parent);
    }
    public function getCategoryOptions()
    {
        $parent = Category::with('grandchilds')->where('parent_id',null)->get();

        foreach ($parent as $opt){
            $opt->children = $opt->grandchilds;
            unset($opt->grandchilds);
        }

        return response()->json($parent);
    }
    public function destroy($id){
        $category = Category::findOrFail($id);
        $category->delete();
        return ['message' => 'Category Deleted'];
    }

    //Navigation Image
    public function storeImage(Request $request){

        $temp = [];
        foreach ($request->images as $key => $value) {
            $name = md5(microtime()) . '.' . explode('/', explode(':', substr($value, 0, strpos($value, ';')))[1])[1];
            array_push($temp, $name);
            //laravel intervention to convert base 64 image
            \Image::make($value)->save(public_path('image/banner/') . $name);
        }
        $img = [];
        foreach ($temp as $key => $value) {
            $img[$key] = $temp[$key];
        }
        $val = implode(",", $img);

        Banner::updateOrCreate(
            [
                'category' => $request->get('category'),
                'gender' => $request->get('gender')
            ],
            [
                'images' => $val
            ]
        );
//        $product->save();
    }

    //Backend navigation banner list display
    public function bannerList(){
        $banner = Banner::get();
        return $banner;
    }

    //banner delete
    public function deleteBanner($id){
        $banner = Banner::findOrFail($id);

        //deleting banner images from local disk
        $image = public_path("image/banner/" . $banner->images);
        if (File::exists($image)) {
            File::delete($image);
        }

        $banner->delete();
        return ['message' => 'Banner Deleted'];
    }

}
