<?php

namespace App\Http\Controllers\backend;

use App\Store;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class StoreController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
            return Store::latest()->paginate(5);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'owner_name' => 'required|string|max:50',
            'store_name' => 'required|string|max:100',
            'email' => 'required|string|email|max:191|unique:stores',
            'address' => 'required|string|min:10',
            'phone' => 'required|string|min:10'
        ]);

        return Store::create([
            'owner_name' => $request['owner_name'],
            'store_name' => $request['store_name'],
            'email' => $request['email'],
            'address' => $request['address'],
            'phone' => $request['phone'],
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {


        $store = Store::findOrFail($id);

        //validation
        $this->validate($request, [
            'owner_name' => 'required|string|max:191',
            'store_name' => 'required|string|max:191',
            'email' => 'required|string|email|max:191|unique:stores,email,' . $store->id,
            'address' => 'required|string|min:10',
            'phone' => 'required|string|min:10'
        ]);

        $store->update($request->all());
        return ['message' => "Updated Successfully"];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = Store::findOrFail($id);
        $user->delete();
        return ['message' => 'User Deleted'];
    }

    public function getAllStores(){
        return Store::all();
    }

}
