<?php

namespace App\Http\Controllers\backend;

use App\Invoice;
use App\Http\Controllers\Controller;
use App\Order;
use App\OrderItem;
use Illuminate\Support\Facades\DB;

class InvoiceController extends Controller
{
    public function createInvoice($id, $total){

        $invoice = Invoice::where('order_id', $id)->first();

        if($invoice){
            return response()->json('Already created');
        }

        DB::table('orders')->where('id', $id)->update(['status' => '1']);

        Invoice::create([
           'total' => $total,
            'order_id' => $id
        ]);

        return response()->json('Successful');
    }

    public function getInvoice(){
        $invoice = Invoice::all();

        return response()->json($invoice);
    }

    public function getItemDetails($id){

        $invoice = Invoice::where('order_id', $id)->first();

        $order = Order::where('id',$id)
            ->select('id', 'name', 'address', 'mobile', 'created_at','voucher_amount')->first();

        $items = OrderItem::with('products:id,name,price')
            ->select('order_id', 'product_id', 'quantity', 'color', 'size')
            ->where('order_id', $id)
            ->get();

        return response()->json(['invoice' => $invoice, 'order' => $order, 'items' => $items]);
    }

    public function orderCompleted($id, $order_id){
        $invoice = Invoice::findOrFail($id);

        $invoice->update([
            'status' => true
        ]);

        $order = Order::findOrFail($order_id);
        $order->update([
            'status' => '2'
        ]);

        return response()->json('success');
    }

}
