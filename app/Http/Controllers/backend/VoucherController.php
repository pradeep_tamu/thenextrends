<?php

namespace App\Http\Controllers\backend;

use App\Voucher;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;

class VoucherController extends Controller
{
    public function store(Request $request)
    {
        $request->validate([
            'amount' => 'required|int',
        ]);
        $voucher = new Voucher([
            'voucher' => strtoupper(Str::uuid()),
            'amount' => $request->get('amount')
        ]);

        $voucher->save();

        return response()->json('success');
    }

    public function getVoucher()
    {
        $voucher = Voucher::get();
        return response()->json($voucher);
    }

    public function destroy($id)
    {
        $voucher = Voucher::findOrFail($id);
        $voucher->delete();
        return ['message' => 'Fabric Deleted'];
    }

    //frontend voucher methods
    public function pickVoucher($voucher)
    {
        $voucher = Voucher::where('voucher', $voucher)->get();

        return response()->json($voucher);

    }

    public function updateVoucher($voucher, Request $request)
    {
        $voucher = Voucher::where('voucher', $voucher)->first();

        $voucher->amount = $request->get('amount');
        $voucher->save();
    }
}
