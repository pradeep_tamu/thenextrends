<?php

namespace App\Http\Controllers\backend;

use App\Brand;
use App\Category;
use App\Color;
use App\ColorSize;
use App\Product;
use App\Image;
use App\Size;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Mockery\Exception;
use Intervention\Image\Facades\Image as ImageGD;

class ProductController extends Controller
{
    //product insert
    public function store(Request $request)
    {
        $request->validate([
            'category' => 'required',
            'name' => 'required|string',
            'price' => 'required|numeric',
            'purchase_price' => 'required|numeric',
            'description' => 'required|string',
            'store' => 'required',
            'brand' => 'required',
            'fabric' => 'required',
            'occasion' => 'required',
            'thumbnail' => 'required',
        ]);

        //image validation section as it contains multiple images
        //throwing custom exception
        $cimages = $request->cimages;
        for ($i = 0; $i < count($cimages); $i++) {
            if (count($cimages[$i]['image']) == 0) {
                throw new Exception('image' . $i . ' field is required');
            }
        }
        //end of custom exception

        //image for thumbnail
        if ($request->thumbnail) {
            $name = md5(microtime()) . '.' . explode('/', explode(':', substr($request->thumbnail, 0, strpos
                ($request->thumbnail, ';')))[1])[1];
        }
        if ($request->phototype == 'camera') {
            $nameImage = ImageGD::make($request->thumbnail);
            $nameImage->rotate(90)->fit(385, 500)->save(public_path('image/thumbnail/') . $name);
            $thumbnail = $name;
        } else {
            $nameImage = ImageGD::make($request->thumbnail);
            $nameImage->fit(385, 500)->save(public_path('image/thumbnail/') . $name);
            $thumbnail = $name;
        }

        //end of image for thumbnail

        $brand = Brand::where('id', $request->get('brand'))->select('name')->first();
        $slug = strtolower($brand->name . '-' . str_replace(' ', '-', $request->get('name')));

        $product = new Product([
            'category_id' => $request->get('category'),
            'store_id' => $request['store'],
            'name' => $request->get('name'),
            'purchase_price' => $request->get('purchase_price'),
            'price' => $request->get('price'),
            'description' => $request->get('description'),
            'brand_id' => $request->get('brand'),
            'fabric_id' => $request->get('fabric'),
            'occasion_id' => $request->get('occasion'),
            'thumbnail' => $thumbnail,
            'slug' => $slug
        ]);
        $product->save();

        //image section
        for ($i = 0; $i < count($cimages); $i++) {
            $imageCount = $cimages[$i]['image'];
            $temp = [];
            foreach ($imageCount as $key => $value) {
                $name = md5(microtime()) . '.' . explode('/', explode(':', substr($value, 0, strpos($value, ';')))[1])[1];
                array_push($temp, $name);
                //laravel intervention to convert base 64 image and save to profile
                //reduces the image size with aspect ratio

                if ($request->phototype == 'camera') {
                    ImageGD::make($value)->rotate(90)->resize(640, 800, function ($constraint) {
                        $constraint->aspectRatio();
                    })->save(public_path('image/product/') . $name);
                } else {
                    ImageGD::make($value)->resize(640, 800, function ($constraint) {
                        $constraint->aspectRatio();
                    })->save(public_path('image/product/') . $name);
                }
            }
            $img = [];
            foreach ($temp as $key => $value) {
                $img[$key] = $temp[$key];
            }
            $imagInsert = new Image();
            $val = implode(",", $img);
            $imagInsert['images'] = $val;
            $imagInsert['product_id'] = $product->id;
            $imagInsert['color_id'] = $cimages[$i]['color'];
            $imagInsert->save();
        }

        // for sku generation
        $cat = Category::with('parent')->where('id', $request->get('category'))->first();
        $gender = strtoupper(substr($cat->parent->grandparent->label, 0, 1));
        $category = strtoupper(substr($cat->parent->label, 0, 2));
        $subCategory = strtoupper(substr($cat->label, 0, 2));

        foreach ($request->get('fields') as $key => $field) {
            $color_size = new ColorSize([
                'product_id' => $product->id,
                'color_id' => $field['color'],
                'size_id' => $field['size'],
                'quantity' => $field['quantity'],
                'minimum_stock' => $field['minimum'],
            ]);
            $color_size->save();

            // for sku generation
            $color = Color::select('name')->where('id', $field['color'])->first();
            $color = strtoupper(substr($color->name, 0, 2));
            $size = Size::select('name')->where('id', $field['size'])->first();
            $size = strtoupper($size->name);
            $color_size->sku = $gender . $category . $subCategory . $color_size->id . $color . $size;
            $color_size->save();
        }
        return response()->json([$color_size, $product]);
    }

    //product update
    public function updateProduct(Request $request, $id)
    {

        $product = Product::findOrFail($id);
        $request->validate([
            'category' => 'required',
            'name' => 'required|string',
            'price' => 'required|numeric',
            'purchase_price' => 'required|numeric',
            'description' => 'required|string',
            'store' => 'required',
            'brand' => 'required',
            'fabric' => 'required',
            'occasion' => 'required',
        ]);

        //image validation section as it contains multiple images
        //throwing custom exception
        $cimages = $request->cimages;
        for ($i = 0; $i < count($cimages); $i++) {
            if ($cimages[$i]['image'][0] == 'chosen') {

            } else if (count($cimages[$i]['image']) == 0) {
                throw new Exception('image' . $i . ' field is required');
            }
        }

        //end of custom exception

        //image for thumbnail
        if ($request->thumbnail) {
            $name = md5(microtime()) . '.' . explode('/', explode(':', substr($request->thumbnail, 0, strpos
                ($request->thumbnail, ';')))[1])[1];

            if ($request->phototype == 'camera') {
                $nameImage = ImageGD::make($request->thumbnail);
                $nameImage->rotate(90)->fit(385, 500)->save(public_path('image/thumbnail/') . $name);
                $thumbnail = $name;
            } else {
                $nameImage = ImageGD::make($request->thumbnail);
                $nameImage->fit(385, 500)->save(public_path('image/thumbnail/') . $name);
                $thumbnail = $name;
            }


            //deleting thumbnail of images
            $thumbnailPath = public_path("image/thumbnail/" . $product->thumbnail);
            if (File::exists($thumbnailPath)) {
                File::delete($thumbnailPath);
            }
            $product->thumbnail = $thumbnail;
        }

        //end of image for thumbnail

        $product->category_id = $request->get('category');
        $product->store_id = $request['store'];
        $product->name = $request->get('name');
        $product->purchase_price = $request->get('purchase_price');
        $product->price = $request->get('price');
        $product->description = $request->get('description');
        $product->brand_id = $request->get('brand');
        $product->fabric_id = $request->get('fabric');
        $product->occasion_id = $request->get('occasion');
        $product->save();


        //image section
        for ($i = 0; $i < count($cimages); $i++) {
            $imageCount = $cimages[$i]['image'];
            if ($cimages[$i]['image'][0] != 'chosen' && array_key_exists("id", $cimages[$i])) {
                //return response()->json('not chosen');
                $temp = [];
                foreach ($imageCount as $key => $value) {
                    $name = md5(microtime()) . '.' . explode('/', explode(':', substr($value, 0, strpos($value, ';')))[1])[1];
                    array_push($temp, $name);
                    //laravel intervention to convert base 64 image and save to profile
                    //reduces the image size with aspect ratio
                    if ($request->phototype == 'camera') {
                        ImageGD::make($value)->rotate(90)->resize(640, 800, function ($constraint) {
                            $constraint->aspectRatio();
                        })->save(public_path('image/product/') . $name);
                    } else {
                        ImageGD::make($value)->resize(640, 800, function ($constraint) {
                            $constraint->aspectRatio();
                        })->save(public_path('image/product/') . $name);
                    }
                }
                $img = [];
                foreach ($temp as $key => $value) {
                    $img[$key] = $temp[$key];
                }
                $val = implode(",", $img);
                $imagInsert = Image::findOrFail($cimages[$i]['id']);
                $imagInsert['images'] = $val;
                $imagInsert['product_id'] = $product->id;
                $imagInsert['color_id'] = $cimages[$i]['color'];

                //deleting product images
                $images = Image::where('product_id', '=', $product->id)->pluck('images');
                foreach ($images as $key) {
                    $imgArr = explode(",", $key);
                    foreach ($imgArr as $key => $value) {
                        $productPath = public_path("image/product/" . $value);
                        if (File::exists($productPath)) {
                            File::delete($productPath);
                        }
                    }
                }
                $imagInsert->save();
            } else if ($cimages[$i]['image'][0] != 'chosen' && !array_key_exists("id", $cimages[$i])) {
                $temp = [];
                foreach ($imageCount as $key => $value) {
                    $name = md5(microtime()) . '.' . explode('/', explode(':', substr($value, 0, strpos($value, ';')))[1])[1];
                    array_push($temp, $name);
                    //laravel intervention to convert base 64 image and save to profile
                    //reduces the image size with aspect ratio
                    if ($request->phototype == 'camera') {
                        ImageGD::make($value)->rotate(90)->resize(640, 800, function ($constraint) {
                            $constraint->aspectRatio();
                        })->save(public_path('image/product/') . $name);
                    } else {
                        ImageGD::make($value)->resize(640, 800, function ($constraint) {
                            $constraint->aspectRatio();
                        })->save(public_path('image/product/') . $name);
                    }
                }
                $img = [];
                foreach ($temp as $key => $value) {
                    $img[$key] = $temp[$key];
                }
                $val = implode(",", $img);
                $imagInsert = new Image();
                $imagInsert['images'] = $val;
                $imagInsert['product_id'] = $product->id;
                $imagInsert['color_id'] = $cimages[$i]['color'];

                //deleting product images
                $images = Image::where('product_id', '=', $product->id)->pluck('images');
                foreach ($images as $key) {
                    $imgArr = explode(",", $key);
                    foreach ($imgArr as $key => $value) {
                        $productPath = public_path("image/product/" . $value);
                        if (File::exists($productPath)) {
                            File::delete($productPath);
                        }
                    }
                }
                $imagInsert->save();
            } else {
                $imagInsert = Image::findOrFail($cimages[$i]['id']);
                $imagInsert['color_id'] = $cimages[$i]['color'];
                $imagInsert->save();
            }
        }

        ColorSize::where('product_id', $id)->delete();
        foreach ($request->get('fields') as $key => $field) {
            $color_size = new ColorSize([
                'product_id' => $product->id,
                'color_id' => $field['color'],
                'size_id' => $field['size'],
                'quantity' => $field['quantity'],
                'minimum_stock' => $field['minimum'],
            ]);
            $color_size->save();
        }
        return response()->json([$color_size, $product]);
    }


    //data for edit
    public function editInfo($id)
    {
        //        $product = Product::where('id', $id)->first();
        $product = DB::table('products')
            ->join('categories', 'categories.id', '=', 'products.category_id')
            ->join('fabrics', 'fabrics.id', '=', 'products.fabric_id')
            ->join('occassions', 'occassions.id', '=', 'products.occasion_id')
            ->join('brands', 'brands.id', '=', 'products.brand_id')
            ->join('stores', 'stores.id', '=', 'products.store_id')
            ->where('products.id', $id)
            ->select('products.name', 'products.purchase_price', 'products.description', 'products.price',
                'categories.id as category', 'brands.id as brand_id', 'stores.id as store_id', 'fabrics.id as fabric_id', 'occassions.id as occasion_id')
            ->first();
        return response()->json($product);
    }

//color size data for edit
    public function colorSize($id)
    {
        $product = ColorSize::where('product_id', $id)->get();
        return response()->json($product);
    }


    public function editImages($id)
    {
        $product = Image::where('product_id', $id)->get();
        return response()->json($product);
    }

//for single product view backend
    public function getProductFull($id)
    {

        $brand = Product::where('id', $id)->pluck('brand_id')->first();
        if ($brand == null) {
            $product = DB::table('products')
                ->join('categories', 'categories.id', '=', 'products.category_id')
                ->join('fabrics', 'fabrics.id', '=', 'products.fabric_id')
                ->join('occassions', 'occassions.id', '=', 'products.occasion_id')
//                ->join('images', 'images.product_id', '=', 'products.id')
                ->where('products.id', $id)
                ->select('products.name', 'products.description', 'products.price',
//                    'images.images',
                    'categories.label', 'fabrics.name as fabric_name', 'occassions.name as occasion_name')
                ->first();
            return response()->json($product);
        }
        $product = DB::table('products')
            ->join('categories', 'categories.id', '=', 'products.category_id')
            ->join('brands', 'brands.id', '=', 'products.brand_id')
            ->join('fabrics', 'fabrics.id', '=', 'products.fabric_id')
            ->join('occassions', 'occassions.id', '=', 'products.occasion_id')
//            ->join('images', 'images.product_id', '=', 'products.id')
            ->where('products.id', $id)
            ->select('products.name', 'products.description', 'products.price',
//                'images.images',
                'categories.label', 'brands.name as brand_name', 'fabrics.name as fabric_name', 'occassions.name as occasion_name')
            ->first();
        return response()->json($product);

    }

//color and size for single product view
    public function getColorSize($id)
    {
        $colorSize = DB::table('colorsizes')
            ->join('products', 'products.id', '=', 'colorsizes.product_id')
            ->join('colors', 'colors.id', '=', 'colorsizes.color_id')
            ->join('sizes', 'sizes.id', '=', 'colorsizes.size_id')
            ->where('colorsizes.product_id', $id)
            ->select('colors.name as color', 'colors.value', 'sizes.name as size', 'colorsizes.quantity')
            ->get();
        return response()->json($colorSize);
    }

    //products images
    public function getImages($id)
    {
        $image = DB::table('images')->where('product_id', $id)
            ->join('colors', 'colors.id', '=', 'images.color_id')
            ->select('colors.name', 'images')
            ->get();
        return response()->json($image);
    }

//all products lists
    public function getProduct()
    {
        $product = DB::table('products')
            ->join('categories', 'categories.id', '=', 'products.category_id')
            ->select('products.id', 'products.name', 'products.description', 'products.price', 'categories.label')
            ->get();

        return response()->json($product);
    }

    //delete product
    public function destroy($id)
    {
        $product = Product::findOrFail($id);
//        //deleting thumbnail of images
        $thumbnailPath = public_path("image/thumbnail/" . $product->thumbnail);
        if (File::exists($thumbnailPath)) {
            File::delete($thumbnailPath);
        }

        //deleting product images
        $images = Image::where('product_id', '=', $product->id)->pluck('images');
        foreach ($images as $key) {
            $imgArr = explode(",", $key);
            foreach ($imgArr as $key => $value) {
                $productPath = public_path("image/product/" . $value);
                if (File::exists($productPath)) {
                    File::delete($productPath);
                }
            }
        }
        $product->delete();
        return ['message' => 'Product Deleted'];
    }

}
