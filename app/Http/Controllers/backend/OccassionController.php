<?php

namespace App\Http\Controllers\backend;

use App\Occassion;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OccassionController extends Controller
{
    public function store(Request $request){
        $request->validate([
            'name' => 'required|string',
        ]);
        $occasion = new Occassion([
            'name' => $request->get('name')
        ]);

        $occasion->save();

        return response()->json('success');
    }

    public function getOccasion(){
        $occasion = Occassion::get();
        return response()->json($occasion);
    }

    public function destroy($id){
        $occasion = Occassion::findOrFail($id);
        $occasion->delete();
        return ['message' => 'Occasion Deleted'];
    }
}
