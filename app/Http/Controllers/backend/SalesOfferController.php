<?php

namespace App\Http\Controllers\backend;

use App\Product;
use App\SalesOffer;
use App\Store;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SalesOfferController extends Controller
{

    public function store(Request $request)
    {

        $request->validate([
            'rate' => 'required|numeric|between:1,99'
        ]);

        $rate = $request->get('rate');
        $rate = doubleval($rate) / 100;

        SalesOffer::updateOrCreate(
            ['product_id' => $request->get('product_id')],
            ['rate' => $rate]
        );

        return response()->json('success');
    }

    public function getStores()
    {

        $stores = Store::select('id', 'store_name')->get();

        return response()->json($stores);
    }

    public function getStoreProducts($id)
    {

        $product = Product::with('store:id,store_name')
            ->with('category')
            ->with('sales')
            ->select('id', 'store_id', 'category_id', 'name', 'price')
            ->where('store_id', '=', $id)
            ->get();

        return response()->json($product);
    }

    public function getSales()
    {
        $sales = SalesOffer::with('product:id,name,price')
            ->with('store')
            ->get();

        return response()->json($sales);
    }

    public function delete($id)
    {
        SalesOffer::destroy($id);

        return response()->json('Success');
    }

}
