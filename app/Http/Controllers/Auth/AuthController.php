<?php

namespace App\Http\Controllers\Auth;

use App\Notifications\AccountActivation;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class AuthController extends Controller
{
    public function register(Request $request)
    {
        $request->validate([
            'name' => 'required|string|min:3|max:50',
            'email' => 'required|email|string|unique:users',
            'password' => 'required|confirmed|min:8|max:50',
            'gender' => 'required|',
            'birthday' => 'required|date|before:last year',
            'address' => 'required|max:50',
            'phone' => 'required|max:25',
        ]);

        $user = User::create([
            'name' => request('name'),
            'email' => request('email'),
            'password' => bcrypt(request('password')),
            'gender' => request('gender'),
            'dob' => request('birthday'),
            'address' => request('address'),
            'phone' => request('phone'),
            'remember_token' => Str::random(60)
        ]);

        $user->notify(new AccountActivation($user));

        return response()->json([
            'message' => 'Success',
            'status' => 201
        ]);
    }

    public function login()
    {
        // Check if a user with the specified email exists
        $user = User::whereEmail(request('email'))->first();

        if (!$user) {
            return response()->json([
                'message' => 'Invalid email address.',
                'status' => 422
            ], 422);
        }

        if($user->type != 'user'){
            return response()->json([
                'message' => 'Invalid Credentials',
                'status' => 422
            ], 422);
        }

        // If a user with the email was found - check if the specified password belongs to this user
        if (!Hash::check(request('password'), $user->password)) {
            return response()->json([
                'message' => 'Incorrect Password. Try again',
                'status' => 422
            ], 422);
        }

        if (!$user->active) {
            return response()->json([
                'message' => 'Your account is not active yet. Please activate your account.',
                'status' => 422
            ], 422);
        }

        // Send an internal API request to get an access token
        $client = DB::table('oauth_clients')
            ->where('password_client', true)
            ->first();

        // Make sure a Password Client exists in the DB
        if (!$client) {
            return response()->json([
                'message' => 'Server error. Please try again later.',
                'status' => 500
            ], 500);
        }

        $data = [
            'grant_type' => 'password',
            'client_id' => $client->id,
            'client_secret' => $client->secret,
            'username' => request('email'),
            'password' => request('password'),
        ];

        $request = Request::create('/oauth/token', 'POST', $data);

        $response = app()->handle($request);

        // Check if the request was successful
        if ($response->getStatusCode() != 200) {
            return response()->json([
                'message' => 'Something went wrong. Please, try again',
                'status' => 422
            ], 422);
        }

        // Get the data from the response
        $data = json_decode($response->getContent());

        // Format the final response in a desirable format
        return response()->json([
            'token' => $data->access_token,
            'user' => $user,
            'status' => 200
        ]);
    }

    public function logout()
    {
        $accessToken = auth()->user()->token();

        $refreshToken = DB::table('oauth_refresh_tokens')
            ->where('access_token_id', $accessToken->id)
            ->update([
                'revoked' => true
            ]);

        $accessToken->revoke();

        return response()->json(['status' => 200]);
    }

    public function update(Request $request){

        $request->validate([
            'name' => 'required|string|min:3|max:50',
            'gender' => 'required',
            'dob' => 'required|date',
            'address' => 'required|max:50',
            'phone' => 'required|max:25',
        ]);

        $user = User::findOrFail($request->get('id'));

        if(!$user){
            return response()->json([
                'message' => 'No Such Users',
                'status' => 422
            ], 422);
        }

        $user->name = $request->get('name');
        $user->address = $request->get('address');
        $user->phone = $request->get('phone');
        $user->dob = $request->get('dob');
        $user->gender = $request->get('gender');
        if($request->get('password')){
            $user->password = bcrypt($request->get('password'));
        }
        $user->save();

        return response()->json([
            'user' => $user,
            'status' => 201
        ]);

    }

    public function accountActivation($token){
        $user = User::where('remember_token', $token)->first();

        if (!$user) {
            return response()->json(['message' => 'This activation token is either invalid or the account has already been activated.'], 401);
        }
        $user->active = true;
        $user->email_verified_at = Carbon::now();
        $user->remember_token = '';
        $user->save();

        return response()->json(['message' => 'Confirmation Success. Now you can login.'], 200);
    }

}
