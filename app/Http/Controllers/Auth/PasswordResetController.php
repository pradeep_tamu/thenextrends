<?php

namespace App\Http\Controllers\Auth;

use App\Notifications\PasswordResetRequest;
use App\Notifications\PasswordResetSuccess;
use App\PasswordReset;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;

class PasswordResetController extends Controller
{
    public function create(Request $request)
    {

        $request->validate([
            'email' => 'required|string|email'
        ]);

        $user = User::where('email', $request->email)->first();

        if (!$user) {
            return response('Invalid Email', 401);
        }

        $passwordReset = PasswordReset::updateOrCreate(

            ['email' => $user->email],
            [
                'email' => $user->email,
                'token' => Str::random(60),
                'created_at' => Carbon::now()
            ]
        );

        if ($user && $passwordReset) {
            $user->notify(
                new PasswordResetRequest($user, $passwordReset->token)
            );
        }
        return response('Success', 200);

    }

    public function reset(Request $request)
    {

        $request->validate([
            'password' => 'required|string|confirmed',
            'token' => 'required|string'
        ]);

        $passwordReset = PasswordReset::where('token', '=' , $request->token)->first();

        if (!$passwordReset) {
            return response('Invalid token', 401);
        }

        $user = User::where('email', '=', $passwordReset->email)->first();

        if (!$user) {
            return response('Invalid token', 401);
        }

        $user->password = bcrypt($request->password);
        $user->save();
        PasswordReset::where('email', '=', $passwordReset->email)->delete();

        $user->notify(new PasswordResetSuccess($user));

        return response('Success', 200);
    }
}
