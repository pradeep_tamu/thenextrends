<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Laravel\Socialite\Facades\Socialite;

class SocialLoginController extends Controller
{
    public function SocialSignUp($provider)
    {
        // Socialite will pick response data automatic
        $user = Socialite::driver($provider)->stateless()->user();

        $existingUser = User::where('email', $user->email)->first();

        if($existingUser){
            $access_token = $this->socialLogin($user, $provider);
        } else{
            User::create([
                'name' => $user->name,
                'email' => $user->email,
                'active' => true
            ]);
            $access_token = $this->socialLogin($user, $provider);
        }

        $newUser = User::where('email', $user->email)->first();

        // Format the final response in a desirable format
        return response()->json([
            'token' => $access_token,
            'user' => $newUser,
            'status' => 200
        ]);
    }

    public function socialLogin($user, $provider){
        // Send an internal API request to get an access token
        $client = DB::table('oauth_clients')
            ->where('password_client', true)
            ->first();

        // Make sure a Password Client exists in the DB
        if (!$client) {
            return response()->json([
                'message' => 'Laravel Passport is not setup properly.',
                'status' => 500
            ], 500);
        }

        $data = [
            'grant_type' => 'social',
            'client_id' => $client->id,
            'client_secret' => $client->secret,
            'provider' => $provider,
            'access_token' => $user->token
        ];

        $request = Request::create('/oauth/token', 'POST', $data);

        $response = app()->handle($request);

        // Check if the request was successful
        if ($response->getStatusCode() != 200) {
            return response()->json([
                'message' => 'Something went wrong. Please, try again',
                'status' => 422
            ], 422);
        }

        // Get the data from the response
        $data = json_decode($response->getContent());

        return $data->access_token;
    }
}
