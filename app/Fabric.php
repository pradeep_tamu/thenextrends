<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Fabric extends Model
{
    protected $table = "fabrics";
    protected $fillable = ['name'];
    protected $hidden = array('created_at', 'updated_at');

    public function product(){
        return $this->hasMany(Product::class);
    }
}
