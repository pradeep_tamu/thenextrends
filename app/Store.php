<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Store extends Model
{

    protected  $table='stores';
    protected  $fillable=['owner_name','email','store_name','address','phone'];
    protected $hidden = array('email_verified_at', 'created_at', 'updated_at');

    public function product(){
        return $this->hasMany(Product::class);
    }

//    public function routeNotificationForMail($notification)
//    {
//        return $this->email_address;
//    }


}
