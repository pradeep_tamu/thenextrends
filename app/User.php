<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use Notifiable, HasApiTokens;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'dob', 'address', 'phone', 'gender', 'remember_token'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'created_at', 'updated_at', 'email_verified_at',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function orders()
    {
        return $this->hasMany(Order::class);
    }

    public function routeNotificationForMail($notification)
    {
        if ($notification->order) {
            $emails = [];
            $email = $notification->order->stores[0]->email;
            if (count($notification->order->users) != 0) {
                $email2 = $notification->order->users[0]->email;
                array_push($emails, $email, $email2);
            } elseif ($notification->order->orders['guestemail'] != null) {
                $email3 = $notification->order->orders['guestemail'];
                array_push($emails, $email, $email3);
            }
            return $emails;
        }

        return $notification->user->email;
    }
}