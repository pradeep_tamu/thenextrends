<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = "products";

    protected $fillable = [
        'name',
        'description',
        'purchase_price',
        'price',
        'thumbnail',
        'category_id',
        'brand_id',
        'fabric_id',
        'occasion_id',
        'store_id',
        'slug'
    ];
    protected $hidden = array('created_at', 'updated_at');

    public function color()
    {
        return $this->hasMany('App\Color');
    }

    public function size()
    {
        return $this->hasMany('App\Size');
    }

    public function store(){
        return $this->belongsTo(Store::class);
    }

    public function brand(){
        return $this->belongsTo(Brand::class);
    }

    public function fabric(){
        return $this->belongsTo(Fabric::class);
    }

    public function occasion(){
        return $this->belongsTo(Occassion::class);
    }

    public function category(){
        return $this->belongsTo(Category::class);
    }

    public function orderItems()
    {
        return $this->hasMany('App\OrderItem');
    }

    public function sales(){
        return $this->hasOne(SalesOffer::class);
    }
}
