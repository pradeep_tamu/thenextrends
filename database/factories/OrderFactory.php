<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\Order;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Order::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'customer_id' => 2,
        'region' => $faker->randomElement(['Kathmandu', 'Bhaktapur', 'Lalitpur']),
        'address' => $faker->address,
        'shipping' => $faker->randomElement([50, 100]),
        'mobile' => $faker->phoneNumber,
        'total' => mt_rand(100, 10000)/10,
        'created_at' => $faker->dateTimeBetween('-3 years', 'now')
    ];
});

$factory->define(\App\OrderItem::class, function (Faker $faker) {
    return [
        'order_id' => mt_rand(2, 102),
        'product_sku' => $faker->firstName,
        'product_id' => mt_rand(1, 15),
        'quantity' => mt_rand(5, 20),
        'color' => $faker->randomElement(['Black', 'White', 'Navy', 'Green', 'Yellow', 'Maroon']),
        'size' => $faker->randomElement(['XS', 'S', 'M', 'L', 'XL']),
        'image' => $faker->name,
        'created_at' => $faker->dateTimeBetween('-3 years', 'now')
    ];
});
