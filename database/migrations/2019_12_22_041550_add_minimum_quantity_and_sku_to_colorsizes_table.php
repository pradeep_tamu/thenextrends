<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMinimumQuantityAndSkuToColorsizesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('colorsizes', function (Blueprint $table) {
            $table->string('sku')->nullable()->after('id');
            $table->integer('minimum_stock')->after('quantity');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('colorsizes', function (Blueprint $table) {
            //
        });
    }
}
