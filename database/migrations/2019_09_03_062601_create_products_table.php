<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('store_id');
            $table->string('name');
            $table->text('description');
            $table->float('price');
            $table->string('thumbnail');
            $table->string('slug');

            $table->unsignedBigInteger('brand_id')->nullable();
            $table->foreign('brand_id')
                ->references('id')
                ->on('brands')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->unsignedBigInteger('fabric_id')->nullable();
            $table->foreign('fabric_id')
                ->references('id')
                ->on('fabrics')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->unsignedBigInteger('occasion_id')->nullable();
            $table->foreign('occasion_id')
                ->references('id')
                ->on('occassions')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
