
application/x-httpd-php DatabaseSeeder.php ( C++ source, ASCII text )
<?php

use App\User;
use Faker\Provider\Base;
use Illuminate\Database\Seeder;
use Faker\Generator as Faker;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        echo PHP_EOL , 'cleaning old data....';

        DB::table('users')->delete();
        DB::table('categories')->delete();
        DB::table('stores')->delete();
        DB::table('colors')->delete();
        // DB::table('brands')->delete();
        DB::table('sizes')->delete();
        DB::table('fabrics')->delete();
        DB::table('occassions')->delete();
        // DB::table('brand_genders')->delete();
        // DB::table('brand_types')->delete();

        echo PHP_EOL, 'seeding tables...';

        User::create(
            [
                'name' => 'Pradeep Gurung',
                'email' => 'admin@gmail.com',
                'type' => 'admin',
                'active' => true,
                'password' => bcrypt('password'),
                'remember_token' => null,
            ]
        );

        User::create(
            [
                'name' => 'Bivek Shrestha',
                'email' => 'user@gmail.com',
                'active' => true,
                'password' => bcrypt('password'),
                'remember_token' => null,
            ]
        );

        $category = [
            ['label' => 'Women'], //1
            ['label' => 'Men'],  // 2
            ['label' => 'Clothing', 'parent_id' => 1], //3
            ['label' => 'Clothing', 'parent_id' => 2], //4
            ['label' => 'Shoes', 'parent_id' => 1], //5
            ['label' => 'Shoes', 'parent_id' => 2], //6
            ['label' => 'Accessories', 'parent_id' => 1], //7
            ['label' => 'Accessories', 'parent_id' => 2], //8
            ['label' => 'Sports', 'parent_id' => 1], //9
            ['label' => 'Sports', 'parent_id' => 2], //10

            ['label' => 'Dresses', 'parent_id' => 3],
            ['label' => 'Tops', 'parent_id' => 3],
            ['label' => 'T-Shirts & Singlets', 'parent_id' => 3],
            ['label' => 'Shorts', 'parent_id' => 3],
            ['label' => 'Skirts', 'parent_id' => 3],
            ['label' => 'Pants', 'parent_id' => 3],
            ['label' => 'Coats & Jackets', 'parent_id' => 3],
            ['label' => 'Sweats & Hoodies', 'parent_id' => 3],
            ['label' => 'Jeans', 'parent_id' => 3],

            ['label' => 'Shirts & Polos', 'parent_id' => 4],
            ['label' => 'T-Shirts & Singlets', 'parent_id' => 4],
            ['label' => 'Shorts', 'parent_id' => 4],
            ['label' => 'Jeans', 'parent_id' => 4],
            ['label' => 'Pants', 'parent_id' => 4],
            ['label' => 'Coats & Jackets', 'parent_id' => 4],
            ['label' => 'Sweats & Hoodies', 'parent_id' => 4],

            ['label' => 'Heels', 'parent_id' => 5],
            ['label' => 'Flats', 'parent_id' => 5],
            ['label' => 'Lifestyle Shoes', 'parent_id' => 5],
            ['label' => 'Performance Shoes', 'parent_id' => 5],
            ['label' => 'Sneakers', 'parent_id' => 5],
            ['label' => 'Boots', 'parent_id' => 5],
            ['label' => 'Casual Shoes', 'parent_id' => 5],
            ['label' => 'Ankle Boots', 'parent_id' => 5],
            ['label' => 'Dress Shoes', 'parent_id' => 5],
            ['label' => 'Sandals', 'parent_id' => 5],

            ['label' => 'Slippers', 'parent_id' => 6],
            ['label' => 'Thongs', 'parent_id' => 6],
            ['label' => 'Lifestyle Shoes', 'parent_id' => 6],
            ['label' => 'Performance Shoes', 'parent_id' => 6],
            ['label' => 'Sneakers', 'parent_id' => 6],
            ['label' => 'Boots', 'parent_id' => 6],
            ['label' => 'Casual Shoes', 'parent_id' => 6],
            ['label' => 'Dress Shoes', 'parent_id' => 6],
            ['label' => 'Sandals', 'parent_id' => 6],

            ['label' => 'Headwear', 'parent_id' => 7],
            ['label' => 'Bags', 'parent_id' => 7],
            ['label' => 'Watches', 'parent_id' => 7],
            ['label' => 'Jewellery', 'parent_id' => 7],
            ['label' => 'Tech Accessories', 'parent_id' => 7],
            ['label' => 'Belts', 'parent_id' => 7],
            ['label' => 'Wallets', 'parent_id' => 7],
            ['label' => 'Underwear & Socks', 'parent_id' => 7],
            ['label' => 'Sunglasses', 'parent_id' => 7],

            ['label' => 'Headwear', 'parent_id' => 8],
            ['label' => 'Bags', 'parent_id' => 8],
            ['label' => 'Watches', 'parent_id' => 8],
            ['label' => 'Jewellery', 'parent_id' => 8],
            ['label' => 'Tech Accessories', 'parent_id' => 8],
            ['label' => 'Belts', 'parent_id' => 8],
            ['label' => 'Wallets', 'parent_id' => 8],
            ['label' => 'Underwear & Socks', 'parent_id' => 8],
            ['label' => 'Sunglasses', 'parent_id' => 8],

            ['label' => 'Sneakers', 'parent_id' => 9],
            ['label' => 'Lifestyle Shoes', 'parent_id' => 9],
            ['label' => 'Performance Shoes', 'parent_id' => 9],
            ['label' => 'T-Shirts & Singlets', 'parent_id' => 9],
            ['label' => 'Sweats & Hoodies', 'parent_id' => 9],
            ['label' => 'Pants', 'parent_id' => 9],
            ['label' => 'Shorts', 'parent_id' => 9],
            ['label' => 'Compression', 'parent_id' => 9],
            ['label' => 'Bags', 'parent_id' => 9],
            ['label' => 'Tech Accessories', 'parent_id' => 9],

            ['label' => 'Sneakers', 'parent_id' => 10],
            ['label' => 'Lifestyle Shoes', 'parent_id' => 10],
            ['label' => 'Performance Shoes', 'parent_id' => 10],
            ['label' => 'T-Shirts & Singlets', 'parent_id' => 10],
            ['label' => 'Sweats & Hoodies', 'parent_id' => 10],
            ['label' => 'Pants', 'parent_id' => 10],
            ['label' => 'Shorts', 'parent_id' => 10],
            ['label' => 'Compression', 'parent_id' => 10],
            ['label' => 'Bags', 'parent_id' => 10],
            ['label' => 'Tech Accessories', 'parent_id' => 10],
        ];

        foreach ($category as $cat){
            \App\Category::create($cat);
        }

        $stores = [
            [
                'owner_name' => 'Bivek Shrestha',
                'email' => 'bvkstha5@gmail.com',
                'store_name' => 'Thenextrends kathmandu',
                'address' => 'Thamel, Kathmandu',
                'phone' => '1234455531'
            ],
            [
                'owner_name' => 'Pradeep Gurung',
                'email' => 'pgtamu2@gmail.com',
                'store_name' => 'Thenextrends Australia',
                'address' => 'Newroad, Kathmandu',
                'phone' => '1234455531'
            ],
            [
                'owner_name' => 'Sam Timalsina',
                'email' => 'msntraveltours@gmail.com',
                'store_name' => 'Lvd Store',
                'address' => 'Gongobu, Kathmandu',
                'phone' => '1234455531'
            ],
            [
                'owner_name' => 'Ricky Dhungana',
                'email' => 'ityashri@gmail.com',
                'store_name' => 'Adidas Store',
                'address' => 'Kingsway, Kathmandu',
                'phone' => '1234455531'
            ],
        ];

        foreach ($stores as $store){
            \App\Store::create($store);
        }

        // $brands = [
        //     ['name' => 'Levis'],
        //     ['name' => 'Kilometer'],
        //     ['name' => 'LvD'],
        //     ['name' => 'Polo'],
        //     ['name' => 'Atoms'],
        //     ['name' => 'Adidas'],
        //     ['name' => 'Nike'],
        //     ['name' => 'Calvin Klein'],
        //     ['name' => 'Rolex'],
        //     ['name' => 'North Face'],
        // ];

        // foreach ($brands as $brand){
        //     \App\Brand::create($brand);
        // }

        $fabrics = [
            ['name' => 'Silk'],
            ['name' => 'Cotton'],
            ['name' => 'Fiber'],
            ['name' => 'Linen'],
            ['name' => 'Leather'],
            ['name' => 'Nylon'],
            ['name' => 'Polyster'],
            ['name' => 'Wool'],
            ['name' => 'Gold'],
            ['name' => 'Silver'],
            ['name' => 'Knit'],
        ];

        foreach ($fabrics as $fabric){
            \App\Fabric::create($fabric);
        }

        $occasions = [
            ['name' => 'Party'],
            ['name' => 'Casual'],
            ['name' => 'Outing'],
            ['name' => 'Fashion'],
            ['name' => 'Formal'],
            ['name' => 'Festival'],
            ['name' => 'Wedding'],
        ];

        foreach ($occasions as $occasion){
            \App\Occassion::create($occasion);
        }

        $colors = [
            ['name' => 'Navy', 'value' => '#000080'],
            ['name' => 'Black', 'value' => '#000000'],
            ['name' => 'White', 'value' => '#ffffff'],
            ['name' => 'Maroon', 'value' => '#800000'],
            ['name' => 'Green', 'value' => '#008000'],
            ['name' => 'Yellow', 'value' => '#ffff00'],
            ['name' => 'Orange', 'value' => '#ffA500'],
            ['name' => 'Brown', 'value' => '#A52A2A'],
            ['name' => 'Baby Pink', 'value' => '#f4c2c2'],
            ['name' => 'Gray', 'value' => '#808080'],
            ['name' => 'Red', 'value' => '#ff0000'],
        ];

        foreach ($colors as $color){
            \App\Color::create($color);
        }

        $sizes = [
            ['name' => 'XXS'],
            ['name' => 'XS'],
            ['name' => 'S'],
            ['name' => 'M'],
            ['name' => 'L'],
            ['name' => 'XL'],
            ['name' => 'XXL'],
            ['name' => 'XXXL'],
        ];

        foreach ($sizes as $size){
            \App\Size::create($size);
        }

    }
}
